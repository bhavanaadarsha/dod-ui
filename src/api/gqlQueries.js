import { gql } from "@apollo/client";

/************************************************************************************/
/*Authentication*/
/************************************************************************************/
export const VERIFY_TOKEN = gql`
  mutation VerifyToken($token: String) {
    verifyToken(token: $token) {
      payload
    }
  }
`;

export const REFRESH_TOKEN = gql`
  mutation RefreshToken($token: String) {
    refreshToken(token: $token) {
      payload
      token
      refreshExpiresIn
    }
  }
`;

export const GET_USER_NAME = gql`
  query GetUserName {
    me {
      id
      firstName
      lastName
      hasChangedDefaultPassword
      alwaysOpenGuide
      showPrefsExample
      assignmentSet {
        id
        role {
          id
          name
        }
        primary
        office {
          id
          name
        }
        #shiftType {
        #  name
        #}
        schedulable
        hiredshiftSet {
          id
          shiftDefinition {
            description
            start
            end
            type {
              type
            }
            weekday {
              name
            }
          }
        }
        rotationLength
      }
    }
  }
`;

export const GET_NOTIFICATION_COUNT = gql`
  query GetNotificationCount {
    me {
      notificationSet {
        id
        title
        description
        read
      }
    }
  }
`;

export const UPDATE_PASSWORD = gql`
  mutation UpdateEmployee($id: ID!, $input: PatchEmployeeInput!) {
    updateEmployee(id: $id, input: $input) {
      employee {
        id
        hasChangedDefaultPassword
      }
    }
  }
`;

/************************************************************************************/
/*Profile*/
/************************************************************************************/
export const GET_PROFILE = gql`
  query GetProfile {
    me {
      id
      firstName
      lastName
      email
      personalEmail
      primaryPhone
      secondaryPhone
      assignmentSet {
        shiftType {
          id
          name
        }
        primary
        office {
          name
          constraintSet {
            type {
              name
              definition
            }
            value
          }
          maxGreyoutRequests
        }
        role {
          name
        }
        employeeType {
          name
        }
      }
      skills {
        name
        variety
      }
    }
  }
`;

export const UPDATE_PROFILE = gql`
  mutation UpdateEmployee($id: ID!, $input: PatchEmployeeInput!) {
    updateEmployee(id: $id, input: $input) {
      employee {
        firstName
        lastName
        personalEmail
        primaryPhone
        secondaryPhone
      }
    }
  }
`;

export const GET_NOTIFICATION_PREFS = gql`
  query GetEmployee {
    me {
      id
      notificationByText
      notificationByEmail
      notifyScheduleFinal
      notifyPreferencesDue
      notifyShiftSwitchRequest
      notifyScheduleAvailable
    }
  }
`;

export const UPDATE_NOTIFICATION_PREFS = gql`
  mutation UpdateEmployee($id: ID!, $input: PatchEmployeeInput!) {
    updateEmployee(id: $id, input: $input) {
      employee {
        notificationByText
        notificationByEmail
        notifyScheduleFinal
        notifyPreferencesDue
        notifyShiftSwitchRequest
        notifyScheduleAvailable
      }
    }
  }
`;

/************************************************************************************/
/*Preferences*/
/************************************************************************************/

export const UPDATE_PREFS_SETTINGS = gql`
  mutation UpdateEmployee($id: ID!, $input: PatchEmployeeInput!) {
    updateEmployee(id: $id, input: $input) {
      employee {
        alwaysOpenGuide
        showPrefsExample
      }
    }
  }
`;

export const GET_PREFERENCES = gql`
  query GetPreferences($id: Int!, $office: Int!) {
    preferences: preferences(employeeId: $id) {
      id
      type {
        id
        type
      }
      value
    }
    preferenceIndicators(office: $office) {
      preferenceType
      desirability
    }
    preferenceTypes {
      id
      type
    }
  }
`;

export const UPDATE_PREFERENCE = gql`
  mutation UpdatePreference($id: ID!, $input: PatchPreferenceInput!) {
    updatePreference(id: $id, input: $input) {
      preference {
        id
        type {
          type
        }
        value
      }
    }
  }
`;

export const BATCH_CREATE_PREFERENCES = gql`
  mutation BatchCreatePreferences($input: [BatchCreatePreferenceInput]!) {
    batchCreatePreference(input: $input) {
      preferences {
        id
        value
        type {
          type
        }
      }
    }
  }
`;

export const BATCH_UPDATE_PREFERENCES = gql`
  mutation BatchUpdatePreferences($input: [BatchPatchPreferenceInput]!) {
    batchUpdatePreference(input: $input) {
      preferences {
        id
        value
        type {
          type
        }
      }
    }
  }
`;

export const CREATE_TEMP_PREFERENCES = gql`
  mutation CreateTempPreferences($input: CreateTempPreferenceInput!) {
    createTempPreference(input: $input) {
      tempPreference {
        id
        employee {
          id
          firstName
          lastName
        }
        preferences
      }
    }
  }
`;

export const UPDATE_TEMP_PREFERENCES = gql`
  mutation UpdateTempPreferences($id: ID!, $input: PatchTempPreferenceInput!) {
    updateTempPreference(id: $id, input: $input) {
      tempPreference {
        id
        employee {
          id
          firstName
          lastName
        }
        preferences
      }
    }
  }
`;

/************************************************************************************/
/*Calender*/
/************************************************************************************/

export const GET_SHIFTS = gql`
  query GetShifts(
    $officeId: Int!
    $employeeId: Int!
    $rangeStart: DateTime
    $rangeEnd: DateTime
  ) {
    allShifts: shifts(
      officeId: $officeId
      rangeStart: $rangeStart
      rangeEnd: $rangeEnd
    ) {
      id
      start
      end
      shiftassignmentSet {
        isArchived
        id
        employee {
          id
          firstName
          lastName
          skills {
            id
            name
            variety
          }
        }
        taskSet {
          id
        }
      }
    }
    userShifts: shiftAssignments(employeeId: $employeeId) {
      isArchived
      id
      shift {
        id
        start
        office {
          id
        }
      }
    }
    timeOff: availability(officeId: $officeId) {
      id
        firstday
        lastday
        startTime
        endTime
        employee {
          id
          firstName
          lastName
        }
        approvedby {
          id
          firstName
          lastName
        }
        type {
          name
          id
        }
        office {
          id
        }
        workHours
        comment
        deniedText
        userCancelled
    }
    softRequests: offPreferences(employeeId: $employeeId) {
      id
      date
      value
    }
    officeInfo: offices(id: $officeId) {
      maxGreyoutRequests
      allowCallIns
      scheduleDuration
    }
  }
`;

export const GET_MORE_SHIFTS = gql`
  query GetShifts(
    $officeId: Int!
    $employeeId: Int!
    $rangeStart: DateTime
    $rangeEnd: DateTime
  ) {
    allShifts: shifts(
      officeId: $officeId
      rangeStart: $rangeStart
      rangeEnd: $rangeEnd
    ) {
      id
      start
      end
      shiftassignmentSet {
        isArchived
        id
        employee {
          id
          firstName
          lastName
          skills {
            id
            name
          }
        }
        taskSet {
          id
        }
      }
    }
    userShifts: shiftAssignments(employeeId: $employeeId) {
      isArchived
      id
      shift {
        id
        start
        office {
          id
        }
      }
    }
  }
`;

export const GET_SHIFTS_WITH_RANGE = gql`
  query GetShifts($officeId: Int!, $rangeStart: DateTime, $rangeEnd: DateTime) {
    shifts(officeId: $officeId, rangeStart: $rangeStart, rangeEnd: $rangeEnd) {
      id
      start
      end
    }
  }
`;

export const GET_MISSIONS = gql`
  query getMissions($office: Int, $start: Date, $end: Date) {
    procedureRequirements(office: $office, startRange: $start, endRange: $end) {
      id
      name
      procedureType {
        name
        id
      }
      earliestDate
      latestDate
      duration
      additiveConstraint
      procedureemployeeconstraintSet {
        employeeCapability {
          id
          name
        }
      }
      procedure {
        id
        name
        start
        end
        taskSet {
          id
          shiftAssignment {
            id
            employee {
              id
              firstName
              lastName
              skills {
                id
                name
              }
            }
            isArchived
            shift {
              id
              start
              end
            }
          }
          start
          end
        }
        assetassignmentSet {
          id
          asset {
            name
          }
        }
      }
    }
  }
`;

export const GET_ASSIGNMENT = gql`
  query getAssignment($employeeId: Int) {
    assignments(employeeId: $employeeId) {
      office {
        id
      }
      role {
        name
      }
      schedulable
    }
    employees(id: $employeeId) {
      employeeskillSet {
        id
        skill {
          id
          name
          variety
        }
      }
    }
  }
`;

export const GET_SLACK = gql`
  query GetSlack($rangeStart: DateTime, $rangeEnd: DateTime, $office: Int) {
    slacks: datetimeSlacks(
      rangeStart: $rangeStart
      rangeEnd: $rangeEnd
      office: $office
      issuesOnly: true
    ) {
      id
      count
      slack
      start
      end
      skill {
        name
      }
    }
    acuitySlacks: datetimeAcuitySlacks(
      rangeStart: $rangeStart
      rangeEnd: $rangeEnd
      office: $office
      issuesOnly: true
    ) {
      id
      assigned
      lower
      upper
      start
      end
    }
  }
`;

// slacks(shift: $shift) {
//   id
//   count
//   slack
//   skill {
//     id
//     name
//     variety
//   }
// }
// acuitySlacks(shift: $shift) {
//   id
//   assigned
//   lower
//   upper
// }
// export const GET_MY_SHIFTS = gql`
//     query GetMyShifts {
//         me {
//             shifts {
//                 id
//                 start
//                 end
//                 office {
//                     id
//                 }
//                 shiftassignmentSet {
//                     isArchived
//                     employee {
//                         id
//                         firstName
//                         lastName
//                     }
//                 }
//             }
//         }
//     }
// `;

export const GET_TWO_SHIFTS_BY_IDS = gql`
  query GetShifts($shiftOneId: Int, $shiftTwoId: Int) {
    firstShift: shifts(id: $shiftOneId) {
      id
      start
      end
      employeeSet {
        id
        firstName
        lastName
      }
    }
    secondShift: shifts(id: $shiftTwoId) {
      id
      start
      end
      employeeSet {
        id
        firstName
        lastName
      }
    }
  }
`;

export const GET_ALL_USERS = gql`
  query GetAllUsers($id: Int!) {
    offices(id: $id) {
      employeeSet {
        id
        firstName
        lastName
        skills {
          id
          name
        }
        assignmentSet {
          primary
          office {
            id
          }
          role {
            name
          }
        }
      }
    }
  }
`;

export const GET_ALL_USERS_AND_SHIFTS = gql`
  query GetAllUsersAndShifts($id: Int!) {
    employees: offices(id: $id) {
      employeeSet {
        id
        firstName
        lastName
        shifts {
          id
          office {
            id
          }
        }
        employeeskillSet {
          id
          skill {
            id
            name
            variety
          }
        }
      }
    }
  }
`;

export const CREATE_SOFT_REQUEST = gql`
  mutation CreateOffPreference($input: CreateOffPreferenceInput!) {
    createOffPreference(input: $input) {
      offPreference {
        id
        date
        value
      }
    }
  }
`;

export const EDIT_SOFT_REQUEST = gql`
  mutation UpdateOffPreference($id: ID!, $input: PatchOffPreferenceInput!) {
    updateOffPreference(id: $id, input: $input) {
      offPreference {
        id
        date
        value
      }
    }
  }
`;

export const DELETE_SOFT_REQUEST = gql`
  mutation DeleteOffPreference($id: ID!) {
    deleteOffPreference(id: $id) {
      found
      deletedId
    }
  }
`;

export const AVAILABILITY_TYPES = gql`
  query availabilityTypes {
    availabilityTypes {
      id
      name
    }
  }
`;

export const CREATE_EMPLOYEE_AVAILABILITY = gql`
  mutation CreateEmployeeAvailability(
    $employee: ID!
    $firstday: Date!
    $lastday: Date!
    $type: ID!
    $workHours: Int!
    $office: ID!
    $comment: String
    $startTime: Time
    $endTime: Time
  ) {
    createTimeOffRequestWithNotifications(
      employee: $employee
      firstday: $firstday
      lastday: $lastday
      type: $type
      workHours: $workHours
      office: $office
      comment: $comment
      startTime: $startTime
      endTime: $endTime
    ) {
      timeOffRequest {
        id
        firstday
        lastday
        startTime
        endTime
        employee {
          id
          firstName
          lastName
        }
        approvedby {
          id
          firstName
          lastName
        }
        type {
          name
          id
        }
        office {
          id
        }
        workHours
        comment
        deniedText
        userCancelled
      }
    }
  }
`;

export const MANAGER_CREATE_EMPLOYEE_AVAILABILITY = gql`
  mutation ManagerCreateEmployeeAvailability(
    $employee: ID!
    $firstday: Date!
    $lastday: Date!
    $type: ID!
    $workHours: Int!
    $office: ID!
    $comment: String
    $startTime: Time
    $endTime: Time
  ) {
    managerInitiateTimeOff(
      employee: $employee
      firstday: $firstday
      lastday: $lastday
      type: $type
      workHours: $workHours
      office: $office
      comment: $comment
      startTime: $startTime
      endTime: $endTime
    ) {
      timeOffRequest {
        id
        firstday
        lastday
        startTime
        endTime
        employee {
          id
          firstName
          lastName
        }
        approvedby {
          id
          firstName
          lastName
        }
        type {
          name
          id
        }
        office {
          id
        }
        workHours
        comment
        deniedText
        userCancelled
      }
    }
  }
`;

export const BATCH_CREATE_EMPLOYEE_AVAILABILITY = gql`
  mutation BatchCreateEmployeeAvailability(
    $input: [BatchCreateEmployeeAvailabilityInput]!
  ) {
    batchCreateEmployeeAvailability(input: $input) {
      employeeAvailabilitys {
        id
        firstday
        lastday
        startTime
        endTime
        employee {
          id
          firstName
          lastName
        }
        approvedby {
          id
        }
        type {
          name
          id
        }
        office {
          id
        }
        workHours
        comment
        userCancelled
      }
    }
  }
`;

export const UPDATE_EMPLOYEE_AVAILABILITY = gql`
  mutation UpdateEmployeeAvailability(
    $id: ID!
    $input: PatchEmployeeAvailabilityInput!
  ) {
    updateEmployeeAvailability(id: $id, input: $input) {
      employeeAvailability {
        id
        approvedby {
          id
          firstName
          lastName
        }
        type {
          id
          name
        }
        firstday
        lastday
        startTime
        endTime
        workHours
        deniedText
        userCancelled
        comment
      }
    }
  }
`;

export const MANAGER_APPROVE_EMPLOYEE_AVAILABILITY = gql`
  mutation ApproveTimeOffRequestWithNotifications(
    $id: ID!
    $approvingManager: ID!
  ) {
    approveTimeOffRequestWithNotifications(
      id: $id
      approvingManager: $approvingManager
    ) {
      timeOffRequest {
        id
        approvedby {
          id
          firstName
          lastName
        }
      }
    }
  }
`;

export const MANAGER_DENY_EMPLOYEE_AVAILABILITY = gql`
  mutation DenyTimeOffRequestWithNotifications(
    $id: ID!
    $denyingManager: ID!
    $deniedText: String!
  ) {
    denyTimeOffRequestWithNotifications(
      id: $id
      denyingManager: $denyingManager
      deniedText: $deniedText
    ) {
      timeOffRequest {
        id
        deniedText
      }
    }
  }
`;

export const CREATE_TASK = gql`
  mutation CreateTask($input: CreateTaskInput!) {
    createTask(input: $input) {
      task {
        id
        start
        end
        procedure {
          id
          start
          end
        }
        shiftAssignment {
          employee {
            id
            firstName
            lastName
          }
          shift {
            id
            start
            end
          }
        }
      }
    }
  }
`;

export const MANAGER_ADD_EMPLOYEE_TO_SHIFT = gql`
  mutation AddEmployee(
    $employeeId: ID!
    $managerId: ID!
    $shiftId: ID!
    $note: String
  ) {
    managerCreateShiftAssignment(
      employeeId: $employeeId
      managerId: $managerId
      shiftId: $shiftId
      note: $note
    ) {
      shiftAssignment {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          id
        }
      }
    }
  }
`;

export const MANAGER_REMOVE_EMPLOYEE_FROM_SHIFT = gql`
  mutation RemoveEmployee(
    $managerId: ID!
    $shiftAssignmentId: ID!
    $note: String
  ) {
    managerCallOff(
      managerId: $managerId
      shiftAssignmentId: $shiftAssignmentId
      note: $note
    ) {
      shiftAssignment {
        id
        isArchived
      }
    }
  }
`;

export const ADD_AND_DROP_OPTIONS_FOR_SHIFT = gql`
  query AddAndDropOptions($shifts: [Int]) {
    rescheduleAdds(shifts: $shifts) {
      id
      optionType
      userGenerated
      status
      rescheduleindividualSet {
        id
        cost
        benefit
        employeeResponse
        employee {
          id
          firstName
          lastName
        }
      }
    }
    rescheduleDrops(shifts: $shifts) {
      id
      optionType
      userGenerated
      status
      rescheduleindividualSet {
        id
        cost
        benefit
        employeeResponse
        employee {
          id
          firstName
          lastName
        }
      }
    }
  }
`;

export const ADD_OPTIONS_FOR_SHIFT = gql`
  query AddAndDropOptions($shifts: [Int]) {
    rescheduleAdds(shifts: $shifts) {
      id
      optionType
      userGenerated
      status
      rescheduleindividualSet {
        id
        cost
        benefit
        employeeResponse
        employee {
          id
          firstName
          lastName
        }
      }
    }
  }
`;

export const EMPLOYEE_CALL_IN = gql`
  mutation EmployeeCallIN($id: ID!, $note: String) {
    employeeCallIn(shiftAssignmentId: $id, note: $note) {
      shiftAssignment {
        id
        isArchived
      }
    }
  }
`;

export const SHIFT_ASSIGNMENT = gql`
  query shiftAssignment($id: Int) {
    shiftAssignments(id: $id) {
      id
      employee {
        id
      }
    }
  }
`;

/************************************************************************************/
/*Shift Switch*/
/************************************************************************************/

export const GET_SHIFT_SWITCH_REQUESTS = gql`
  query GetShiftSwitchRequests($officeId: Int) {
    unapprovedShiftSwitches(office: $officeId) {
      id
      requestorShift {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          id
          office {
            id
          }
        }
      }
      candidate1 {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          id
        }
      }
      candidate2 {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          id
        }
      }
      candidate3 {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          id
        }
      }
      candidate4 {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          id
        }
      }
      candidate1Denied
      candidate2Denied
      candidate3Denied
      candidate4Denied
      acceptor {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          id
        }
      }
      approver {
        id
        firstName
        lastName
      }
      deniedText
      userCancelled
    }
  }
`;

export const GET_SHIFT_ASSIGNMENT = gql`
  query ShiftAssignments($employeeId: Int, $shiftId: Int) {
    shiftAssignments(employeeId: $employeeId, shiftId: $shiftId) {
      id
      employee {
        id
      }
      shift {
        id
        start
        end
      }
    }
  }
`;

export const GET_SHIFT_ASSIGNMENTS_BY_SHIFT = gql`
  query ShiftAssignments($shiftId: Int) {
    shiftAssignments(shiftId: $shiftId) {
      id
      isArchived
      employee {
        id
        firstName
        lastName
      }
      shift {
        start
      }
    }
  }
`;

export const CREATE_SHIFT_SWITCH = gql`
  mutation CreateShiftSwitch(
    $requestorShift: ID!
    $candidate1: ID!
    $candidate2: ID
    $candidate3: ID
    $candidate4: ID
  ) {
    createShiftSwitchWithNotifications(
      requestorShift: $requestorShift
      candidate1: $candidate1
      candidate2: $candidate2
      candidate3: $candidate3
      candidate4: $candidate4
    ) {
      shiftSwitchRequest {
        id
        requestorShift {
          id
        }
        candidate1 {
          id
        }
        candidate2 {
          id
        }
        candidate3 {
          id
        }
        candidate4 {
          id
        }
        acceptor {
          id
        }
        approver {
          id
        }
        notificationSet {
          id
          title
          description
          read
          timestamp
          depreciatedText
          type
          employee {
            id
            firstName
            lastName
          }
        }
      }
    }
  }
`;

// createShiftAssignment(input: $input) {
//     shiftAssignment {
//         id
//         employee {
//             id
//             firstName
//             lastName
//             assignmentSet{
//                 office {
//                     id
//                 }
//                 jobType {
//                     name
//                 }
//             }
//         }
//         shift {
//             start
//         }
//     }
// }
// updateShiftAssignment(id: $employeeRemove, input: $updateInput) {
//     shiftAssignment {
//         id
//         employee {
//             id
//             firstName
//             lastName
//         }
//         isArchived
//     }
// }
export const ADD_AND_REMOVE_EMPLOYEES = gql`
  mutation AddRemoveEmployee(
    $employeeOneAdd: ID!
    $employeeTwoAdd: ID!
    $shiftOneId: ID!
    $shiftTwoId: ID!
    $employeeOneShiftAssignmentRemove: ID!
    $employeeTwoShiftAssignmentRemove: ID!
    $managerId: ID!
    $note: String
  ) {
    createShiftOne: managerCreateShiftAssignment(
      employeeId: $employeeOneAdd
      managerId: $managerId
      shiftId: $shiftOneId
      note: $note
    ) {
      shiftAssignment {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          id
        }
      }
    }
    createShiftTwo: managerCreateShiftAssignment(
      employeeId: $employeeTwoAdd
      managerId: $managerId
      shiftId: $shiftTwoId
      note: $note
    ) {
      shiftAssignment {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          id
        }
      }
    }
    removeShiftOne: managerCallOff(
      managerId: $managerId
      shiftAssignmentId: $employeeOneShiftAssignmentRemove
      note: $note
    ) {
      shiftAssignment {
        id
        isArchived
        employee {
          id
        }
      }
    }
    removeShiftTwo: managerCallOff(
      managerId: $managerId
      shiftAssignmentId: $employeeTwoShiftAssignmentRemove
      note: $note
    ) {
      shiftAssignment {
        id
        isArchived
        employee {
          id
        }
      }
    }
  }
`;

export const EMPLOYEE_ACCEPT_SHIFT_SWITCH = gql`
  mutation AcceptShiftSwitch($id: ID!, $candidateNumber: Int!) {
    candidateAcceptShiftSwitchWithNotifications(
      shiftSwitchId: $id
      acceptor: $candidateNumber
    ) {
      shiftSwitchRequest {
        id
        acceptor {
          id
        }
      }
    }
  }
`;

export const MANAGER_APPROVE_SHIFT_SWITCH = gql`
  mutation ApproveShiftSwitch($id: ID!, $approver: Int!) {
    managerApproveShiftSwitchWithNotifications(
      shiftSwitchId: $id
      approver: $approver
    ) {
      shiftSwitchRequest {
        id
        approver {
          id
        }
      }
    }
  }
`;

export const EMPLOYEE_DENY_SHIFT_SWITCH = gql`
  mutation CandidateDenyShiftSwitch($id: ID!, $candidateNumber: Int!) {
    candidateRejectShiftSwitchWithNotifications(
      shiftSwitchId: $id
      candidateNumber: $candidateNumber
    ) {
      shiftSwitchRequest {
        id
        candidate1Denied
        candidate2Denied
        candidate3Denied
        candidate4Denied
      }
    }
  }
`;

export const MANAGER_DENY_SHIFT_SWITCH = gql`
  mutation AcceptShiftSwitch($id: ID!, $denier: ID!, $deniedText: String!) {
    managerDenyShiftSwitchWithNotifications(
      shiftSwitchId: $id
      denier: $denier
      deniedText: $deniedText
    ) {
      shiftSwitchRequest {
        id
        deniedText
      }
    }
  }
`;

export const RESCHEDULE_SWAP_OPTIONS = gql`
  query RescheduleSwapOption($shiftAssignments: [Int]) {
    rescheduleSwaps(shiftAssignments: $shiftAssignments) {
      id
      optionType
      userGenerated
      status
      rescheduleindividualSet {
        id
        cost
        benefit
        employeeResponse
        employee {
          id
          firstName
          lastName
        }
        rescheduleactionSet {
          id
          actionType
          shiftAssignment
          shift {
            id
            start
            end
            shiftType {
              type
            }
          }
        }
      }
    }
  }
`;

/************************************************************************************/
/*Notifications*/
/************************************************************************************/

export const GET_ALL_NOTIFICATIONS = gql`
  query GetNotifications($employeeId: Int) {
    notifications(employeeId: $employeeId) {
      id
      title
      description
      read
      timestamp
      depreciatedText
      type
      employee {
        id
      }
      availability {
        id
      }
      shiftSwitch {
        id
        candidate1 {
          id
          employee {
            id
          }
        }
        candidate2 {
          id
          employee {
            id
          }
        }
        candidate3 {
          id
          employee {
            id
          }
        }
        candidate4 {
          id
          employee {
            id
          }
        }
        acceptor {
          id
        }
        approver {
          id
        }
        deniedText
      }
    }
  }
`;

export const UPDATE_NOTIFICATION = gql`
  mutation UpdateNotification($id: ID!, $input: PatchNotificationInput!) {
    updateNotification(id: $id, input: $input) {
      notification {
        id
        read
        depreciatedText
        userDeleted
      }
    }
  }
`;

export const BATCH_UPDATE_NOTIFICATION = gql`
  mutation BatchUpdateNotification($input: [BatchPatchNotificationInput]!) {
    batchUpdateNotification(input: $input) {
      notifications {
        id
        read
        depreciatedText
        userDeleted
      }
    }
  }
`;

/************************************************************************************/
/*Office Settings*/
/************************************************************************************/

export const FILE_UPLOAD = gql`
  mutation ($id: ID!, $file: Upload!) {
    updateLocation(id: $id, input: { logo: $file }) {
      location {
        id
        logo
      }
    }
  }
`;

export const LOCATION = gql`
  query Location {
    locations {
      id
      name
      address1
      address2
      address3
      hexcolor
      logo
    }
  }
`;

export const PUBLICLOCATION = gql`
  query LocationPublicSettings {
    locationPublicSettings {
      id
      hexcolor
      logo
    }
  }
`;

export const UPDATE_LOCATION = gql`
  mutation UpdateLocation($id: ID!, $input: PatchLocationInput!) {
    updateLocation(id: $id, input: $input) {
      location {
        id
        hexcolor
      }
    }
  }
`;

export const ALL_OFFICES = gql`
  query Offices {
    offices {
      id
      name
      assignmentSet {
        id
      }
    }
  }
`;

export const DELETE_OFFICE = gql`
  mutation DeleteOffice($id: ID!) {
    deleteOffice(id: $id) {
      deletedId
    }
  }
`;

export const GET_OFFICE = gql`
  query ($id: Int) {
    offices(id: $id) {
      id
      name
      maxGreyoutRequests
      numberDaysBeforePreferencesDue
      reviewWindowDays
      shiftSwitchBufferDays
      schedulePublishedBufferWeeks
      scheduleDuration
      timezone
      allowCallIns
      samlLoginOnly
      modeltype {
        id
        modeltag
        modelname
      }
      skillSet {
        id
        name
        variety
      }
      shiftdefinitionSet {
        id
        type {
          type
        }
        description
        start
        end
      }
      constraintSet {
        id
        starttime
        endtime
        monday
        tuesday
        wednesday
        thursday
        friday
        saturday
        sunday
        effectstart
        effectend
        employeetype {
          id
          name
        }
        type {
          id
          name
        }
        value
        skill {
          id
          name
          variety
        }
      }
    }
  }
`;

export const UPDATE_OFFICE = gql`
  mutation UpdateOffice($id: ID!, $input: PatchOfficeInput!) {
    updateOffice(id: $id, input: $input) {
      office {
        id
        name
        maxGreyoutRequests
        numberDaysBeforePreferencesDue
        reviewWindowDays
        shiftSwitchBufferDays
        schedulePublishedBufferWeeks
        allowCallIns
        timezone
        samlLoginOnly
        modeltype {
          modelname
        }
      }
    }
  }
`;

export const ADD_OFFICE = gql`
  mutation CreateOffice($input: CreateOfficeInput!) {
    createOffice(input: $input) {
      office {
        id
        name
      }
    }
  }
`;

export const ADD_LOCATION_SKILLS = gql`
  mutation CreateSkill($input: CreateSkillInput!) {
    createSkill(input: $input) {
      skill {
        id
        name
        variety
      }
    }
  }
`;

export const DELETE_SKILL = gql`
  mutation DeleteSkill($id: ID!) {
    deleteSkill(id: $id) {
      deletedId
    }
  }
`;

export const CREATE_CONSTRAINT = gql`
  mutation CreateConstraint($input: CreateConstraintInput!) {
    createConstraint(input: $input) {
      constraint {
        type {
          id
          name
        }
        effectstart
        starttime
        endtime
        value
        office {
          id
          name
        }
      }
    }
  }
`;

export const CREATE_SKILLSET_CONSTRAINT = gql`
  mutation CreateConstraint($input: CreateConstraintInput!) {
    createConstraint(input: $input) {
      constraint {
        id
        type {
          id
          name
        }
        effectstart
        starttime
        endtime
        value
        monday
        tuesday
        wednesday
        thursday
        friday
        saturday
        sunday
        office {
          id
          name
        }
        skill {
          id
          name
          variety
        }
      }
    }
  }
`;

export const UPDATE_CONSTRAINT = gql`
  mutation UpdateConstraint($id: ID!, $input: PatchConstraintInput!) {
    updateConstraint(id: $id, input: $input) {
      constraint {
        type {
          id
          name
        }
        effectstart
        starttime
        endtime
        monday
        tuesday
        wednesday
        thursday
        friday
        saturday
        sunday
        value
        office {
          id
          name
        }
      }
    }
  }
`;

export const GET_CONSTRAINT_TYPE = gql`
  query ConstraintDefinitions {
    constraintDefinitions {
      id
      name
      definition
    }
  }
`;

export const FIND_OFFICE_CONSTRAINT = gql`
  query FindOfficeConstraint($officeId: Int) {
    constraints(officeId: $officeId) {
      id
      type {
        id
        name
      }
      skill {
        id
        name
        variety
      }
    }
  }
`;

export const DELETE_CONSTRAINT = gql`
  mutation DeleteConstraint($id: ID!) {
    deleteConstraint(id: $id) {
      deletedId
    }
  }
`;

export const BATCH_DELETE_CONSTRAINTS = gql`
  mutation BatchDeleteConstraints($ids: [ID]!) {
    batchDeleteConstraint(ids: $ids) {
      deletionCount
      deletedIds
    }
  }
`;

export const MODEL_TYPE = gql`
  query ModelType {
    preferenceModelTypes {
      id
      modelname
      modeltag
    }
  }
`;

export const SHIFT_DEFINITIONS = gql`
  query ShiftDefinitions($officeId: Int) {
    shiftDefinitions(officeId: $officeId) {
      id
      start
      end
      description
      office {
        id
      }
      type {
        id
        type
      }
      weekday {
        name
      }
    }
  }
`;

export const SHIFT_DEFINITION_TYPE = gql`
  query ShiftType {
    shiftType2s {
      id
      type
    }
  }
`;

export const WEEKDAY = gql`
  query Weekday {
    weekdays {
      id
      name
    }
  }
`;

export const DELETE_SHIFT_DEFINITIONS = gql`
  mutation DeleteShiftDefinitionMutation($id: ID!) {
    deleteShiftDefinition(id: $id) {
      deletedId
    }
  }
`;

export const CREATE_SHIFT_DEFINITIONS = gql`
  mutation CreateShiftDefinition($input: CreateShiftDefinitionInput!) {
    createShiftDefinition(input: $input) {
      shiftDefinition {
        id
        start
        end
        office {
          id
        }
        type {
          id
        }
        weekday {
          id
        }
      }
    }
  }
`;

export const BATCH_CREATE_SHIFT_DEFINITIONS = gql`
  mutation BatchCreateShiftDefinition(
    $input: [BatchCreateShiftDefinitionInput]!
  ) {
    batchCreateShiftDefinition(input: $input) {
      shiftDefinitions {
        id
        start
        end
        office {
          id
        }
        type {
          id
        }
        weekday {
          id
        }
      }
    }
  }
`;

export const BATCH_DELETE_SHIFT_DEFINITIONS = gql`
  mutation BatchDeleteShiftDefinitionMutation($ids: [ID]!) {
    batchDeleteShiftDefinition(ids: $ids) {
      deletedIds
    }
  }
`;

export const UPDATE_SHIFT_DEFINITIONS = gql`
  mutation UpdateShiftDefinition($id: ID!, $input: PatchShiftDefinitionInput!) {
    updateShiftDefinition(id: $id, input: $input) {
      shiftDefinition {
        id
        start
        end
        office {
          id
        }
        type {
          type
        }
        weekday {
          name
        }
      }
    }
  }
`;

export const JOB_TYPE_ROLE = gql`
  query JobTypeRoles {
    jobTypeRoles {
      id
      jobType {
        id
        name
        office {
          id
        }
      }
      role {
        id
        name
      }
    }
  }
`;

export const CREATE_JOB_TYPE_ROLE = gql`
  mutation createJobTypeRole($input: CreateJobTypeRoleInput!) {
    createJobTypeRole(input: $input) {
      jobTypeRole {
        id
        jobType {
          id
          name
        }
        role {
          id
          name
        }
      }
    }
  }
`;

export const UPDATE_JOB_TYPE_ROLE = gql`
  mutation updateJobTypeRole($id: ID!, $input: PatchJobTypeRoleInput!) {
    updateJobTypeRole(id: $id, input: $input) {
      jobTypeRole {
        id
        jobType {
          id
          name
        }
        role {
          id
          name
        }
      }
    }
  }
`;

export const PERMISSIONS = gql`
  query Permissions {
    permissions {
      id
      name
      description
    }
  }
`;

export const CREATE_ABILITIES = gql`
  mutation CreateAbilities($input: CreateAbilityInput!) {
    createAbility(input: $input) {
      ability {
        id
        role {
          id
          name
        }
        permission {
          id
          name
        }
      }
    }
  }
`;

export const DELETE_ABILITIES = gql`
  mutation DeleteAbilities($id: ID!) {
    deleteAbility(id: $id) {
      deletedId
    }
  }
`;

export const ABILITIES = gql`
  query Abilities {
    abilities {
      id
      role {
        id
        name
      }
      permission {
        id
        name
      }
    }
  }
`;

/************************************************************************************/
/*Manage Users*/
/************************************************************************************/

export const Get_Employees = gql`
  query Offices($id: Int) {
    offices(id: $id) {
      id
      name
      constraintSet {
        type {
          id
          name
          definition
        }
        value
      }
      maxGreyoutRequests
      numberDaysBeforePreferencesDue
      reviewWindowDays
      employeeSet {
        id
        firstName
        lastName
        email
        primaryPhone
        personalEmail
        preferenceSet {
          id
          updatedAt
        }
        offices {
          id
          name
        }
        assignmentSet {
          id
          primary
          startDate
          schedulable
          rotationLength
          employeeType {
            id
            name
          }
          role {
            id
            name
          }
          shiftType {
            name
            id
          }
          office {
            id
            name
          }
          hiredshiftSet {
            id
            shiftDefinition {
              id
              description
              start
              end
              type {
                type
              }
              weekday {
                name
              }
            }
          }
        }
        skills {
          id
          variety
          name
        }
      }
    }
  }
`;

export const GET_EMPLOYEE_NAMES = gql`
  query GetEmployeeNames($office: Int) {
    offices(id: $office) {
      employeeSet {
        id
        firstName
        lastName
        skills {
          id
          name
        }
      }
    }
  }
`;

export const UPDATE_EMPLOYEE = gql`
  mutation UpdateEmployee($id: ID!, $input: PatchEmployeeInput!) {
    updateEmployee(id: $id, input: $input) {
      employee {
        id
        firstName
        lastName
        email
        personalEmail
        primaryPhone
      }
    }
  }
`;
export const ADD_EMPLOYEE_SKILLS = gql`
  mutation CreateEmployeeSkill($input: CreateEmployeeSkillInput!) {
    createEmployeeSkill(input: $input) {
      employeeSkill {
        employee {
          id
        }
        skill {
          id
          name
        }
      }
    }
  }
`;
export const DELETE_EMPLOYEE_SKILLS = gql`
  mutation DeleteEmployeeSkills($id: ID!) {
    deleteEmployeeSkill(id: $id) {
      found
      deletedId
    }
  }
`;
export const UPDATE_EMPLOYEE_SKILLS = gql`
  mutation UpdateEmployeeSkills($id: ID!, $input: PatchEmployeeSkillInput!) {
    updateEmployeeSkill(id: $id, input: $input) {
      employeeSkill {
        id
        skill {
          name
          variety
        }
      }
    }
  }
`;

export const BATCH_CREATE_EMPLOYEE_SKILLS = gql`
  mutation batchCreateEmployeeSkills($input: [BatchCreateEmployeeSkillInput]!) {
    batchCreateEmployeeSkill(input: $input) {
      employeeSkills {
        id
        employee {
          id
          firstName
          lastName
        }
        skill {
          id
          name
        }
        expiration
      }
    }
  }
`;

// export const CREATE_EMPLOYEE_PREFERENCES = gql`
//   mutation CreatePreferences($input: CreatePreferenceInput!) {
//     createPreference(input: $input) {
//       preference {
//         id
//         type {
//           id
//           definition
//         }
//         value
//         employee {
//           id
//           firstName
//           lastName
//         }
//       }
//     }
//   }
// `;

export const PREFERENCE_TYPE = gql`
  query PreferenceType {
    preferenceTypes {
      id
      definition
      type
    }
  }
`;

export const UPDATE_EMPLOYEE_ASSIGNMENT = gql`
  mutation UpdateEmployeeAssignment($id: ID!, $input: PatchAssignmentInput!) {
    updateAssignment(id: $id, input: $input) {
      assignment {
        id
        role {
          id
          name
        }
        shiftType {
          id
          name
        }
        schedulable
        employeeType {
          id
          name
        }
        office {
          id
          name
        }
        primary
        rotationLength
      }
    }
  }
`;
export const CREATE_EMPLOYEE_ASSIGNMENT = gql`
  mutation CreateEmployeeAssignment($input: CreateAssignmentInput!) {
    createAssignment(input: $input) {
      assignment {
        id
        employee {
          id
          email
        }
        shiftType {
          id
          name
        }
        schedulable
        rotationLength
        office {
          id
        }
        primary
        role {
          id
        }
        employeeType {
          id
        }
      }
    }
  }
`;

export const CREATE_HIRED_SHIFT_ASSIGNMENT = gql`
  mutation CreateHiredShiftAssignment($input: CreateHiredShiftInput!) {
    createHiredShift(input: $input) {
      hiredShift {
        id
        shiftDefinition {
          id
          type {
            type
          }
        }
      }
    }
  }
`;

export const BATCH_CREATE_HIRED_SHIFTS = gql`
  mutation createHiredShifts($input: [BatchCreateHiredShiftInput]!) {
    batchCreateHiredShift(input: $input) {
      hiredShifts {
        id
        assignment {
          id
          employee {
            firstName
            lastName
            email
          }
          office {
            name
          }
        }
        shiftDefinition {
          id
          description
          start
          end
          type {
            type
          }
          weekday {
            name
          }
        }
      }
    }
  }
`;

export const BATCH_UPDATE_HIRED_SHIFTS = gql`
  mutation updateHiredShifts($input: [BatchPatchHiredShiftInput]!) {
    batchUpdateHiredShift(input: $input) {
      hiredShifts {
        id
        assignment {
          id
          employee {
            firstName
            lastName
            email
          }
          office {
            name
          }
        }
        shiftDefinition {
          id
          description
          start
          end
          type {
            type
          }
          weekday {
            name
          }
        }
      }
    }
  }
`;

export const BATCH_DELETE_HIRED_SHIFTS = gql`
  mutation deleteHiredShifts($ids: [ID]!) {
    batchDeleteHiredShift(ids: $ids) {
      deletionCount
      deletedIds
      missedIds
    }
  }
`;

export const FIND_SKILL_TYPE = gql`
  query Skills($office: Int) {
    skills(officeId: $office) {
      id
      name
      variety
      office {
        id
        name
      }
    }
  }
`;

export const FIND_EMPLOYEE_BY_ID = gql`
  query Employee($id: Int, $email: String) {
    employees(id: $id, email: $email) {
      id
      email
      employeeskillSet {
        id
        skill {
          id
          variety
          name
        }
      }
    }
  }
`;

export const GET_EMPLOYEE_SKILL = gql`
  query getEmployeeSkills {
    employeeSkills {
      id
      employee {
        id
        firstName
        lastName
      }
      skill {
        id
        name
        variety
      }
    }
  }
`;

export const ROLES = gql`
  query {
    roles {
      id
      name
    }
  }
`;

export const EMPLOYEE_TYPES = gql`
  query {
    employeeTypes {
      id
      name
    }
  }
`;

export const CREATE_EMPLOYEE_TYPE = gql`
  mutation CreateEmployeeType($input: CreateEmployeeTypeInput!) {
    createEmployeeType(input: $input) {
      employeeType {
        id
        name
      }
    }
  }
`;

export const DELETE_EMPLOYEE_TYPES = gql`
  mutation ($id: ID!) {
    deleteEmployeeType(id: $id) {
      deletedId
    }
  }
`;

// export const ALL_SHIFT_TYPES = gql`
//   query {
//     shiftTypes {
//       id
//       name
//     }
//   }
// `;

export const ALL_SHIFT_TYPES = gql`
  query AllShiftTypes($office: Int) {
    shiftDefinitions(officeId: $office) {
      id
      type {
        type
      }
      office {
        id
      }
      description
      start
      end
      weekday {
        name
      }
    }
  }
`;

export const ADD_EMPLOYEE = gql`
  mutation CreateEmployee($input: CreateEmployeeInput!) {
    createEmployee(input: $input) {
      employee {
        id
        firstName
        lastName
        email
        primaryPhone
        hasChangedDefaultPassword
      }
    }
  }
`;

export const BATCH_ADD_EMPLOYEE = gql`
  mutation BatchCreateEmployee($input: [BatchCreateEmployeeInput]!) {
    batchCreateEmployee(input: $input) {
      employees {
        id
        firstName
        lastName
        email
        primaryPhone
        hasChangedDefaultPassword
      }
    }
  }
`;

export const BATCH_CREATE_EMPLOYEE_ASSIGNMENT = gql`
  mutation BatchCreateEmployeeAssignment(
    $input: [BatchCreateAssignmentInput]!
  ) {
    batchCreateAssignment(input: $input) {
      assignments {
        id
        employee {
          id
          email
        }
        shiftType {
          id
          name
        }
        schedulable
        rotationLength
        office {
          id
        }
        primary
        role {
          id
        }
        employeeType {
          id
        }
      }
    }
  }
`;

export const Delete_Employee = gql`
  mutation DeleteEmployee($id: ID!) {
    deleteEmployee(id: $id) {
      deletedId
      found
    }
  }
`;

export const Check_Email_Exists = gql`
  query emailExists($email: String) {
    emailExists(email: $email)
  }
`;

export const Reset_Password = gql`
  mutation resetPassword($email: String!, $newPassword: String!) {
    resetPassword(email: $email, newPassword: $newPassword) {
      success
    }
  }
`;

export const SCHEDULE_FITNESS = gql`
  query EmployeeFitness($schedulePeriod: Int, $office: Int) {
    employeeFitnesses(schedulePeriod: $schedulePeriod, office: $office) {
      id
      employee {
        id
        firstName
        lastName
      }
      score
      maxScore
      ratio
      schedulePeriod {
        id
        start
        end
      }
    }
  }
`;

/************************************************************************************/
/*Schedule Actions*/
/************************************************************************************/

export const GET_SCHEDULE_INFO = gql`
  query GetScheduleInfo($id: Int) {
    offices(id: $id) {
      id
      name
      numberDaysBeforePreferencesDue
      reviewWindowDays
      schedulePublishedBufferWeeks
      scheduleDuration
      timeOff {
        approvedby {
          id
        }
        deniedText
        userCancelled
      }
    }
  }
`;

export const GET_SCHEDULE_PERIODS = gql`
  query GetSchedulePeriods($officeId: Int, $id: Int) {
    schedulePeriods(office: $officeId, id: $id) {
      id
      start
      end
      isUnderReview
      isPublished
      approver {
        id
      }
    }
    offices(id: $officeId) {
      schedulingStatus
    }
  }
`;

export const RELEASE_DRAFT = gql`
  mutation OpenReviewWindow($schedulePeriodId: ID!) {
    openReviewWindowWithNotifications(schedulePeriod: $schedulePeriodId) {
      schedulePeriod {
        id
        isUnderReview
        isPublished
        approver {
          id
        }
      }
    }
  }
`;

export const PUBLISH_SCHEDULE = gql`
  mutation PublishSchedule($schedulePeriodId: ID!, $approver: ID!) {
    publishScheduleWithNotifications(
      schedulePeriod: $schedulePeriodId
      approver: $approver
    ) {
      schedulePeriod {
        id
        isUnderReview
        isPublished
        approver {
          id
        }
      }
    }
  }
`;

/************************************************************************************/
/*Requests*/
/************************************************************************************/

export const GET_REQUESTS = gql`
  query GetRequests($officeId: Int, $userId: Int) {
    me {
      softRequests: offpreferenceSet {
        id
        date
        value
      }
      timeOff: availabilityEmployee {
        id
        userCancelled
        comment
        approvedby {
          id
          firstName
          lastName
        }
        firstday
        lastday
        startTime
        endTime
        type {
          name
          id
        }
        office {
          id
        }
        workHours
        deniedText
        employee {
          firstName
          lastName
        }
        createdAt
      }
    }
    shiftSwitches: shiftSwitches(requestorEmployeeId: $userId) {
      id
      requestorShift {
        shift {
          id
          start
          end
        }
      }
      candidate1 {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          end
          id
        }
      }
      candidate2 {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          end
          id
        }
      }
      candidate3 {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          end
          id
        }
      }
      candidate4 {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          end
          id
        }
      }
      candidate1Denied
      candidate2Denied
      candidate3Denied
      candidate4Denied
      acceptor {
        id
        employee {
          id
          firstName
          lastName
        }
        shift {
          start
          id
        }
      }
      approver {
        id
        firstName
        lastName
      }
      deniedText
      userCancelled
      createdAt
    }
    schedulePeriods(office: $officeId) {
      start
      end
      isUnderReview
      isPublished
    }
    offices(id: $officeId) {
      maxGreyoutRequests
      scheduleDuration
    }
  }
`;

export const GET_SHIFTS_FOR_SS_FORM = gql`
  query GetShifts($officeId: Int!, $employeeId: Int!, $rangeStart: DateTime) {
    allShifts: shifts(officeId: $officeId, rangeStart: $rangeStart) {
      id
      start
      end
      shiftassignmentSet {
        id
        employee {
          id
          firstName
          lastName
        }
      }
    }
    userShifts: shiftAssignments(employeeId: $employeeId) {
      isArchived
      id
      shift {
        id
        start
        office {
          id
        }
      }
    }
  }
`;

/************************************************************************************/
/*Analytics*/
/************************************************************************************/

export const ANALYTICS_DATA = gql`
  query analytics(
    $id: Int
    $officeId: Int
    $source: String
    $schedulePeriod: Int
  ) {
    analyticsDatums(
      id: $id
      office: $officeId
      source: $source
      schedulePeriod: $schedulePeriod
    ) {
      id
      data
      source
    }
    offices(id: $officeId) {
      employeeSet {
        id
        firstName
        lastName
      }
      scheduleperiodSet {
        id
        start
        end
      }
    }
  }
`;

export const EMPLOYEE_NAMES = gql`
  query EmployeeNames($officeId: Int) {
    offices(id: $officeId) {
      employeeSet {
        id
        firstName
        lastName
      }
    }
  }
`;

/************************************************************************************/
/*Procedures*/
/************************************************************************************/
export const BATCH_CREATE_PROCEDURE = gql`
  mutation BatchCreateProcedures($input: [BatchCreateProcedureInput]!) {
    batchCreateProcedure(input: $input) {
      procedures {
        id
        start
        end
        procedureRequirement {
          id
          name
        }
      }
    }
  }
`;

export const BATCH_CREATE_PROCEDURE_REQUIREMENTS = gql`
  mutation BatchCreateProcedureRequirements(
    $input: [BatchCreateProcedureRequirementInput]!
  ) {
    batchCreateProcedureRequirement(input: $input) {
      procedureRequirements {
        id
        name
        procedureType {
          id
          name
        }
        office {
          id
          name
        }
        earliestDate
        latestDate
        duration
        additiveConstraint
      }
    }
  }
`;

export const BATCH_CREATE_PROCEDURE_EMPLOYEE_CONSTRAINTS = gql`
  mutation BatchCreateProcedureEmployeeConstraint(
    $input: [BatchCreateProcedureEmployeeConstraintInput]!
  ) {
    batchCreateProcedureEmployeeConstraint(input: $input) {
      procedureEmployeeConstraints {
        id
        value
        start
        end
        employeeCapability {
          id
          name
        }
      }
    }
  }
`;

export const PROCEDURE_REQUIREMENTS = gql`
  query ProcedureRequirements ($office: Int) {
    procedureRequirements(office: $office) {
      id
      name
      earliestDate
      latestDate
      duration
      procedureType {
        id
        name
      }
      office {
        id
        name
      }
    }
  }
`;

export const EMPLOYEE_AVAILABILITY = gql`
  query EmployeeAvailability($officeId:Int){
  availability(officeId: $officeId) {
      id
      employee {
        id
        firstName
        lastName
      }
      approvedby {
        id
        firstName
        lastName
      }
      firstday
      lastday
      type {
        name
        id
      }
      comment
      workHours
      office {
        id
      }
    }
}
`;

