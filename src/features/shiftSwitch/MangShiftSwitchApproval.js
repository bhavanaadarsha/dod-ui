import React from 'react';
import { 
    Grid, Typography, Paper, 
    CircularProgress, IconButton 
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import MangShiftSwitchApprovalItem from './MangShiftSwitchApprovalItem';
import { useQuery } from '@apollo/client';
import { GET_SHIFT_SWITCH_REQUESTS } from '../../api/gqlQueries';
import { userVar } from '../../cache';

const MangShiftSwitchApproval = (props) => {
    const user = userVar();

    const {
        closeView,
        setToast, 
        setShowToast,
        setErrorToast,
        setShowErrorToast,
        refetch
    } = props;

    const { loading, error, data, refetch: ssRefetch } = useQuery(
        GET_SHIFT_SWITCH_REQUESTS, {
            variables: {
                officeId: parseInt(user.office.id)
            }
        }
    );
    
    if(loading) {
        return <CircularProgress />;
    } else if (error) {
        console.error(error);
        return <Typography>Something went wrong. Please try again.</Typography>
    } else {
        const shiftSwitchRequests = data.unapprovedShiftSwitches.filter(shiftSwitch => (
            shiftSwitch.acceptor !== null && shiftSwitch.deniedText === null 
            && shiftSwitch.userCancelled === false
        ));

        const renderedRequests = shiftSwitchRequests.map(request => (
            <MangShiftSwitchApprovalItem 
                key={request.id} 
                requestToShow={request}
                setToast={setToast}
                setShowToast={setShowToast}
                setErrorToast={setErrorToast}
                setShowErrorToast={setShowErrorToast}
                refetch={refetch}
                ssRefetch={ssRefetch}
            />
        ));
        
        if(shiftSwitchRequests.length !== 0){
            return (  
                <Grid 
                    container 
                    component={Paper} 
                    direction="column" 
                    wrap='nowrap' 
                    spacing={1} 
                    style={{backgroundColor: '#F7F7F7', padding: 20, height: 600, overflow: 'auto'}}
                >
                    <Grid item container justify='space-between'>
                        <Grid item style={{margin: 20}}>
                            <Typography variant='h3'>Shift Switch Requests</Typography>
                        </Grid>
                        <Grid item>
                            <IconButton onClick={closeView}><CloseIcon /></IconButton>
                        </Grid>
                    </Grid>
                    {renderedRequests}
                </Grid>
            );
        } else {
            return (
                <Grid container component={Paper}  justify='space-between' style={{backgroundColor: '#F7F7F7', padding: 20}}>
                    <Grid item>
                        <Typography variant='h3' style={{margin: 20}}>No Shift Switch Requests</Typography>
                    </Grid>
                    <Grid item>
                        <IconButton onClick={closeView}><CloseIcon /></IconButton>
                    </Grid>
                </Grid>
            );
        }
    }
};


export default MangShiftSwitchApproval;