import React, { useState } from 'react';
import { 
    Button, 
    Grid,
    Typography, 
    makeStyles, 
    Paper, 
    Accordion, 
    AccordionDetails, 
    AccordionSummary, 
    CircularProgress,
    TextField,
    Dialog,
    DialogContent
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ShiftSwitchItem from './ShiftSwitchItem';
import { useQuery, useMutation } from '@apollo/client';
import { 
    GET_TWO_SHIFTS_BY_IDS, 
    MANAGER_APPROVE_SHIFT_SWITCH,
    MANAGER_DENY_SHIFT_SWITCH
} from '../../api/gqlQueries';
import { userVar } from '../../cache';
import { format } from 'date-fns';

const useStyles = makeStyles((theme) => ({
    buttonSpacing: {
        marginLeft: 10, 
        marginTop: 10,
        width: 75
    },
    addSpacing: {
        marginTop: 10,
        marginBottom: 10,
    },
    paper: {
        paddingTop: 10,
        paddingBottom: 20,
        paddingLeft: 30,
        paddingRight: 30,
        marginTop: 10, 
        marginBottom: 10,
        backgroundColor: theme.palette.background.default
    },
    input: {
        width: 384
    }
}));

const MangShiftSwitchApprovalItem = (props) => {
    const classes = useStyles();

    const {
        requestToShow,
        setToast, 
        setShowToast,
        setErrorToast,
        setShowErrorToast,
        refetch,
        ssRefetch
    } = props;

    const user = userVar();

    const [showDenialReason, setShowDenialReason] = useState(false);
    const [denialReason, setDenialReason] = useState('');

    const { loading, error, data } = useQuery(GET_TWO_SHIFTS_BY_IDS, {variables: {
        shiftOneId: requestToShow.requestorShift.shift.id,
        shiftTwoId: requestToShow.acceptor.shift.id
    }});

    const [approveRequest] = useMutation(MANAGER_APPROVE_SHIFT_SWITCH, {
        // update(cache, { data: { managerApproveShiftSwitchWithNotifications }}) {
        //     cache.evict({
        //         id: `ShiftSwitchNode:${managerApproveShiftSwitchWithNotifications.shiftSwitchRequest.id}`
        //     });
        // },
        onCompleted(data) {
            refetch();
            ssRefetch();
            console.log(data);
            setToast('Manager Approve Shift Switch');
            setShowToast(true);
        },
        onError(error) {
            console.error(error);
            setErrorToast('Error Approve Shift Switch');
            setShowErrorToast(true);
        }
    });

    const [denyShiftSwitch] = useMutation(MANAGER_DENY_SHIFT_SWITCH, {
        // update(cache, { data: { updateShiftSwitch }}) {
        //     cache.evict({
        //         id: `ShiftSwitchNode:${updateShiftSwitch.shiftSwitch.id}`
        //     });
        // },
        onCompleted(data) {
            console.log(data);
            setToast('Manager Deny Shift Switch');
            setShowToast(true);
        },
        onError(error) {
            console.error(error);
            setErrorToast('Error Deny Shift Switch');
            setShowErrorToast(true);
        }
    });

    if(loading) {
        return <CircularProgress />;
    } else if (error) {
        console.error(error);
        return <Typography>Something went wrong. Please try again.</Typography>
    } else {
        console.log(data)
        const requestorShift = data.firstShift[0];
        const acceptorShift = data.secondShift[0];
        const requestorEmployeeId = requestToShow.requestorShift.employee.id;
        const acceptorEmployeeId = requestToShow.acceptor.employee.id;
        const officeId = requestToShow.requestorShift.shift.office.id;

        const requestorEmployee = requestToShow.requestorShift.employee.firstName + ' ' +
            requestToShow.requestorShift.employee.lastName;
        const acceptorEmployee = requestToShow.acceptor.employee.firstName + ' ' +
            requestToShow.acceptor.employee.lastName;

        const requestorCurrentDate = format(new Date(requestorShift.start), 'MM/dd/yyyy');
        const acceptorCurrentDate = format(new Date(acceptorShift.start), 'MM/dd/yyyy');

        const title = `Shift Switch Request between ${requestorEmployee} and ${acceptorEmployee}`;
        const details = `${requestorEmployee} would move to ${acceptorCurrentDate} and ${acceptorEmployee} would move to ${requestorCurrentDate}.`;

        const handleApproveRequest = (e) => {
            e.stopPropagation();
            approveRequest({variables: {
                id: parseInt(requestToShow.id),
                approver: parseInt(user.id)
            }});
        };
                
        const handleDenyRequest = (e, request) => {
            e.stopPropagation();
            denyShiftSwitch({variables: {
                id: request.id,
                deniedText: denialReason,
                denier: parseInt(user.id)
            }});
            setShowDenialReason(false);
            setDenialReason('');
        };

        const showManagerDenialForm = (e) => {
            e.stopPropagation();
            setShowDenialReason(true);
        };
        
        return (  
            <Paper component={Accordion} className={classes.paper}>
                <Grid 
                    container 
                    component={AccordionSummary} 
                    justify='space-between' 
                    alignContent='center' 
                    className={classes.addSpacing} 
                    expandIcon={<ExpandMoreIcon />}
                >
                    <Grid item container direction='column' xs={8} md={8}>
                        <Grid item>
                            <Typography variant='h5' className={classes.addSpacing}>
                                {title}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography>{details}</Typography>
                        </Grid>
                    </Grid>
                    <Grid item container xs={4} md={4} alignContent='center' justify='flex-end'>
                        <Grid item>
                            <Button 
                                variant='outlined' 
                                color='primary' 
                                onClick={e => showManagerDenialForm(e)} 
                                className={classes.buttonSpacing}
                            >
                                Deny
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button 
                                variant='contained' 
                                color='primary' 
                                onClick={handleApproveRequest} 
                                className={classes.buttonSpacing}
                            >
                                Approve
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid component={AccordionDetails} container justify='space-between' className={classes.addSpacing}>
                    <ShiftSwitchItem shiftEvent={requestorShift} employeeId={requestorEmployeeId} officeId={officeId}/>
                    <ShiftSwitchItem shiftEvent={acceptorShift} employeeId={acceptorEmployeeId} officeId={officeId}/>
                </Grid>
                <Dialog open={showDenialReason} fullWidth maxWidth='xs'>
                    <DialogContent style={{ padding: 30 }}>
                        <Grid container direction='column' spacing={2}>
                            <Grid item >
                                <Typography variant='h2'>Reason for Denial</Typography>
                            </Grid>
                            <Grid item >
                                <TextField
                                    variant='outlined'
                                    value={denialReason}
                                    onChange={e => setDenialReason(e.target.value)}
                                    className={classes.input}
                                    onKeyPress={e => {e.key === 'Enter' && handleDenyRequest(e, requestToShow);}}
                                />
                            </Grid>
                            <Grid item container justify='flex-end' spacing={1}>
                                <Grid item>
                                    <Button 
                                        variant='contained' 
                                        color='primary' 
                                        className={classes.button} 
                                        onClick={(e) => handleDenyRequest(e, requestToShow)}
                                    >
                                        Submit
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Button 
                                        variant='outlined' 
                                        color='primary' 
                                        className={classes.button} 
                                        onClick={() => setShowDenialReason(false)}
                                    >
                                        Cancel
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </DialogContent>
                </Dialog>
            </Paper>
        );
    }
};
 
export default MangShiftSwitchApprovalItem;