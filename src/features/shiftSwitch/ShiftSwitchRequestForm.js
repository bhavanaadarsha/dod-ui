import React, { useState } from "react";
import { selectedDateVar, userVar } from "../../cache";
import roles from "../../Roles/roles";
import MangShiftSwitchRequestForm from "./MangShiftSwitchRequestForm";
import EmpShiftSwitchRequestForm from "./EmpShiftSwitchRequestForm";
import {
  Grid,
  Typography,
  IconButton,
  FormControlLabel,
  Switch,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import RescheduleOptionsKey from '../rescheduling/RescheduleOptionsKey';

const ShiftSwitchRequestForm = (props) => {
  const {
    allEvents,
    userEvents,
    closeDialog,
    shiftNames,
    //date,
    // dayObject,
    // showDayActions,
    scheduleEndDate,
    setToast,
    setShowToast,
    setErrorToast,
    setShowErrorToast,
    view, 
    shiftToChange,
    refetch,
  } = props;

  const user = userVar();
  
  const [personalShiftSwitch, setPersonalShiftSwitch] = useState(false);
  const date = selectedDateVar();

  return (
    <Grid
      container
      component="form"
      direction="column"
      alignItems="flex-start"
      justify="center"
    >
      <Grid container item justify="space-between">
        <Grid item>
          <Typography variant="h2">Switch Shifts</Typography>
        </Grid>
        <Grid item>
          <IconButton color="secondary" onClick={closeDialog}>
            <CloseIcon />
          </IconButton>
        </Grid>
        <Grid item container justify='flex-start'>
          <RescheduleOptionsKey type='switch' />
        </Grid>
      </Grid>
      {user.role === roles.MANAGER && (
        <Grid item style={{ marginTop: 15 }}>
          <FormControlLabel
            control={
              <Switch
                checked={personalShiftSwitch}
                onChange={(e) => setPersonalShiftSwitch(e.target.checked)}
                name="personalShiftSwitch"
                color={personalShiftSwitch ? "primary" : "secondary"}
              />
            }
            label="Shift Switch Request is for Me"
          ></FormControlLabel>
        </Grid>
      )}
      {user.role === roles.EMPLOYEE || personalShiftSwitch ? (
        <EmpShiftSwitchRequestForm
          allEvents={allEvents}
          userEvents={userEvents}
          closeDialog={closeDialog}
          date={date}
          setToast={setToast}
          setShowToast={setShowToast}
          setErrorToast={setErrorToast}
          setShowErrorToast={setShowErrorToast}
          shiftToChange={shiftToChange}
          view={view}
        />
      ) : (
        <MangShiftSwitchRequestForm
          allEvents={allEvents}
          shiftNames={shiftNames}
          closeDialog={closeDialog}
          date={date}
          // dayObject={dayObject}
          scheduleEndDate={scheduleEndDate}
          setToast={setToast}
          setShowToast={setShowToast}
          setErrorToast={setErrorToast}
          setShowErrorToast={setShowErrorToast}
          // showDayActions={showDayActions}
          refetch={refetch}
        />
      )}
    </Grid>
  );
};

export default ShiftSwitchRequestForm;
