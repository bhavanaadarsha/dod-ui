import React, { useEffect, useState } from "react";
import {
  Button,
  Grid,
  Typography,
  Select,
  MenuItem,
  InputLabel,
  makeStyles,
  Box,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import EventParticipant from "../calendar/EventParticipant";
import {
  selectEmployees,
  findEmployeesToSwitch,
} from "../../helpers/shiftSwitch";
import { add, sub, format } from "date-fns";
import { useLazyQuery, useMutation } from "@apollo/client";
import {
  ADD_AND_REMOVE_EMPLOYEES,
  GET_SHIFT_ASSIGNMENT,
  RESCHEDULE_SWAP_OPTIONS,
} from "../../api/gqlQueries";
import { userVar } from "../../cache";
import StarRateIcon from '@material-ui/icons/StarRate';
import isSameDay from "date-fns/isSameDay";

const useStyles = makeStyles((theme) => ({
  input: {
    minWidth: 138,
    maxWidth: 225,
  },
  select: {
    minWidth: 273,
  },
  error: {
    color: theme.palette.primary.main,
  },
  dateError: {
    color: theme.palette.primary.main,
    width: 225,
    paddingLeft: 14,
    paddingRight: 14,
  },
  icon: {
    padding: 0,
    marginRight: -9,
    marginBottom: -6,
    marginLeft: 2
  }
}));

const MangShiftSwitchRequestForm = (props) => {
  const classes = useStyles();

  const {
    allEvents,
    shiftNames,
    // dayObject,
    closeDialog,
    date,
    scheduleEndDate,
    setToast,
    setShowToast,
    setErrorToast,
    setShowErrorToast,
    // showDayActions,
    refetch,
  } = props;

  const currentUser = userVar();
  // const ref = React.createRef();

  const [firstShiftAssignmentId, setFirstShiftAssignmentId] = useState("");
  const [secondShiftAssignmentId, setSecondShiftAssignmentId] = useState("");

  const [firstShiftEmployeeId, setFirstShiftEmployeeId] = useState("");
  const [currentDateShiftId, setCurrentDateShiftId] = useState("");
  const [desiredDateShiftId, setDesiredDateShiftId] = useState("");

  const [getFirstShiftAssignment] = useLazyQuery(GET_SHIFT_ASSIGNMENT, {
    onCompleted(data) {
      if (data.shiftAssignments.length > 0) {
        setFirstShiftEmployeeId(data.shiftAssignments[0].employee.id);
        setFirstShiftAssignmentId(data.shiftAssignments[0].id);
      }
      setError("");
      setRescheduleSwapOptionsPresent(false);
      setRescheduleSwapAddOptions([]);
    },
    onError(error) {
      console.error(error);
      setError("Please try selecting first date and employee again.");
    },
  });

  // const [getSecondShiftAssignment] = useLazyQuery(GET_SHIFT_ASSIGNMENT, {
  //   onCompleted(data) {
  //     setSecondShiftAssignmentId(data.shiftAssignments[0].id);
  //     setError("");
  //   },
  //   onError(error) {
  //     console.error(error);
  //     setError("Please try selecting second date and employee again.");
  //   },
  // });

  const [moveEmployees] = useMutation(ADD_AND_REMOVE_EMPLOYEES, {
    onCompleted(data) {
      console.log(data);
      refetch();
      setToast("Manager Shift Switch");
      setShowToast(true);
      closeDialog();
      // let employeeToAdd;
      // let employeeToRemove;
      // if (showDayActions) {
      //   const matchingShift = dayObject.events.find((event) => {
      //     if (event.eventId === data.createShiftOne.shiftAssignment.shift.id) {
      //       employeeToAdd = data.createShiftOne.shiftAssignment.employee;
      //       employeeToRemove = data.removeShiftOne.shiftAssignment.employee.id;
      //       return event;
      //     } else if (
      //       event.eventId === data.createShiftTwo.shiftAssignment.shift.id
      //     ) {
      //       employeeToAdd = data.createShiftTwo.shiftAssignment.employee;
      //       employeeToRemove = data.removeShiftTwo.shiftAssignment.employee.id;
      //       return event;
      //     } else {
      //       return false;
      //     }
      //   });
      //   if (matchingShift && employeeToAdd && employeeToRemove) {
      //     //create copies of events not changing in day object, the shift to update,
      //     //and the participants of the shift to update
      //     const eventsNotChanging = dayObject.events.filter(
      //       (event) => event.eventId !== matchingShift.eventId
      //     );
      //     const updatedShift = { ...matchingShift };
      //     let participants = [...matchingShift.participants];

      //     //create an array of particpantIds to check against ID of employee to remove
      //     const participantIds = matchingShift.participants.map(
      //       (participant) => participant.id
      //     );
      //     const index = participantIds.indexOf(employeeToRemove);

      //     //remove employee and add new employee
      //     participantIds.splice(index, 1);
      //     participants = participants.filter((participant) =>
      //       participantIds.includes(participant.id)
      //     );
      //     participants.push(employeeToAdd);

      //     //update day object events
      //     updatedShift.participants = participants;
      //     dayObject.events = [...eventsNotChanging, updatedShift];
      //   }
      // }
    },
    onError(error) {
      console.error(error);
      setErrorToast("Non Eligible Shift Switch");
      setShowErrorToast(true);
    },
  });

  const [error, setError] = useState("");
  const [currentDateError, setCurrentDateError] = useState("");
  // const [desiredDateError, setDesiredDateError] = useState("");
  const [shift, setShift] = useState("All Shifts");
  const [currentDate, setCurrentDate] = useState(
    date ? new Date(date) : new Date()
  );
  // const [desiredDate, setDesiredDate] = useState(
  //   date ? new Date(date) : new Date()
  // );
  const [
    currentDateSelectedEmployee,
    setCurrentDateSelectedEmployee,
  ] = useState("");
  const [
    desiredDateSelectedEmployee,
    setDesiredDateSelectedEmployee,
  ] = useState("");

  const [currentDateEmployees, setCurrentDateEmployees] = useState([]);
  // const [desiredDateEmployees, setDesiredDateEmployees] = useState([]);
  // const [eligibleEmployees, setEligibleEmployees] = useState([]);

  useEffect(() => {
    rescheduleSwapOptions({
      variables: {
        shiftAssignments: parseInt(firstShiftAssignmentId),
      },
    });
  }, [firstShiftAssignmentId]);

  const [rescheduleSwapAddOptions, setRescheduleSwapAddOptions] = useState([]);
  const [
    rescheduleSwapOptionsPresent,
    setRescheduleSwapOptionsPresent,
  ] = useState(false);

  const [rescheduleSwapOptions] = useLazyQuery(RESCHEDULE_SWAP_OPTIONS, {
    onCompleted(data) {
      if (data.rescheduleSwaps.length > 0) {
        let individualSet = data.rescheduleSwaps.map(
          (e) => e.rescheduleindividualSet
        );
        let allemployeeChoices = [];
        let allemployeeIds = [];
        let emps = [];
        individualSet.forEach((e) => {
          emps = e.filter(
            (x) => x.employee.id != parseInt(firstShiftEmployeeId)
          );
          emps.forEach((x) => {
            if (!allemployeeIds.includes(x.employee.id)) {
              allemployeeIds.push(x.employee.id);
              if (
                new Date(
                  x.rescheduleactionSet?.find(
                    (x) => x.actionType === "DROP"
                  )?.shift.start
                ) > new Date()
              ) {
                allemployeeChoices.push(x);
              }
            }
          });
        });
        if (emps.length > 0) {
          setRescheduleSwapAddOptions(allemployeeChoices);
          setRescheduleSwapOptionsPresent(true);
        } else {
          setRescheduleSwapOptionsPresent(false);
          setRescheduleSwapAddOptions([]);
        }
      } else {
        setRescheduleSwapOptionsPresent(false);
        setRescheduleSwapAddOptions([]);
      }
    },
    onError(error){
      console.error(error);
    }
  });

  useEffect(() => {
    let dateToView = date ? new Date(date) : new Date();
    let firstDate;
    // let secondDate;
    if (
      format(dateToView, "MM/dd/yyyy") ===
      format(new Date(scheduleEndDate), "MM/dd/yyyy")
    ) {
      firstDate = sub(dateToView, { days: 1 });
    } 
    // else {
    //   secondDate = add(dateToView, { days: 1 });
    // }

    let currentEmployees;
    // let desiredEmployees;
    console.log(allEvents)
    if (firstDate) {
      setCurrentDate(firstDate);
      currentEmployees = selectEmployees(allEvents, shift, firstDate, setError);
      // desiredEmployees = selectEmployees(
      //   allEvents,
      //   shift,
      //   desiredDate,
      //   setError
      // );
    } else {
      // setDesiredDate(secondDate);
      // desiredEmployees = selectEmployees(
      //   allEvents,
      //   shift,
      //   secondDate,
      //   setError
      // );
      currentEmployees = selectEmployees(
        allEvents,
        shift,
        dateToView,
        setError
      );
    }

    // if (currentEmployees && desiredEmployees) {
    //   const newEligible = findEmployeesToSwitch(
    //     currentEmployees,
    //     desiredEmployees
    //   );
    //   const filteredCurrentEmployees = findEmployeesToSwitch(
    //     desiredEmployees,
    //     currentEmployees
    //   );

    //   setCurrentDateEmployees(filteredCurrentEmployees);
    //   setDesiredDateEmployees(desiredEmployees);
    //   setEligibleEmployees(newEligible);
    // }
    console.log(currentEmployees)
    currentEmployees && setCurrentDateEmployees(currentEmployees);
    
  }, []);

  const handleCurrentDateChange = (date) => {
    if (date && !isNaN(date.getTime())) {
      setCurrentDate(date);
      setCurrentDateError("");
      setCurrentDateSelectedEmployee("");
      setDesiredDateSelectedEmployee("");
      const newCurrentEmployees = selectEmployees(
        allEvents,
        shift,
        date,
        setError
      );

      newCurrentEmployees && setCurrentDateEmployees(newCurrentEmployees);
      // if (newCurrentEmployees) {
        // const newEligible = findEmployeesToSwitch(
        //   newCurrentEmployees,
        //   desiredDateEmployees
        // );
        // const filteredCurrentEmployees = findEmployeesToSwitch(
        //   desiredDateEmployees,
        //   newCurrentEmployees
        // );

        // setCurrentDateEmployees(filteredCurrentEmployees);
        // setEligibleEmployees(newEligible);
      // }
    } else {
      setCurrentDateError("Invalid date format");
    }
  };

  // const handleDesiredDateChange = (date) => {
  //   if (date && !isNaN(date.getTime())) {
  //     setDesiredDate(date);
  //     setDesiredDateError("");
  //     setCurrentDateSelectedEmployee("");
  //     setDesiredDateSelectedEmployee("");
  //     const newDesiredEmployees = selectEmployees(
  //       allEvents,
  //       shift,
  //       date,
  //       setError
  //     );
  //     const newCurrentEmployees = selectEmployees(
  //       allEvents,
  //       shift,
  //       currentDate,
  //       setError
  //     );
  //     if (newDesiredEmployees && newCurrentEmployees) {
  //       setDesiredDateEmployees(newDesiredEmployees);
  //       const filteredCurrentEmployees = findEmployeesToSwitch(
  //         newDesiredEmployees,
  //         newCurrentEmployees
  //       );
  //       const newEligible = findEmployeesToSwitch(
  //         newCurrentEmployees,
  //         newDesiredEmployees
  //       );

  //       setCurrentDateEmployees(filteredCurrentEmployees);
  //       setEligibleEmployees(newEligible);
  //     }
  //   } else {
  //     setDesiredDateError("Invalid date format");
  //   }
  // };

  const handleShiftChange = (event) => {
    const newShift = event.target.value;
    setShift(newShift);
    setCurrentDateSelectedEmployee("");
    setDesiredDateSelectedEmployee("");
    const newCurrentEmployees = selectEmployees(
      allEvents,
      newShift,
      currentDate,
      setError
    );
    // const newDesiredEmployees = selectEmployees(
    //   allEvents,
    //   newShift,
    //   desiredDate,
    //   setError
    // );

    newCurrentEmployees && setCurrentDateEmployees(newCurrentEmployees);
    !newCurrentEmployees && setError("Invalid date or shift selection");
    // if (newCurrentEmployees && newDesiredEmployees) {
      // setDesiredDateEmployees(newDesiredEmployees);

      // const newEligibles = findEmployeesToSwitch(
      //   newCurrentEmployees,
      //   newDesiredEmployees
      // );
      // const filteredCurrentEmployees = findEmployeesToSwitch(
      //   newDesiredEmployees,
      //   newCurrentEmployees
      // );

      // setCurrentDateEmployees(filteredCurrentEmployees);
      // setEligibleEmployees(newEligibles);
    // } else {
    //   setError("Invalid date or shift selection");
    // }
  };

  const handleSelectedEmployeeChange = (e, type) => {
    const employeeId = e.target.value;
    if (type === "current") {
      const selectedShift = allEvents.find((event) => {
        const participantIds = event.participants.map(
          (participant) => participant.id
        );
        return (isSameDay(
          event.start, new Date(currentDate)
        ) && participantIds.includes(employeeId)
        );
      });
      console.log(selectedShift)
      setCurrentDateSelectedEmployee(employeeId);
      setCurrentDateShiftId(selectedShift.eventId);
      getFirstShiftAssignment({
        variables: {
          employeeId: parseInt(employeeId),
          shiftId: parseInt(selectedShift.eventId),
        },
      });
    } else {
      if (rescheduleSwapOptionsPresent) {
        setDesiredDateSelectedEmployee(employeeId);

        const emps = rescheduleSwapAddOptions.find(
          (i) => i.employee.id === employeeId
        );
        const secondAction = emps.rescheduleactionSet.find(
          (e) => e.actionType === "DROP"
        );
        setSecondShiftAssignmentId(secondAction.shiftAssignment);
        setDesiredDateShiftId(secondAction.shift.id);
      } 
      // else {
      //   const selectedShift = allEvents.find((event) => {
      //     const participantIds = event.participants.map(
      //       (participant) => participant.id
      //     );
      //     return (
      //       new Date(event.start).toDateString() ===
      //         new Date(desiredDate).toDateString() &&
      //       participantIds.includes(employeeId)
      //     );
      //   });
      //   setDesiredDateSelectedEmployee(employeeId);
      //   setDesiredDateShiftId(selectedShift.eventId);
      //   getSecondShiftAssignment({
      //     variables: {
      //       employeeId: parseInt(employeeId),
      //       shiftId: parseInt(selectedShift.eventId),
      //     },
      //   });
      // }
    }
  };

  const renderEmployeeOptions = (employees) => {
    if (employees && employees.length > 0) {
      return employees.map((employee) => (
        <MenuItem key={employee.id + "select"} value={employee.id}>
          <EventParticipant
            participant={employee}
            showAvatar={false}
            widthMatters={false}
          />
        </MenuItem>
      ));
    } else {
      return (
        <MenuItem value={"None"}>
          <Typography>No Eligible Employees</Typography>
        </MenuItem>
      );
    }
  };

  const renderEmployeeOptionswithDate = (employees) => {
    if (employees && employees.length > 0) {
      const options = [...employees];
      options.sort((a,b) => b.benefit - a.benefit);

      return options.map((element) => (
        <MenuItem
          key={element.employee.id + "select"}
          value={element.employee.id}
        >
          <Typography>
            {element.employee.firstName + " " + element.employee.lastName}
            {element.rescheduleactionSet.length > 0 &&
              format(
                new Date(
                  element.rescheduleactionSet.find(
                    (e) => e.actionType === "DROP"
                  ).shift.start
                ),
                " dd MMM yyyy"
              ) +
                " " +
                element.rescheduleactionSet.find((e) => e.actionType === "DROP")
                  .shift.shiftType.type}
            {element.benefit <= -1 && (
              <StarRateIcon className={classes.icon} />
            )}
            {element.benefit < 1 && element.benefit > -1 && (
              <>
                <StarRateIcon className={classes.icon} />
                <StarRateIcon className={classes.icon} />
              </>
            )}
            {element.benefit >= 1 && (
              <>
                <StarRateIcon className={classes.icon} />
                <StarRateIcon className={classes.icon} />
                <StarRateIcon className={classes.icon} />
              </>
            )}
          </Typography>
        </MenuItem>
      ));
    } else {
      return (
        <MenuItem value={null}>
          <Typography>No eligible employee options</Typography>
        </MenuItem>
      )
    }
  };

  // const validRequest = Boolean(
  //   rescheduleSwapOptionsPresent
  //     ? currentDateShiftId &&
  //         secondShiftAssignmentId &&
  //         currentDateSelectedEmployee &&
  //         desiredDateSelectedEmployee &&
  //         firstShiftAssignmentId &&
  //         currentDate > new Date()
  //     : currentDateShiftId &&
  //         desiredDateShiftId &&
  //         currentDateSelectedEmployee &&
  //         desiredDateSelectedEmployee &&
  //         firstShiftAssignmentId &&
  //         secondShiftAssignmentId &&
  //         currentDate > new Date() &&
  //         desiredDate > new Date()
  // );
  const validRequest = Boolean(
    currentDateShiftId &&
    secondShiftAssignmentId &&
    currentDateSelectedEmployee &&
    desiredDateSelectedEmployee &&
    firstShiftAssignmentId &&
    currentDate > new Date()
  );

  const handleSubmit = () => {
    //mutation to add desired date selected employee to current shift and remove employee from current shift
    moveEmployees({
      variables: {
        employeeOneAdd: desiredDateSelectedEmployee,
        shiftOneId: currentDateShiftId,
        employeeTwoAdd: currentDateSelectedEmployee,
        shiftTwoId: desiredDateShiftId,
        employeeOneShiftAssignmentRemove: firstShiftAssignmentId,
        employeeTwoShiftAssignmentRemove: secondShiftAssignmentId,
        managerId: currentUser.id,
      },
    });

    //mutation to add current date selected employee to desired shift and remove employee from desired shift
    // moveEmployees({variables: {
    //     input: {
    //         employeeTwoAdd: currentDateSelectedEmployee,
    //         shiftTwoId: desiredDateShiftId
    //     },
    //     employeeRemove: secondShiftAssignmentId,
    //     updateInput: {
    //         isArchived: true
    //     }
    // }});
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid item style={{ marginTop: 15 }}>
        <InputLabel id="shift-select-label" shrink={false}>
          <Typography variant="h5">Shift</Typography>
        </InputLabel>
        <Select
          labelId="shift-select-label"
          id="shift-select"
          variant="outlined"
          value={shift}
          onChange={handleShiftChange}
          className={classes.select}
        >
          {shiftNames.map((shiftName) => (
            <MenuItem key={shiftName} value={shiftName}>
              {shiftName}
            </MenuItem>
          ))}
          <MenuItem value={"All Shifts"}>All Shifts</MenuItem>
        </Select>
      </Grid>
      <Grid item style={{ marginTop: 15 }}>
        <Typography variant="h5">Dates to Switch</Typography>
      </Grid>
      <Grid item container justify="flex-start" spacing={2}>
        <Grid item>
          <InputLabel htmlFor="current-date">
            <Typography variant="h5">From</Typography>
          </InputLabel>
          <KeyboardDatePicker
            disableToolbar
            autoOk
            variant="inline"
            inputVariant="outlined"
            format="MM/dd/yyyy"
            id="current-date"
            value={currentDate}
            onChange={handleCurrentDateChange}
            minDate={new Date()}
            className={classes.input}
          />
          {currentDateError && (
            <Typography variant="body2" className={classes.dateError}>
              {currentDateError}
            </Typography>
          )}
        </Grid>
        {/* {!rescheduleSwapOptionsPresent && (
          <Grid item>
            <InputLabel htmlFor="desired-date">
              <Typography variant="h5">To</Typography>
            </InputLabel>
            <KeyboardDatePicker
              disableToolbar
              autoOk
              variant="inline"
              inputVariant="outlined"
              format="MM/dd/yyyy"
              id="desired-date"
              value={desiredDate}
              onChange={handleDesiredDateChange}
              minDate={new Date()}
              className={classes.input}
            />
            {desiredDateError && (
              <Typography variant="body2" className={classes.dateError}>
                {desiredDateError}
              </Typography>
            )}
          </Grid>
        )} */}
      </Grid>
      <Grid item style={{ marginTop: 15 }}>
        <Typography variant="h5">Employees</Typography>
      </Grid>
      <Grid item container justify="space-between" spacing={2}>
        <Grid item>
          <InputLabel id="select-employee-1-label">
            <Typography variant="h5">From</Typography>
          </InputLabel>
          <Select
            labelId="select-employee-1-label"
            id="select-employee-1"
            variant="outlined"
            value={currentDateSelectedEmployee}
            onChange={(e) => handleSelectedEmployeeChange(e, "current")}
            className={classes.select}
            disabled={!!error}
          >
            {renderEmployeeOptions(currentDateEmployees)}
          </Select>
        </Grid>
        <Grid item>
          <InputLabel id="select-employee-2-label">
            <Typography variant="h5">To</Typography>
          </InputLabel>
          <Select
            labelId="select-employee-2-label"
            id="select-employee-2"
            variant="outlined"
            value={desiredDateSelectedEmployee}
            onChange={(e) => handleSelectedEmployeeChange(e, "desired")}
            className={classes.select}
            disabled={!!error}
          >
            {renderEmployeeOptionswithDate(rescheduleSwapAddOptions)}
            {/* {rescheduleSwapOptionsPresent
              ? renderEmployeeOptionswithDate(rescheduleSwapAddOptions)
              : renderEmployeeOptions(eligibleEmployees)} */}
          </Select>
        </Grid>
      </Grid>
      <Grid item container justify="flex-end" style={{ marginTop: 15 }}>
        <Grid item>
          {error && <Typography className={classes.error}>{error}</Typography>}
        </Grid>
      </Grid>
      <Grid
        item
        container
        justify="flex-end"
        style={{
          marginTop: 30,
          zIndex: 4,
          position: "absolute",
          bottom: 30,
          right: 30,
        }}
      >
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmit}
            disabled={!validRequest}
            style={{ width: 169, height: 39 }}
          >
            Switch Shifts
          </Button>
        </Grid>
      </Grid>
    </MuiPickersUtilsProvider>
  );
};

export default MangShiftSwitchRequestForm;
