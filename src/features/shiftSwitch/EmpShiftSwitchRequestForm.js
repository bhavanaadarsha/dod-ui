import React, { useEffect, useState } from "react";
import {
  Button,
  Grid,
  Typography,
  makeStyles,
  InputLabel,
  CircularProgress,
  Select,
  MenuItem
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { findEmployeesToSwitch } from "../../helpers/shiftSwitch";
import EmpSelectEmployeesForm from "./EmpSelectEmployeesForm";
import EmpSelectEmployeesandDates from "./EmpSelectEmployeesandDates";
import { sub, format } from "date-fns";
import {
  GET_SHIFT_ASSIGNMENT,
  GET_SHIFT_ASSIGNMENTS_BY_SHIFT,
  CREATE_SHIFT_SWITCH,
  RESCHEDULE_SWAP_OPTIONS,
} from "../../api/gqlQueries";
import { useLazyQuery, useMutation } from "@apollo/client";
import { userVar } from "../../cache";
import isSameDay from "date-fns/isSameDay";

const useStyles = makeStyles((theme) => ({
  input: {
    minWidth: 138,
    maxWidth: 225,
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  spaceBelow: {
    marginBottom: theme.spacing(1),
  },
  error: {
    color: theme.palette.primary.main,
  },
  coloredText: {
    color: "#8CADE1",
    fontWeight: 700,
  },
  dateError: {
    color: theme.palette.primary.main,
    width: 225,
    paddingLeft: 14,
    paddingRight: 14,
  },
}));

const EmpShiftSwitchRequestForm = (props) => {
  const classes = useStyles();

  const {
    allEvents,
    userEvents,
    closeDialog,
    date,
    setToast,
    setShowToast,
    shiftToChange,
    view,
    setErrorToast,
    setShowErrorToast,
  } = props;

  const user = userVar();

  // const d = new Date(date);

  const [currentDateShiftAssignmentId, setCurrentDateShiftAssignmentId] =
    useState("");
  // const [secondShiftAssignmentIds, setSecondShiftAssignmentIds] = useState([]);
  const [rescheduleShiftAssignmentIds, setRescheduleShiftAssignmentIds] =
    useState([]);

  const [getShiftAssignmentId] = useLazyQuery(GET_SHIFT_ASSIGNMENT, {
    onCompleted(data) {
      if (data.shiftAssignments.length > 0) {
        setCurrentDateShiftAssignmentId(data.shiftAssignments[0].id);
        setMultipleShifts([...data.shiftAssignments])
      }
      setError("");
      setRescheduleSwapOptionsPresent(false);
      setRescheduleSwapAddOptions([]);
    },
    onError(error) {
      console.error(error);
      setError("Please choose different dates and try again.");
    },
  });

  // const [getShiftAssignmentsForShift, { called, loading }] = useLazyQuery(
  //   GET_SHIFT_ASSIGNMENTS_BY_SHIFT,
  //   {
  //     onCompleted(data) {
  //       setSecondShiftAssignmentIds(data.shiftAssignments);
  //       setError("");
  //     },
  //     onError(error) {
  //       console.error(error);
  //       setError("Please choose different dates and try again.");
  //     },
  //     fetchPolicy: "cache-and-network",
  //   }
  // );

  const [rescheduleSwapAddOptions, setRescheduleSwapAddOptions] = useState({});
  const [rescheduleSwapOptionsPresent, setRescheduleSwapOptionsPresent] =
    useState(false);

  const [
    rescheduleSwapOptions,
    { called: rescheduleCalled, loading: rescheduleLoading },
  ] = useLazyQuery(RESCHEDULE_SWAP_OPTIONS, {
    onCompleted(data) {
      if (data.rescheduleSwaps.length > 0) {
        let individualSet = data.rescheduleSwaps.map(
          (e) => e.rescheduleindividualSet
        );
        let allemployeeChoices = [];
        let allemployeeIds = [];
        individualSet.forEach((e) => {
          let emps = e.filter((x) => x.employee.id != parseInt(user.id));
          emps.forEach((x) => {
            if (!allemployeeIds.includes(x.employee.id)) {
              allemployeeIds.push(x.employee.id);
              allemployeeChoices.push(x);
            }
          });
        });
        if (allemployeeChoices.length > 0) {
          setRescheduleSwapOptionsPresent(true);
          setRescheduleSwapAddOptions(allemployeeChoices);
        } else {
          setRescheduleSwapOptionsPresent(false);
          setRescheduleSwapAddOptions({});
        }
      } else {
        setRescheduleSwapOptionsPresent(false);
        setRescheduleSwapAddOptions({});
      }
    },
    onError(error) {
      console.error(error);
    },
  });

  const [createShiftSwitch] = useMutation(CREATE_SHIFT_SWITCH, {
    onCompleted(data) {
      console.log(data);
      setError("");
      setToast("Employee Shift Switch");
      setShowToast(true);
      closeDialog();
    },
    onError(error) {
      console.error(error);
      setError(
        "Unable to create shift switch request. Please check details and try again."
      );
    },
  });

  //if user has hired shift type, save variable as userShift and pass to selectEmployees

  const [error, setError] = useState("");
  // const [desiredDateError, setDesiredDateError] = useState("");
  const [currentDateError, setCurrentDateError] = useState("");
  const [currentDate, setCurrentDate] = useState();
  const [multipleShifts, setMultipleShifts] = useState([]);
  // const [desiredDate, setDesiredDate] = useState();
  // const [currentShiftTitle, setCurrentShiftTitle] = useState();

  // const [currentDateEmployees, setCurrentDateEmployees] = useState([]);
  // const [desiredDateEmployees, setDesiredDateEmployees] = useState([]);
  // const [eligibleEmployees, setEligibleEmployees] = useState([]);
  const [employeesToNotify, setEmployeesToNotify] = useState([]);
  // const [eligibleEmployeesPresent, setEligibleEmployeesPresent] = useState(
  //   false
  // );

  const formattedUserScheduleDates = userEvents.map((event) =>
    format(new Date(event.start), "MM/dd/yyyy")
  );

  useEffect(() => {
    if (currentDateShiftAssignmentId) {
      rescheduleSwapOptions({
        variables: {
          shiftAssignments: parseInt(currentDateShiftAssignmentId),
        },
      });
    }
  }, [currentDateShiftAssignmentId]);

  useEffect(() => {
    //grab date that is passed in from user selection on calendar
    // const selectedDate = new Date(date);
    // selectedDate.setHours(0, 0, 0, 0);

    //find first day of the week to use when finding first eligible shift for switch
    // const dayNumber = selectedDate.getDay();
    // const firstDayOfWeek = sub(selectedDate, { days: dayNumber });
    const filteredByView = userEvents.filter(event => (
      event.calendars.includes('month')
    ));
    // const shifts = filteredByView.filter(event => (
    //   isSameDay(new Date(event.start), newDate) &&
    //   event.eventId === shiftToChange
    // ));
    
    // if (shifts.length > 0) {
    //   shifts.forEach((shift, index) => {
    //     getShiftAssignments(shift, index);
    //   });
    // }

    //find first user event and first nonUser event
    const userUpcomingEvents = filteredByView.filter(event => (
      event.start > date || isSameDay(event.start, date)
      // event.eventId === shiftToChange
    ));
    userUpcomingEvents.sort((a,b) => a.start - b.start)
    const userFirstEvent = userUpcomingEvents[0];

    if (userFirstEvent) {
      getShiftAssignments(userFirstEvent, 0);
      setCurrentDate(userFirstEvent.start);
    } 
    // const nonUserFirstEvent = allEvents.filter(
    //   (event) =>
    //     !formattedUserScheduleDates.includes(
    //       format(new Date(event.start), "MM/dd/yyyy")
    //     ) &&
    //     new Date(event.start) >= firstDayOfWeek &&
    //     event.eventTitle === userFirstEvent.eventTitle
    // )[0];

    //find employees eligible to make switch based on events
    // if (!nonUserFirstEvent) {
    //   setErrorToast("Non Eligible Shift Switch");
    //   setShowErrorToast(true);
    //   closeDialog();
    //   return;
    // }
    // const eligibleEmployees = findEmployeesToSwitch(
    //   userFirstEvent.participants,
    //   nonUserFirstEvent.participants
    // );

    // setCurrentShiftTitle(userFirstEvent.eventTitle);
    
    // setDesiredDate(new Date(nonUserFirstEvent.start));

    //get shift assignment ID for first shift and list of all assignment IDs for second shift
    // getShiftAssignmentId({
    //   variables: {
    //     employeeId: parseInt(user.id),
    //     shiftId: parseInt(userFirstEvent.eventId),
    //   },
    // });

    // getShiftAssignmentsForShift({
    //   variables: {
    //     shiftId: parseInt(nonUserFirstEvent.eventId),
    //   },
    // });

    //set state with all starting data
    // setCurrentDateEmployees(userFirstEvent.participants);
    // setDesiredDateEmployees(nonUserFirstEvent.participants);
    // setEligibleEmployees(eligibleEmployees);
    // setEligibleEmployeesPresent(Boolean(eligibleEmployees.length !== 0));
  }, []);

  const getShiftAssignments = (shift, index) => {
    let matchingShiftId;
    let matchingShiftAssignments;
    if (shift.tasks) {
      let shiftAssignments = [];
      if (shift.eventTitle === 'GHOC') {
        shift.tasks.forEach(task => {
          let matchingAssignment = task.shiftassignmentSet.find(assignment => (
            parseInt(assignment.employee.id) === 
            parseInt(user.id)
          ));
          if (matchingAssignment) {
            matchingAssignment = {...matchingAssignment};
            matchingAssignment.shift = {
              id: task.id,
              start: task.start,
              end: task.end
            }
            shiftAssignments = [
              ...shiftAssignments, 
              matchingAssignment
            ];
          }
        });
      } else {
        const matches = shift.tasks.filter(task => (
          parseInt(task.shiftAssignment.employee.id) === 
          parseInt(user.id)
        ));
        
        if (matches.length > 0) {
          shiftAssignments = matches.map(match => (
            match.shiftAssignment
          ));
        }
      }
      matchingShiftAssignments = shiftAssignments.length > 0;
      // const shiftAssignmentIds = shiftAssignments.map(shiftAssign => (
      //   shiftAssign.id
      // ));

      if (index === 0) {
        setMultipleShifts(shiftAssignments);
        if (view === 'month') {
          setCurrentDateShiftAssignmentId(shiftAssignments[0]?.id);
        } else {
          const match = shiftAssignments.find(shiftAssign => (
            parseInt(shiftAssign.shift.id) === parseInt(shiftToChange)
          ));
          if (match) {
            setCurrentDateShiftAssignmentId(match.id);
          } else {
            setCurrentDateShiftAssignmentId(shiftAssignments[0]?.id);
          }
        }
      } else {
        setMultipleShifts([...multipleShifts, shiftAssignments]);
      }

      // setMultipleShifts(shiftAssignments);
      // setCurrentDateShiftAssignmentId(shiftAssignments[0]?.id);
    } else {
      matchingShiftId = shift.eventId;
    }

    if (matchingShiftAssignments) {
        return;
    } else if (matchingShiftId) {
      // setShiftId(matchingShiftId);
      getShiftAssignmentId({variables: {
        employeeId: parseInt(user.id), 
        shiftId: parseInt(matchingShiftId)
      }});
    } 
  }

  // const findNewDesiredDateShiftEmployees = (shiftTitle) => {
  //   const desiredDateShift = allEvents.find(
  //     (event) =>
  //       new Date(event.start).toDateString() === desiredDate.toDateString() &&
  //       event.eventTitle === shiftTitle
  //   );

  //   if (desiredDateShift) {
  //     setDesiredDate(new Date(desiredDateShift.start));
  //     setEmployeesToNotify([]);
  //     setDesiredDateEmployees(desiredDateShift.participants);
  //     setDesiredDateError("");

  //     getShiftAssignmentsForShift({
  //       variables: {
  //         shiftId: parseInt(desiredDateShift.eventId),
  //       },
  //     });
  //     return desiredDateShift.participants;
  //   } else {
  //     setEmployeesToNotify([]);
  //     setCurrentDateEmployees([]);
  //     setEligibleEmployees([]);
  //     setEligibleEmployeesPresent(false);
  //     setDesiredDateError(
  //       "There is not an eligible shift on your desired date"
  //     );
  //     return null;
  //   }
  // };

  const handleCurrentDateChange = (date) => {
    if (date && !isNaN(date.getTime())) {
      // const newDate = date.toDateString();
      // const currentDateShift = userEvents.find(
      //   (event) => new Date(event.start).toDateString() === newDate
      // );
      setCurrentDate(date);
      const filteredByView = userEvents.filter(event => (
        event.calendars.includes('month')
      ));
      const shifts = filteredByView.filter(event => (
        isSameDay(new Date(event.start), date)
      ));
      console.log(filteredByView, shifts)

      if (shifts.length > 0) {
        setCurrentDateError('');
        shifts.forEach((shift, index) => {
          getShiftAssignments(shift, index);
        });
      } else {
        setCurrentDateError('You must choose a date that you are working');
        setCurrentDate(date);
      }

      // if (currentDateShift) {
      //   setCurrentDate(new Date(currentDateShift.start));

      //   getShiftAssignmentId({
      //     variables: {
      //       employeeId: parseInt(user.id),
      //       shiftId: parseInt(currentDateShift.eventId),
      //     },
      //   });

      //   // rescheduleSwapOptions({
      //   //   variables: {
      //   //     shiftAssignments: parseInt(currentDateShiftAssignmentId),
      //   //   },
      //   // });

      //   // const newCurrentDateEmployees = currentDateShift.participants;

      //   setCurrentDateError("");

      //   // if (currentShiftTitle !== currentDateShift.eventTitle) {
      //   //   setCurrentShiftTitle(currentDateShift.eventTitle);
      //   //   const newDesiredDateEmployees = findNewDesiredDateShiftEmployees(
      //   //     currentDateShift.eventTitle
      //   //   );
      //   //   if (newDesiredDateEmployees) {
      //   //     setCurrentDateEmployees(newCurrentDateEmployees);
      //   //     const newEligible = findEmployeesToSwitch(
      //   //       newCurrentDateEmployees,
      //   //       newDesiredDateEmployees
      //   //     );
      //   //     setEligibleEmployees(newEligible);
      //   //     setEligibleEmployeesPresent(newEligible.length !== 0);
      //   //     setError("");
      //   //   }
      //   // } else {
      //   //   setCurrentDateEmployees(newCurrentDateEmployees);
      //   //   const newEligible = findEmployeesToSwitch(
      //   //     newCurrentDateEmployees,
      //   //     desiredDateEmployees
      //   //   );
      //   //   setEligibleEmployees(newEligible);
      //   //   setEligibleEmployeesPresent(newEligible.length !== 0);
      //   //   setError("");
      //   // }
      // } else {
      //   setCurrentDateError("You must choose a date you are working");
      //   setCurrentDate(date);
      // }
    } else {
      setCurrentDateShiftAssignmentId("");
    }
  };

  // const handleDesiredDateChange = (date) => {
  //   if (date && !isNaN(date.getTime())) {
  //     const currentDateShift = userEvents.find(
  //       (event) =>
  //         new Date(event.start).toDateString() === currentDate.toDateString()
  //     );

  //     const desiredDateShift = allEvents.find(
  //       (event) =>
  //         new Date(event.start).toDateString() === date.toDateString() &&
  //         event.eventTitle === currentDateShift.eventTitle
  //     );

  //     if (desiredDateShift) {
  //       setDesiredDate(new Date(desiredDateShift.start));
  //       setEmployeesToNotify([]);

  //       getShiftAssignmentsForShift({
  //         variables: {
  //           shiftId:
  //             !rescheduleSwapOptionsPresent &&
  //             parseInt(desiredDateShift.eventId),
  //         },
  //       });
  //       const newEmployees = desiredDateShift.participants;
  //       setDesiredDateEmployees(newEmployees);
  //       const newEligible = findEmployeesToSwitch(
  //         currentDateEmployees,
  //         newEmployees
  //       );
  //       setEligibleEmployees(newEligible);
  //       setEligibleEmployeesPresent(newEligible.length !== 0);
  //       setError("");
  //       setDesiredDateError("");
  //     } else {
  //       setDesiredDateEmployees([]);
  //       setEligibleEmployees([]);
  //       setEligibleEmployeesPresent(false);
  //       setDesiredDateError(
  //         "There is not an eligible shift on your desired date"
  //       );
  //       setDesiredDate(date);
  //     }
  //   } else {
  //     setCurrentDateShiftAssignmentId("");
  //   }
  // };

  const handleSubmit = () => {
    // const shiftAssignmentIds = secondShiftAssignmentIds
    //   .filter((shiftAssignment) => {
    //     return employeesToNotify.includes(
    //       parseInt(shiftAssignment.employee.id)
    //     );
    //   })
    //   .map((shiftAssignment) => shiftAssignment.id);

    const candidates = {};
    if (rescheduleSwapOptionsPresent) {
      rescheduleShiftAssignmentIds.forEach((id, index) => {
        candidates[`candidate${index + 1}`] = id;
      });
      createShiftSwitch({
        variables: {
          requestorShift: currentDateShiftAssignmentId,
          ...candidates,
        },
      });
    }
    // else {
    //   shiftAssignmentIds.forEach((id, index) => {
    //     candidates[`candidate${index + 1}`] = id;
    //   });
    // }

    // createShiftSwitch({
    //   variables: {
    //     requestorShift: currentDateShiftAssignmentId,
    //     ...candidates,
    //   },
    // });
  };

  const checkFirstDateInvalid = (date) => {
    const formatted = format(date, "MM/dd/yyyy");
    return !formattedUserScheduleDates.includes(formatted);
  };

  // const checkSecondDateInvalid = (date) => {
  //   const formatted = format(date, "MM/dd/yyyy");
  //   return formattedUserScheduleDates.includes(formatted);
  // };

  const eligibleToSave = Boolean(
    !!currentDateShiftAssignmentId &&
      employeesToNotify.length > 0 &&
      currentDate > new Date()
    // desiredDate > new Date()
  );

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <div>
        <Grid item style={{ marginTop: 15 }}>
          <Typography variant="h5">Dates to Switch</Typography>
        </Grid>
        <Grid item container justify="flex-start" spacing={2}>
          <Grid item>
            <InputLabel htmlFor="current-date">
              <Typography variant="h5">From</Typography>
            </InputLabel>
            <KeyboardDatePicker
              disableToolbar
              autoOk
              variant="inline"
              inputVariant="outlined"
              format="MM/dd/yyyy"
              id="current-date"
              shouldDisableDate={checkFirstDateInvalid}
              value={currentDate || date}
              onChange={handleCurrentDateChange}
              minDate={new Date()}
              className={classes.input}
            />
            {currentDateError && (
              <Typography variant="body2" className={classes.dateError}>
                {currentDateError}
              </Typography>
            )}
          </Grid>
          <Grid item>
            {multipleShifts.length > 0 && 
              <> 
                <Typography variant='h5'>Select Shift:</Typography>
                <Select 
                    id='shift' 
                    name='shift' 
                    variant='outlined'  
                    value={currentDateShiftAssignmentId} 
                    className={classes.input}
                    disabled={multipleShifts.length === 1}
                    onChange={e => setCurrentDateShiftAssignmentId(e.target.value)}
                >
                    {multipleShifts.map(shift => (
                      <MenuItem 
                        key={shift.id}
                        value={shift.id}
                      >
                        {format(new Date(shift.shift.start), 'HH:mm')}{' - '}
                        {format(new Date(shift.shift.end), 'HH:mm')}
                      </MenuItem>
                    ))}
                </Select>
              </>
            }
          </Grid>

          {/* <Grid item>
            {!rescheduleSwapOptionsPresent && called && !loading &&
              rescheduleCalled && !rescheduleLoading && (
              <>
                <InputLabel htmlFor="desired-date">
                  <Typography variant="h5">To</Typography>
                </InputLabel>
                <KeyboardDatePicker
                  disableToolbar
                  autoOk
                  variant="inline"
                  inputVariant="outlined"
                  format="MM/dd/yyyy"
                  id="desired-date"
                  shouldDisableDate={checkSecondDateInvalid}
                  value={desiredDate}
                  onChange={handleDesiredDateChange}
                  minDate={new Date()}
                  className={classes.input}
                />
                {desiredDateError && (
                  <Typography variant="body2" className={classes.dateError}>
                    {desiredDateError}
                  </Typography>
                )}
              </>
            )}
          </Grid>*/}
        </Grid>
        <Grid item style={{ marginTop: 15 }}>
          {(rescheduleSwapOptionsPresent) && (
          // {(rescheduleSwapOptionsPresent || eligibleEmployeesPresent) && (
            <Typography className={classes.spaceBelow}>
              Select employees you would like to send the request to:
            </Typography>
          )}

          <Grid
            container
            direction="column"
            alignItems="flex-start"
            justify="space-evenly"
            spacing={1}
          >
            {error && (
              <Grid item>
                <Typography className={classes.error}>{error}</Typography>
              </Grid>
            )}
            {rescheduleCalled && rescheduleLoading &&
            // {((called && loading) || (rescheduleCalled && rescheduleLoading)) && (
              <CircularProgress />
            }
            {/* {!rescheduleSwapOptionsPresent && eligibleEmployeesPresent &&
              called && !loading && rescheduleCalled && !rescheduleLoading &&
                <EmpSelectEmployeesForm
                  employees={eligibleEmployees}
                  setEmployees={setEmployeesToNotify}
                  setError={setError}
                />
            } */}
            {rescheduleSwapOptionsPresent &&
              <EmpSelectEmployeesandDates
                employees={rescheduleSwapAddOptions}
                setEmployees={setEmployeesToNotify}
                setRescheduleShiftAssignmentIds={
                  setRescheduleShiftAssignmentIds
                }
                getShiftAssignment={getShiftAssignmentId}
                setError={setError}
              />
            }
            {!rescheduleSwapOptionsPresent &&
            // {!eligibleEmployeesPresent && !rescheduleSwapOptionsPresent &&
              <Grid item>
                <Typography>
                  There are no employees who can make this switch. Please try
                  different dates.
                </Typography>
              </Grid>
            }
          </Grid>
        </Grid>
        <Grid
          item
          container
          justify="flex-end"
          style={{
            marginTop: 20,
            zIndex: 4,
            position: "absolute",
            bottom: 25,
            right: 30,
          }}
        >
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              onClick={handleSubmit}
              disabled={!eligibleToSave}
              style={{ width: 169, height: 39 }}
            >
              Switch Shifts
            </Button>
          </Grid>
        </Grid>
      </div>
    </MuiPickersUtilsProvider>
  );
};

export default EmpShiftSwitchRequestForm;
