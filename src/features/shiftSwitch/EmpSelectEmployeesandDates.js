import React, { useEffect, useState } from "react";
import { Grid, Checkbox, Typography, Box, makeStyles } from "@material-ui/core";
import { format } from "date-fns";
import StarRateIcon from '@material-ui/icons/StarRate';

const useStyles = makeStyles((theme) => ({
  icon: {
      padding: 0,
      marginRight: -9,
      marginBottom: -6,
      marginLeft: 2
  }
}));

const EmpSelectEmployeesandDates = ({
  employees,
  setEmployees,
  setError,
  setRescheduleShiftAssignmentIds,
}) => {
  const [checked, setChecked] = useState([]);
  const [shiftassignments, setshiftassignments] = useState([]);

  const classes = useStyles();

  useEffect(() => {
    setChecked([]);
  }, [employees]);

  const handleCheckboxToggle = (element) => () => {
    const currentIndex = checked.indexOf(parseInt(element.employee.id));
    const newChecked = [...checked];
    const newshiftAssignment = [...shiftassignments];
    const d = new Date();
    const currentAssignmentIndex = element.rescheduleactionSet.find(
      (e) => e.actionType === "DROP"
    ).shiftAssignment;

    if (newshiftAssignment.length <= 4 && !newshiftAssignment.includes(currentAssignmentIndex)) {
      newshiftAssignment.push(currentAssignmentIndex);
    }
    if (currentIndex === -1 && newChecked.length < 4) {
      setError("");
      newChecked.push(parseInt(element.employee.id));
    } else if (currentIndex === -1 && newChecked.length >= 4) {
      setError("You may not choose more than 4 employees.");
      return;
    } else {
      setError("");
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
    setshiftassignments(newshiftAssignment);
    setRescheduleShiftAssignmentIds(newshiftAssignment);
    setEmployees(newChecked);
  };

  const formatShiftTitle = (option) => {
    const dropShift = option.rescheduleactionSet.find(
      (e) => e.actionType === "DROP"
    );

    if (dropShift) {
      const start = dropShift.shift.start;
      const end = dropShift.shift.end;
      return format(new Date(start), 'dd MMM yyyy ') +
        format(new Date(start), 'haaaaa') + 
        ' - ' + format(new Date(end), 'haaaaa')
    } else {
      return '';
    }
  }

  const employeesToSwitchWith = () => {
    if (employees.length > 0) {
      const sortedOptions = [...employees];
      sortedOptions.sort((a,b) => b.benefit - a.benefit);

      return sortedOptions.map((option, index) => (
        <Grid
          item
          xs={6}
          container
          alignItems="center"
          spacing={1}
          key={index}
          onClick={handleCheckboxToggle(option)}
        >
          <Grid item xs={2}>
            <Checkbox
              checked={checked.indexOf(parseInt(option.employee.id)) !== -1}
            />
          </Grid>
          <Grid item xs={10}>
            <Box ml={1}>
              <Typography>
                {option.employee.firstName + " " + option.employee.lastName}
                {option.benefit <= -1 && (
                  <StarRateIcon className={classes.icon} />
                )}
                {option.benefit < 1 && option.benefit > -1 && (
                  <>
                    <StarRateIcon className={classes.icon} />
                    <StarRateIcon className={classes.icon} />
                  </>
                )}
                {option.benefit >= 1 && (
                  <>
                    <StarRateIcon className={classes.icon} />
                    <StarRateIcon className={classes.icon} />
                    <StarRateIcon className={classes.icon} />
                  </>
                )}
              </Typography>
              {option.rescheduleactionSet.length > 0 && (
                <Typography variant="subtitle1">
                  {formatShiftTitle(option)}
                </Typography>
              )}
            </Box>
          </Grid>
        </Grid>
      ));
    } else {
      return null;
    }
  };

  return (
    <Grid container item spacing={1}>
      {employeesToSwitchWith()}
    </Grid>
  );
};

export default EmpSelectEmployeesandDates;
