import React, { useEffect, useState } from "react";
import EventParticipant from "../calendar/EventParticipant";
import { Grid, Checkbox } from "@material-ui/core";

const EmpSelectEmployeesForm = ({
  employees,
  setEmployees,
  setError,
}) => {
  const [checked, setChecked] = useState([]);

  useEffect(() => {
    setChecked([]);
  }, [employees]);

  const handleCheckboxToggle = (id) => () => {
    const currentIndex = checked.indexOf(parseInt(id));
    const newChecked = [...checked];

    if (currentIndex === -1 && newChecked.length < 4) {
      setError("");
      newChecked.push(parseInt(id));
    } else if (currentIndex === -1 && newChecked.length >= 4) {
      setError("You may not choose more than 4 employees.");
      return;
    } else {
      setError("");
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
    setEmployees(newChecked);
  };

  const employeesToSwitchWith =  employees.map((employee) => (
        <Grid
          item
          xs={6}
          container
          alignItems="center"
          spacing={1}
          key={employee.id}
          onClick={handleCheckboxToggle(employee.id)}
        >
          <Grid item xs={2}>
            <Checkbox checked={checked.indexOf(parseInt(employee.id)) !== -1} />
          </Grid>
          <Grid item xs={10}>
            <EventParticipant
              participant={employee}
              showAvatar={false}
              widthMatters={false}
            />
          </Grid>
        </Grid>
      ));

  return (
    <Grid container item spacing={1}>
      {employeesToSwitchWith}
    </Grid>
  );
};

export default EmpSelectEmployeesForm;
