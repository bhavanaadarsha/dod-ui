import React, { useState } from 'react';
import { 
    Grid, Typography, Button,
    Dialog, DialogContent, CircularProgress
} from '@material-ui/core';
import ShiftSwitchRequestForm from '../shiftSwitch/ShiftSwitchRequestForm';
import ShiftSwitchItem from './ShiftSwitchItem';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { userVar } from '../../cache';
import { orderShifts } from '../../helpers/formatEvents';
import { GET_SHIFTS_FOR_SS_FORM } from '../../api/gqlQueries';
import { useQuery } from '@apollo/client';

const ShiftSwitch = (props) => {
    const {
        shiftSwitchRequests, scheduleEnd, refetch, 
        setToast, setShowToast, setErrorToast,
        setShowErrorToast
    } = props;

    const user = userVar();

    const [shiftSwitch, setShiftSwitch] = useState(false);
    const [dateToView, setDateToView] = useState(new Date());
    // const [dateToView, setDateToView] = useState(currentScheduleStart);

    const toggleShiftSwitchForm = () => setShiftSwitch(!shiftSwitch);

    const closeShiftSwitchForm = () => {
        setShiftSwitch(false);
        refetch();
    };

    const shiftSwitchRequestsToView = () => {
        const filtered = shiftSwitchRequests.filter(request => (
            new Date(request.requestorShift.shift.start) >= dateToView
        ));
        filtered.length > 0 && filtered.sort((a, b) => (
            new Date(a.requestorShift.shift.start) - new Date(b.requestorShift.shift.start)
        ));
        return filtered;
    };

    const { loading, error, data } = useQuery(GET_SHIFTS_FOR_SS_FORM, {
        variables: {
            officeId: parseInt(user.office.id),
            employeeId: parseInt(user.id)
        },
        onError(error){
            console.error(error);
        }
    });

    const dayObject = {date: new Date(), events: undefined};
    
    let events;
    let userEvents;
    let shiftNames;
    
    if (!loading && !error){
        let scheduleData = orderShifts(data.allShifts);

        events = scheduleData.formatted;
        userEvents = events.filter(event => (
            event.employeeIds && event.employeeIds.includes(user.id)
        ));
        shiftNames = scheduleData.shiftNames 
            ? scheduleData.shiftNames
            : [];
    }

    return (  
        <>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container direction='column'>
                    <Grid 
                        item 
                        container 
                        justify='space-between' 
                        xs={12} 
                        style={{marginTop: 8, marginBottom: 8}}
                    >
                        <Grid item container xs={7} alignItems='center' spacing={1}>
                            <Grid item>
                                <Typography variant='h5'>
                                    See requests from this date on:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <KeyboardDatePicker
                                    disableToolbar
                                    autoOk
                                    variant='inline'
                                    inputVariant='outlined'
                                    format='MM/dd/yyyy'
                                    id='date-to-view'
                                    value={dateToView}
                                    onChange={setDateToView}
                                />
                            </Grid>
                        </Grid>
                        <Grid item >
                            <Button 
                                variant='contained' 
                                color='primary' 
                                onClick={toggleShiftSwitchForm} 
                                disabled={loading || error} 
                                style={{width: 117}}
                            >
                                {loading 
                                    ? <CircularProgress size={20} color='secondary'/> 
                                    : 'Add Request'
                                }
                            </Button>
                        </Grid>
                    </Grid>
                    {shiftSwitchRequestsToView().length > 0
                        ? shiftSwitchRequestsToView().map(request => (
                            <ShiftSwitchItem request={request} key={request.id} />
                        ))
                        : <Typography variant='h5' style={{marginTop: 16}}>
                            No shift switch requests after minimum date
                        </Typography>
                    }
                </Grid>
            </MuiPickersUtilsProvider>
            <Dialog open={shiftSwitch} fullWidth maxWidth='sm'>
                <DialogContent 
                    style={{ 
                        padding: 30, 
                        overflowX: 'hidden', 
                        overflowY: 'auto', 
                        height: 600, 
                        position: 'relative' 
                    }}
                >
                    <ShiftSwitchRequestForm
                        allEvents={events}
                        userEvents={userEvents}
                        closeDialog={closeShiftSwitchForm}
                        shiftNames={shiftNames}
                        date={new Date()}
                        dayObject={dayObject}
                        showDayActions={false}
                        scheduleEndDate={scheduleEnd}
                        setToast={setToast}
                        setShowToast={setShowToast}
                        setErrorToast={setErrorToast}
                        setShowErrorToast={setShowErrorToast}
                        refetch={() => null}
                    />
                </DialogContent>
            </Dialog>
        </>
    );
}
 
export default ShiftSwitch;