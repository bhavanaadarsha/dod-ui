import React, { useEffect, useState } from 'react';
import { 
    Grid, Typography, Button,
    Dialog, DialogContent
} from '@material-ui/core';
import SoftTimeOffForm from '../calendar/SoftTimeOff';
import EditSoftTimeOff from '../calendar/EditSoftTimeOff';
import OffRequestItem from './OffRequestItem';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import { formatSoftRequests } from '../../helpers/formatEvents';
import { userVar } from '../../cache';

const SoftTimeOff = (props) => {
    const {
        softRequests, scheduleEnd, invalidDates, 
        setToast, setShowToast, schedulePeriodWeeks,
        maxGreyout, refetch, 
        // currentScheduleStart
    } = props;

    const user = userVar();
    
    const [softRequest, setSoftRequest] = useState(false);
    const [editSoftRequest, setEditSoftRequest] = useState(false);
    const [softRequestIdToEdit, setSoftRequestIdToEdit] = useState('');
    // const [dateToView, setDateToView] = useState(currentScheduleStart);
    const [dateToView, setDateToView] = useState(new Date());

    const toggleSoftRequest = () => setSoftRequest(!softRequest);
    const toggleEditSoftRequestForm = () => setEditSoftRequest(!editSoftRequest);

    const allUserSoftRequests = formatSoftRequests(softRequests, user.id);

    // let allUserSoftRequests = softRequests;
    // allUserSoftRequests = allUserSoftRequests.length > 0 
    //     ? allUserSoftRequests.map(request => {
    //         const highPriority = request.value > 5;
    //         return {
    //             eventId: request.id,
    //             start: `${request.date}T08:00:00`,
    //             eventTitle: 'Soft Time Off',
    //             type: 'softRequest',
    //             highPriority
    //         };
    //     })
    //     : [];

    const softRequestsToView = () => {
        const filtered = softRequests.filter(request => (
            new Date(request.date) >= dateToView
        ));
        filtered.length > 0 && filtered.sort((a, b) => new Date(a.date) - new Date(b.date));
        return filtered;
    };

    // const dayObject = {date: new Date(), events: undefined};

    const closeSoftForm = () => {
        setSoftRequest(false);
        refetch();
    };

    const closeEditSoftForm = () => {
        setEditSoftRequest(false);
        refetch();
    };
    
    return (  
        <>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container direction='column'>
                    <Grid 
                        item 
                        container 
                        justify='space-between' 
                        xs={12} 
                        style={{marginTop: 8, marginBottom: 8}}
                    >
                        <Grid item container xs={7} alignItems='center' spacing={1}>
                            <Grid item>
                                <Typography variant='h5'>
                                    See requests from this date on:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <KeyboardDatePicker
                                    disableToolbar
                                    autoOk
                                    variant='inline'
                                    inputVariant='outlined'
                                    format='MM/dd/yyyy'
                                    id='date-to-view'
                                    value={dateToView}
                                    onChange={setDateToView}
                                />
                            </Grid>
                        </Grid>
                        <Grid item >
                            <Button variant='contained' color='primary' onClick={toggleSoftRequest}>
                                Add Request
                            </Button>
                        </Grid>
                    </Grid>
                    {softRequestsToView().length > 0
                        ? softRequestsToView().map(request => (
                            <OffRequestItem 
                                request={request}
                                setIdToEdit={setSoftRequestIdToEdit}
                                toggleEditForm={toggleEditSoftRequestForm}
                                type='soft'
                                key={request.id}
                            />
                        ))
                        : <Typography variant='h5' style={{marginTop: 16}}>
                            No soft requests after minimum date
                        </Typography>
                    }
                </Grid>
            </MuiPickersUtilsProvider>
            <Dialog open={softRequest} fullWidth maxWidth='xs'>
                <DialogContent style={{ padding: 30 }}>
                    <SoftTimeOffForm
                        closeDialog={closeSoftForm}
                        // dayObject={dayObject}
                        scheduleEndDate={scheduleEnd}
                        invalidDates={invalidDates}
                        setToast={setToast}
                        setShowToast={setShowToast}
                        // showDayActions={false}
                        maxGreyout={maxGreyout}
                        schedulePeriodWeeks={schedulePeriodWeeks}
                        softRequests={allUserSoftRequests}
                    />
                </DialogContent>
            </Dialog>
            <Dialog open={editSoftRequest} fullWidth maxWidth='xs'>
                <DialogContent style={{ padding: 30 }}>
                    {softRequestIdToEdit &&
                        <EditSoftTimeOff
                            closeDialog={closeEditSoftForm}
                            // dayObject={dayObject}
                            scheduleEndDate={scheduleEnd}
                            invalidDates={invalidDates}
                            setToast={setToast}
                            setShowToast={setShowToast}
                            // showDayActions={false}
                            maxGreyout={maxGreyout}
                            schedulePeriodWeeks={schedulePeriodWeeks}
                            softRequests={allUserSoftRequests}
                            softRequestIdToEdit={softRequestIdToEdit}
                            setSoftRequestIdToEdit={setSoftRequestIdToEdit}
                        />
                    }
                </DialogContent>
            </Dialog>
        </>
    );
}
 
export default SoftTimeOff;