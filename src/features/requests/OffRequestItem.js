import React from 'react';
import { 
    Grid, Typography, Button,
    Paper, makeStyles
} from '@material-ui/core';
import { format } from 'date-fns';
import { useMutation, useQuery } from '@apollo/client';
import { 
    UPDATE_EMPLOYEE_AVAILABILITY, AVAILABILITY_TYPES
} from '../../api/gqlQueries';

const useStyles = makeStyles((theme) => ({
    requestCard: {
        padding: 16,
        marginTop: 10, 
        marginBottom: 10,
        backgroundColor: theme.palette.background.default
    },
    button: {
        width: 75
    }
}));

const OffRequestItem = ({request, setIdToEdit, toggleEditForm, type}) => {
    const classes = useStyles();

    const {data} = useQuery(AVAILABILITY_TYPES);

    const [deleteTimeOff] = useMutation(UPDATE_EMPLOYEE_AVAILABILITY, {
        onError(error){
            console.error(error);
        }
    });

    const handleEditClick = () => {
        setIdToEdit(request.id);
        toggleEditForm();
    };

    const handleDeleteClick = () => {
        deleteTimeOff({variables: {
            id: parseInt(request.id), 
            input: {userCancelled: true}
        }});
    };
    
    let typeName;
    if (type === 'timeOff') {
        typeName = data?.availabilityTypes.find(type => (
            type.id === request.type.id
        ));
    } 
    
    return (  
        <Grid 
            item 
            container 
            justify='space-between' 
            component={Paper} 
            xs={12}
            md={7} 
            className={classes.requestCard}
        >
            <Grid item >
                {type === 'timeOff'
                    ? <Typography variant='h5'>
                        Time Off Request {typeName 
                            ? `- ${typeName.name}`
                            : null
                        }
                    </Typography>
                    : <Typography variant='h5'>Soft Request</Typography>
                }
            </Grid>
            {request.approvedby || request.deniedText
                ? <Grid item>
                    <Button 
                        variant='outlined' 
                        color='primary' 
                        onClick={handleDeleteClick} 
                        className={classes.button}
                    >
                        Delete
                    </Button>
                </Grid>
                : <Grid item>
                    <Button 
                        variant='outlined' 
                        color='primary' 
                        onClick={handleEditClick} 
                        className={classes.button}
                    >
                        Edit
                    </Button>
                </Grid>
            }
            <Grid item xs={12} style={{marginBottom: 8}}>
                {type === 'timeOff'
                    ? <> 
                        <Typography variant='subtitle1'>
                            Status: {request.approvedby && !request.deniedText
                                ? request.approvedby.firstName && request.approvedby.lastName
                                    ? `Approved by ${request.approvedby.firstName} ${request.approvedby.lastName}` 
                                    : 'Approved' 
                                : null
                            }
                            {!request.approvedby && !request.deniedText 
                                && 'Pending approval'
                            }
                            {request.deniedText && `Denied - Reason: ${request.deniedText}`}
                        </Typography>
                        <Typography>
                            <i>Request was made on {format(new Date(request.createdAt), 'dd MMM yyyy')}</i>
                        </Typography>
                    </>
                    : <Typography variant='subtitle1'>
                        Priority: {request.value > 5 ? 'High' : 'Standard'}
                    </Typography>
                }
            </Grid>
            <Grid item xs={12} style={{marginTop: 8, marginBottom: 8}}>
                {type === 'timeOff' && request.firstday === request.lastday
                    ? <> 
                        <Typography>
                            Request dates: 
                            {format(new Date(`${request.firstday}T08:00:00`), ' dd MMM yyyy')} -
                            {format(new Date(`${request.lastday}T08:00:00`), ' dd MMM yyyy')}
                        </Typography>
                        <Typography>Hours used: {request.workHours}</Typography>
                    </>
                    : <>
                        <Typography>
                            Request date: {
                                format(
                                    new Date(`${request.date 
                                        ? request.date : request.firstday}T08:00:00`
                                    ), ' dd MMM yyyy')
                            }
                        </Typography>
                        {type === 'timeOff' &&
                            <Typography>Hours used: {request.workHours}</Typography>
                        }
                    </>
                }
            </Grid>
            {type === 'timeOff' && request.comment &&
                <Grid item xs={12} style={{marginTop: 8}}>
                    <Typography>Comment: {request.comment}</Typography>
                </Grid>
            }
        </Grid>
    );
}
 
export default OffRequestItem;