import React, { useState, useEffect } from 'react';
import { 
    Grid, Typography, Button,
    Dialog, DialogContent
} from '@material-ui/core';
import EditTimeOffRequestForm from '../calendar/EditTimeOffRequestForm';
import TimeOffRequestForm from '../calendar/TimeOffRequestForm';
import OffRequestItem from './OffRequestItem';
import { userVar } from '../../cache';
import { formatSoftRequests, formatTimeOff } from '../../helpers/formatEvents';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker
} from '@material-ui/pickers';
import { useQuery } from '@apollo/client';
import { GET_EMPLOYEE_NAMES } from '../../api/gqlQueries';
import DateFnsUtils from '@date-io/date-fns';
import { isSameDay } from 'date-fns';

const TimeOff = (props) => {
    const {
        timeOffRequests, scheduleEnd, 
        // currentScheduleStart,
        invalidDates, softRequests, setToast, 
        setShowToast, refetch,
    } = props;

    const user = userVar();
    
    const [timeOffRequestForm, setTimeOffRequestForm] = useState(false);
    const [editTimeOffRequest, setEditTimeOffRequest] = useState(false);
    const [timeOffRequestIdToEdit, setTimeOffRequestIdToEdit] = useState('');
    const [employeeResources, setEmployeeResources] = useState([]);
    // const [dateToView, setDateToView] = useState(currentScheduleStart);
    const [dateToView, setDateToView] = useState(new Date());
    
    const { data: employeeData } = useQuery(GET_EMPLOYEE_NAMES, {
        variables: { office: parseInt(user.office.id) },
        onError(error) {
            console.error(error);
        },
    });

    useEffect(() => {
        if (employeeData) {
            addEmployeeData(employeeData);
        }
    }, [employeeData]);

    const toggleTimeOffForm = () => setTimeOffRequestForm(!timeOffRequestForm);
    const toggleEditTimeOffForm = () => setEditTimeOffRequest(!editTimeOffRequest);

    const closeTimeOffForm = () => {
        setTimeOffRequestForm(false);
        refetch();
    }

    const closeEditTimeOffForm = () => {
        setEditTimeOffRequest(false);
        refetch();
    }

    const userTimeOff = formatTimeOff(timeOffRequests, user.id);
    const allUserSoftRequests = formatSoftRequests(softRequests, user.id);
    
    const timeOffRequestsToView = () => {
        const filtered = timeOffRequests.filter(request => (
            new Date(request.firstday) > dateToView ||
            isSameDay(new Date(request.firstday), dateToView)
        ));

        filtered.length > 0 && filtered.sort((a, b) => (
            new Date(a.firstday) - new Date(b.firstday)
        ));
        return filtered;
    };

    const addEmployeeData = (data) => {
        const resources = data.offices[0].employeeSet.map((employee) => {
            if (parseInt(employee.id) === parseInt(user.id)) {
                return {
                    id: employee.id,
                    name: `${employee.firstName} ${employee.lastName}`,
                    skills: employee.skills.map((e) => e.name),
                };
            } else {
                return {
                    id: employee.id,
                    name: `${employee.firstName} ${employee.lastName}`,
                    skills: employee.skills.map((e) => e.name),
                };
            }
        });
        setEmployeeResources(resources);
    };

    return (  
        <>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container direction='column'>
                    <Grid 
                        item 
                        container 
                        justify='space-between' 
                        xs={12} 
                        style={{marginTop: 8, marginBottom: 8}}
                    >
                        <Grid item container xs={7} alignItems='center' spacing={1}>
                            <Grid item>
                                <Typography variant='h5'>
                                    See requests from this date on:
                                </Typography>
                            </Grid>
                            <Grid item >
                                <KeyboardDatePicker
                                    disableToolbar
                                    autoOk
                                    variant='inline'
                                    inputVariant='outlined'
                                    format='MM/dd/yyyy'
                                    id='date-to-view'
                                    value={dateToView}
                                    onChange={setDateToView}
                                />
                            </Grid>
                        </Grid>
                        <Grid item >
                            <Button variant='contained' color='primary' onClick={toggleTimeOffForm}>
                                Add Request
                            </Button>
                        </Grid>
                    </Grid>
                    {timeOffRequestsToView().length > 0
                        ? timeOffRequestsToView().map(request => (
                            <OffRequestItem 
                                request={request}
                                setIdToEdit={setTimeOffRequestIdToEdit}
                                toggleEditForm={toggleEditTimeOffForm}
                                type='timeOff'
                                key={request.id}
                            />
                        ))
                        : <Typography variant='h5' style={{marginTop: 16}}>
                            No time off requests after minimum date
                        </Typography>
                    }
                </Grid>
            </MuiPickersUtilsProvider>
            <Dialog open={timeOffRequestForm} fullWidth maxWidth='sm'>
                <DialogContent style={{ padding: 30, height: 700, position: 'relative' }}>
                    <TimeOffRequestForm
                        closeDialog={closeTimeOffForm}
                        // dayObject={dayObject}
                        // scheduleEndDate={scheduleEnd}
                        invalidDates={invalidDates}
                        setToast={setToast}
                        setShowToast={setShowToast}
                        employees={employeeResources}
                        // showDayActions={false}
                    />
                </DialogContent>
            </Dialog>
            <Dialog open={editTimeOffRequest} fullWidth maxWidth='sm'>
                <DialogContent style={{ padding: 30, height: 700, position: 'relative' }}>
                    {timeOffRequestIdToEdit &&
                        <EditTimeOffRequestForm
                            closeDialog={closeEditTimeOffForm}
                            // dayObject={dayObject}
                            // scheduleEndDate={scheduleEnd}
                            userSoft={allUserSoftRequests}
                            // userTimeOffEvents={userTimeOff}
                            setToast={setToast}
                            setShowToast={setShowToast}
                            userTimeOff={userTimeOff}
                            // userTimeOff={timeOffRequests}
                            // showDayActions={false}
                            timeOffRequestIdToEdit={timeOffRequestIdToEdit}
                            setTimeOffRequestIdToEdit={setTimeOffRequestIdToEdit}
                            refetch={refetch}
                        />
                    }
                </DialogContent>
            </Dialog>
        </>
    );
}
 
export default TimeOff;