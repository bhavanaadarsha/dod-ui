import React, { useState } from 'react';
import { 
    Grid, Typography, Tab,
    Tabs, makeStyles, Snackbar
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import TimeOff from './TimeOff';
import SoftTimeOff from './SoftTimeOff';
import ShiftSwitch from './ShiftSwitch';
import { useQuery } from '@apollo/client';
import { GET_REQUESTS } from '../../api/gqlQueries';
import { userVar } from '../../cache';
import { eachDayOfInterval, format } from 'date-fns';

const useStyles = makeStyles(() => ({
    headerSpacing: {
        marginTop: 30
    },
    tabs: {
        minWidth: 100,
        width: 125
    },
}));

const Requests = () => {
    const classes = useStyles();

    const user = userVar();

    const { loading, error, data, refetch } = useQuery(GET_REQUESTS, {
        variables: {
            officeId: parseInt(user.office.id),
            userId: parseInt(user.id)
        },
        fetchPolicy: 'cache-and-network'
    });

    const [view, setView] = useState(0);
    const [toast, setToast] = useState('');
    const [errorToast, setErrorToast] = useState('');
    const [showToast, setShowToast] = useState(false); 
    const [showErrorToast, setShowErrorToast] = useState(false); 

    const toastMessage = () => {
        const toasts = {
            'Employee Shift Switch': 'Shift switch request sent!',
            'Manager Shift Switch': 'Shift switch successful! Employees notified.',
            'New Time Off Request': 'Time off request sent for approval!',
            'Edit Time Off Request': 'Time off request updated and sent for approval!',
            'Delete Time Off Request': 'Time off request deleted!',
            'New Soft Request': 'Soft request entered successfully!',
            'Delete Soft Request': 'Soft request deleted!',
        };
        return toasts[toast] ? toasts[toast] : 'Success!';
    };

    const errorToastMessage = () => {
        const errorToasts = {
            'Non Eligible Shift Switch': 'No eligible shifts to switch.',
            'Error Delete Soft Request': 'Unable to delete soft request. Please try again.',
            'Error Delete Time Off': 'Unable to delete time off request. Please try again.'
        };
        return errorToasts[errorToast] ? errorToasts[errorToast] : 'Error. Please try again.';
    };
    
    if (loading) {
        return <Typography>Loading...</Typography>;
    } else if (error) {
        console.error(error);
        return <Typography>Something went wrong. Please try again.</Typography>
    } else {
        console.log(data)
        let allViewableSchedules = [...data.schedulePeriods];
        allViewableSchedules = allViewableSchedules.filter(schedule => (
            schedule.isPublished || schedule.isUnderReview
        ));
        const mostRecentIncludingDraft = allViewableSchedules.length > 0
            ? allViewableSchedules.sort((a, b) => new Date(b.start) - new Date(a.start))[0]
            : null;

        let scheduleEnd;
        if (mostRecentIncludingDraft) {
            scheduleEnd = new Date(mostRecentIncludingDraft.end);
        } else {
            scheduleEnd = new Date();
        }

        const maxGreyout = data.offices[0].maxGreyoutRequests;
        const schedulePeriodWeeks = data.offices[0].scheduleDuration 
            ? data.offices[0].scheduleDuration 
            : 4;

        const timeOffRequestsNotCancelled = data.me.timeOff.filter(request => (
            request.userCancelled === false
        ));

        let invalidDates = [];
        timeOffRequestsNotCancelled.forEach(request => {
            let firstDay = new Date(`${request.firstday}T08:00:00`);
            let lastDay = new Date(`${request.lastday}T08:00:00`);
            let arrayOfTimeOff = eachDayOfInterval(
                { start: firstDay, end: lastDay }
            );
            invalidDates = [...invalidDates, ...arrayOfTimeOff]
        });
    
        data.me.softRequests.forEach(softRequest => {
            invalidDates.push(new Date(`${softRequest.date}T08:00:00`));
        });
        invalidDates = invalidDates.map(date => format(date, 'MM/dd/yyyy'));

        return (  
            <>
                <Grid container className={classes.headerSpacing}>
                    <Grid item xs={12}>
                        <Typography variant='h3'>Requests</Typography>
                    </Grid>
                    <Grid 
                        component={Tabs} 
                        item 
                        value={view} 
                        onChange={(e, value) => setView(value)} 
                        indicatorColor='primary' 
                        xs={12}
                    >
                        <Tab label='Time Off' />
                        {/* <Tab label='Soft Time Off' /> */}
                        <Tab label='Shift Switch' />
                    </Grid>
                    {view === 0 && 
                        <TimeOff 
                            timeOffRequests={timeOffRequestsNotCancelled} 
                            scheduleEnd={scheduleEnd}
                            invalidDates={invalidDates}
                            softRequests={[...data.me.softRequests]}
                            setToast={setToast}
                            setShowToast={setShowToast}
                            refetch={refetch}
                        />
                    }
                    {/* {view === 1 && 
                        <SoftTimeOff 
                            softRequests={[...data.me.softRequests]} 
                            scheduleEnd={scheduleEnd}
                            invalidDates={invalidDates} 
                            setToast={setToast}
                            setShowToast={setShowToast}
                            schedulePeriodWeeks={schedulePeriodWeeks}
                            maxGreyout={maxGreyout}
                            refetch={refetch}
                        />
                    } */}
                    {view === 1 && 
                        <ShiftSwitch 
                            shiftSwitchRequests={data.shiftSwitches}
                            scheduleEnd={scheduleEnd}
                            refetch={refetch}
                            setToast={setToast}
                            setShowToast={setShowToast}
                            setErrorToast={setErrorToast}
                            setShowErrorToast={setShowErrorToast}
                        />
                    }
                </Grid>
                <Snackbar
                    open={showToast}
                    autoHideDuration={3000}
                    onClose={() => setShowToast(false)}
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                >
                    <MuiAlert elevation={6} onClose={() => setShowToast(false)} severity="success">
                        <Typography>{toastMessage()}</Typography>
                    </MuiAlert>
                </Snackbar>
                <Snackbar
                    open={showErrorToast}
                    autoHideDuration={3000}
                    onClose={() => setShowErrorToast(false)}
                    anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                >
                    <MuiAlert elevation={6} onClose={() => setShowErrorToast(false)} severity="error">
                        <Typography>{errorToastMessage()}</Typography>
                    </MuiAlert>
                </Snackbar>
            </>
        );
    }
};
 
export default Requests;