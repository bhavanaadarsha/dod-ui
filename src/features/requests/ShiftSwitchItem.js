import React from 'react';
import { 
    Grid, Typography, Button,
    Paper, makeStyles
} from '@material-ui/core';
import { format } from 'date-fns';

const useStyles = makeStyles((theme) => ({
    requestCard: {
        padding: 16,
        marginTop: 10, 
        marginBottom: 10,
        backgroundColor: theme.palette.background.default
    },
    button: {
        width: 75
    }
}));

const ShiftSwitchItem = ({request}) => {
    const classes = useStyles();

    const {
        candidate1, 
        candidate2, 
        candidate3, 
        candidate4, 
        candidate1Denied,
        candidate2Denied,
        candidate3Denied,
        candidate4Denied,
        requestorShift, 
        acceptor, 
        approver,
        deniedText,
        userCancelled
    } = request;

    const candidates = [
        {...candidate1, denied: candidate1Denied},
        candidate2 ? {...candidate2, denied: candidate2Denied} : '',
        candidate3 ? {...candidate3, denied: candidate3Denied} : '',
        candidate4 ? {...candidate4, denied: candidate4Denied} : ''
    ];

    const statusText = (candidate) => {
        if (candidate.denied) {
            return 'denied request';
        } else if (acceptor && candidate.employee.id === acceptor.employee.id) {
            return 'accepted request';
        } else {
            return 'pending response'
        }
    };

    const currentDate = format(new Date(requestorShift.shift.start), 'dd MMM yyyy ');
    const currentShift = format(new Date(requestorShift.shift.start), 'haaaaa -') + format(new Date(requestorShift.shift.end), ' haaaaa');
    const requestedDate = format(new Date(candidate1.shift.start), 'dd MMM yyyy ');
    const requestedShift = format(new Date(candidate1.shift.start), 'haaaaa -') + format(new Date(candidate1.shift.end), ' haaaaa');

    return (  
        <Grid 
            item 
            container 
            justify='space-between' 
            component={Paper} 
            xs={12}
            md={7} 
            className={classes.requestCard}
        >
            <Grid item xs={12} style={{marginBottom: 16}}>
                <Typography variant='h5'>
                    Shift Switch Request
                </Typography>
            </Grid>
            <Grid item xs={12} style={{marginBottom: 8}}>
                <Typography>
                    Scheduled date: {currentDate} {currentShift}
                </Typography>
                <Typography>
                    Requested date: {requestedDate} {requestedShift}
                </Typography>
            </Grid>
            <Grid item xs={12} style={{marginTop: 8, marginBottom: 8}}>
                <Typography variant='subtitle1'>
                    Status: 
                    {acceptor && !approver && !deniedText
                        && ` Accepted by ${acceptor.employee.firstName} ${acceptor.employee.lastName} and pending manager approval` 
                    }
                    {acceptor && approver 
                        && ` Accepted by ${acceptor.employee.firstName} ${acceptor.employee.lastName} and manager approval given by ${approver.firstName} ${approver.lastName}` 
                    }
                    {!acceptor && !approver && ' Pending acceptance by employee'}
                </Typography>
                <Typography>
                    <i>Request was made on {format(new Date(request.createdAt), 'dd MMM yyyy')}</i>
                </Typography>
            </Grid>
            <Grid item xs={12} style={{marginTop: 8}}>
                    <Typography>Request sent to:</Typography>
            </Grid>
            {candidates.map((candidate, index) => {
                if (candidate) {
                    return (
                        <Grid item xs={12} key={candidate.id} style={{marginLeft: 16}}>
                            <Typography>
                                {candidate.employee.firstName} {candidate.employee.lastName} - {statusText(candidate)}
                            </Typography>
                        </Grid>
                    )
                } else {
                    return <Grid key={'empty' + index}></Grid>
                }
            })}
        </Grid>
    );
}
 
export default ShiftSwitchItem;