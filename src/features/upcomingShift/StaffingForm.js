import React, { useState } from 'react';
import { Grid, Typography, IconButton, InputLabel, TextField, Button, makeStyles } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(() => ({
    input: {
        width: 50
    }
}));

const StaffingForm = ({under, closeDialog, setShiftUnderStaffed, setShiftOverStaffed, setNeededNurses, setExtraNurses}) => {
    const classes = useStyles();

    const [nurses, setNurses] = useState(0);

    const handleSave = () => {
        if (under) {
            setShiftUnderStaffed(true);
            setShiftOverStaffed(false);
            setNeededNurses(nurses);
            setExtraNurses(0);
            closeDialog();
        } else {
            setShiftUnderStaffed(false);
            setShiftOverStaffed(true);
            setExtraNurses(nurses);
            setNeededNurses(0);
            closeDialog();
        }
    };

    return ( 
        <Grid container direction='column' spacing={2}>
            <Grid item container justify='space-between' alignItems='center'>
                <Grid item>
                    <Typography variant='h3'>{under ? 'Under' : 'Over'}-Staffed</Typography>
                </Grid>
                <Grid item>
                    <IconButton onClick={closeDialog}><CloseIcon/></IconButton>
                </Grid>
            </Grid>
            <Grid item>
                <InputLabel htmlFor="nurses" shrink={false}>
                    <Typography variant='h5'>Number of {under ? 'Nurses Needed' : 'Extra Nurses'}</Typography>
                </InputLabel>
                <TextField 
                    id='nurses'
                    name='nurses'
                    variant='outlined'
                    type='number'
                    value={nurses}
                    onChange={e => setNurses(e.target.value)}
                    className={classes.input}
                />
            </Grid>
            <Grid item container justify='flex-end'>
                <Grid item>
                    <Button variant='contained' color='primary' onClick={handleSave}>Save</Button>
                </Grid>
            </Grid>
        </Grid>
    );
};
 
export default StaffingForm;