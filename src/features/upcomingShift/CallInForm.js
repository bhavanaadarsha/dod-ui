import React from 'react';
import { Grid, Typography, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

const CallInForm = ({closeDialog}) => {
    return ( 
        <Grid container direction='column' spacing={2}>
            <Grid item container justify='space-between' alignItems='center'>
                <Grid item>
                    <Typography variant='h3'>Call In</Typography>
                </Grid>
                <Grid item>
                    <IconButton onClick={closeDialog}><CloseIcon/></IconButton>
                </Grid>
            </Grid>
        </Grid>
    );
};
 
export default CallInForm;