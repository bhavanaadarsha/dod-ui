import React, { useState } from 'react';
import { Grid, Typography, makeStyles, TableRow, TableCell, Table, TableBody, TableHead, Button, Dialog, DialogContent } from '@material-ui/core';
import CheckIcon from '@material-ui/icons/Check';
import StaffingForm from './StaffingForm';
import { format } from 'date-fns';
import CallInForm from './CallInForm';

const useStyles = makeStyles((theme) => ({
    headerSpacing: {
        marginTop: 30,
        marginBottom: 20
    },
    button: {
        width: 150
    },
    indicatorText: {
        color: theme.palette.primary.main
    }
}));

const ShiftDetails = () => {
    const classes = useStyles();

    // const nextShift = useSelector(selectNextShift);
    // const shiftStart = format(new Date(nextShift.start), 'h aa MM/dd/yy');
    // const shiftEnd = format(new Date(nextShift.end), 'h aa MM/dd/yy');
    // const participants = nextShift.participants.filter(participant => (participant.job_title !== 'Hospital Admin') && (participant.job_title !== 'Nurse Manager'));

    const shiftStart = format(new Date(), 'h aa MM/dd/yy');
    const shiftEnd = format(new Date(), 'h aa MM/dd/yy');
    const participants = [
        {
            id: '3', 
            firstName: 'Will', 
            lastName: 'Hanlen', 
            assignmentSet: [{
                jobType: {
                    name: 'NURSE'
                },
                office: {
                    id: '1'
                }
            }]
        },
        {
            id: '9', 
            firstName: 'Max', 
            lastName: 'Clark', 
            assignmentSet: [{
                jobType: {
                    name: 'NURSE'
                },
                office: {
                    id: '1'
                }
            }]
        },
        {
            id: '12', 
            firstName: 'Frank', 
            lastName: 'Hill', 
            assignmentSet: [{
                jobType: {
                    name: 'NURSE'
                },
                office: {
                    id: '1'
                }
            }]
        }
    ];

    const [staffingForm, setStaffingForm] = useState(false);
    const [callInForm, setCallInForm] = useState(false);
    const [shiftUnderStaffed, setShiftUnderStaffed] = useState(false);
    const [shiftOverStaffed, setShiftOverStaffed] = useState(false);
    const [extraNurses, setExtraNurses] = useState(0);
    const [neededNurses, setNeededNurses] = useState(0);
    const [under, setUnder] = useState();

    const toggleStaffingForm = () => setStaffingForm(!staffingForm);
    const toggleCallInForm = () => setCallInForm(!callInForm);

    const handleUnderClick = () => {
        setUnder(true);
        toggleStaffingForm();
    };

    const handleOverClick = () => {
        setUnder(false);
        toggleStaffingForm();
    };
    
    const renderEmployeeInformation = () => {
        if (participants) {
            return participants.map(participant => {
                return (
                    <TableRow key={participant.id}>
                        <TableCell>
                            <Typography>{participant.name}</Typography>
                            <Typography>{participant.job_title}</Typography>
                        </TableCell>
                        <TableCell><Typography>24</Typography></TableCell>
                        <TableCell><Typography>12</Typography></TableCell>
                        <Grid component={TableCell} container direction='column' style={{width: 275}}>
                            <Grid item>
                                <Typography>Pediatric Med Surg</Typography>
                                <Typography>Pediatric ICU</Typography>
                                <Typography>Pediatric Oncology</Typography>
                                <Typography>Grad NICU</Typography>
                            </Grid>
                        </Grid>
                        <Grid component={TableCell} container direction='column' style={{width: 275}}>
                            <Grid item>
                                <Typography>Charge Nurse</Typography>
                                <Typography>Cardiac</Typography>
                                <Typography>ECMO</Typography>
                            </Grid>
                        </Grid>
                    </TableRow>
                );
            });
        }
    };

    const renderOnCallEmployee = () => {
        return (
            <TableRow>
                <TableCell><Typography>Jen Newman</Typography></TableCell>
                <TableCell><Typography>5 AM - 5 PM</Typography></TableCell>
                <TableCell><Typography>(303) 444-5555</Typography></TableCell>
                <TableCell><Typography>12</Typography></TableCell>
                <Grid component={TableCell} container direction='column' style={{width: 275}}>
                    <Grid item>
                        <Typography>Pediatric Med Surg</Typography>
                        <Typography>Pediatric ICU</Typography>
                        <Typography>Pediatric Oncology</Typography>
                        <Typography>Grad NICU</Typography>
                    </Grid>
                </Grid>
                <Grid component={TableCell} container direction='column' style={{width: 275}}>
                    <Grid item>
                        <Typography>Charge Nurse</Typography>
                        <Typography>Cardiac</Typography>
                        <Typography>ECMO</Typography>
                    </Grid>
                </Grid>
                <TableCell>
                    <Button variant='contained' color='primary' onClick={toggleCallInForm}>Call In</Button>
                </TableCell>
            </TableRow>
        );
    };
    
    return ( 
        <>
            <Grid container direction='column' spacing={2}>
                <Grid item className={classes.headerSpacing}>
                    <Typography variant='h3'>Upcoming Shift</Typography>
                </Grid>
                <Grid item container justify='space-between'>
                    <Grid item xs={4}>
                        <Typography variant='h5'>Start: {shiftStart}</Typography>
                        <Typography variant='h5'>End: {shiftEnd}</Typography>
                    </Grid>
                    <Grid item container justify='flex-end' xs={6} spacing={2}>
                        <Grid item>
                            <Button variant='outlined' color='primary' className={classes.button} onClick={handleUnderClick}>
                                Under-Staffed
                                {shiftUnderStaffed && <CheckIcon/>}
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button variant='outlined' color='primary' className={classes.button} onClick={handleOverClick}>
                                Over-Staffed
                                {shiftOverStaffed && <CheckIcon/>}
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item container justify='space-between'>
                    <Grid item xs={6}>
                        <Typography variant='h5'>{participants ? participants.length : 'No'} nurses scheduled to work</Typography>
                    </Grid>
                    <Grid item container justify='flex-end' xs={6}>
                        <Grid item xs={8}>
                            <Typography variant='h5' className={classes.indicatorText}>
                                {shiftUnderStaffed && parseInt(neededNurses) > 1 && `You have indicated you need ${neededNurses} nurses to staff your unit safely`}
                                {shiftUnderStaffed && parseInt(neededNurses) === 1 && `You have indicated you need ${neededNurses} nurse to staff your unit safely`}
                                {shiftOverStaffed && parseInt(extraNurses) > 1 && `You have indicated you have ${extraNurses} extra nurses who are able to float`}
                                {shiftOverStaffed && parseInt(extraNurses) === 1 && `You have indicated you have ${extraNurses} extra nurse who are able to float`}
                            </Typography>
                        </Grid>
                        {(shiftUnderStaffed || shiftOverStaffed) &&
                            <Grid item container justify='flex-end' xs={4}>
                                <Grid item>
                                    <Button>Edit</Button>
                                </Grid>
                                <Grid item>
                                    <Button>Cancel</Button>
                                </Grid>
                            </Grid>
                        }
                    </Grid>
                </Grid>
                <Grid item container justify='flex-end'>
                    <Grid item>
                        {shiftUnderStaffed &&
                            <Button variant='contained' color='primary'>View Staffing</Button>
                        }
                    </Grid>
                </Grid>
                <Grid item>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell>CC Hours</TableCell>
                                <TableCell>Float Hours</TableCell>
                                <TableCell>Units Can Float To</TableCell>
                                <TableCell>Home Unit Trainings</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {renderEmployeeInformation()}
                        </TableBody>
                    </Table>
                </Grid>
                <Grid item>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>On Call Nurse</TableCell>
                                <TableCell>On Call Hours</TableCell>
                                <TableCell>Phone Number</TableCell>
                                <TableCell>Float Hours</TableCell>
                                <TableCell>Units Can Float To</TableCell>
                                <TableCell>Home Unit Trainings</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {renderOnCallEmployee()}
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>
            {/* <Dialog open={overStaffedForm} fullWidth maxWidth='xs'>
                <DialogContent style={{padding: 30, overflowX: 'hidden'}}>
                    <OverStaffedForm closeDialog={toggleOverStaffedForm} setShiftOverStaffed={setShiftOverStaffed} setExtraNurses={setExtraNurses}/>
                </DialogContent>
            </Dialog> */}
            <Dialog open={staffingForm} fullWidth maxWidth='xs'>
                <DialogContent style={{padding: 30, overflowX: 'hidden'}}>
                    <StaffingForm 
                        under={under} 
                        closeDialog={toggleStaffingForm} 
                        setShiftUnderStaffed={setShiftUnderStaffed} 
                        setShiftOverStaffed={setShiftOverStaffed} 
                        setNeededNurses={setNeededNurses}
                        setExtraNurses={setExtraNurses}
                    />
                </DialogContent>
            </Dialog>
            <Dialog open={callInForm} fullWidth maxWidth='xs'>
                <DialogContent style={{padding: 30, overflowX: 'hidden'}}>
                    <CallInForm closeDialog={toggleCallInForm}/>
                </DialogContent>
            </Dialog>
        </>
    );
};
 
export default ShiftDetails;