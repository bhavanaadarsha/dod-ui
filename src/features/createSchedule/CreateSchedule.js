import React from 'react';
//import Settings from './Settings';
import Actions from './Actions';
import {
    Typography, CircularProgress, makeStyles
} from '@material-ui/core';
import { useQuery } from '@apollo/client';
import { GET_SCHEDULE_INFO } from '../../api/gqlQueries';
import { userVar } from '../../cache';

const useStyles = makeStyles(() => ({
    headerSpacing: {
        marginTop: 30,
        marginBottom: 20
    }
}));

const CreateSchedule = () => {
    const classes = useStyles();

    const user = userVar();
    // const [view, setView] = useState(0);

    const { loading, error, data } = useQuery(GET_SCHEDULE_INFO, {
        variables: { id: user.office.id },
        fetchPolicy: 'cache-and-network'
    });

    if (loading) {
        return <CircularProgress />;
    } else if (error) {
        console.log(error);
        return <Typography>Something went wrong. Please try again.</Typography>;
    } else {
        const scheduleData = data.offices[0];

        return (
            <>
                <Typography variant='h3' className={classes.headerSpacing}>
                    Create Schedule
                </Typography>
                <Actions scheduleData={scheduleData} />
            </>
        );
    }
};

export default CreateSchedule;