import React, { useEffect, useState } from "react";
import {
  Typography,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Button,
  Grid,
  makeStyles,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  InputLabel,
  TextField,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { format, add, sub } from "date-fns";
import axios from "axios";
import {
  userVar,
  creatingScheduleVar,
  initialScheduleStartDateVar,
  newScheduleStartDateVar,
} from "../../cache";
import { useReactiveVar, useQuery, useMutation } from "@apollo/client";
import {
  GET_SCHEDULE_PERIODS,
  RELEASE_DRAFT,
  PUBLISH_SCHEDULE,
} from "../../api/gqlQueries";
import Steps from "./Steps";

const useStyles = makeStyles((theme) => ({
  button: {
    width: 156,
  },
  smallButton: {
    width: 70,
  },
  otherButton: {
    width: 82,
  },
  input: {
    minWidth: 138,
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  timeInput: {
    paddingRight: 8,
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    minWidth: 138,
  },
  dateError: {
    color: theme.palette.primary.main,
    width: 225,
    marginTop: -4,
    paddingLeft: 14,
    paddingRight: 14,
  },
}));

const Actions = ({ scheduleData }) => {
  const classes = useStyles();

  const {
    schedulePublishedBufferWeeks,
    reviewWindowDays,
    numberDaysBeforePreferencesDue,
    timeOff,
    scheduleDuration
  } = scheduleData;

  const currentUser = userVar();
  const creatingSchedule = useReactiveVar(creatingScheduleVar);
  const initialScheduleStartDate = initialScheduleStartDateVar();

  const [activeStep, setActiveStep] = useState(creatingSchedule ? 1 : 0);
  const [publish, setPublish] = useState(false);
  const [error, setError] = useState("");
  const [status, setStatus] = useState("");
  const [draftStartDate, setDraftStartDate] = useState("");
  const [draftEndDate, setDraftEndDate] = useState("");
  const [draftReleased, setDraftReleased] = useState(false);
  const [draftSchedulePeriodId, setDraftSchedulePeriodId] = useState("");
  const [disableCreateSchedule, setDisableCreateSchedule] = useState(false);
  const [firstSchedule, setFirstSchedule] = useState(false);
  const [initialStartDate, setInitialStartDate] = useState(
    initialScheduleStartDate ? initialScheduleStartDate : new Date()
  );
  const [dateError, setDateError] = useState("");
  const [showPopupForStartDate, setShowPopupForStartDate] = useState(false);
  
  const scheduleDurationWeeks = isNaN(parseInt(scheduleDuration))
    ? 4
    : parseInt(scheduleDuration);

  const { data, startPolling, stopPolling } = useQuery(GET_SCHEDULE_PERIODS, {
    variables: { officeId: currentUser.office.id },
    fetchPolicy: "cache-and-network",
    onError(error) {
      console.error(error);
    },
  });

  const [publishSchedule] = useMutation(PUBLISH_SCHEDULE, {
    onCompleted(data) {
      console.log(data);
      handleNext();
      setPublish(false);
    },
    onError(error) {
      console.error(error);
    },
  });

  const [releaseDraftSchedule] = useMutation(RELEASE_DRAFT, {
    onCompleted(data) {
      console.log(data);
      handleNext();
      setDraftReleased(true);
    },
    onError(error) {
      console.error(error);
    },
  });

  let mostRecent;
  if (data && data.schedulePeriods.length > 0) {
    const schedules = [...data.schedulePeriods];
    mostRecent = schedules.sort(
      (a, b) => new Date(b.start) - new Date(a.start)
    )[0];
  }

  let scheduleEnd =
    mostRecent && !newScheduleStartDateVar()
      ? new Date(`${mostRecent.end}T12:00:00`)
      : initialStartDate;

  const nextScheduleStart = scheduleEnd;
  const nextScheduleEnd = add(nextScheduleStart, { 
    weeks: scheduleDurationWeeks
  });

  let reviewUntil;
  let scheduleCreationDate;
  if (reviewWindowDays !== 0) {
    if (
      disableCreateSchedule &&
      mostRecent &&
      (mostRecent.isUnderReview ||
        (!mostRecent.isUnderReview && !mostRecent.isPublished))
    ) {
      reviewUntil = sub(new Date(`${mostRecent.start}T12:00:00`), {
        weeks: schedulePublishedBufferWeeks,
      });
      scheduleCreationDate = sub(reviewUntil, { days: reviewWindowDays });
    } else {
      reviewUntil = sub(nextScheduleStart, {
        weeks: schedulePublishedBufferWeeks,
      });
      scheduleCreationDate = sub(reviewUntil, { days: reviewWindowDays });
    }
  } else {
    scheduleCreationDate = sub(nextScheduleStart, {
      weeks: schedulePublishedBufferWeeks,
    });
  }

  const preferencesDue = sub(scheduleCreationDate, {
    days: numberDaysBeforePreferencesDue,
  });

  const pendingTimeOffRequests = timeOff.filter(
    (request) =>
      request.approvedby === null &&
      request.deniedText === null &&
      request.userCancelled === false
  );

  useEffect(() => {
    if (data && data.schedulePeriods.length > 0) {
      const newStatus = data.offices[0].schedulingStatus;
      const schedulePeriods = [...data.schedulePeriods];
      const mostRecent = schedulePeriods.sort(
        (a, b) => new Date(b.start) - new Date(a.start)
      )[0];

      if (newStatus === "ERROR") {
        if (
          (newStatus === status || status === "STARTING") &&
          creatingSchedule
        ) {
          setError("");
          setStatus("STARTING");
        } else {
          setActiveStep(1);
          setStatus(newStatus);
          stopPolling();
          setError(
            "There has been an error creating the schedule. We are working hard to fix it and will notify you when you can create a schedule."
          );
          setDisableCreateSchedule(true);
          creatingScheduleVar(false);
        }
      } else if (
        mostRecent.isUnderReview === false &&
        mostRecent.isPublished === false &&
        newStatus === "COMPLETE"
      ) {
        setActiveStep(2);
        creatingScheduleVar(false);
        setDisableCreateSchedule(true);
        stopPolling();
        setDraftReleased(mostRecent.isUnderReview);
        setDraftStartDate(`${mostRecent.start}T12:00:00`);
        setDraftEndDate(`${mostRecent.end}T00:00:00`);
        setDraftSchedulePeriodId(mostRecent.id);
        setError("");
        setStatus(newStatus);
      } else if (mostRecent.isUnderReview && newStatus === "COMPLETE") {
        setActiveStep(4);
        creatingScheduleVar(false);
        setDisableCreateSchedule(true);
        setDraftReleased(mostRecent.isUnderReview);
        setDraftStartDate(`${mostRecent.start}T12:00:00`);
        setDraftEndDate(`${mostRecent.end}T00:00:00`);
        setDraftSchedulePeriodId(mostRecent.id);
        stopPolling();
        setError("");
        setStatus(newStatus);
      } else if (mostRecent.isPublished && newStatus === "COMPLETE") {
        setActiveStep(0);
        creatingScheduleVar(false);
        setDisableCreateSchedule(false);
        stopPolling();
        setError("");
        setStatus(newStatus);
      } else if (newStatus === "COMPLETE") {
        if (
          (newStatus === status || status === "STARTING") &&
          creatingSchedule
        ) {
          setError("");
          setStatus("STARTING");
        }
      } else if (newStatus === "RESCHEDULING") {
        setError("");
        setStatus(newStatus);
        setActiveStep(2);
        creatingScheduleVar(false);
        setDisableCreateSchedule(true);
        // stopPolling();
        setDraftReleased(mostRecent.isUnderReview);
        setDraftStartDate(`${mostRecent.start}T12:00:00`);
        setDraftEndDate(`${mostRecent.end}T00:00:00`);
        setDraftSchedulePeriodId(mostRecent.id);
      } else if (newStatus !== "COMPLETE" && newStatus !== "ERROR") {
        creatingScheduleVar(true);
        setActiveStep(1);
        setDisableCreateSchedule(true);
        startPolling(5000);
        setError("");
        setStatus(newStatus);
      }

      setShowPopupForStartDate(false);
      setFirstSchedule(false);
    } else if (data && data.schedulePeriods.length === 0) {
      if (!creatingSchedule && !initialScheduleStartDate) {
        setShowPopupForStartDate(true);
        setFirstSchedule(true);
      }
    }
  }, [data]);

  useEffect(() => {
    startPolling(5000);
  }, []);

  let steps = [];
  if (reviewUntil) {
    steps = [
      "Notify Employees to Update Preferences",
      "Create",
      "Management Review",
      "Release for Staff Review",
      "Review Window",
      "Publish",
    ];
  } else {
    steps = [
      "Notify Employees to Update Preferences",
      "Create",
      "Management Review",
      "Publish",
    ];
  }

  const openDialog = () => {
    setPublish(true);
  };

  const handleCancel = () => {
    setPublish(false);
  };

  const handleReleaseDraft = () => {
    releaseDraftSchedule({
      variables: {
        schedulePeriodId: draftSchedulePeriodId,
      },
    });
  };

  const handlePublish = () => {
    setDraftStartDate("");
    setDraftEndDate("");
    publishSchedule({
      variables: {
        schedulePeriodId: draftSchedulePeriodId,
        approver: parseInt(currentUser.id),
      },
    });
  };

  const generateSchedule = () => {
    const dateString = format(initialStartDate, "yyyy-MM-dd");
    //  + initialStartTime;
    // const newScheduleDate = new Date(dateString).toISOString();
    const newStartDate = Boolean(newScheduleStartDateVar() || firstSchedule);

    //const url = "https://4zyqpo8d4d.execute-api.us-east-2.amazonaws.com/triggerAlgorithm";
    const demourl =
      "https://nah5sgak5h.execute-api.us-east-2.amazonaws.com/run-schedule";
    const testUrl =
      "https://0evbdhtqeh.execute-api.us-east-2.amazonaws.com/run-schedule";

    const environment = process.env.NODE_ENV;
    const baseURL = window.location.origin;
    let backend = "";
    if (environment === "development" || baseURL.includes("amplify")) {
      backend = "development";
    } else {
      backend = "production";
    }

    axios({
      method: "post",
      url: backend === "development" ? testUrl : demourl,
      data: {
        office: currentUser.office.id,
        environment: backend,
        start: newStartDate ? dateString : null,
      },
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(function () {
        creatingScheduleVar(true);
        setStatus("STARTING");
        startPolling(5000);
      })
      .catch(function (error) {
        console.error(error);
      });
  };

  const getContent = (index) => {
    return (
      <Steps
        index={index}
        reviewUntil={reviewUntil}
        preferencesDue={preferencesDue}
        draftStartDate={draftStartDate}
        draftEndDate={draftEndDate}
        pendingTimeOffRequests={pendingTimeOffRequests}
        draftReleased={draftReleased}
        generateSchedule={generateSchedule}
        handleReleaseDraft={handleReleaseDraft}
        disableCreateSchedule={disableCreateSchedule}
        error={error}
        status={status}
      />
    );
  };

  const handleNext = () => {
    const step = activeStep;
    setActiveStep(step + 1);
  };

  const handleBack = () => {
    const step = activeStep;
    setActiveStep(step - 1);
  };

  const checkNextDisabled = () => {
    if (reviewUntil) {
      if (activeStep === 1 && !draftStartDate && !draftEndDate) {
        return true;
      } else if (creatingSchedule) {
        return true;
      } else if (activeStep === 3 && !draftReleased) {
        return true;
      } else {
        return false;
      }
    } else {
      if (activeStep === 1 && !draftStartDate && !draftEndDate) {
        return true;
      } else if (creatingSchedule) {
        return true;
      } else {
        return false;
      }
    }
  };

  const handleDateChange = (date) => {
    if (date && !isNaN(date.getTime())) {
      if (date < sub(new Date(), { days: 1 })) {
        setInitialStartDate(date);
      } else {
        setInitialStartDate(date);
        setDateError("");
      }
    } else {
      setDateError("Invalid date");
    }
  };

  return (
    <>
      <Typography variant="h5">{currentUser.office.name}</Typography>
      {nextScheduleStart &&
        nextScheduleEnd &&
        !disableCreateSchedule &&
        !creatingSchedule && (
          <Grid
            container
            alignItems="center"
            spacing={2}
            style={{ marginTop: 20 }}
          >
            <Grid item>
              <Typography variant="h6">
                Next Schedule to Create:
                {" " + format(nextScheduleStart, "dd MMM yyyy") + " - "}
                {format(nextScheduleEnd, "dd MMM yyyy")}
              </Typography>
            </Grid>
            <Grid item>
              <Button
                color="primary"
                variant="outlined"
                onClick={() => setShowPopupForStartDate(true)}
                disabled={
                  creatingSchedule || draftReleased || disableCreateSchedule
                }
              >
                Edit Dates
              </Button>
            </Grid>
          </Grid>
        )}
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.length > 0 &&
          steps.map((label, index) => (
            <Step key={label}>
              <StepLabel>
                <Typography variant="h5">{label}</Typography>
              </StepLabel>
              <StepContent>
                {getContent(index)}
                <Grid
                  container
                  spacing={2}
                  style={{ marginTop: 8, marginBottom: 8 }}
                >
                  <Grid item>
                    <Button
                      color="primary"
                      disabled={activeStep === 0 || creatingSchedule === true}
                      onClick={handleBack}
                      className={classes.smallButton}
                    >
                      Previous
                    </Button>
                  </Grid>
                  <Grid item>
                    {activeStep === steps.length - 1 ? (
                      <Button
                        color="primary"
                        variant="contained"
                        onClick={openDialog}
                        className={classes.smallButton}
                        disabled={
                          creatingSchedule === true ||
                          (!draftStartDate && !draftEndDate)
                        }
                      >
                        Publish
                      </Button>
                    ) : (
                      <Button
                        color="primary"
                        variant="outlined"
                        onClick={handleNext}
                        className={classes.smallButton}
                        disabled={checkNextDisabled()}
                      >
                        Next
                      </Button>
                    )}
                  </Grid>
                </Grid>
              </StepContent>
            </Step>
          ))}
      </Stepper>
      {((activeStep === 6 && reviewUntil) ||
        (activeStep === 4 && !reviewUntil)) && (
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            setActiveStep(0);
            setDisableCreateSchedule(false);
          }}
        >
          Create Next Schedule
        </Button>
      )}
      <Dialog open={publish} onClose={handleCancel} style={{ padding: 30 }}>
        <DialogContent>
          <Typography variant="h5">
            Are you sure you want to publish schedule?
          </Typography>
          <DialogContentText style={{ marginTop: 16 }}>
            This action cannot be undone.
          </DialogContentText>
        </DialogContent>
        <DialogActions style={{ padding: 30 }}>
          <Button
            onClick={handleCancel}
            color="primary"
            variant="outlined"
            className={classes.otherButton}
          >
            Cancel
          </Button>
          <Button
            onClick={handlePublish}
            color="primary"
            variant="contained"
            className={classes.otherButton}
            style={{ marginLeft: 16 }}
          >
            Publish
          </Button>
        </DialogActions>
      </Dialog>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <Dialog
          open={showPopupForStartDate}
          onClose={() => setShowPopupForStartDate(false)}
          fullWidth
          maxWidth="xs"
        >
          <DialogContent style={{ padding: 30 }}>
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <Typography variant="h3">Schedule Start Date</Typography>
              </Grid>
              <Grid item>
                {firstSchedule ? (
                  <Typography variant="h5">
                    To get started creating your first schedule, enter the start
                    date.
                  </Typography>
                ) : (
                  <Typography variant="h5">
                    Edit the next schedule start date.
                  </Typography>
                )}
              </Grid>
              <Grid item>
                <InputLabel htmlFor="date">
                  <Typography variant="h5">Date:</Typography>
                </InputLabel>
                <KeyboardDatePicker
                  disableToolbar
                  autoOk
                  variant="inline"
                  inputVariant="outlined"
                  format="MM/dd/yyyy"
                  id="date"
                  minDate={new Date()}
                  minDateMessage="Date should not be before today"
                  value={initialStartDate}
                  onChange={handleDateChange}
                  className={classes.input}
                />
                {dateError && (
                  <Typography variant="body2" className={classes.dateError}>
                    {dateError}
                  </Typography>
                )}
              </Grid>
              <Grid container justify="flex-end" spacing={2}>
                {!firstSchedule && (
                  <Grid item>
                    <Button
                      onClick={() => {
                        setShowPopupForStartDate(false);
                        newScheduleStartDateVar(false);
                      }}
                      color="primary"
                      variant="outlined"
                      className={classes.otherButton}
                    >
                      Cancel
                    </Button>
                  </Grid>
                )}
                <Grid item>
                  <Button
                    onClick={() => {
                      setShowPopupForStartDate(false);
                      initialScheduleStartDateVar(initialStartDate);
                      newScheduleStartDateVar(true);
                    }}
                    color="primary"
                    variant="contained"
                    className={classes.otherButton}
                  >
                    Save
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
        </Dialog>
      </MuiPickersUtilsProvider>
    </>
  );
};

export default Actions;
