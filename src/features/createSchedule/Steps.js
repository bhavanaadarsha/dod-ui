import React from 'react';
import { 
    Typography, Grid, Button, 
    Box, makeStyles, LinearProgress, Fade 
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';
import { creatingScheduleVar } from '../../cache';

const useStyles = makeStyles((theme) => ({
    button: {
        width: 156
    },
    error: {
        color: theme.palette.primary.main
    }
}));

const Steps = (props) => {
    const {
        index, 
        reviewUntil, 
        preferencesDue, 
        draftStartDate, 
        draftEndDate,
        pendingTimeOffRequests,
        draftReleased,
        generateSchedule,
        handleReleaseDraft,
        disableCreateSchedule,
        error,
        status
    } = props;

    const classes = useStyles();

    const creatingSchedule = creatingScheduleVar();

    const mapStatusToValue = {
        'STARTING': 10,
        'READING': 25,
        'GENERATING': 50,
        'OPTIMIZING': 75,
    };

    const mappedToExplanations = {
        'STARTING': 'starting the algorithm',
        'READING': 'reading employee preferences and unit constraints',
        'GENERATING': 'generating the schedule',
        'OPTIMIZING': 'optimizing the schedule',
    };

    const getContentReviewWindowTrue = () => {
        switch (index) {
            case 0:
                return (
                    <Grid container direction='column' spacing={2}>
                        {preferencesDue &&
                            <>
                                <Grid item>
                                    <Typography>
                                        Preferences due: {format(preferencesDue, 'dd MMM yyyy')}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    {preferencesDue > new Date() 
                                        ? <Typography>
                                            All employees will be notified to update their preferences.
                                        </Typography>
                                        : <Typography>
                                            All employees have been notified to update their preferences.
                                        </Typography>
                                    }
                                </Grid>
                            </>
                        }
                        <Grid item>
                            <Typography>
                                Outstanding Time Off Requests: {pendingTimeOffRequests.length}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Button
                                variant='contained'
                                color='primary'
                                component={Link}
                                to='/Landing/time-off'
                                className={classes.button}
                            >
                                Time Off Requests
                            </Button>
                        </Grid>
                    </Grid>
                );
            case 1:
                return (
                    <Grid container direction='column' spacing={2}>
                        <Grid item>
                            <Button
                                variant='contained'
                                onClick={generateSchedule}
                                color='primary'
                                disabled={creatingSchedule === true || disableCreateSchedule}
                                style={{ marginTop: 8 }}
                                className={classes.button}
                            >
                                Create Schedule
                            </Button>
                        </Grid>
                        {
                            creatingSchedule && status !== 'ERROR'
                                ? <>
                                    <Grid item style={{ marginTop: '10px' }}>
                                        <Typography>Creating Schedule</Typography> 
                                    </Grid>
                                    <Grid item>
                                        <LinearProgress 
                                            variant='determinate' 
                                            value={mapStatusToValue[status] ? mapStatusToValue[status] : 0} 
                                            color='primary' 
                                        />
                                    </Grid>
                                    <Grid item>
                                        <Typography>
                                            Right now we are {mappedToExplanations[status] ? mappedToExplanations[status] : 'gathering data'}.
                                        </Typography>
                                    </Grid>
                                    <Grid item style={{width: 450}}>
                                        <Typography>
                                            You can continue working while the schedule is being generated and you will be notified when it finishes.
                                        </Typography>
                                    </Grid>
                                </> 
                                : null
                        }
                        {error &&
                            <Grid item>
                                <Typography className={classes.error}>{error}</Typography>
                            </Grid>
                        }
                    </Grid>
                );
            case 2:
                if (draftStartDate && draftEndDate) {
                    return (
                        <Grid container direction='column' spacing={2}>
                            <Grid item>
                                <Typography>
                                    A schedule has been created for 
                                    <b>
                                        {format(new Date(draftStartDate), ' dd MMM yyyy')} - 
                                        {format(new Date(draftEndDate), ' dd MMM yyyy')}
                                    </b>.
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography>
                                    View the draft schedule and make updates.
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    component={Link}
                                    to={`/Landing/${draftStartDate}/draft`}
                                    className={classes.button}
                                >
                                    View Schedule
                                </Button>
                            </Grid>
                        </Grid>
                    );
                } else {
                    return (
                        <Typography>No draft schedule to review at this time.</Typography>
                    );
                }
            case 3:
                if (!draftReleased && draftStartDate && draftEndDate) {
                    return (
                        <Grid container direction='column' spacing={2}>
                            <Grid item>
                                <Typography style={{width: 400}}>
                                    To release the draft schedule for your staff to review, click the Release Draft button below.
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={handleReleaseDraft}
                                    className={classes.button}
                                >
                                    Release Draft
                                </Button>
                            </Grid>
                        </Grid>
                    );
                } else {
                    return <Typography>
                        {draftReleased
                            ? 'You have already released the draft schedule.'
                            : 'No draft schedule to release at this time.'
                        }
                    </Typography>;
                }
            case 4:
                if (draftReleased && draftStartDate && draftEndDate) {
                    return (
                        <Grid container direction='column' spacing={2}>
                            <Grid item>
                                <Typography>
                                    The schedule for
                                    <b>{format(new Date(draftStartDate), ' dd MMM yyyy')} - 
                                        {format(new Date(draftEndDate), ' dd MMM yyyy')}
                                    </b> will be open for review until
                                    <b>{format(reviewUntil, ' dd MMM yyyy')}</b>.
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    component={Link}
                                    to={`/Landing/${draftStartDate}/draft`}
                                    className={classes.button}
                                >
                                    View Schedule
                                </Button>
                            </Grid>
                        </Grid>
                    );
                } else {
                    return <Typography>No draft schedule to review at this time.</Typography>;
                }
            case 5:
                if (draftStartDate && draftEndDate) {
                    return (
                        <Typography>
                            Schedule to publish: 
                            <b>
                                {format(new Date(draftStartDate), ' dd MMM yyyy')} - 
                                {format(new Date(draftEndDate), ' dd MMM yyyy')}
                            </b>
                        </Typography>
                    );
                } else {
                    return <Typography>No schedule to publish at this time.</Typography>;
                }
        }
    };

    const getContentReviewWindowFalse = () => {
        switch (index) {
            case 0:
                return (
                    <Grid container direction='column' spacing={2}>
                        {preferencesDue &&
                            <>
                                <Grid item>
                                    <Typography>
                                        Preferences due: {format(preferencesDue, 'dd MMM yyyy')}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    {preferencesDue > new Date() 
                                        ? <Typography>
                                            All employees will be notified to update their preferences.
                                        </Typography>
                                        : <Typography>
                                            All employees have been notified to update their preferences.
                                        </Typography>
                                    }
                                </Grid>
                            </>
                        }
                        <Grid item>
                            <Typography>
                                Outstanding Time Off Requests: {pendingTimeOffRequests.length}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Button
                                variant='contained'
                                color='primary'
                                component={Link}
                                to='/Landing/time-off'
                                className={classes.button}
                            >
                                Time Off Requests
                            </Button>
                        </Grid>
                    </Grid>
                );
            case 1:
                return (
                    <Grid container direction='column' spacing={2}>
                        <Grid item>
                            <Button
                                variant='contained'
                                onClick={generateSchedule}
                                color='primary'
                                disabled={creatingSchedule === true || disableCreateSchedule}
                                style={{ marginTop: 8 }}
                                className={classes.button}
                            >
                                Create Schedule
                            </Button>
                        </Grid>
                        {
                            creatingSchedule && status !== 'ERROR'
                                ? <>
                                    <Grid item style={{ marginTop: '10px' }}>
                                        <Typography>Creating Schedule</Typography> 
                                    </Grid>
                                    <Grid item>
                                        <LinearProgress variant='determinate' value={mapStatusToValue[status]} color='primary' />
                                    </Grid>
                                    <Grid item>
                                        <Typography>Right now we are {mappedToExplanations[status]}.</Typography>
                                    </Grid>
                                    <Grid item style={{width: 450}}>
                                        <Typography>
                                            You can continue working while the schedule is being generated and you will be notified when it finishes.
                                        </Typography>
                                    </Grid>
                                </> 
                                : null
                        }
                        {error &&
                            <Grid item>
                                <Typography className={classes.error}>{error}</Typography>
                            </Grid>
                        }
                    </Grid>
                );
            case 2:
                if (draftStartDate && draftEndDate) {
                    return (
                        <Grid container direction='column' spacing={2}>
                            <Grid item>
                                <Typography>
                                    A schedule has been created for 
                                    <b>
                                        {format(new Date(draftStartDate), ' dd MMM yyyy')} - 
                                        {format(new Date(draftEndDate), ' dd MMM yyyy')}
                                    </b>.
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography>
                                    View the draft schedule and make updates.
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    component={Link}
                                    to={`/Landing/${draftStartDate}/draft`}
                                    className={classes.button}
                                >
                                    View Schedule
                                </Button>
                            </Grid>
                        </Grid>
                    );
                } else {
                    return (
                        <Typography>No draft schedule to review at this time.</Typography>
                    );
                }
            case 3:
                if (draftStartDate && draftEndDate) {
                    return (
                        <Typography>
                            Schedule to publish: 
                            <b>
                                {format(new Date(draftStartDate), ' dd MMM yyyy')} - 
                                {format(new Date(draftEndDate), ' dd MMM yyyy')}
                            </b>
                        </Typography>
                    );
                } else {
                    return <Typography>No schedule to publish at this time.</Typography>;
                }
        }
    };

    if (reviewUntil) {
        return getContentReviewWindowTrue();
    } else {
        return getContentReviewWindowFalse();
    }
};
 
export default Steps;