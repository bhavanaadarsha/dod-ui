import React, {useState} from 'react';
import { 
    Typography, 
    Grid, 
    FormControlLabel, 
    TextField, 
    InputLabel, 
    InputAdornment, 
    Button, 
    FormLabel, 
    RadioGroup, 
    Radio, 
    Icon, 
    makeStyles, 
    Switch,
    Snackbar
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import  mobiscroll from '@mobiscroll/react';
import '@mobiscroll/react/dist/css/mobiscroll.react.min.css';

const useStyles = makeStyles((theme) => ({
    numberInput: {
        width: 40
    },
    button: {
        width: 94
    },
    helpText: {
        color: theme.palette.text.hint
    }
}));

const Settings = () => {
    const classes = useStyles();

    const [schedulePeriod, setSchedulePeriod] = useState(scheduleSettings.schedulePeriod);
    const [schedulePeriodTimeUnit, setSchedulePeriodTimeUnit] = useState(scheduleSettings.schedulePeriodTimeUnit);
    const [initialScheduleStartDate, setInitialScheduleStartDate] = useState(scheduleSettings.initialScheduleStartDate);
    const [createScheduleInAdvance, setCreateScheduleInAdvance] = useState(scheduleSettings.createScheduleInAdvance);
    const [allowForReview, setAllowForReview] = useState(scheduleSettings.allowForReview);
    const [reviewWindow, setReviewWindow] = useState(scheduleSettings.reviewWindow);
    const [updated, setUpdated] = useState(false);
    const [toast, setToast] = useState('');
    const [showToast, setShowToast] = useState(false);
    
    const handleSubmit = () => {
        const object = {
            schedulePeriod,
            schedulePeriodTimeUnit,
            initialScheduleStartDate,
            createScheduleInAdvance,
            allowForReview,
            reviewWindow
        };
        setToast('Save');
        setShowToast(true);
        setUpdated(false);
    };

    const handleCancel = () => {
        setUpdated(false);
        setSchedulePeriod(scheduleSettings.schedulePeriod);
        setSchedulePeriodTimeUnit(scheduleSettings.schedulePeriodTimeUnit);
        setInitialScheduleStartDate(scheduleSettings.initialScheduleStartDate);
        setCreateScheduleInAdvance(scheduleSettings.createScheduleInAdvance);
        setAllowForReview(scheduleSettings.allowForReview);
        setReviewWindow(scheduleSettings.reviewWindow);
        setToast('Cancel');
        setShowToast(true);
    };

    const handleChange = (value, setter) => {
        setUpdated(true);
        setter(value);
    };

    return ( 
        <Grid container direction='column' spacing={4} style={{marginTop: 20}}>
            <Grid item container spacing={2} alignItems='center'>
                <Grid item xs={8}>
                    <InputLabel htmlFor='dateForRequest'>
                        <Typography>Initial Schedule Start Date:</Typography>
                    </InputLabel>         
                    <mobiscroll.Calendar 
                        value={initialScheduleStartDate} 
                        display='bubble' 
                        onClose={e => handleChange(new Date(e.valueText), setInitialScheduleStartDate)}
                    >
                        <TextField 
                            id='dateForRequest'
                            name='dateForRequest'
                            variant='outlined'
                            InputProps={{
                                endAdornment: <InputAdornment position='end' style={{marginRight: 5}}><Icon>today</Icon></InputAdornment>
                            }}
                        />
                    </mobiscroll.Calendar>
                </Grid>
                <Grid item container justify='flex-end' xs={4} spacing={2} style={{marginTop: -280, paddingTop: 0}}>
                    <Grid item>
                        <Button variant='outlined' color='primary' onClick={handleCancel} disabled={!updated} className={classes.button}>Cancel</Button>
                    </Grid>
                    <Grid item>
                        <Button variant='contained' color='primary' onClick={handleSubmit} disabled={!updated} className={classes.button}>Save</Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item>
                <FormLabel>Schedule Period:</FormLabel>
                <Grid container spacing={2} alignItems='center'> 
                    <Grid item >
                        <TextField 
                            id='schedulePeriod'
                            name='schedulePeriod'
                            variant='outlined'
                            type='number'
                            value={schedulePeriod}
                            className={classes.numberInput}
                            onChange={e => handleChange(parseInt(e.target.value), setSchedulePeriod)}
                        />
                    </Grid>
                    <Grid item>
                        <RadioGroup 
                            value={schedulePeriodTimeUnit}
                            onChange={e => handleChange(e.target.value, setSchedulePeriodTimeUnit)}
                            row
                        >
                            <FormControlLabel value={'weeks'} label="Weeks" control={<Radio />} />
                            <FormControlLabel value={'months'} label="Months" control={<Radio />} />
                        </RadioGroup>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item>
                <FormControlLabel
                    control={
                        <Switch
                            checked={allowForReview}
                            onChange={e => handleChange(e.target.checked, setAllowForReview)}
                            name="allowForReview"
                            color={allowForReview ? 'primary' : 'secondary'}
                        />
                    }
                    labelPlacement="start"
                    label="Allow schedule review period by staff"
                    style={{marginLeft: 0, color: 'rgba(0, 0, 0, 0.54)'}}
                >
                </FormControlLabel>
            </Grid>
            <Grid item container direction='column'>
                <Grid item>
                    <FormLabel>Review Window:</FormLabel>
                </Grid>
                <Grid item container spacing={1} alignItems='center'>
                    <Grid item>
                        <TextField 
                            id='reviewWindow'
                            name='reviewWindow'
                            variant='outlined'
                            type='number'
                            value={reviewWindow}
                            className={classes.numberInput}
                            onChange={e => handleChange(parseInt(e.target.value), setReviewWindow)}
                            disabled={!allowForReview}
                        />
                    </Grid>
                    <Grid item>
                        <Typography>Days</Typography>
                    </Grid>
                </Grid>
                <Grid item>
                    <Typography variant='body2' className={classes.helpText}>For Notification Purposes Only</Typography>
                </Grid>
            </Grid>
            <Grid item container direction='column'>
                <Grid item>
                    <FormLabel>Publish Schedule:</FormLabel>
                </Grid>
                <Grid item container spacing={1} alignItems='center'>
                    <Grid item>
                        <TextField 
                            id='createSchedule'
                            name='createSchedule'
                            variant='outlined'
                            type='number'
                            value={createScheduleInAdvance}
                            className={classes.numberInput}
                            onChange={e => handleChange(parseInt(e.target.value), setCreateScheduleInAdvance)}
                        />
                    </Grid>
                    <Grid item>
                        <Typography>Weeks Prior to Schedule Start</Typography>
                    </Grid>
                </Grid>
            </Grid>
            <Snackbar open={showToast} autoHideDuration={3000} onClose={() => setShowToast(false)} anchorOrigin={{vertical: 'top', horizontal: 'center'}}>
                <MuiAlert elevation={6} onClose={() => setShowToast(false)} severity="success">
                    {toast === 'Save' 
                        ? <Typography>Settings saved!</Typography>
                        : <Typography>Changes cancelled!</Typography>
                    }
                </MuiAlert>
            </Snackbar>
        </Grid>
    );
};
 
export default Settings;