import React, { useState } from "react";
import {
  Grid,
  Typography,
  Button,
  makeStyles,
  Box,
  Snackbar,
  LinearProgress,
  withStyles,
  TextField,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  InputAdornment,
  IconButton,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import CSVReader from "react-csv-reader";
import { CSVLink } from "react-csv";
import {
  ADD_EMPLOYEE_SKILLS,
  FIND_SKILL_TYPE,
  EMPLOYEE_TYPES,
  ALL_OFFICES,
  ALL_SHIFT_TYPES,
  Get_Employees,
  JOB_TYPE_ROLE,
  BATCH_ADD_EMPLOYEE,
  BATCH_CREATE_EMPLOYEE_ASSIGNMENT,
  BATCH_CREATE_HIRED_SHIFTS,
  Check_Email_Exists,
} from "../../api/gqlQueries";
import {
  addEmpDetails,
  handlefileValdation,
} from "../../helpers/updateUserDetails";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import CalendarIcon from "@material-ui/icons/CalendarTodayRounded";
import { useMutation, useQuery, useLazyQuery } from "@apollo/client";
import { useHistory } from "react-router-dom";
import { format } from "date-fns";
import { Formik, Form, FieldArray } from "formik";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";

const SUPPORTED_FORMATS = [
  "application/vnd.ms-excel",
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
];

const useStyles = makeStyles(() => ({
  root: {
    margin: 0,
    padding: 0,
  },
  table: {
    border: "none",
    boxSizing: "none",
    boxShadow: "none",
    borderRadius: "none",
    width: "100%",
  },
  deleteButton: {
    color: "#333333",
  },
  headerSpacing: {
    marginTop: 30,
    marginBottom: 20,
  },
}));

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "#EAEAEA",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRottom: "0",
    border: "none",
    textAlign: "center",
  },
  body: {
    fontSize: 14,
    backgroundColor: "#ffffff",
    paddingTop: "5px",
    paddingBottom: "0",
    paddingRottom: "0",
    border: "none",
    textAlign: "center",
  },
}))(TableCell);

const ImportEmployees = () => {
  const history = useHistory();
  const classes = useStyles();
  const [values, SetValues] = useState({});
  const [fileLoaded, setFileLoaded] = useState(false);
  const [fileFormatError, setFileFormatError] = useState(false);
  const [snackbaropen, setsnackbaropen] = useState(false);
  const [errorsnackbaropen, seterrorsnackbaropen] = useState(false);
  const [snackBarMessage, setsnackBarMessage] = useState(false);
  const [showLoader, setShowLoader] = useState(false);
  const [addedEmps, SetAddedEmps] = useState([]);
  const [errMsg, SetErrMsg] = useState(false);

  const alloffices = useQuery(ALL_OFFICES, {
    onError(error) {
      console.error(error);
    },
  });
  const allEmployeeTypes = useQuery(EMPLOYEE_TYPES, {
    onError(error) {
      console.error(error);
    },
  });
  const allShiftTypes = useQuery(ALL_SHIFT_TYPES, {
    onError(error) {
      console.error(error);
    },
  });
  const allskilltypes = useQuery(FIND_SKILL_TYPE, {
    onError(error) {
      console.error(error);
    },
  });
  const jobtyperoles = useQuery(JOB_TYPE_ROLE, {
    onError(error) {
      console.error(error);
    },
  });
  const { data, loading, refetch } = useQuery(Get_Employees, {
    onError(error) {
      console.error(error);
    },
  });

  const jobType =
    !allskilltypes.loading &&
    allskilltypes.data.skills.filter((e) => e.variety === "JOB_TYPE");

  const [batchCreateEmployeeAssignment] = useMutation(
    BATCH_CREATE_EMPLOYEE_ASSIGNMENT,
    {
      onCompleted(data) {
        data.batchCreateAssignment.assignments.map((e) => {
          const emp = values.find((a) => a.Email === e.employee.email);
          const assignmentId = e.id;
          const officeid =
            !alloffices.loading &&
            alloffices.data.offices.find((i) => i.name === emp.Department).id;

          let currEmpShifts = [];
          if (emp.ShiftType != "" || emp.ShiftType != "Any") {
            currEmpShifts = emp.ShiftType.split(",");
            currEmpShifts.map((x) => {
              let shiftTypes = allShiftTypes.data.shiftDefinitions.filter(
                (e) => e.description === x && e.office.id === officeid
              );
              if (shiftTypes.length > 0) {
                let shiftTypeData = shiftTypes.map((s) => {
                  return {
                    assignment: assignmentId,
                    shiftDefinition: s.id,
                  };
                });
                batchCreateHiredShifts({ variables: { input: shiftTypeData } });
              }
            });
          }
        });
        refetch();
      },
      onError(error) {
        console.error(error);
      },
    }
  );

  const [batchCreateEmployee] = useMutation(BATCH_ADD_EMPLOYEE, {
    onCompleted(data) {
      const assignmentData = data.batchCreateEmployee.employees.map((e) => {
        const emp = values.find((a) => a.Email === e.email);
        const officeid =
          !alloffices.loading &&
          alloffices.data.offices.find((i) => i.name === emp.Department).id;
        const roleid =
          !jobtyperoles.loading &&
          jobtyperoles.data.jobTypeRoles.find(
            (i) => i.jobType.name === emp.JobType
          ).role.id;
        const employeetypeid =
          !allEmployeeTypes.loading &&
          allEmployeeTypes.data.employeeTypes.find(
            (i) => i.name === emp.EmploymentType
          ).id;
        const jobtype = jobType.find(
          (i) => i.name === emp.JobType && i.office.name === emp.Department
        );
        //create employee skillset
        addEmployeeSkills({
          variables: {
            input: {
              employee: parseInt(e.id),
              skill: parseInt(jobtype.id),
            },
          },
        });
        return {
          employee: parseInt(e.id),
          office: parseInt(officeid),
          primary: true,
          role: parseInt(roleid),
          employeeType: parseInt(employeetypeid),
          startDate: format(new Date(emp.AssignmentStartDate), "yyyy-MM-dd"),
        };
      });
      batchCreateEmployeeAssignment({ variables: { input: assignmentData } });
      refetch();
    },
    onError(error) {
      console.error(error);
    },
  });

  const [batchCreateHiredShifts] = useMutation(BATCH_CREATE_HIRED_SHIFTS, {
    onCompleted() {
      refetch();
    },
    onError(error) {
      console.error(error);
    },
  });

  const [addEmployeeSkills] = useMutation(ADD_EMPLOYEE_SKILLS, {
    onCompleted(data) {
      refetch();
    },
    onError(error) {
      console.error(error);
    },
  });

  const [emailExist] = useLazyQuery(Check_Email_Exists, {
    onCompleted(data) {
      if (data.emailExists != null) {
        SetErrMsg(true);
        setsnackBarMessage(
          "Employee with the same email exists. Please try with another email"
        );
        seterrorsnackbaropen(true);
        setFileLoaded(false);
      }
    },
    onError(error) {
      console.error(error);
    },
  });

  const handleFileRead = (data, fileInfo) => {
    if (SUPPORTED_FORMATS.includes(fileInfo.type)) {
      setFileFormatError(false);
      for (let i = 0; i < data.length; i++) {
        emailExist({
          variables: {
            email: data[i].Email,
          },
        });
      }
      if (handlefileValdation(data, alloffices, jobType).length > 0) {
        setsnackBarMessage(handlefileValdation(data, alloffices, jobType));
        seterrorsnackbaropen(true);
        setFileLoaded(false);
        SetErrMsg(true);
      } else {
        SetValues(data);
        setFileLoaded(true);
      }
    } else {
      setFileFormatError(true);
    }
  };

  const papaparseOptions = {
    header: true,
    dynamicTyping: true,
    skipEmptyLines: true,
  };

  const handleImportEmployees = (vals) => {
    SetValues(vals.rows);
    if (fileLoaded) {
      setShowLoader(true);
      addEmpDetails(vals.rows, batchCreateEmployee);
      if (addedEmps.length <= vals.rows.length) {
        refetch();
        setsnackBarMessage("Employees successfully added!");
        setsnackbaropen(true);
      } else if (addedEmps.length <= 0) {
        setsnackBarMessage("Something went wrong, please try again.");
        seterrorsnackbaropen(true);
      }
      refetch();
    }
  };

  const headerData = [
    [
      "Name",
      "Email",
      "Phone",
      "JobType",
      "ShiftType",
      "EmploymentType",
      "Department",
      "AssignmentStartDate",
    ],
  ];

  return (
    <Grid
      container
      direction="row"
      className={classes.headerSpacing}
      spacing={4}
    >
      <Snackbar
        open={snackbaropen}
        autoHideDuration={3000}
        onClose={() => {
          setsnackbaropen(false);
          setShowLoader(false);
          refetch();
        }}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <MuiAlert
          onClose={() => {
            setsnackbaropen(false);
            setsnackBarMessage("");
          }}
          severity="success"
        >
          {snackBarMessage}
        </MuiAlert>
      </Snackbar>
      <Snackbar
        open={errorsnackbaropen}
        autoHideDuration={6000}
        onClose={() => {
          seterrorsnackbaropen(false);
        }}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <MuiAlert onClose={() => seterrorsnackbaropen(false)} severity="error">
          {snackBarMessage}
        </MuiAlert>
      </Snackbar>
      <Grid item xs={12}>
        <Typography variant="h3">Import Employees</Typography>
      </Grid>
      <Grid direction="row" container spacing={2}>
        <Grid item xs={6}>
          <Box
            m={2}
            p={2}
            style={{
              border: "1px solid gray",
              width: "100%",
              borderRadius: "5px",
            }}
          >
            <CSVReader
              onFileLoaded={handleFileRead}
              parserOptions={papaparseOptions}
            />
          </Box>

          <Typography color="error" variant="body2">
            {fileFormatError &&
              "File format not supported.Upload a .csv or excel file"}
          </Typography>
        </Grid>
        <Grid item xs={6}></Grid>

        <Grid item xs={3}>
          <Box m={2} p={2}>
            <Button
              color="secondary"
              component={CSVLink}
              data={headerData}
              target="_blank"
              color="primary"
              variant="outlined"
            >
              Download CSV Format
            </Button>
          </Box>
        </Grid>
        {/* <Grid item xs={1}>
          <Box m={2} p={2}>
            <Button onClick={handleClear} color="primary">
              clear
            </Button>
          </Box>
        </Grid> */}
        <Grid item xs={9}>
          {showLoader ? <LinearProgress color="primary" /> : null}
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Formik
          enableReinitialize
          initialValues={{
            rows: values,
          }}
          onSubmit={(values) => {
            handleImportEmployees(values);
          }}
        >
          {({ values, handleChange, setFieldValue }) => (
            <Form>
              <Grid container direction="row" item xs={12} spacing={4}>
                <Grid item xs={12}>
                  <Box m={4} style={{ textAlign: "right" }}>
                    <Button
                      type="submit"
                      color="primary"
                      variant="contained"
                      disabled={errMsg}
                    >
                      Submit
                    </Button>
                  </Box>
                </Grid>

                <Grid item xs={12}>
                  <FieldArray name="rows">
                    {({ push, remove }) => (
                      <Table>
                        <TableHead>
                          <TableRow>
                            <StyledTableCell>First Name</StyledTableCell>
                            <StyledTableCell>Last Name</StyledTableCell>
                            <StyledTableCell>Email</StyledTableCell>
                            <StyledTableCell>Phone</StyledTableCell>
                            <StyledTableCell>Job Type</StyledTableCell>
                            <StyledTableCell>Shift Type</StyledTableCell>
                            <StyledTableCell>Employment Type</StyledTableCell>
                            <StyledTableCell>Department</StyledTableCell>
                            <StyledTableCell>
                              AssignmentStartDate
                            </StyledTableCell>
                            <StyledTableCell>Remove</StyledTableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {values.rows.length > 0 &&
                            values.rows.map((e, index) => (
                              <TableRow key={index}>
                                <StyledTableCell>
                                  <TextField
                                    id={`rows.${index}.Name1`}
                                    name={`rows.${index}.Name`}
                                    value={
                                      e.Name.includes(",")
                                        ? e.Name.split(",")[0]
                                        : e.Name.split(" ")[0]
                                    }
                                    variant="outlined"
                                    onChange={handleChange}
                                  />
                                </StyledTableCell>
                                <StyledTableCell>
                                  <TextField
                                    id={`rows.${index}.Name2`}
                                    name={`rows.${index}.Name`}
                                    value={
                                      e.Name.includes(",")
                                        ? e.Name.split(",")[1]
                                        : e.Name.split(" ")[1]
                                    }
                                    variant="outlined"
                                    onChange={handleChange}
                                  />
                                </StyledTableCell>
                                <StyledTableCell>
                                  <TextField
                                    variant="outlined"
                                    id={`rows.${index}.Email`}
                                    name={`rows.${index}.Email`}
                                    value={e.Email}
                                    variant="outlined"
                                    onChange={handleChange}
                                  />
                                </StyledTableCell>
                                <StyledTableCell>
                                  <TextField
                                    id={`rows.${index}.Phone`}
                                    name={`rows.${index}.Phone`}
                                    value={e.Phone}
                                    variant="outlined"
                                    onChange={handleChange}
                                  />
                                </StyledTableCell>
                                <StyledTableCell>
                                  <TextField
                                    id={`rows.${index}.JobType`}
                                    name={`rows.${index}.JobType`}
                                    value={e.JobType}
                                    variant="outlined"
                                    onChange={handleChange}
                                  />
                                </StyledTableCell>
                                <StyledTableCell>
                                  <TextField
                                    id={`rows.${index}.ShiftType`}
                                    name={`rows.${index}.ShiftType`}
                                    value={e.ShiftType}
                                    variant="outlined"
                                    onChange={handleChange}
                                  />
                                </StyledTableCell>
                                <StyledTableCell>
                                  <TextField
                                    id={`rows.${index}.EmploymentType`}
                                    name={`rows.${index}.EmploymentType`}
                                    value={e.EmploymentType}
                                    variant="outlined"
                                    onChange={handleChange}
                                  />
                                </StyledTableCell>
                                <StyledTableCell>
                                  <TextField
                                    id={`rows.${index}.Department`}
                                    name={`rows.${index}.Department`}
                                    value={e.Department}
                                    variant="outlined"
                                    onChange={handleChange}
                                  />
                                </StyledTableCell>
                                <StyledTableCell>
                                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                    <Grid container justify="space-around">
                                      <DatePicker
                                        variant="inline"
                                        id={`rows.${index}.AssignmentStartDate`}
                                        name={`rows.${index}.AssignmentStartDate`}
                                        value={new Date(e.AssignmentStartDate)}
                                        onChange={(e) => {
                                          setFieldValue(
                                            `rows.${index}.AssignmentStartDate`,
                                            e
                                          );
                                        }}
                                        InputProps={{
                                          endAdornment: (
                                            <InputAdornment position="end">
                                              <IconButton>
                                                <CalendarIcon />
                                              </IconButton>
                                            </InputAdornment>
                                          ),
                                        }}
                                      />
                                    </Grid>
                                  </MuiPickersUtilsProvider>
                                </StyledTableCell>
                                <StyledTableCell>
                                  <IconButton
                                    color="secondary"
                                    onClick={() => {
                                      remove(index);
                                    }}
                                  >
                                    <DeleteIcon />
                                  </IconButton>
                                </StyledTableCell>
                              </TableRow>
                            ))}
                          <TableRow>
                            <StyledTableCell>
                              <Button
                                color="primary"
                                startIcon={<AddIcon />}
                                onClick={() =>
                                  push({
                                    Name: "",
                                    Email: "xyz@example.com",
                                    Phone: "(123)4567890",
                                    JobType: "",
                                    ShiftType: "",
                                    EmploymentType: "Full Time",
                                    Department: "",
                                    AssignmentStartDate: new Date(),
                                  })
                                }
                              >
                                Add Another
                              </Button>
                            </StyledTableCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                    )}
                  </FieldArray>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </Grid>
    </Grid>
  );
};
export default ImportEmployees;
