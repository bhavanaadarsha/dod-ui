import React, { useState } from 'react';
import { 
    Typography, Grid, Select, 
    MenuItem, makeStyles 
} from '@material-ui/core';
import ImportEmployees from './ImportEmployees';
import ImportProcedures from './ImportProcedures';
import ImportTrainings from './ImportTrainings';
import ImportAvailability from './ImportAvailability';
import ImportEmployeeTrainings from './ImportEmployeeTrainings';

const useStyles = makeStyles(() => ({
  headerSpacing: {
    marginTop: 30,
    marginBottom: 20,
  },
  select: {
      width: 200
  }
}));

const ImportData = () => {
    const classes = useStyles();
    const [selectedImport, setSelectedImport] = useState(0);
  
    return (
      <Grid
        container
        direction="row"
        className={classes.headerSpacing}
      >
        <Grid item xs={4}>
          <Typography variant="h3">Import Data</Typography>
        </Grid>
        <Grid item xs={4}>
            <Select
                variant="outlined"
                value={selectedImport}
                onChange={(e) => setSelectedImport(parseInt(e.target.value))}
                className={classes.select}
            >
                <MenuItem key={0} value={0}>
                    Import Employees
                </MenuItem>
                <MenuItem key={1} value={1}>
                    Import Skills
                </MenuItem>
                <MenuItem key={2} value={2}>
                    Import Employee Trainings
                </MenuItem>
                <MenuItem key={3} value={3}>
                    Import Procedures
                </MenuItem>
                <MenuItem key={4} value={4}>
                    Import Availability
                </MenuItem>
            </Select>
        </Grid>
        {selectedImport === 0 && <ImportEmployees />}
        {selectedImport === 1 && <ImportTrainings />}
        {selectedImport === 2 && <ImportEmployeeTrainings />}
        {selectedImport === 3 && <ImportProcedures />}
        {selectedImport === 4 && <ImportAvailability />}
      </Grid>
    );
}
 
export default ImportData;