import React, { useState, useEffect } from "react";
import { add, sub, format, isAfter } from "date-fns";
import {
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  IconButton,
  Grid,
  Popover,
  Button,
  Box,
} from "@material-ui/core";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import EventParticipant from "./EventParticipant";
import { GET_ALL_USERS } from "../../api/gqlQueries";
import { useQuery, useReactiveVar } from "@apollo/client";
import { filterListVar, selectedDateVar, userVar } from "../../cache";
import sun from "../../assets/quizImages/sun.png";
import Brightness3Icon from "@material-ui/icons/Brightness3";
import CheckIcon from "@material-ui/icons/Check";
import PriorityHighIcon from "@material-ui/icons/PriorityHigh";
import clsx from "clsx";

const useStyles = makeStyles(() => ({
  paper: {
    height: 1000,
    backgroundColor: "#F7F7F7",
    overflow: "hidden",
    padding: 10,
  },
  tableCell: {
    verticalAlign: "baseline",
    padding: 16,
  },
  filledCell: {
    backgroundColor: "white",
    borderRight: "1px solid #CCCCCC",
    padding: 5,
    verticalAlign: "baseline",
  },
  filledPersonalBox: {
    backgroundColor: "#F5E1DB",
    borderRadius: 8,
    padding: 10,
    verticalAlign: "baseline",
  },
  filledNonPersonalBox: {
    backgroundColor: "#DFE0E2",
    borderRadius: 8,
    padding: 10,
    verticalAlign: "baseline",
  },
  emptyCell: {
    backgroundColor: "white",
    borderRight: "1px solid #CCCCCC",
    padding: 5,
    verticalAlign: "baseline",
  },
  extraRow: {
    height: 50,
  },
  timeOffNames: {
    marginTop: 5,
    marginBottom: 5,
    maxWidth: 150,
  },
  todayButton: {
    padding: 0,
    fontSize: 16,
    marginTop: -2,
    marginLeft: -16,
  },
}));

const EmpWeekView = React.forwardRef((props, ref) => {
  const classes = useStyles();

  const {
    date,
    // setSelectedDate,
    scheduleEndDate,
    setCurrentSchedule,
    allEvents,
    userEvents,
    userPending,
    userApproved,
    userSoft,
    approvedTimeOff,
    pendingTimeOff,
    shiftNames,
    draftStart,
    draftEnd,
  } = props;

  const user = userVar();

  const filterList = useReactiveVar(filterListVar);
  const allFilters = filterList.allFilters;

  const [startDate, setStartDate] = useState();
  const [arrayOfDates, setArrayOfDates] = useState([]);
  const [weekEvents, setWeekEvents] = useState([]);
  const [weekUserEvents, setWeekUserEvents] = useState([]);
  const [weekPendingTimeOff, setWeekPendingTimeOff] = useState([]);
  const [weekApprovedTimeOff, setWeekApprovedTimeOff] = useState([]);
  const [userApprovedTimeOff, setUserApprovedTimeOff] = useState([]);
  const [userPendingTimeOff, setUserPendingTimeOff] = useState([]);
  const [userSoftTimeOff, setUserSoftTimeOff] = useState([]);
  // const [extraAnchorEl, setExtraAnchorEl] = useState(null);
  const [viewMoreAnchorEl, setViewMoreAnchorEl] = useState(null);
  const [employeesToShow, setEmployeesToShow] = useState([]);
  // const [extraEmployees, setExtraEmployees] = useState([]);
  const [draftSchedule, setDraftSchedule] = useState(false);

  useEffect(() => {
    let selectedDate;

    if (date) {
      selectedDate = new Date(date);
    } else {
      selectedDate = new Date();
    }

    selectedDate.setHours(0, 0, 0, 0);

    const dayNumber = selectedDate.getDay();
    const firstDayOfWeek = sub(selectedDate, { days: dayNumber });

    if (firstDayOfWeek >= draftStart && firstDayOfWeek <= draftEnd) {
      setDraftSchedule(true);
    } else {
      setDraftSchedule(false);
    }

    setStartDate(firstDayOfWeek);
  }, []);

  useEffect(() => {
    const array = [];
    if (startDate) {
      for (let i = 0; i < 7; i++) {
        const day = startDate;
        let newDay = add(day, { days: i });
        newDay = format(newDay, "MM/dd/yyyy");
        array.push(newDay);
      }
    }
    setArrayOfDates(array);
    const lastDate = new Date(array[6]);
    lastDate.setHours(23, 59, 59, 59);
    const weekEvents = allEvents.filter(
      (event) =>
        new Date(event.start) >= startDate && new Date(event.start) <= lastDate
    );
    const weekUserEvents = userEvents.filter(
      (event) =>
        new Date(event.start) >= startDate && new Date(event.start) <= lastDate
    );

    setWeekEvents(weekEvents);
    setWeekUserEvents(weekUserEvents);
  }, [startDate]);

  useEffect(() => {
    const lastDate = new Date(arrayOfDates[6]);
    const weekPendingTimeOff = pendingTimeOff.filter(
      (event) =>
        new Date(event.start) >= startDate && new Date(event.start) <= lastDate
    );
    const weekApprovedTimeOff = approvedTimeOff.filter(
      (event) =>
        new Date(event.start) >= startDate && new Date(event.start) <= lastDate
    );
    const userApprovedTimeOff = userApproved.filter(
      (event) =>
        new Date(event.start) >= startDate && new Date(event.start) <= lastDate
    );
    const userPendingTimeOff = userPending.filter(
      (event) =>
        new Date(event.start) >= startDate && new Date(event.start) <= lastDate
    );
    const userSoftTimeOff = userSoft.filter(
      (event) =>
        new Date(event.start) >= startDate && new Date(event.start) <= lastDate
    );
    setWeekPendingTimeOff(weekPendingTimeOff);
    setWeekApprovedTimeOff(weekApprovedTimeOff);
    setUserApprovedTimeOff(userApprovedTimeOff);
    setUserPendingTimeOff(userPendingTimeOff);
    setUserSoftTimeOff(userSoftTimeOff);
  }, [arrayOfDates]);

  const columns = arrayOfDates.map((date) => {
    return (
      <TableCell
        key={date}
        style={{ backgroundColor: "#F7F7F7", paddingBottom: 8, paddingTop: 8 }}
      >
        <Typography style={{ textAlign: "center", fontSize: 16 }}>
          {format(new Date(date), "EEE")}
        </Typography>
        <Typography style={{ textAlign: "center" }}>
          {format(new Date(date), "dd MMM yyyy")}
        </Typography>
      </TableCell>
    );
  });

  const rows = [];

  const createRow = (shiftEvents, shiftName) => {
    const title = shiftName.substr(0, shiftName.indexOf("-"));
    const cells = arrayOfDates.map((date) => {
      const shiftEvent = shiftEvents.find(
        (event) => format(new Date(event.start), "MM/dd/yyyy") === date
      );
      const shiftEventParticipantIds =
        shiftEvent && shiftEvent.participants
          ? shiftEvent.participants.map((participant) => participant.id)
          : [];
      const personal = shiftEventParticipantIds.includes(user.id);
      if (shiftEvent) {
        return (
          <TableCell
            key={date + `${shiftEvent.id}`}
            className={classes.filledCell}
          >
            <div
              className={
                personal
                  ? classes.filledPersonalBox
                  : classes.filledNonPersonalBox
              }
            >
              <Grid container direction="column" spacing={1}>
                {renderEmployees(shiftEvent.participants)}
              </Grid>
            </div>
          </TableCell>
        );
      } else {
        return (
          <TableCell
            key={date + "noEvent"}
            className={classes.emptyCell}
          ></TableCell>
        );
      }
    });
    return (
      <TableRow key={shiftName}>
        <TableCell className={classes.tableCell}>
          <Grid container alignItems="center" spacing={1}>
            <Grid item>
              {title.includes("a") ? (
                <img
                  alt="day shift"
                  src={sun}
                  style={{ width: 20, marginTop: 3, marginRight: 3 }}
                />
              ) : (
                <Brightness3Icon
                  style={{ width: 20, marginTop: 3, marginRight: 3 }}
                />
              )}
            </Grid>
            <Grid item>
              <Typography variant="subtitle1">{shiftName}</Typography>
            </Grid>
          </Grid>
        </TableCell>
        {cells}
      </TableRow>
    );
  };

  const { loading, error, data } = useQuery(GET_ALL_USERS, {
    variables: {
      id: parseInt(user.office.id),
    },
    onError(error){
      console.error(error);
    }
  });

  // let extraRow;

  // if (!loading && !error) {
  // const users = data.offices[0].employeeSet;

  // const findExtraEmployees = () => {
  //     const weekEventsByDay = {};

  //     weekEvents.forEach(event => {
  //         const date = format(new Date(event.start), 'MM/dd/yyyy');
  //         if (weekEventsByDay[date]) {
  //             weekEventsByDay[date] = [...weekEventsByDay[date], ...event.participants];
  //         } else {
  //             weekEventsByDay[date] = [...event.participants];
  //         }
  //     });
  //     const weekParticipants = [];
  //     arrayOfDates.forEach(date => {
  //         if (weekEventsByDay[date]) {
  //             weekParticipants.push(weekEventsByDay[date]);
  //         } else {
  //             weekParticipants.push(null);
  //         }
  //     });

  //     const extraParticipants = [];
  //     for (let i = 0; i < 7; i++) {
  //         if (weekParticipants[i]) {
  //             const allParticipantsIds = weekParticipants[i].map(participant => participant.id);
  //             const allUserIds = users.map(user => user.id);
  //             const extras = allUserIds.filter(id => !allParticipantsIds.includes(id));
  //             const userObjects = users.filter(user => extras.includes(user.id));
  //             extraParticipants.push(userObjects);
  //         } else {
  //             extraParticipants.push(null);
  //         }
  //     }
  //     return extraParticipants;
  // };

  // const handleExtraClick = (e, participants) => {
  //     setExtraEmployees(participants);
  //     setExtraAnchorEl(e.currentTarget);
  // };

  // const createExtraRow = () => {
  //     if (weekEvents.length !== 0) {
  //         const extraParticipantsPerDay = findExtraEmployees();
  //         const cells = extraParticipantsPerDay.map((participants, index) => {
  //             if (participants) {
  //                 return (
  //                     <TableCell key={index + 'extra'} className={classes.filledCell} >
  //                         <Button onClick={e => handleExtraClick(e, participants)}>
  //                             {participants.length} Employees
  //                         </Button>
  //                     </TableCell>
  //                 );
  //             } else {
  //                 return (
  //                     <TableCell key={index + 'extra'} className={classes.emptyCell}></TableCell>
  //                 );
  //             }
  //         });
  //         const extraCellComponents = [];
  //         if (extraParticipantsPerDay.length < 7) {
  //             const extraCells = 7 - extraParticipantsPerDay.length;
  //             for (let i = 0; i < extraCells; i++) {
  //                 extraCellComponents.push(<TableCell key={i + 'emptyExtra'} className={classes.emptyCell}></TableCell>);
  //             }
  //         }

  //         const allCells = cells.concat(extraCellComponents);

  //         return (
  //             !props.printpreview
  //                 ? <TableRow key='extra' className={classes.extraRow}>
  //                     <TableCell>
  //                         <Typography variant='subtitle1'>Available Employees</Typography>
  //                     </TableCell>
  //                     {allCells}
  //                 </TableRow>
  //                 : null
  //         );
  //     }
  // };
  // extraRow = createExtraRow();
  // }

  const createTimeOffRow = (timeOffEvents) => {
    const cells = arrayOfDates.map((date) => {
      const timeOff = timeOffEvents.filter(
        (event) => format(new Date(event.start), "MM/dd/yyyy") === date
      );
      const personalTimeOffNames = [];
      const timeOffNames = [];
      timeOff.forEach((event) => {
        if (event.type === "softRequest") {
          const name = event.highPriority ? (
            <Grid
              item
              container
              alignItems="center"
              key={event.eventId + "personal"}
              className={classes.timeOffNames}
            >
              <Grid item xs={2}>
                <PriorityHighIcon
                  fontSize="small"
                  style={{ marginRight: 5, marginTop: 5 }}
                />
              </Grid>
              <Grid item xs={10}>
                <Typography>{event.eventTitle}</Typography>
              </Grid>
            </Grid>
          ) : (
            <Grid
              item
              key={event.eventId + "personal"}
              className={classes.timeOffNames}
            >
              <Typography>{event.eventTitle}</Typography>
            </Grid>
          );
          personalTimeOffNames.push(name);
        } else if (
          event.category === "personal" &&
          event.status === "approved"
        ) {
          personalTimeOffNames.push(
            <Grid
              item
              container
              alignItems="center"
              key={event.eventId + "personal"}
              className={classes.timeOffNames}
            >
              <Grid item xs={2}>
                <CheckIcon fontSize="small" style={{ marginRight: 5 }} />
              </Grid>
              <Grid item xs={10}>
                <Typography>{event.eventTitle}</Typography>
              </Grid>
            </Grid>
          );
        } else if (
          event.category === "personal" &&
          event.status === "pending"
        ) {
          personalTimeOffNames.push(
            <Grid
              item
              key={event.eventId + "personal"}
              className={classes.timeOffNames}
            >
              <Typography>{event.eventTitle}</Typography>
            </Grid>
          );
        } else {
          const name =
            event.status === "approved" ? (
              <Grid
                item
                container
                alignItems="center"
                key={event.eventId}
                className={classes.timeOffNames}
              >
                <Grid item xs={2}>
                  <CheckIcon fontSize="small" style={{ marginRight: 5 }} />
                </Grid>
                <Grid item xs={10}>
                  <Typography>{event.eventTitle}</Typography>
                </Grid>
              </Grid>
            ) : (
              <Grid item key={event.eventId} className={classes.timeOffNames}>
                <Typography>{event.eventTitle}</Typography>
              </Grid>
            );
          timeOffNames.push(name);
        }
      });

      if (timeOffNames.length > 0 || personalTimeOffNames.length > 0) {
        return (
          <TableCell key={date + "timeOff"} className={classes.filledCell}>
            {personalTimeOffNames.length > 0 && (
              <div className={classes.filledPersonalBox}>
                <Grid container direction="column">
                  {personalTimeOffNames}
                </Grid>
              </div>
            )}
            {timeOffNames.length > 0 && (
              <div className={classes.filledNonPersonalBox}>
                <Grid container direction="column">
                  {timeOffNames}
                </Grid>
              </div>
            )}
          </TableCell>
        );
      } else {
        return (
          <TableCell
            key={date + "noEvent"}
            className={classes.emptyCell}
          ></TableCell>
        );
      }
    });
    return (
      <TableRow key={"timeOff"}>
        <TableCell>
          <Typography variant="subtitle1">Time Off</Typography>
        </TableCell>
        {cells}
      </TableRow>
    );
  };

  const handleViewMoreClick = (e, participants) => {
    setViewMoreAnchorEl(e.currentTarget);
    setEmployeesToShow(participants);
  };

  const renderEmployees = (participants) => {
    if (participants.length > 6) {
      const firstSix = participants.slice(0, 6);
      const remainingParticipants = participants.slice(6);
      const renderedSix = firstSix.map((participant) => (
        <EventParticipant
          key={participant.id}
          participant={participant}
          showAvatar={true}
          color={false}
          showJobTitle={true}
        />
      ));
      renderedSix.push(
        <Button
          color="inherit"
          key="viewMore"
          onClick={(e) => handleViewMoreClick(e, remainingParticipants)}
        >
          View More
        </Button>
      );
      return renderedSix;
    } else if (participants.length === 0) {
      return (
        <Typography style={{ width: 100 }}>No employees scheduled</Typography>
      );
    } else {
      return participants.map((participant) => (
        <EventParticipant
          key={participant.id}
          participant={participant}
          showAvatar={true}
          color={false}
          showJobTitle={true}
        />
      ));
    }
  };

  const renderAllEmployees = (participants) => {
    return participants.map((participant) => (
      <EventParticipant
        key={participant.id}
        participant={participant}
        showAvatar={true}
        showJobTitle={true}
      />
    ));
  };

  const allEventsAndRequests = [
    ...allEvents,
    ...userPending,
    ...userApproved,
    ...userSoft,
    ...approvedTimeOff,
    ...pendingTimeOff,
  ];

  if (allEventsAndRequests.length !== 0) {
    rows.splice(0, rows.length);

    const timeOffEvents = [];

    if (
      (allFilters.includes("Soft Requests") ||
        allFilters.includes("All Requests")) &&
      (allFilters.includes("All Shifts") || allFilters.includes("Personal"))
    ) {
      timeOffEvents.push(userSoftTimeOff);
    }

    if (
      (allFilters.includes("Pending Time Off") ||
        allFilters.includes("All Requests")) &&
      allFilters.includes("All Shifts")
    ) {
      timeOffEvents.push(userPendingTimeOff);
      timeOffEvents.push(weekPendingTimeOff);
    } else if (
      (allFilters.includes("Pending Time Off") ||
        allFilters.includes("All Requests")) &&
      allFilters.includes("Personal")
    ) {
      timeOffEvents.push(userPendingTimeOff);
    }

    if (
      (allFilters.includes("Approved Time Off") ||
        allFilters.includes("All Requests")) &&
      allFilters.includes("All Shifts")
    ) {
      timeOffEvents.push(userApprovedTimeOff);
      timeOffEvents.push(weekApprovedTimeOff);
    } else if (
      (allFilters.includes("Approved Time Off") ||
        allFilters.includes("All Requests")) &&
      allFilters.includes("Personal")
    ) {
      timeOffEvents.push(userApprovedTimeOff);
    }

    // if (extraRow) {
    //     rows.push(extraRow);
    // }

    const weekRows = shiftNames
      ? shiftNames.map((e) =>
          createRow(
            weekEvents.filter((event) => event.eventTitle === e),
            e
          )
        )
      : [];
    const weekUserRows = shiftNames
      ? shiftNames.map((e) =>
          createRow(
            weekUserEvents.filter((event) => event.eventTitle === e),
            e
          )
        )
      : [];

    if (
      allFilters.includes("All Missions") &&
      allFilters.includes("All Shifts")
    ) {
      rows.push(weekRows);
    } else if (
      allFilters.includes("All Missions") &&
      allFilters.includes("Personal")
    ) {
      rows.push(weekUserRows);
    } else if (
      !allFilters.includes("All Missions") &&
      allFilters.includes("All Shifts")
    ) {
      const rowsToShow = shiftNames
        ? shiftNames.filter((name) => allFilters.includes(name))
        : [];
      const weekRows = rowsToShow.map((e) =>
        createRow(
          weekEvents.filter((event) => event.eventTitle === e),
          e
        )
      );
      rows.push(weekRows);
    } else if (
      !allFilters.includes("All Missions") &&
      allFilters.includes("Personal")
    ) {
      const rowsToShow = shiftNames
        ? shiftNames.filter((name) => allFilters.includes(name))
        : [];
      const weekUserRows = rowsToShow.map((e) =>
        createRow(
          weekUserEvents.filter((event) => event.eventTitle === e),
          e
        )
      );
      rows.push(weekUserRows);
    }

    if (
      allFilters.includes("All Requests") ||
      allFilters.includes("Approved Time Off") ||
      allFilters.includes("Pending Time Off") ||
      allFilters.includes("Soft Requests")
    ) {
      const timeOffRow = createTimeOffRow(timeOffEvents.flat());
      rows.push(timeOffRow);
    }
  }

  const changeToNextWeek = () => {
    const lastDay = arrayOfDates[6];
    const newFirstDay = add(new Date(lastDay), { days: 1 });
    setStartDate(newFirstDay);
    selectedDateVar(newFirstDay);
    // setSelectedDate(newFirstDay);

    const currentSchedule = isAfter(new Date(scheduleEndDate), newFirstDay);
    setCurrentSchedule(currentSchedule);

    if (newFirstDay >= draftStart && newFirstDay <= draftEnd) {
      setDraftSchedule(true);
    } else {
      setDraftSchedule(false);
    }
  };

  const changeToPreviousWeek = () => {
    const newFirstDay = sub(startDate, { days: 7 });
    setStartDate(newFirstDay);
    selectedDateVar(newFirstDay);
    // setSelectedDate(newFirstDay);

    const currentSchedule = isAfter(new Date(scheduleEndDate), newFirstDay);
    setCurrentSchedule(currentSchedule);

    if (newFirstDay >= draftStart && newFirstDay <= draftEnd) {
      setDraftSchedule(true);
    } else {
      setDraftSchedule(false);
    }
  };

  const changeToToday = () => {
    const today = new Date();

    today.setHours(0, 0, 0, 0);

    const dayNumber = today.getDay();
    const firstDayOfWeek = sub(today, { days: dayNumber });

    setStartDate(firstDayOfWeek);
    selectedDateVar(today);

    const currentSchedule = isAfter(new Date(scheduleEndDate), today);
    setCurrentSchedule(currentSchedule);

    if (firstDayOfWeek >= draftStart && firstDayOfWeek <= draftEnd) {
      setDraftSchedule(true);
    } else {
      setDraftSchedule(false);
    }
  };

  return (
    <Paper className={clsx(classes.paper, "cal")} ref={ref}>
      <style>{`@media print {.cal{margin:30px;size:landscape;}}`}</style>
      {props.printpreview && (
        <Box m={2}>
          <Typography variant="h4" style={{ textAlign: "center" }}>
            {props.primaryUnit.name} -Weekly Schedule
          </Typography>
        </Box>
      )}
      <Grid
        container
        wrap="nowrap"
        align="center"
        justify="space-between"
        style={{ backgroundColor: "#F7F7F7" }}
      >
        <Grid
          item
          container
          justify="flex-start"
          xs={5}
          style={{ paddingLeft: 3 }}
        >
          <Grid item>
            <Typography
              variant="h3"
              style={{ color: "#007BFF", fontWeight: 400 }}
            >
              {startDate ? format(startDate, "MMMM") + " " : null}
              <span style={{ color: "#007BFF", fontWeight: 100 }}>
                {startDate ? format(startDate, "yyyy") : null}
              </span>
            </Typography>
          </Grid>
          <Grid
            item
            container
            spacing={1}
            xs={4}
            style={{ marginTop: -15, marginRight: -15, marginLeft: 15 }}
          >
            <Grid item>
              <IconButton
                style={{ color: "#007BFF", fontSize: 26 }}
                onClick={changeToPreviousWeek}
              >
                <KeyboardArrowLeftIcon />
              </IconButton>
            </Grid>
            <Grid item>
              <Typography
                style={{
                  color: "#007BFF",
                  // fontWeight: 500,
                  fontSize: 16,
                  paddingTop: 12,
                  paddingBottom: 12,
                  marginLeft: -15,
                  marginRight: -15,
                }}
              >
                Week
                {/* {arrayOfDates.length === 7 ? format(new Date(arrayOfDates[0]), 'M/d') 
                                + ' - ' + format(new Date(arrayOfDates[6]), 'M/d') : null} */}
              </Typography>
            </Grid>
            <Grid item>
              <IconButton
                style={{ color: "#007BFF" }}
                onClick={changeToNextWeek}
              >
                <KeyboardArrowRightIcon />
              </IconButton>
            </Grid>
          </Grid>
          <Grid item>
            <Button
              style={{ color: "#007BFF" }}
              className={classes.todayButton}
              onClick={changeToToday}
            >
              Today
            </Button>
          </Grid>
        </Grid>
        {draftSchedule && (
          <Grid item xs={2}>
            <Typography variant="h3" wrap="nowrap" style={{ color: "#8CADE1" }}>
              Draft
            </Typography>
          </Grid>
        )}
        <Grid item xs={5}></Grid>
      </Grid>
      <TableContainer
        style={{
          backgroundColor: "#F7F7F7",
          height: 930,
        }}
      >
        <Table stickyHeader style={{ height: 930 }}>
          <TableHead>
            <TableRow>
              <TableCell style={{ backgroundColor: "#F7F7F7" }}>
                <Typography>Shift</Typography>
              </TableCell>
              {columns}
            </TableRow>
          </TableHead>
          <TableBody>{rows}</TableBody>
        </Table>
      </TableContainer>
      <Popover
        anchorEl={viewMoreAnchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        keepMounted
        open={Boolean(viewMoreAnchorEl)}
        onClose={() => setViewMoreAnchorEl(null)}
        elevation={2}
      >
        <Grid
          container
          direction="column"
          spacing={2}
          style={{ padding: 5, margin: 5 }}
        >
          {renderAllEmployees(employeesToShow)}
        </Grid>
      </Popover>
      {/* <Popover
                anchorEl={extraAnchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                keepMounted
                open={Boolean(extraAnchorEl)}
                onClose={() => setExtraAnchorEl(null)}
                elevation={2}
            >
                <Paper style={{ padding: 15, backgroundColor: '#F7F7F7'}}>
                    {extraEmployees.length !== 0 
                        ? renderAllEmployees(extraEmployees) 
                        : <Typography>No Available Employees</Typography>
                    }
                </Paper>
            </Popover> */}
    </Paper>
  );
});

export default EmpWeekView;
