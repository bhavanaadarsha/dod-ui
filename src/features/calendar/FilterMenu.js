import React, { useEffect } from 'react';
import { 
    Grid, Typography, Checkbox, 
    Button, Accordion, AccordionSummary, 
    AccordionDetails, FormControlLabel, Radio,
    RadioGroup
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Roles from '../../Roles/roles';
import { useReactiveVar } from '@apollo/client';
import { filterListVar, userVar } from '../../cache';

const MuiAccordion = withStyles({
    root: {
        '&$expanded': {
            margin: '0',
        },
    },
    expanded: {}
})(Accordion);

const FilterMenu = ({missionNames}) => {

    const filterList = useReactiveVar(filterListVar);
    const { shiftsFilters, missionFilters, requestsFilters } = filterList;
    const user = userVar();
    const userRole = user.role;

    const initialShiftsFilters = (userRole === Roles.MANAGER) ? ['All Shifts'] : ['Personal'];
    const initialMissionFilters = [...missionNames, 'All Missions'];
    const initialRequestsFilters = (userRole === Roles.MANAGER) 
        ? [] 
        : ['Soft Requests', 'Pending Time Off', 'Approved Time Off', 'All Requests'];
    
    useEffect(() => {
        if (filterList.missionFilters.includes('All Missions')) {
            if (filterList.missionFilters.length !== missionNames.length + 1) {
                filterListVar({
                    ...filterList,
                    missionFilters: [...missionNames, 'All Missions']
                });
            }
        }
    }, [missionNames]);

    const handleCheckboxToggle = (value, list) => () => {
        let checked;
        let allNotChanging;
        let allLabel;
        let allList;
        switch (list) {
            case 'shiftsFilters':
                checked = [value];
                allNotChanging = [...missionFilters, ...requestsFilters];
                break;
            case 'missionFilters':
                checked = [...missionFilters];
                allNotChanging = [...shiftsFilters, ...requestsFilters];
                allLabel = 'All Missions';
                allList = [...missionNames, 'All Missions'];
                break;
            case 'requestsFilters':
                checked = [...requestsFilters];
                allNotChanging = [...missionFilters, ...shiftsFilters];
                allLabel = 'All Requests';
                allList = ['Soft Requests', 'Pending Time Off', 'Approved Time Off', 'All Requests'];
                break;
            default:
                handleResetAll();
                break;
        }

        const currentIndex = checked.indexOf(value);
        let indexOfAll = checked.indexOf(allLabel);
        if (list !== 'shiftsFilters') {
            if (currentIndex === -1 && value === allLabel) {
                checked = [...allList];
            } else if (currentIndex === -1) {
                checked.push(value);
            } else if (currentIndex !== -1 && value === allLabel) {
                checked = [];
            } else if (currentIndex !== -1 && indexOfAll !== -1) {
                checked.splice(currentIndex, 1);
                indexOfAll = checked.indexOf(allLabel);
                checked.splice(indexOfAll, 1);
            } else {
                checked.splice(currentIndex, 1);
            } 
        }
        filterListVar({
            ...filterListVar(),
            [list]: checked,
            allFilters: [...allNotChanging, ...checked]
        });
        // setShowDayActions(false);
    };

    // const handleReset = (event, list) => {
    //     event.stopPropagation();
     
    //     let initialValue;
    //     let allNotChanging;
    //     switch (list) {
    //         case 'missionFilters':
    //             initialValue = initialMissionFilters;
    //             allNotChanging = [...shiftsFilters, ...requestsFilters];
    //             break;
    //         case 'requestsFilters':
    //             initialValue = initialRequestsFilters;
    //             allNotChanging = [...missionFilters, ...shiftsFilters];
    //             break;
    //         default:
    //             handleResetAll();
    //             break;
    //     }
    //     setShowDayActions(false);
    //     filterListVar({
    //         ...filterListVar(),
    //         [list]: initialValue,
    //         allFilters: [...allNotChanging, ...initialValue]
    //     });
    // };

    const handleResetAll = () => {
        const allFilters = [...initialShiftsFilters, ...initialMissionFilters, ...initialRequestsFilters];
        filterListVar({
            allFilters, 
            requestsFilters: initialRequestsFilters, 
            missionFilters: initialMissionFilters, 
            shiftsFilters: initialShiftsFilters
        });
        // setShowDayActions(false);
    };
    
    return ( 
        <Grid container direction='column' style={{width: 300}}>
            <Grid item align='right'>
                <Button color='primary' onClick={handleResetAll} style={{marginBottom: 8}}>
                    Reset All Filters
                </Button>
            </Grid>
            <Grid item container component={MuiAccordion} defaultExpanded={true}>
                <Grid 
                    item 
                    container 
                    component={AccordionSummary} 
                    alignItems='center' 
                    justify='space-between' 
                    expandIcon={<ExpandMoreIcon />} 
                    style={{height: 60, marginTop: -15}}
                >
                    <Grid item xs={6} style={{paddingTop: 12, paddingBottom: 12}}>
                        <Typography variant='subtitle1'>Events to View</Typography>
                    </Grid>
                </Grid>
                <Grid item container component={AccordionDetails} direction='column'>
                    <Grid 
                        item 
                        component={RadioGroup}
                        style={{marginTop: -5, marginBottom: -5}}
                    >
                        <FormControlLabel label="Personal" control={
                            <Radio 
                                checked={shiftsFilters.indexOf('Personal') !== -1}
                                onChange={handleCheckboxToggle('Personal', 'shiftsFilters')}
                                style={{marginTop: -5, marginBottom: -5, marginLeft: 10}}
                            />
                        } />
                        <FormControlLabel label="All" control={
                            <Radio 
                                checked={shiftsFilters.indexOf('All Shifts') !== -1}
                                onChange={handleCheckboxToggle('All Shifts', 'shiftsFilters')}
                                style={{marginTop: -5, marginBottom: -5, marginLeft: 10}}
                            />
                        } />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item container component={MuiAccordion} defaultExpanded={true}>
                <Grid 
                    item 
                    container 
                    component={AccordionSummary} 
                    alignItems='center' 
                    justify='space-between' 
                    expandIcon={<ExpandMoreIcon />} 
                    style={{height: 60, marginTop: -15}}
                >
                    <Grid item xs={6} style={{paddingTop: 12, paddingBottom: 12}}>
                        <Typography variant='subtitle1'>Mission Type</Typography>
                    </Grid>
                    {/* <Grid item container xs={6} justify='flex-end'>
                        <Button color='primary' onClick={event => handleReset(event, 'missionFilters')}>
                            Clear
                        </Button>
                    </Grid> */}
                </Grid>
                <Grid item container component={AccordionDetails} direction='column'>
                    {missionNames.map(name => (
                        <Grid 
                            item 
                            container 
                            alignItems='center' 
                            onClick={handleCheckboxToggle(name, 'missionFilters')} 
                            key={name}
                            style={{marginTop: -5, marginBottom: -5}}
                        >
                            <Grid item>
                                <Checkbox checked={missionFilters.indexOf(name) !== -1}/>
                            </Grid>
                            <Grid item>
                                <Typography variant='body1'>{name}</Typography>
                            </Grid>
                        </Grid>
                    ))}
                    <Grid 
                        item 
                        container 
                        alignItems='center' 
                        onClick={handleCheckboxToggle('All Missions', 'missionFilters')}
                        style={{marginTop: -5, marginBottom: -5}}
                    >
                        <Grid item>
                            <Checkbox checked={missionFilters.indexOf('All Missions') !== -1}/>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>All</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>     
            <Grid item container component={MuiAccordion} defaultExpanded={true}>
                <Grid 
                    item 
                    container 
                    component={AccordionSummary} 
                    alignItems='center' 
                    justify='space-between' 
                    expandIcon={<ExpandMoreIcon />} 
                    style={{height: 60, marginTop: -15}}
                >
                    <Grid item xs={6} style={{paddingTop: 12, paddingBottom: 12}}>
                        <Typography variant='subtitle1'>Requests</Typography>
                    </Grid>
                    {/* <Grid item container justify='flex-end' xs={6}>
                        <Button color='primary' onClick={event => handleReset(event, 'requestsFilters')}>
                            Clear
                        </Button>
                    </Grid> */}
                </Grid>
                <Grid item container component={AccordionDetails} direction='column'>
                    <Grid 
                        item 
                        container 
                        alignItems='center' 
                        onClick={handleCheckboxToggle('Soft Requests', 'requestsFilters')}
                        style={{marginTop: -5, marginBottom: -5}}
                    >
                        <Grid item>
                            <Checkbox checked={requestsFilters.indexOf('Soft Requests') !== -1}/>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>Personal Soft Requests</Typography>
                        </Grid>
                    </Grid>
                    <Grid 
                        item 
                        container
                        alignItems='center' 
                        onClick={handleCheckboxToggle('Pending Time Off', 'requestsFilters')}
                        style={{marginTop: -5, marginBottom: -5}}
                    >
                        <Grid item>
                            <Checkbox checked={requestsFilters.indexOf('Pending Time Off') !== -1}/>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>Pending Time Off</Typography>
                        </Grid>
                    </Grid>
                    <Grid 
                        item 
                        container 
                        alignItems='center' 
                        onClick={handleCheckboxToggle('Approved Time Off', 'requestsFilters')}
                        style={{marginTop: -5, marginBottom: -5}}
                    >
                        <Grid item>
                            <Checkbox checked={requestsFilters.indexOf('Approved Time Off') !== -1}/>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>Approved Time Off</Typography>
                        </Grid>
                    </Grid>
                    <Grid 
                        item 
                        container 
                        alignItems='center' 
                        onClick={handleCheckboxToggle('All Requests', 'requestsFilters')}
                        style={{marginTop: -5, marginBottom: -5}}
                    >
                        <Grid item>
                            <Checkbox checked={requestsFilters.indexOf('All Requests') !== -1}/>
                        </Grid>
                        <Grid item>
                            <Typography variant='body1'>All</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default FilterMenu;