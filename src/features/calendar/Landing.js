import { CircularProgress, Typography } from '@material-ui/core';
import React from 'react';
import EmpCalendar from './EmpCalendar';
import { userVar } from '../../cache';
import { useQuery } from '@apollo/client';
import { GET_SCHEDULE_PERIODS } from '../../api/gqlQueries';

function Landing(){
    const user = userVar();
    
    const {loading, error, data, refetch} = useQuery(GET_SCHEDULE_PERIODS, {
        variables: {officeId: parseInt(user.office.id)}
    });
    
    if (loading) {
        return <CircularProgress />;
    } else if (error) {
        console.log(error);
        return <Typography>Something went wrong. Please try again.</Typography>;
    } else {
        return <EmpCalendar schedulePeriods={data.schedulePeriods} refetchSchedulePeriods={refetch}/>;
    }
}

export default Landing;