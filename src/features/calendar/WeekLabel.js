import React from "react";
import { Grid, makeStyles, Tooltip, Typography } from "@material-ui/core";
import sun from "../../assets/quizImages/sun.png";
import Brightness3Icon from "@material-ui/icons/Brightness3";
import PriorityHighIcon from "@material-ui/icons/PriorityHigh";
import CheckIcon from "@material-ui/icons/Check";
import WarningIcon from "@material-ui/icons/Warning";
import AlertOutline from "mdi-material-ui/AlertOutline";
import ShiftSlack from "./ShiftSlack";

const useStyles = makeStyles(() => ({
  tooltip: {
    minWidth: 450,
    backgroundColor: "rgba(57, 57, 60, 0.95)",
  },
  label: {
    // height: 19.5,
    // borderRadius: 4,
    // padding: 1,
    // height: 100
    // paddingLeft: 4
  },
}));

const WeekLabel = ({ data, skills }) => {
  const classes = useStyles();
  if (data.participants) {
    // const title = data.eventTitle.substr(0, data.eventTitle.indexOf('-'));
    // const participantsCount = data.participants.length;
    return (
      <Grid
        container
        direction="column"
        // className={classes.label}
        // style={{backgroundColor: data.color}}
      >
        {/* <Grid item >
                    {title.includes('a')
                        ? <img 
                            alt='day shift' 
                            src={sun} 
                            style={{
                                width: 20, 
                                // marginTop: 2, 
                                marginRight: 5
                            }}
                        />
                        : <Brightness3Icon style={{
                            width: 20, 
                            // marginTop: -3, 
                            marginRight: 5}}
                        />
                    }
                </Grid> */}
        <Grid item>
          <Typography variant="body1">{data.eventTitle}</Typography>
          <Typography variant="body2">{skills}</Typography>
        </Grid>
        {/* {data.participants.map(participant => (
                    <Grid item >
                        <Typography variant='body2'>
                            {participant.firstName} {participant.lastName}
                        </Typography>
                    </Grid>
                ))} */}
      </Grid>
    );
  } else if (data.type === "softRequest") {
    return (
      <>
        {data.highPriority ? (
          <Grid
            container
            wrap="nowrap"
            // className={classes.label}
            // style={{backgroundColor: data.color}}
          >
            <Grid item>
              <PriorityHighIcon style={{ marginBottom: -2, marginRight: 5 }} />
            </Grid>
            <Grid item>
              <Typography variant="body1">{data.eventTitle}</Typography>
              <Typography variant="body2">{skills}</Typography>
            </Grid>
          </Grid>
        ) : (
          <Grid
            container
            wrap="nowrap"
            // className={classes.label}
            // style={{backgroundColor: data.color}}
          >
            <Grid item>
              <Typography variant="body1">{data.eventTitle}</Typography>
              <Typography variant="body2">{skills}</Typography>
            </Grid>
          </Grid>
        )}
      </>
    );
  } else if (data.status === "approved") {
    return (
      <Grid
        container
        wrap="nowrap"
        // className={classes.label}
        // style={{backgroundColor: data.color}}
      >
        <Grid item>
          <CheckIcon style={{ marginBottom: -2, marginRight: 5 }} />
        </Grid>
        <Grid item>
          <Typography variant="body1">{data.eventTitle}</Typography>
          <Typography variant="body2">{skills}</Typography>
        </Grid>
      </Grid>
    );
  } else {
    return (
      <Grid
        container
        wrap="nowrap"
        // className={classes.label}
        // style={{backgroundColor: data.color}}
      >
        <Grid item>
          <Typography variant="body1">{data.eventTitle}</Typography>
          <Typography variant="body2">{skills}</Typography>
        </Grid>
      </Grid>
    );
  }
};

export default WeekLabel;
