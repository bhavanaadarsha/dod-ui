import React, { useState } from 'react';
import EventParticipant from './EventParticipant';
import {
    Typography, Grid, Button,
    makeStyles, Dialog, DialogContent,
    TextField, Box, TableContainer,
    Table, TableRow, TableCell, 
    TableHead, TableBody
} from '@material-ui/core';
// import moon from '../../assets/quizImages/moon.png';
import sun from '../../assets/quizImages/sun.png';
import Brightness3Icon from '@material-ui/icons/Brightness3';
// import NightsStayIcon from '@material-ui/icons/NightsStay';
// import WbSunnyIcon from '@material-ui/icons/WbSunny';
import Roles from '../../Roles/roles';
import { format, isEqual } from 'date-fns';
import {
    MANAGER_APPROVE_EMPLOYEE_AVAILABILITY,
    MANAGER_DENY_EMPLOYEE_AVAILABILITY,
    UPDATE_EMPLOYEE_AVAILABILITY
} from '../../api/gqlQueries';
import { userVar } from '../../cache';
import { useMutation } from '@apollo/client';
import ShiftSlack from './ShiftSlack';
import WarningIcon from '@material-ui/icons/Warning';
import ShiftSlackBySkill from './ShiftSlackBySkill';

const useStyles = makeStyles((theme) => ({
    button: {
        width: 135
    },
    input: {
        width: 384
    },
    darkText: {
        color: 'black',
        paddingTop: 4,
        paddingBottom: 4
    }, 
    underText: {
        color: theme.palette.primary.light,
        paddingTop: 4,
        paddingBottom: 4
    }, 
    table: {
        marginTop: 24,
        marginBottom: 16
    },
    icon: {
        padding: 0,
        marginRight: 5,
        marginBottom: -2,
        color: theme.palette.primary.light
    },
    iconColumn: {
        paddingTop: 4,
        paddingBottom: 4
    }
}));

const AgendaEvent = React.forwardRef((props, ref) => {
    const classes = useStyles();

    const {
        event,
        toggleShiftSwitchForm,
        handleCallInClick,
        handleEditClick,
        toggleEditTimeOffForm,
        setTimeOffRequestIdToEdit,
        toggleEditSoftRequestForm,
        setSoftRequestIdToEdit,
        view,
        formattedUserScheduleDates,
        setToast,
        setShowToast,
        setErrorToast,
        setShowErrorToast,
        scheduleEndDate,
        selectedDate,
        allowCallIns,
        refetch,
        slacks
    } = props;
    
    const user = userVar();

    const participants = event.participants;
    const userShiftDate = formattedUserScheduleDates.includes(format(event.start, 'MM/dd/yyyy'));
    const userRole = user.role;

    const [showStaffing, setShowStaffing] = useState(false);
    const [showStaffingBySkill, setShowStaffingBySkill] = useState(false);
    const [showDenialReason, setShowDenialReason] = useState(false);
    const [denialReason, setDenialReason] = useState('');
    const [requestIdToDeny, setRequestIdToDeny] = useState();

    const [approveRequest] = useMutation(MANAGER_APPROVE_EMPLOYEE_AVAILABILITY, {
        onCompleted(data) {
            console.log(data);
            setToast('Manager Approve Time Off Request');
            setShowToast(true);
        },
        onError(error) {
            console.error(error);
            setErrorToast('Error Approve Time Off');
            setShowErrorToast(true);
        }
    });

    const [denyRequest] = useMutation(MANAGER_DENY_EMPLOYEE_AVAILABILITY, {
        update(cache, { data: { denyTimeOffRequestWithNotifications } }) {
            cache.evict({
                id: `EmployeeAvailabilityNode:${denyTimeOffRequestWithNotifications.timeOffRequest.id}`
            });
        },
        onCompleted(data) {
            console.log(data);
            setShowDenialReason(false);
            setDenialReason('');
            setRequestIdToDeny();
            setToast('Manager Deny Time Off Request');
            setShowToast(true);
        },
        onError(error) {
            console.error(error);
            setErrorToast('Error Deny Time Off');
            setShowErrorToast(true);
        }
    });
        
    const [deleteRequest] = useMutation(UPDATE_EMPLOYEE_AVAILABILITY, {
        onCompleted(data) {
            console.log(data);
            refetch();

            setToast('Delete Time Off Request');
            setShowToast(true);
        },
        onError(error) {
            console.error(error);
            setErrorToast('Error Delete Time Off');
            setShowErrorToast(true);
        }
    });

    const handleApproveTORequest = (id) => {
        approveRequest({
            variables: {
                id: id,
                approvingManager: parseInt(user.id)
            }
        });
    };

    const handleDenyTORequest = (id) => {
        denyRequest({
            variables: {
                id: id,
                denyingManager: parseInt(user.id),
                deniedText: denialReason
            }
        });
    };

    const handleEditSoftRequestClick = (id) => {
        setSoftRequestIdToEdit(id);
        toggleEditSoftRequestForm();
    };

    const handleEditTimeOffClick = (id) => {
        setTimeOffRequestIdToEdit(id);
        toggleEditTimeOffForm();
    };
        
    const handleDeleteTORequest = (eventId) => {
        deleteRequest({variables: {
            id: eventId, 
            input: {userCancelled: true}
        }});
    };

    const renderParticipants = (participants, showAvatar) => {
        return participants.map(participant => (
            <EventParticipant
                key={participant.id}
                participant={participant}
                showAvatar={showAvatar}
                showJobTitle={true}
                widthMatters={false}
            />
        ));
    };

    if (participants) {
        const title = event.eventTitle.substr(0, event.eventTitle.indexOf('-'));
        return (
            <div className='md-custom-event-cont' style={{ padding: 15 }} ref={ref}>
                <div className='md-custom-event-wrapper'>
                    <div className='md-custom-event-details' style={{ paddingLeft: 5 }}>
                        <div className='md-custom-event-title' ref={ref} >
                            {props.printpreview && 
                                <Box m={2}>
                                    <Typography variant="h4" style={{ textAlign: 'center' }}>
                                        {props.primaryUnit.name} - Schedule - {format(props.selectedDate, 'MMM-dd-yyyy')}
                                    </Typography>
                                </Box>
                            }
                            <Grid container justify='space-between' >
                                <Grid item container xs={4}>
                                    <Grid item>
                                        {title.includes('a')
                                            ? <img alt='day shift' src={sun} style={{ width: 22, marginTop: 4, marginRight: 5 }} />
                                            : <Brightness3Icon style={{ width: 22, marginTop: 3, marginRight: 3 }} />
                                        }
                                    </Grid>
                                    <Grid item ref={ref}>
                                        <Typography variant='h5' style={{ marginTop: 5, marginBottom: 5 }}>{event.eventTitle}</Typography>
                                    </Grid>
                                </Grid>
                                {!props.printpreview &&
                                    <Grid item xs={8} container spacing={2} justify='flex-end'>
                                        <Grid item>
                                            <Button
                                                variant='outlined'
                                                color='primary'
                                                onClick={toggleShiftSwitchForm}
                                                disabled={
                                                    (userRole === Roles.EMPLOYEE && !userShiftDate) || 
                                                    new Date(event.start) <= new Date()
                                                } 
                                                className={classes.button}
                                            >
                                                Switch Shifts
                                            </Button>
                                        </Grid>
                                        {allowCallIns &&
                                            <Grid item>
                                                <Button
                                                    variant='outlined'
                                                    color='primary'
                                                    onClick={() => handleCallInClick(event.start)}
                                                    disabled={!userShiftDate}
                                                    className={classes.button}
                                                >
                                                    Call In
                                                </Button>
                                            </Grid>
                                        }
                                        {(userRole === Roles.LEAD || userRole === Roles.MANAGER) &&
                                            <Grid item>
                                                <Button
                                                    variant='contained'
                                                    color='primary'
                                                    onClick={() => handleEditClick(event)}
                                                    className={classes.button}
                                                >
                                                    Edit Shift
                                                </Button>
                                            </Grid>
                                        }
                                    </Grid>
                                }
                            </Grid>
                        </div>
                        {/* {(userRole === Roles.MANAGER || userRole === Roles.SCHEDULER) &&
                            slacks && slacks.length > 0
                            ? <Grid container justify='space-between'>
                                <Grid 
                                    item 
                                    sm={12} 
                                    md={4}
                                    container 
                                    direction='column' 
                                    spacing={1} 
                                    className='md-custom-event-img-cont' 
                                    ref={ref} 
                                    >
                                    {renderParticipants(participants, true)}
                                </Grid>
                                <Grid 
                                    item 
                                    sm={12}
                                    md={8}
                                    lg={7}
                                    justify='flex-end'
                                    component={TableContainer}
                                    className='md-custom-event-img-cont' 
                                    ref={ref}
                                >
                                    {renderSlacks()}
                                </Grid>
                            </Grid>
                            : <Grid 
                                container 
                                direction='column' 
                                spacing={1} 
                                className='md-custom-event-img-cont' 
                                ref={ref} 
                            >
                                {renderParticipants(participants, true)}
                            </Grid>
                        } */}
                        <Grid 
                            container 
                            direction='column' 
                            spacing={1} 
                            className='md-custom-event-img-cont' 
                            ref={ref} 
                        >
                            {renderParticipants(participants, true)}
                        </Grid>
                    </div>
                </div>
            </div>
        );
    } else if ((event.type === 'timeOff') && view === 2) {
        return (
            <>
                <div className='md-custom-event-cont' style={{ padding: 15 }}>
                    <div className='md-custom-event-wrapper'>
                        <div className='md-custom-event-details' style={{ paddingLeft: 5 }}>
                            <div className='md-custom-event-title'>
                                <Grid container justify='space-between'>
                                    <Grid item xs={6}>
                                        <Typography variant='h5' style={{ marginTop: 5, marginBottom: 5 }}>{event.eventTitle}</Typography>
                                    </Grid>
                                    {event.status === 'pending' &&
                                        <Grid item container justify='flex-end' xs={6} spacing={2}>
                                            {event.category === 'personal' &&
                                                <Grid item>
                                                    <Button
                                                        variant='outlined'
                                                        color='primary'
                                                        onClick={() => handleEditTimeOffClick(event.eventId)}
                                                        className={classes.button}
                                                    >
                                                        Edit Request
                                                    </Button>
                                                </Grid>
                                            }
                                            {(userRole === Roles.LEAD || userRole === Roles.MANAGER) &&
                                                <>
                                                    <Grid item>
                                                        <Button
                                                            variant='outlined'
                                                            color='primary'
                                                            onClick={() => { setShowDenialReason(true); setRequestIdToDeny(event.eventId); }}
                                                            className={classes.button}
                                                        >
                                                            Deny
                                                        </Button>
                                                    </Grid>
                                                    <Grid item>
                                                        <Button
                                                            variant='contained'
                                                            color='primary'
                                                            onClick={() => handleApproveTORequest(event.eventId)}
                                                            className={classes.button}
                                                        >
                                                            Approve
                                                        </Button>
                                                    </Grid>
                                                </>
                                            }
                                        </Grid>
                                    }
                                    {event.status === 'approved' && event.category === 'personal' &&
                                        <Grid item>
                                            <Button
                                                variant='outlined'
                                                color='primary'
                                                onClick={() => handleDeleteTORequest(event.eventId)}
                                                className={classes.button}
                                            >
                                                Delete Request
                                            </Button>
                                        </Grid>
                                    }
                                </Grid>
                            </div>
                            <Grid item>
                                <Typography variant='subtitle1'>Status: {event.status}</Typography>
                            </Grid>
                            <Grid item>
                                <Typography>
                                    Dates: {format(new Date(event.requestStart), 'MM/dd/yyy')} - {format(new Date(event.requestEnd), 'MM/dd/yyy')}
                                </Typography>
                            </Grid>
                        </div>
                    </div>
                </div>
                <Dialog open={showDenialReason} fullWidth maxWidth='xs'>
                    <DialogContent style={{ padding: 30 }}>
                        <Grid container direction='column' spacing={2}>
                            <Grid item >
                                <Typography variant='h2'>Reason for Denial</Typography>
                            </Grid>
                            <Grid item >
                                <TextField
                                    variant='outlined'
                                    value={denialReason}
                                    onChange={e => setDenialReason(e.target.value)}
                                    className={classes.input}
                                    onKeyPress={e => { e.key === 'Enter' && handleDenyTORequest(requestIdToDeny); }}
                                />
                            </Grid>
                            <Grid item container justify='flex-end' spacing={1}>
                                <Grid item>
                                    <Button
                                        variant='contained'
                                        color='primary'
                                        type='submit'
                                        className={classes.button}
                                        onClick={() => handleDenyTORequest(requestIdToDeny)}
                                    >
                                        Submit
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Button
                                        variant='outlined'
                                        color='primary'
                                        className={classes.button}
                                        onClick={() => { setShowDenialReason(false); setDenialReason(''); setRequestIdToDeny(); }}
                                    >
                                        Cancel
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </DialogContent>
                </Dialog>
            </>
        );
    } else if (event.type === 'softRequest') {
        return (
            <div className='md-custom-event-cont' style={{ padding: 15 }}>
                <div className='md-custom-event-wrapper'>
                    <div className='md-custom-event-details' style={{ paddingLeft: 5 }}>
                        <div className='md-custom-event-title'>
                            <Grid container justify='space-between'>
                                <Grid item xs={4}>
                                    <Typography variant='h5' style={{ marginTop: 5, marginBottom: 5 }}>{event.eventTitle}</Typography>
                                </Grid>
                                {selectedDate > scheduleEndDate &&
                                    <Grid item>
                                        <Button
                                            variant='outlined'
                                            color='primary'
                                            onClick={() => handleEditSoftRequestClick(event.eventId)}
                                            className={classes.button}
                                        >
                                            Edit Request
                                        </Button>
                                    </Grid>
                                }
                            </Grid>
                        </div>
                        <Typography variant='subtitle1'>Priority: {event.highPriority ? 'High' : 'Standard'}</Typography>
                    </div>
                </div>
            </div>
        );
    } else if (event.type === 'staffing') {
        return (
            <>
                <div className='md-custom-event-cont' style={{ padding: 15 }}>
                    <div className='md-custom-event-wrapper'>
                        <div className='md-custom-event-details' style={{ paddingLeft: 5 }}>
                            <div className='md-custom-event-title'>
                                {/* <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={() => setShowStaffing(true)}
                                    // className={classes.button}
                                >
                                    Staffing Details
                                </Button> */}
                                <Button
                                    variant='contained'
                                    color='primary'
                                    onClick={() => setShowStaffingBySkill(true)}
                                    // className={classes.button}
                                >
                                    Staffing Details
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
                <Dialog open={showStaffing} fullWidth maxWidth='sm'>
                    <DialogContent
                        style={{ padding: 30, overflowX: 'hidden', overflowY: 'auto' }}
                    >
                        <ShiftSlack 
                            shift={event} 
                            slacks={slacks} 
                            tooltip={false} 
                            closeDialog={() => setShowStaffing(false)}
                        />
                    </DialogContent>
                </Dialog>
                <Dialog open={showStaffingBySkill} maxWidth={false}>
                    <DialogContent
                        style={{ padding: 30, overflowX: 'hidden', overflowY: 'auto' }}
                    >
                        <ShiftSlackBySkill
                            shift={event} 
                            slacks={slacks} 
                            tooltip={false} 
                            closeDialog={() => setShowStaffingBySkill(false)}
                        />
                    </DialogContent>
                </Dialog>
            </>
        );
    } else {
        return <div></div>;
    }

});

export default AgendaEvent;