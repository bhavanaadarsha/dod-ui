import React, { useState } from 'react';
import { 
    Grid, Typography, Button, 
    Accordion, AccordionSummary, AccordionDetails, 
    IconButton, Box, makeStyles,
    Paper, CircularProgress, TextField,
    Dialog, DialogContent
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EventParticipant from './EventParticipant';
import Roles from '../../Roles/roles';
import { format } from 'date-fns';
import { 
    MANAGER_APPROVE_EMPLOYEE_AVAILABILITY, 
    MANAGER_DENY_EMPLOYEE_AVAILABILITY,
    UPDATE_EMPLOYEE_AVAILABILITY
} from '../../api/gqlQueries';
import { useMutation, useReactiveVar } from '@apollo/client';
import { filterListVar, userVar } from '../../cache';

const useStyles = makeStyles(() => ({
    button: {
        width: 150
    },
    approveDeny: {
        width: 87
    },
    input: {
        width: 384
    }
}));

const EmpDayActions = (props) => {
    const classes = useStyles();

    const {
        dayObject, 
        closeDialog, 
        toggleShiftSwitchForm, 
        handleCallInClick, 
        toggleEditShift, 
        setShiftToEdit, 
        toggleSoftRequest, 
        toggleTimeOffForm,
        setTimeOffRequestIdToEdit,
        toggleEditTimeOffForm, 
        toggleEditSoftRequestForm,
        setSoftRequestIdToEdit,
        invalidDates, 
        formattedUserScheduleDates,
        setToast, 
        setShowToast,
        setErrorToast,
        setShowErrorToast,
        scheduleEndDate,
        allowCallIns,
        refetch
    } = props;

    const filterList = useReactiveVar(filterListVar);
    const allFilters = filterList.allFilters;
    
    const user = userVar();
    const userRole = user.role;

    const showingAll = Boolean(allFilters.includes('All Shifts') && allFilters.includes('All Missions'));
    const [expanded, setExpanded] = useState(true);
    const [showDenialReason, setShowDenialReason] = useState(false);
    const [denialReason, setDenialReason] = useState('');
    const [requestIdToDeny, setRequestIdToDeny] = useState();

    const personalOffRequests = dayObject.events ? dayObject.events.filter(event => (event.category === 'personal' || event.type === 'softRequest')) : [];
    const currentSchedule = dayObject.currentSchedule;
    const formattedInvalidDates = invalidDates.map(date => format(date, 'MM/dd/yyyy'));
    const invalidDateForRequest = formattedInvalidDates.includes(format(dayObject.date, 'MM/dd/yyyy'));
    const userShiftDate = formattedUserScheduleDates.includes(format(dayObject.date, 'MM/dd/yyyy'));
    
    let events = dayObject.events ? [...dayObject.events] : undefined;
    if(events){
        events.sort((a, b) => a.start - b.start);
    }

    const renderParticipants = (participants, showAvatar) => {
        return participants.map(participant => (
            <EventParticipant 
                key={participant.id} 
                participant={participant} 
                showAvatar={showAvatar} 
                showJobTitle={true} 
            />
        ));
    };

    const toggleAccordion = () => setExpanded(!expanded);

    const handleEditClick = (event, shift) => {
        event.stopPropagation();
        setShiftToEdit(shift);
        toggleEditShift();
    };

    const [approveRequest] = useMutation(MANAGER_APPROVE_EMPLOYEE_AVAILABILITY, {
        onCompleted(data) {
            console.log(data);
            
            const hardRequestEvent = events.find(event => (
                event.eventId === data.approveTimeOffRequestWithNotifications.timeOffRequest.id
            ));
            const changedEvent = {...hardRequestEvent, status: 'approved'};
            const eventsNotChanged = dayObject.events.filter(event => event.eventId !== hardRequestEvent.eventId);
            if (eventsNotChanged.length === 0){
                if (allFilters.includes('All Requests') || allFilters.includes('Approved Time Off')) {
                    dayObject.events = [changedEvent];
                } else {
                    dayObject.events = undefined;
                }
            } else {
                if (allFilters.includes('All Requests') || allFilters.includes('Approved Time Off')) {
                    dayObject.events = [...eventsNotChanged, changedEvent];
                } else {
                    dayObject.events = [...eventsNotChanged];
                }
            }

            setToast('Manager Approve Time Off Request');
            setShowToast(true);
        },
        onError(error){
            console.error(error);
            setErrorToast('Error Approve Time Off');
            setShowErrorToast(true);
        }
    });

    const [denyRequest, {loading: deleteLoading}] = useMutation(MANAGER_DENY_EMPLOYEE_AVAILABILITY, {
        update(cache, { data: { denyTimeOffRequestWithNotifications }}) {
            cache.evict({
                id: `EmployeeAvailabilityNode:${denyTimeOffRequestWithNotifications.timeOffRequest.id}`
            });
        },
        onCompleted(data) {
            console.log(data);
            setShowDenialReason(false);
            setDenialReason('');
            setRequestIdToDeny();

            const eventsNotDeleted = dayObject.events.filter(event => (
                event.eventId !== data.denyTimeOffRequestWithNotifications.timeOffRequest.id
            ));
            if (eventsNotDeleted.length === 0){
                dayObject.events = undefined;
            } else {
                dayObject.events = [...eventsNotDeleted];
            }

            setToast('Manager Deny Time Off Request');
            setShowToast(true);
        },
        onError(error){
            console.error(error);
            setErrorToast('Error Deny Time Off');
            setShowErrorToast(true);
        }
    });
    
    const [deleteRequest] = useMutation(UPDATE_EMPLOYEE_AVAILABILITY, {
        onCompleted(data) {
            console.log(data);
            refetch();
            const eventsNotChanging = dayObject.events 
                ? dayObject.events.filter(event => event.eventId !== data.updateEmployeeAvailability.employeeAvailability.id)
                : [];

            if (eventsNotChanging.length === 0){
                dayObject.events = undefined;
            } else {
                dayObject.events = [...eventsNotChanging];
            }
            
            setToast('Delete Time Off Request');
            setShowToast(true);
        },
        onError(error) {
            console.error(error);
            setErrorToast('Error Delete Time Off');
            setShowErrorToast(true);
        }
    });

    const handleApproveTORequest = (id) => {
        approveRequest({variables: {
            id: id, 
            approvingManager: parseInt(user.id)
        }});
        //send notification to employee to say request approved
        //change event in backend from pending to approved
        //delete notification
    };

    const handleDenyTORequest = (id) => {
        denyRequest({variables: {
            id: id,
            denyingManager: parseInt(user.id),
            deniedText: denialReason
        }});
        //send notification to employee to say denied
        //delete event in backend
        //delete notification
    };

    const handleEditSoftRequestClick = (id) => {
        setSoftRequestIdToEdit(id);
        toggleEditSoftRequestForm();
    };

    const handleEditTimeOffClick = (id) => {
        setTimeOffRequestIdToEdit(id);
        toggleEditTimeOffForm();
    };
    
    const handleDeleteRequest = (eventId) => {
        deleteRequest({variables: {
            id: eventId, 
            input: {userCancelled: true}
        }});
    };
    
    const renderEventContent = () => {
        if (events && events.length > 0) {
            return events.map(event => {
                if (event.participants){
                    return (
                        <Grid component={Accordion} item key={event.eventId} >
                            <Grid 
                                container 
                                component={AccordionSummary} 
                                justify='space-between' 
                                onClick={toggleAccordion} 
                                expandIcon={<ExpandMoreIcon />}
                            >
                                <Grid item xs={8}>
                                    <Typography variant='h4' style={{padding: 8}}>
                                        {event.eventTitle}
                                    </Typography>
                                </Grid>
                                {(userRole === Roles.LEAD || userRole === Roles.MANAGER) && 
                                    <Grid item container justify='flex-end' xs={4}>
                                        <Grid item>
                                            <Button 
                                                color='primary' 
                                                onClick={(e) => handleEditClick(e, event)}
                                            >
                                                Edit Shift
                                            </Button>
                                        </Grid>
                                    </Grid>
                                }
                            </Grid>
                            <Grid 
                                component={AccordionDetails} 
                                container 
                                direction='column' 
                                spacing={1} 
                                alignItems='flex-start' 
                                className='md-custom-event-cont'
                            >
                                {renderParticipants(event.participants, true)}
                            </Grid>
                        </Grid>
                    );
                } else if (event.status === 'pending') {
                    return (
                        <Grid 
                            item 
                            container 
                            direction='column' 
                            key={event.eventId} 
                            component={Paper} 
                            spacing={2} 
                            style={{marginTop: 5, marginBottom: 5, padding: 15}}
                        >
                            <Grid item container justify='space-between' alignItems='center'>
                                <Grid item>
                                    <Typography variant='h5'>{event.eventTitle}</Typography>
                                </Grid>
                                {event.category === 'personal' &&
                                    <Grid item>
                                        <Button 
                                            color='primary' 
                                            onClick={() => handleEditTimeOffClick(event.eventId)}
                                        >
                                            Edit Request
                                        </Button>
                                    </Grid>
                                }
                            </Grid>
                            <Grid item>
                                <Typography variant='subtitle1'>Status: {event.status}</Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant='body1'>
                                    {format(new Date(event.requestStart), 'dd MMM yyyy') + ' - '}
                                    {format(new Date(event.requestEnd), 'dd MMM yyyy')}
                                </Typography>
                            </Grid>
                            <Grid item container spacing={2} justify='flex-end'>
                                {(userRole === Roles.LEAD || userRole === Roles.MANAGER) && 
                                    <>
                                        <Grid item>
                                            <Button 
                                                variant='outlined' 
                                                color='primary' 
                                                onClick={() => {setShowDenialReason(true); setRequestIdToDeny(event.eventId);}} 
                                                className={classes.approveDeny}
                                            >
                                                Deny
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Button 
                                                variant='contained' 
                                                color='primary' 
                                                onClick={() => handleApproveTORequest(event.eventId)} 
                                                className={classes.approveDeny}
                                            >
                                                Approve
                                            </Button>
                                        </Grid>
                                    </>
                                }
                            </Grid>
                        </Grid>
                    );
                } else if (event.status === 'approved') {
                    return (
                        <Grid 
                            item 
                            container 
                            direction='column' 
                            key={event.eventId} 
                            component={Paper} 
                            spacing={2} 
                            style={{marginTop: 5, marginBottom: 5, padding: 15}}
                        >
                            <Grid item container justify='space-between' alignItems='center'>
                                <Grid item>
                                    <Typography variant='h5'>{event.eventTitle}</Typography>
                                </Grid>
                                {event.category === 'personal' &&
                                    <Grid item>
                                        <Button 
                                            color='primary' 
                                            onClick={() => handleDeleteRequest(event.eventId)}
                                        >
                                            Delete Request
                                        </Button>
                                    </Grid>
                                }
                            </Grid>

                            <Grid item>
                                <Typography variant='subtitle1'>Status: {event.status}</Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant='body1'>
                                    {format(new Date(event.requestStart), 'dd MMM yyyy') + ' - '}
                                    {format(new Date(event.requestEnd), 'dd MMM yyyy')}
                                </Typography>
                            </Grid>
                        </Grid>
                    );
                } else {
                    return (
                        <Grid 
                            item 
                            container 
                            direction='column' 
                            key={event.eventId} 
                            component={Paper} 
                            spacing={2} 
                            style={{marginTop: 5, marginBottom: 5, padding: 15}}
                        >
                            <Grid item container justify='space-between' alignItems='center'>
                                <Grid item>
                                    <Typography variant='h5'>{event.eventTitle}</Typography>
                                </Grid>
                                {event.type === 'softRequest' && dayObject.date > scheduleEndDate &&
                                    <Grid item>
                                        <Button 
                                            color='primary' 
                                            onClick={() => handleEditSoftRequestClick(event.eventId)}
                                        >
                                            Edit Request
                                        </Button>
                                    </Grid>
                                }
                            </Grid>
                            {event.type === 'softRequest' &&
                                <Grid item>
                                    <Typography variant='subtitle1'>
                                        Priority: {event.highPriority ? 'High' : 'Standard' }
                                    </Typography>
                                </Grid>
                            }
                        </Grid>
                    );
                }
            });
        } else if (!events && showingAll && currentSchedule) {
            return (
                <Box whiteSpace='normal' >
                    <Typography variant='h6'>
                        There are no employees scheduled today.
                    </Typography>
                </Box>
            );
        } else if (!events && currentSchedule) {
            return (
                <Box whiteSpace='normal' >
                    <Typography variant='h6'>
                        There are no employees to show for filtered shifts.
                    </Typography>
                </Box>
            );
        }
    };

    const handleGrayout = () => {
        toggleSoftRequest();
    };

    const handleBlackout = () => {
        toggleTimeOffForm();
    };

    return (  
        <>
            <Grid container direction='column' spacing={2} >
                <Grid item container justify='space-between'>
                    <Grid item>
                        <Typography variant='h3' style={{color: '#007BFF', fontWeight: 400}}>
                            {format(dayObject.date, 'dd MMM')}
                            <span style={{color: '#007BFF', fontWeight: 100}}>
                                {format(dayObject.date, ' yyyy')}
                            </span>
                        </Typography>
                    </Grid>
                    <Grid item>
                        <IconButton 
                            aria-label='close' 
                            color='secondary' 
                            size='small' 
                            onClick={closeDialog} 
                        >
                            <CloseIcon />
                        </IconButton>
                    </Grid>
                </Grid>
                <Grid 
                    item 
                    container 
                    justify='flex-end' 
                    style={{marginTop: 10, marginBottom: 10}} 
                    spacing={1}
                >
                    {currentSchedule &&
                        <>
                            <Grid item>
                                <Button 
                                    variant='outlined' 
                                    color='primary' 
                                    onClick={toggleShiftSwitchForm} 
                                    disabled={
                                        (userRole === Roles.EMPLOYEE && !userShiftDate) || 
                                        new Date(dayObject.date) <= new Date()
                                    } 
                                    className={classes.button}
                                >
                                    Switch Shifts
                                </Button>
                            </Grid>
                            {allowCallIns && 
                                <Grid item>
                                    <Button 
                                        variant='outlined' 
                                        color='primary' 
                                        onClick={() => handleCallInClick(dayObject.date)} 
                                        disabled={
                                            !userShiftDate ||
                                            new Date(dayObject.date) <= new Date()
                                        } 
                                        className={classes.button}
                                    >
                                        Call In
                                    </Button>
                                </Grid>
                            }
                        </>
                    }
                    {!currentSchedule && personalOffRequests.length === 0 
                    && invalidDateForRequest && !deleteLoading &&
                        <Box whiteSpace='normal' >
                            <Typography variant='h6'>
                                You have already made a time off request for this date.
                            </Typography>
                        </Box>
                    }
                    {deleteLoading && <CircularProgress />}
                    {!currentSchedule && personalOffRequests.length === 0 
                    && !invalidDateForRequest &&
                        <>
                            <Grid item>
                                <Button 
                                    variant='outlined' 
                                    color='primary' 
                                    onClick={handleGrayout} 
                                    className={classes.button}
                                >
                                    Soft Time Off
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button 
                                    variant='outlined' 
                                    color='primary' 
                                    onClick={handleBlackout} 
                                    className={classes.button}
                                >
                                    Time Off Request
                                </Button>
                            </Grid>
                        </>
                    }              
                </Grid>
                {renderEventContent()}
            </Grid>
            <Dialog open={showDenialReason} fullWidth maxWidth='xs'>
                <DialogContent style={{ padding: 30 }}>
                    <Grid container direction='column' spacing={2}>
                        <Grid item >
                            <Typography variant='h2'>Reason for Denial</Typography>
                        </Grid>
                        <Grid item >
                            <TextField
                                variant='outlined'
                                value={denialReason}
                                onChange={e => setDenialReason(e.target.value)}
                                className={classes.input}
                                onKeyPress={e => {e.key === 'Enter' && handleDenyTORequest(requestIdToDeny);}}
                            />
                        </Grid>
                        <Grid item container justify='flex-end' spacing={1}>
                            <Grid item>
                                <Button 
                                    variant='contained' 
                                    color='primary' 
                                    className={classes.button} 
                                    onClick={() => handleDenyTORequest(requestIdToDeny)}
                                >
                                    Submit
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button 
                                    variant='outlined' 
                                    color='primary' 
                                    className={classes.button} 
                                    onClick={() => {setShowDenialReason(false); setDenialReason(''); setRequestIdToDeny();}}
                                >
                                    Cancel
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </DialogContent>
            </Dialog>
        </>
    );
};
 
export default EmpDayActions;