import React, { useEffect, useState } from "react";
import { Switch, useParams } from "react-router-dom";
// import { CSVLink } from "react-csv";
import "./Calendar.css";
import {
  Typography,
  Button,
  Grid,
  Paper,
  makeStyles,
  DialogContent,
  Dialog,
  Popover,
  Snackbar,
  CircularProgress,
  Badge,
  MenuItem,
  Box,
  IconButton,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import TuneIcon from "@material-ui/icons/Tune";
import PrintIcon from "@material-ui/icons/Print";
import GetAppIcon from "@material-ui/icons/GetApp";
import CloseIcon from "@material-ui/icons/Close";
import ShiftSwitchRequestForm from "../shiftSwitch/ShiftSwitchRequestForm";
import MangShiftSwitchApproval from "../shiftSwitch/MangShiftSwitchApproval";
import MangTimeOffApproval from "./MangTimeOffApproval";
import EmpCallInForm from "./EmpCallInForm";
// import ShiftEvent from "./ShiftEvent";
import SoftTimeOff from "./SoftTimeOff";
import EditSoftTimeOff from "./EditSoftTimeOff";
import TimeOffRequestForm from "./TimeOffRequestForm";
import EditTimeOffRequestForm from "./EditTimeOffRequestForm";
import Roles from "../../Roles/roles";
import MangEditShift from "./MangEditShift";
import {
  formatTimeOff,
  formatSoftRequests,
  orderShifts,
  formatMissions,
} from "../../helpers/formatEvents";
import {
  format,
  add,
  sub,
  startOfMonth,
  endOfMonth,
  startOfWeek,
  lastDayOfWeek,
  eachDayOfInterval,
  isSameDay,
} from "date-fns";
import FilterMenu from "./FilterMenu";
import Scheduler from "./Scheduler";
import { useLazyQuery, useQuery, useReactiveVar } from "@apollo/client";
import { filterListVar, userVar, selectedDateVar } from "../../cache";
import {
  GET_SHIFTS,
  SCHEDULE_FITNESS,
  GET_SLACK,
  GET_EMPLOYEE_NAMES,
  GET_MISSIONS,
  GET_MORE_SHIFTS,
} from "../../api/gqlQueries";

const useStyles = makeStyles(() => ({
  headerSpacing: {
    marginTop: 30,
    marginBottom: 20,
  },
  label: {
    fontSize: 12,
    marginTop: -7,
  },
  tabs: {
    minWidth: 100,
    width: 125,
  },
  downloadPopover: {
    textAlign: "left",
  },
}));

function EmpCalendar({ schedulePeriods, refetchSchedulePeriods }) {
  const classes = useStyles();

  const user = userVar();
  const selectedDate = useReactiveVar(selectedDateVar);
  // const [dateSelected, SetDateSelected] = useState(""); // for schedule fitness display only
  const primaryUnit = user.office;
  const userRole = user.role;

  const managerAccess =
    userRole === Roles.MANAGER ||
    userRole === Roles.SCHEDULER ||
    userRole === Roles.ADMIN;

  const notViewable = schedulePeriods.find(
    (schedule) =>
      schedule.isUnderReview === false && schedule.isPublished === false
  );

  const releasedSchedules = schedulePeriods.filter(
    (schedule) =>
      schedule.isPublished === true || schedule.isUnderReview === true
  );

  const allSchedules = [...schedulePeriods];

  const mostRecentReleased =
    releasedSchedules.length > 0
      ? releasedSchedules.sort(
          (a, b) => new Date(b.start) - new Date(a.start)
        )[0]
      : null;

  const mostRecentIncludingDraft =
    allSchedules.length > 0
      ? allSchedules.sort((a, b) => new Date(b.start) - new Date(a.start))[0]
      : null;

  let draftStart;
  let draftEnd;
  if (mostRecentIncludingDraft) {
    if (mostRecentIncludingDraft.isUnderReview === true) {
      draftStart = new Date(`${mostRecentIncludingDraft.start}T12:00:00`);
      draftEnd = sub(new Date(`${mostRecentIncludingDraft.end}T12:00:00`), {
        hours: 12,
      });
    } else if (notViewable && userRole === Roles.MANAGER) {
      draftStart = new Date(`${notViewable.start}T12:00:00`);
      draftEnd = sub(new Date(`${notViewable.end}T12:00:00`), { hours: 12 });
    }
  }

  allSchedules.sort((a, b) => new Date(b.start) - new Date(a.start));
  // const scheduleStartDate =
  //   allSchedules.length > 0
  //     ? allSchedules[allSchedules.length - 1].start
  //     : null;
  const scheduleEndDate = allSchedules.length > 0 ? allSchedules[0].end : null;

  function getVariables(startDate, endDate) {
    let variables;
    if (notViewable && userRole === Roles.MANAGER) {
      // const rangeEnd = `${notViewable.end}T23:00:00`;
      variables = {
        officeId: parseInt(primaryUnit.id),
        employeeId: parseInt(user.id),
        rangeStart: startDate.toISOString(),
        rangeEnd: endDate.toISOString(),
        // rangeEnd: rangeEnd
      };
    } else if (mostRecentReleased) {
      const rangeEnd = `${mostRecentReleased.end}T23:00:00`;

      let end;
      if (new Date(rangeEnd) < endDate) {
        end = rangeEnd;
      } else {
        end = endDate.toISOString();
      }

      variables = {
        officeId: parseInt(primaryUnit.id),
        employeeId: parseInt(user.id),
        rangeStart: startDate.toISOString(),
        rangeEnd: end,
        // rangeEnd: rangeEnd,
      };
    } else if (!mostRecentReleased && notViewable) {
      const rangeEnd = `${notViewable.start}T08:00:00`;

      let end;
      if (new Date(rangeEnd) < endDate) {
        end = rangeEnd;
      } else {
        end = endDate.toISOString();
      }

      variables = {
        officeId: parseInt(primaryUnit.id),
        employeeId: parseInt(user.id),
        // rangeEnd: rangeEnd,
        rangeStart: startDate.toISOString(),
        rangeEnd: end,
      };
    } else {
      variables = {
        officeId: parseInt(primaryUnit.id),
        employeeId: parseInt(user.id),
        rangeStart: startDate.toISOString(),
        rangeEnd: endDate.toISOString(),
      };
    }
    return variables;
  }

  const today = new Date();
  let firstDayOfCurrentView = startOfMonth(today);
  firstDayOfCurrentView = startOfWeek(firstDayOfCurrentView);
  let lastDayOfCurrentView = endOfMonth(today);
  lastDayOfCurrentView = lastDayOfWeek(lastDayOfCurrentView);
  lastDayOfCurrentView = add(lastDayOfCurrentView, { days: 1 });

  const variablesForInitialQuery = getVariables(
    firstDayOfCurrentView,
    lastDayOfCurrentView
  );

  const oneYearAgo = sub(new Date(), { years: 1 });
  const sixMonthsFromNow = add(new Date(), { months: 6 });

  const { loading, error, data, refetch } = useQuery(GET_SHIFTS, {
    variables: variablesForInitialQuery,
    onCompleted() {
      const futureVariables = getVariables(
        sub(lastDayOfCurrentView, { days: 1 }),
        sixMonthsFromNow
      );
      const pastVariables = getVariables(
        oneYearAgo,
        add(firstDayOfCurrentView, { days: 1 })
      );
      getFutureShifts({
        variables: futureVariables,
      });
      getPastShifts({
        variables: pastVariables,
      });
    },
  });

  const [getFutureShifts, { data: futureData }] = useLazyQuery(
    GET_MORE_SHIFTS,
    {
      onError(error) {
        console.error(error);
      },
    }
  );

  const [missioneventsLoaded, SetMissioneventsLoaded] = useState(false);

  const missionVariables = getVariables(
    oneYearAgo,
    sixMonthsFromNow
  );
  const missionEnd = missionVariables.rangeEnd.split('T')[0];

  const { data: missionData } = useQuery(GET_MISSIONS, {
    variables: {
      office: parseInt(primaryUnit.id),
      end: missionEnd
    },
    onError(error) {
      console.error(error);
    },
  });

  const { data: employeeData } = useQuery(GET_EMPLOYEE_NAMES, {
    variables: { office: parseInt(primaryUnit.id) },
    onError(error) {
      console.error(error);
    },
  });

  const [getPastShifts, { data: pastData }] = useLazyQuery(GET_MORE_SHIFTS, {
    onError(error) {
      console.error(error);
    },
  });

  const ref = React.useRef();
  const params = useParams();

  const filterList = useReactiveVar(filterListVar);
  const allFilters = filterList.allFilters;

  const [scheduleData, setScheduleData] = useState({
    formatted: [],
    userShifts: [],
    shiftNames: [],
  });
  const [allSlackEvents, setAllSlackEvents] = useState([]);
  const [allSlacks, setAllSlacks] = useState([]);
  const [slackIssues, setSlackIssues] = useState([]);
  const [issueDates, setIssueDates] = useState([]);
  const [view, setView] = useState("month");
  const [shiftSwitch, setShiftSwitch] = useState(false);
  const [callIn, setCallIn] = useState(false);
  const [showDayActions, setShowDayActions] = useState(false);
  const [selectedDay, setSelectedDay] = useState({
    date: new Date(),
    events: [],
    currentSchedule: true,
  });
  const [currentSchedule, setCurrentSchedule] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);
  const [shiftSwitchApproval, setShiftSwitchApproval] = useState(false);
  const [timeOffApproval, setTimeOffApproval] = useState(false);
  const [showShiftView, setShowShiftView] = useState(true);
  const [editShift, setEditShift] = useState(false);
  const [shiftToEdit, setShiftToEdit] = useState();
  const [shiftToChange, setShiftToChange] = useState();
  const [softRequest, setSoftRequest] = useState(false);
  const [softRequestIdToEdit, setSoftRequestIdToEdit] = useState();
  const [editSoftRequest, setEditSoftRequest] = useState(false);
  const [timeOffRequestIdToEdit, setTimeOffRequestIdToEdit] = useState();
  const [timeOffRequestForm, setTimeOffRequestForm] = useState(false);
  const [editTimeOffRequest, setEditTimeOffRequest] = useState(false);
  const [openDownload, setopenDownload] = useState(null);
  const [toast, setToast] = useState("");
  const [showToast, setShowToast] = useState(false);
  const [errorToast, setErrorToast] = useState("");
  const [showErrorToast, setShowErrorToast] = useState(false);
  const [addRequestPopoverAnchor, setAddRequestPopoverAnchor] = useState(null);
  const openD = Boolean(openDownload);
  const anchorId = openD ? "download-popover" : undefined;
  // const [openPrint, setOpenPrint] = useState(false);
  // const documentTitle = `${primaryUnit.name} - Schedule`;
  const [employeeResources, setEmployeeResources] = useState([]);
  const [missionResources, setMissionResources] = useState([
    { name: "GHOC", id: "GHOC", color: "#DFE0E2" },
  ]);
  const [missionEvents, setMissionEvents] = useState([]);

  const [allFitnesses, SetallFitnesses] = useState([]);
  const [empFitness, SetempFitness] = useState({
    score: "",
    maxScore: "",
    ratio: "",
  });

  const [scheduleFitness] = useLazyQuery(SCHEDULE_FITNESS, {
    onCompleted(data) {
      SetallFitnesses(
        data.employeeFitnesses.filter((e) => e.employee.id === user.id)
      );
    },
    onError(error) {
      console.error(error);
    },
  });

  useEffect(() => {
    let a =
      allSchedules.length > 0 &&
      allSchedules.find(
        (x) =>
          new Date(x.start) <= new Date(selectedDate) &&
          new Date(x.end) >= new Date(selectedDate)
      );
    if (a) {
      scheduleFitness({
        schedulePeriod: parseInt(a.id),
      });
    }
  }, []);

  useEffect(() => {
    let a =
      allSchedules.length > 0 &&
      allSchedules.find(
        (x) =>
          new Date(x.start) <= new Date(selectedDate) &&
          new Date(x.end) >= new Date(selectedDate)
      );
    if (a) {
      scheduleFitness({
        schedulePeriod: parseInt(a.id),
      });
    } else {
      SetempFitness({
        score: "",
        maxScore: "",
        ratio: "",
      });
    }
  }, [selectedDate]);

  useEffect(() => {
    let emp = allFitnesses.find(
      (x) =>
        new Date(x.schedulePeriod.start) <= new Date(selectedDate) &&
        new Date(x.schedulePeriod.end) >= new Date(selectedDate)
    );
    if (emp) {
      SetempFitness({
        score: emp.score,
        maxScore: emp.maxScore,
        ratio: emp.ratio,
      });
    } else {
      SetempFitness({
        score: "",
        maxScore: "",
        ratio: "",
      });
    }
  }, [allFitnesses, selectedDate]);

  const [getSlack] = useLazyQuery(GET_SLACK, {
    onCompleted(data) {
      // const acuityIssues = data.acuitySlacks.filter(
      //   (slack) => slack.assigned > slack.upper || slack.assigned < slack.lower
      // );
      // const slackIssues = data.slacks.filter((slack) => slack.slack < 0);
      setSlackIssues([...data.acuitySlacks, ...data.slacks]);

      let issueDates = [];
      data.acuitySlacks.forEach((acuity) => {
        const start = format(new Date(acuity.start), "MM/dd/yyyy");
        !issueDates.includes(start) && issueDates.push(start);
      });
      data.slacks.forEach((slack) => {
        const start = format(new Date(slack.start), "MM/dd/yyyy");
        !issueDates.includes(start) && issueDates.push(start);
      });
      setIssueDates(issueDates);

      // let datesWithSlackInfo = [];
      // data.slacks.forEach((slack) => {
      //   const start = format(new Date(slack.start), "yyyy-MM-dd");
      //   !datesWithSlackInfo.includes(start) && datesWithSlackInfo.push(start);
      // });
      // const slackEvents = datesWithSlackInfo.map((dateString) => {
      //   const date = new Date(`${dateString}T08:00:00`);
      //   date.setHours(0, 0, 0, 0);
      //   return {
      //     eventId: `${dateString}-slack`,
      //     type: "staffing",
      //     start: date,
      //     color: "rgba(150,150,150,0)",
      //   };
      // });
      // setAllSlackEvents(slackEvents);
      // setAllSlacks([...data.acuitySlacks, ...data.slacks]);
    },
  });

  useEffect(() => {
    const currentAndFutureSchedules = schedulePeriods.filter(
      (schedule) => new Date(`${schedule.end}T23:00:00`) > new Date()
    );

    currentAndFutureSchedules.sort(
      (a, b) =>
        new Date(`${a.start}T23:00:00`) - new Date(`${b.start}T23:00:00`)
    );

    if (currentAndFutureSchedules.length > 0) {
      const rangeStart = currentAndFutureSchedules[0].start + "T00:00:00";
      const rangeEnd =
        currentAndFutureSchedules[currentAndFutureSchedules.length - 1].end +
        "T23:00:00";

      if (userRole === Roles.MANAGER || userRole === Roles.SCHEDULER) {
        getSlack({
          variables: {
            rangeStart: rangeStart,
            rangeEnd: rangeEnd,
            office: parseInt(primaryUnit.id),
          },
        });
      }
    }
  }, []);

  useEffect(() => {
    if (employeeData) {
      addEmployeeData(employeeData);
    }
  }, [employeeData]);

  useEffect(() => {
    if (missionData) {
      addMissionData(missionData);
    }
  }, [missionData]);

  const combineGhocShifts = (events) => {
    const nonGhoc = events.filter((event) => event.eventTitle !== "GHOC");
    const ghoc = events.filter((event) => event.eventTitle === "GHOC");

    const noDuplicates = [];
    ghoc.forEach((event) => {
      const exists = noDuplicates.find(
        (newEvent) => event.eventId === newEvent.eventId
      );
      if (exists) {
        const newIds = event.employeeIds.filter(
          (id) => !exists.employeeIds.includes(id)
        );
        exists.employeeIds = [...exists.employeeIds, ...newIds];
      } else {
        noDuplicates.push(event);
      }
    });

    return [...nonGhoc, ...noDuplicates];
  };

  const addDataToCurrentScheduleData = (data) => {
    let currentScheduleData = { ...scheduleData };
    let userShifts = [...currentScheduleData.userShifts];
    let shiftIds = currentScheduleData.formatted.map((event) => event.eventId);
    let userShiftIds = userShifts.map((shift) => shift.shift.id);

    const newEvents = data.allShifts.filter(
      (shift) => !shiftIds.includes(shift.id)
    );
    const newIds = newEvents.map((event) => event.id);

    shiftIds = [...shiftIds, ...newIds];

    const newData = orderShifts(newEvents);
    currentScheduleData.formatted = [
      ...currentScheduleData.formatted,
      ...newData.formatted,
    ];
    currentScheduleData.formatted.sort((a, b) => a.start - b.start);
    currentScheduleData.formatted = combineGhocShifts(
      currentScheduleData.formatted
    );

    const newShiftNames = newData.shiftNames.filter(
      (shift) => !currentScheduleData.shiftNames.includes(shift)
    );

    currentScheduleData.shiftNames = [
      ...currentScheduleData.shiftNames,
      ...newShiftNames,
    ];
    currentScheduleData.shiftNames.sort();

    const newUserShifts = data.userShifts.filter(
      (shift) => !userShiftIds.includes(shift.shift.id)
    );
    const newUserShiftIds = newUserShifts.map((shift) => shift.shift.id);

    userShifts = [...userShifts, ...newUserShifts];
    userShiftIds = [...userShiftIds, ...newUserShiftIds];

    currentScheduleData.userShifts = userShifts;
    return currentScheduleData;
  };

  const addEmployeeData = (data) => {
    const resources = data.offices[0].employeeSet.map((employee) => {
      if (parseInt(employee.id) === parseInt(user.id)) {
        return {
          id: employee.id,
          name: `${employee.firstName} ${employee.lastName}`,
          color: "#F5E1DB",
          skills: employee.skills.map((e) => e.name),
        };
      } else {
        return {
          id: employee.id,
          name: `${employee.firstName} ${employee.lastName}`,
          color: "#DFE0E2",
          skills: employee.skills.map((e) => e.name),
        };
      }
    });
    setEmployeeResources(resources);
  };

  const addMissionData = (data) => {
    const formattedData = formatMissions(
      data.procedureRequirements, 
      missionResources, 
      missionEvents
    );
    
    setMissionResources(formattedData.currentMissionResources);
    setMissionEvents(formattedData.currentMissionEvents);
    SetMissioneventsLoaded(true);
  };

  useEffect(() => {
    if (data) {
      let newScheduleData = orderShifts(data.allShifts);
      newScheduleData.userShifts = [...data.userShifts];
      newScheduleData = { ...scheduleData, ...newScheduleData };
      setScheduleData(newScheduleData);
    }
  }, [data]);

  useEffect(() => {
    if (futureData) {
      const newScheduleData = addDataToCurrentScheduleData(futureData);
      setScheduleData(newScheduleData);
    }
  }, [futureData]);

  useEffect(() => {
    if (pastData) {
      const newScheduleData = addDataToCurrentScheduleData(pastData);
      setScheduleData(newScheduleData);
    }
  }, [pastData]);

  useEffect(() => {
    if (
      (userRole === Roles.MANAGER || userRole === Roles.LEAD) &&
      params.request
    ) {
      if (params.request === "time-off-calendar") {
        setTimeOffApproval(false);
        setShiftSwitchApproval(false);

        const pendingRequests = data
          ? data.timeOff.filter((request) => request.approvedby === null)
          : [];
        const earliestRequest =
          pendingRequests.length > 0
            ? pendingRequests.sort(
                (a, b) => new Date(a.firstday) - new Date(b.firstday)
              )[0]
            : null;
        const earliestDate = earliestRequest
          ? new Date(`${earliestRequest.firstday}T08:00:00`)
          : new Date();

        selectedDateVar(earliestDate);
        setView("Month");

        filterListVar({
          allFilters: ["All Shifts", "Pending Time Off"],
          shiftsFilters: ["All Shifts"],
          missionFilters: [],
          requestsFilters: ["Pending Time Off"],
        });
      } else if (params.request === "time-off") {
        showTimeOffApproval();
      } else if (params.request === "shift-switch") {
        showShiftSwitchApproval();
      }
    } else if (params.date) {
      const showDate = new Date(params.date);
      selectedDateVar(showDate);
      refetchSchedulePeriods();
      if (userRole === Roles.MANAGER) {
        filterListVar({
          allFilters: ["All Shifts", "All Missions"],
          shiftsFilters: ["All Shifts"],
          missionFilters: ["All Missions"],
          requestsFilters: [],
        });
      } else {
        filterListVar({
          allFilters: [
            "Personal",
            "All Missions",
            "Soft Requests",
            "Pending Time Off",
            "Approved Time Off",
            "All Requests",
          ],
          shiftsFilters: ["Personal"],
          missionFilters: ["All Missions"],
          requestsFilters: [
            "Soft Requests",
            "Pending Time Off",
            "Approved Time Off",
            "All Requests",
          ],
        });
      }
      getFutureShifts({
        variables: {
          officeId: parseInt(primaryUnit.id),
          employeeId: parseInt(user.id),
          rangeStart: params.date,
        },
      });
    }
  }, []);

  const toggleShiftSwitchForm = () => setShiftSwitch(!shiftSwitch);
  const toggleCallInForm = () => setCallIn(!callIn);
  const toggleEditShift = () => setEditShift(!editShift);
  const toggleSoftRequest = () => setSoftRequest(!softRequest);
  const toggleTimeOffForm = () => setTimeOffRequestForm(!timeOffRequestForm);
  const toggleEditTimeOffForm = () =>
    setEditTimeOffRequest(!editTimeOffRequest);
  const toggleEditSoftRequestForm = () => setEditSoftRequest(!editSoftRequest);

  const showShiftSwitchApproval = () => {
    setShowDayActions(false);
    setShiftSwitchApproval(true);
    setTimeOffApproval(false);
  };

  const showTimeOffApproval = () => {
    setShowDayActions(false);
    setTimeOffApproval(true);
    setShiftSwitchApproval(false);
  };

  const handleEditClick = (shift) => {
    setShiftToEdit(shift);
    toggleEditShift();
  };

  // const handleCallInClick = (date) => {
  //   selectedDateVar(date);
  //   toggleCallInForm();
  // };

  const toastMessage = () => {
    const toasts = {
      "Employee Shift Switch": "Shift switch request sent!",
      "Manager Shift Switch": "Shift switch successful! Employees notified.",
      "New Time Off Request": "Time off request sent for approval!",
      "Edit Time Off Request":
        "Time off request updated and sent for approval!",
      "Delete Time Off Request": "Time off request deleted!",
      "New Soft Request": "Soft request entered successfully!",
      "Edit Soft Request": "Soft request updated successfully!",
      "Delete Soft Request": "Soft request deleted!",
      "Call In": "Call in successful!",
      "Manager Edit Shift": "Shift updated and employees notified!",
      "Manager Approve Time Off Request": "Time off request approved!",
      "Manager Deny Time Off Request": "Time off request denied!",
      "Manager Approve Shift Switch": "Shift switch request approved!",
      "Manager Deny Shift Switch": "Shift switch request denied!",
    };
    return toasts[toast] ? toasts[toast] : "Success!";
  };

  const errorToastMessage = () => {
    const errorToasts = {
      "Non Eligible Shift Switch": "No eligible shifts to switch.",
      "Error Edit Shift": "Unable to edit shift. Please try again.",
      "Error Call In": "Unable to call in. Please try again.",
      "Error Approve Time Off":
        "Unable to approve time off request. Please try again.",
      "Error Deny Time Off":
        "Unable to deny time off request. Please try again.",
      "Error Approve Shift Switch":
        "Unable to approve shift switch request. Please try again.",
      "Error Deny Shift Switch":
        "Unable to deny shift switch request. Please try again.",
      "Error Delete Soft Request":
        "Unable to delete soft request. Please try again.",
      "Error Delete Time Off":
        "Unable to delete time off request. Please try again.",
    };
    return errorToasts[errorToast]
      ? errorToasts[errorToast]
      : "Error. Please try again.";
  };

  if (loading || !missionData || !employeeData) {
    return <CircularProgress color="primary" />;
  } else if (error) {
    console.log(error);
    return <Typography>Something went wrong. Please try again.</Typography>;
  } else {
    const events = [...scheduleData.formatted, ...missionEvents];
    const userEvents = events.filter(
      (event) => event.employeeIds && event.employeeIds.includes(user.id)
    );

    const shiftNames = scheduleData.shiftNames ? scheduleData.shiftNames : [];

    const allTimeOffRequests = data.timeOff.filter(
      (timeOff) =>
        timeOff.deniedText === null && timeOff.userCancelled === false
    );

    const allTimeOffEvents = formatTimeOff(allTimeOffRequests, user.id);

    const userTimeOff = allTimeOffEvents.filter(
      (event) => event.category === "personal"
    );

    const userSoftRequests = formatSoftRequests(data.softRequests, user.id);

    let invalidDates = [];
    userSoftRequests.forEach((request) => {
      invalidDates.push(request.start);
    });

    userTimeOff.forEach((request) => {
      const arrayOfTimeOff = eachDayOfInterval({
        start: request.start,
        end: request.end,
      });
      invalidDates = [...invalidDates, ...arrayOfTimeOff];
    });

    invalidDates = invalidDates.map((date) => format(date, "MM/dd/yyyy"));

    const maxGreyout = data.officeInfo[0].maxGreyoutRequests;
    // const allowCallIns = data.officeInfo[0].allowCallIns;
    const schedulePeriodWeeks = data.officeInfo[0].scheduleDuration
      ? data.officeInfo[0].scheduleDuration
      : 4;

    const toggleEditRequest = (type, id) => {
      if (type === "softRequest") {
        const matchingRequest = userSoftRequests.find(
          (request) => parseInt(request.eventId) === parseInt(id)
        );
        if (matchingRequest) {
          setSoftRequestIdToEdit(matchingRequest.eventId);
          setEditSoftRequest(true);
        }
      } else {
        const matchingRequest = managerAccess
          ? allTimeOffEvents.find(
            (request) => parseInt(request.eventId) === parseInt(id)
          )
          : userTimeOff.find(
            (request) => parseInt(request.eventId) === parseInt(id)
          );
        if (matchingRequest) {
          setTimeOffRequestIdToEdit(matchingRequest.eventId);
          setEditTimeOffRequest(true);
        }
      }
    };

    // const handleDayClick = (e) => {
    //   // if (view === 0) {
    //   //   setShowDayActions(true);
    //   // }
    //   changeView({}, 2);

    //   const eventDate = new Date(e.date);
    //   eventDate.setHours(0, 0, 0, 0);

    //   const startDate = new Date(scheduleStartDate);
    //   startDate.setHours(0, 0, 0, 0);

    //   const endDate = new Date(scheduleEndDate);
    //   endDate.setHours(0, 0, 0, 0);

    //   const newCurrentSchedule = Boolean(
    //     eventDate >= startDate && eventDate <= endDate
    //   );

    //   setSelectedDay({
    //     date: e.date,
    //     events: e.events,
    //     currentSchedule: newCurrentSchedule,
    //   });
    //   selectedDateVar(e.date);
    //   setCurrentSchedule(newCurrentSchedule);

    //   let emp =
    //     allFitnesses.length > 0 &&
    //     allFitnesses.find(
    //       (x) =>
    //         new Date(x.schedulePeriod.start) <= new Date(e.date) &&
    //         new Date(x.schedulePeriod.end) >= new Date(e.date)
    //     );
    //   if (emp) {
    //     SetempFitness({
    //       score: emp.score,
    //       maxScore: emp.maxScore,
    //       ratio: emp.ratio,
    //     });
    //   } else {
    //     SetempFitness({
    //       score: "",
    //       maxScore: "",
    //       ratio: "",
    //     });
    //   }
    // };
    //test

    function eventsToView() {
      let eventsToView = [];
      let requests = [];
      if (allFilters) {
        if (allFilters.includes("All Shifts")) {
          eventsToView = [...events];

          if (allFilters.includes("All Requests")) {
            requests = [...userSoftRequests, ...allTimeOffEvents];
          } else {
            if (allFilters.includes("Soft Requests")) {
              requests.push(userSoftRequests);
            }
            if (allFilters.includes("Pending Time Off")) {
              const allPending = allTimeOffEvents.filter(
                (event) => event.status === "pending"
              );
              requests.push(allPending);
            }
            if (allFilters.includes("Approved Time Off")) {
              const allApproved = allTimeOffEvents.filter(
                (event) => event.status === "approved"
              );
              requests.push(allApproved);
            }
            requests = requests.flat();
          }
        } else if (allFilters.includes("Personal")) {
          // const userMissionEvents = missionEvents.filter((event) =>
          //   event.employeeIds.includes(user.id)
          // );

          eventsToView = [...userEvents];

          if (allFilters.includes("All Requests")) {
            requests = [...userSoftRequests, ...userTimeOff];
          } else {
            if (allFilters.includes("Soft Requests")) {
              requests.push(userSoftRequests);
            }
            if (allFilters.includes("Pending Time Off")) {
              const userPending = userTimeOff.filter(
                (event) => event.status === "pending"
              );
              requests.push(userPending);
            }
            if (allFilters.includes("Approved Time Off")) {
              const userApproved = userTimeOff.filter(
                (event) => event.status === "approved"
              );
              requests.push(userApproved);
            }
            requests = requests.flat();
          }
        }

        if (!allFilters.includes("All Missions")) {
          const filteredList = [];
          const missionNames = missionResources.map((mission) => mission.name);
          missionNames.forEach((name) => {
            if (allFilters.includes(name)) {
              const filtered = eventsToView.filter(
                (event) => event.missionId === name || event.eventTitle === name
              );
              filteredList.push(filtered);
            }
          });
          eventsToView = filteredList.flat();
        }
      }

      const filtered = eventsToView.filter((event) =>
        event.calendars.includes(view)
      );
      return [...filtered, ...requests];
    }

    const PrintIconClick = () => {
      ref.current.print();
    };

    const ExportToICS = () => {
      const scheduleObj =
        document.querySelector(".e-schedule").ej2_instances[0];
      scheduleObj.exportToICalendar();
    };

    const ExportToExcel = () => {
      const scheduleObj =
        document.querySelector(".e-schedule").ej2_instances[0];
      let exportValues = {
        dataSource: events,
      };
      let a =
        allSchedules.length > 0 &&
        allSchedules.find(
          (x) =>
            new Date(x.start) <= new Date() && new Date(x.end) >= new Date()
        );
      scheduleObj.exportToExcel(events);
    };

    const hideFilterBadge = Boolean(
      allFilters.includes("All Shifts") &&
        allFilters.includes("All Missions") &&
        allFilters.includes("All Requests")
    );

    const environment = process.env.NODE_ENV;
    let backend = "";
    if (environment === "development") {
      backend = "https://backendtest.balancescheduler.com";
    } else {
      backend = "https://backenddemo.balancescheduler.com";
    }
    return (
      <>
        <Grid
          container
          justify="space-between"
          className={classes.headerSpacing}
        >
          <Grid item xs={5}>
            <Typography variant="h3">Schedule</Typography>
          </Grid>
          {(userRole === Roles.MANAGER || userRole === Roles.LEAD) && (
            <Grid item container justify="flex-end" spacing={2} xs={6}>
              <Grid item>
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={showTimeOffApproval}
                  style={{ width: 178 }}
                >
                  Time Off Requests
                </Button>
              </Grid>
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={showShiftSwitchApproval}
                  style={{ width: 178 }}
                >
                  Shift Switch Requests
                </Button>
              </Grid>
            </Grid>
          )}
        </Grid>
        <div className="col-lg-3 property-section">
          <Grid container direction="row">
            <Grid item container justify="flex-start" xs={4}>
              {/* {view === 1 &&
                currentSchedule &&
                !shiftSwitchApproval &&
                !timeOffApproval && (
                  <Grid item>
                    <Button color="primary" onClick={toggleShiftSwitchForm}>
                      Switch Shifts
                    </Button>
                  </Grid>
                )}
              {!currentSchedule && !shiftSwitchApproval && !timeOffApproval && (
                <>
                  <Grid item>
                    <Button color="primary" onClick={toggleSoftRequest}>
                      Soft Request
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button color="primary" onClick={toggleTimeOffForm}>
                      Time Off
                    </Button>
                  </Grid>
                </>
              )} */}
            </Grid>
            <Grid item xs={4}>
              <Box style={{ textAlign: "center" }}>
                {empFitness.ratio === ""
                  ? ""
                  : "Your schedule quality for this schedule period: " +
                    empFitness.ratio.toFixed(2) * 100 +
                    "%"}
              </Box>
            </Grid>
            <Grid item container justify="flex-end" xs={4}>
              <Grid item>
                <Button
                  color="secondary"
                  aria-describedby={anchorId}
                  onClick={(event) => setopenDownload(event.currentTarget)}
                >
                  <GetAppIcon style={{ marginRight: 5 }} /> Download
                </Button>
                <Popover
                  id={anchorId}
                  anchorEl={openDownload}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "right",
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  open={openD}
                  onClose={() => setopenDownload(null)}
                  style={{ overflow: "auto" }}
                >
                  {/* <MenuItem
                    color="secondary"
                    target="_blank"
                    filename="schedule.csv"
                    onClick={ExportToExcel}
                    className={classes.downloadPopover}
                    style={{ marginBottom: -8 }}
                  >
                    Export to CSV File
                  </MenuItem> 
                  <br /> */}
                  <MenuItem
                    color="secondary"
                    target="_blank"
                    // href={`${backend}/scheduler/employee_calendar/${parseInt(
                    //   user.id
                    // )}`}
                    onClick={ExportToICS}
                    className={classes.downloadPopover}
                    style={{ marginTop: -8 }}
                  >
                    Export to my Calendar
                  </MenuItem>
                </Popover>
              </Grid>
              <Grid item>
                <Button id="print" color="secondary" onClick={PrintIconClick}>
                  <PrintIcon style={{ marginRight: 5 }} /> Print
                </Button>
              </Grid>
              <Grid item>
                <Button
                  color="secondary"
                  onClick={(e) => setAnchorEl(e.currentTarget)}
                >
                  <Badge
                    variant="dot"
                    color="primary"
                    style={{ marginRight: 5 }}
                    invisible={hideFilterBadge}
                  >
                    <TuneIcon />
                  </Badge>
                  Filter
                </Button>
                <Popover
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "right",
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={() => setAnchorEl(null)}
                  style={{ overflow: "auto" }}
                >
                  <Paper style={{ padding: 10 }}>
                    <FilterMenu
                      // setShowDayActions={setShowDayActions}
                      missionNames={missionResources.map(
                        (mission) => mission.name
                      )}
                    />
                  </Paper>
                </Popover>
              </Grid>
            </Grid>
          </Grid>
        </div>
        {missioneventsLoaded ? (
          !shiftSwitchApproval &&
          !timeOffApproval && (
            <Scheduler
              events={eventsToView()}
              draftStart={draftStart}
              draftEnd={draftEnd}
              employeeResources={employeeResources}
              missionResources={missionResources}
              view={view}
              setView={setView}
              scheduleEndDate={scheduleEndDate}
              toggleEditRequest={toggleEditRequest}
              toggleTimeOffForm={toggleTimeOffForm}
              toggleCallInForm={toggleCallInForm}
              toggleShiftSwitchForm={toggleShiftSwitchForm}
              issueDates={issueDates}
              slackIssues={slackIssues}
              ref={ref}
              // SetDateSelected={SetDateSelected}
              handleEditClick={handleEditClick}
              setShiftToChange={setShiftToChange}
            />
          )
        ) : (
          <div>Loading calendar events...</div>
        )}
        {shiftSwitchApproval && (
          <MangShiftSwitchApproval
            closeView={() => setShiftSwitchApproval(false)}
            setToast={setToast}
            setShowToast={setShowToast}
            setErrorToast={setErrorToast}
            setShowErrorToast={setShowErrorToast}
            refetch={refetch}
          />
        )}
        {timeOffApproval && (
          <MangTimeOffApproval
            // changeView={changeView}
            closeView={() => setTimeOffApproval(false)}
            timeOffRequests={allTimeOffRequests}
            setToast={setToast}
            setShowToast={setShowToast}
            setErrorToast={setErrorToast}
            setShowErrorToast={setShowErrorToast}
          />
        )}
        <Dialog open={shiftSwitch} fullWidth maxWidth="sm">
          <DialogContent
            style={{
              padding: 30,
              overflowX: "hidden",
              overflowY: "auto",
              height: 650,
              position: "relative",
            }}
          >
            <ShiftSwitchRequestForm
              allEvents={events}
              userEvents={userEvents}
              closeDialog={toggleShiftSwitchForm}
              shiftNames={shiftNames}
              // date={selectedDate}
              // dayObject={selectedDay}
              // showDayActions={showDayActions}
              scheduleEndDate={scheduleEndDate}
              shiftToChange={shiftToChange}
              setToast={setToast}
              setShowToast={setShowToast}
              setErrorToast={setErrorToast}
              setShowErrorToast={setShowErrorToast}
              // shiftToChange={shiftToChange}
              view={view}
              refetch={refetch}
            />
          </DialogContent>
        </Dialog>
        <Dialog open={callIn} fullWidth maxWidth="xs">
          <DialogContent
            style={{
              padding: 30,
              overflowX: "hidden",
              height: 460,
              position: "relative",
            }}
          >
            <EmpCallInForm
              closeDialog={toggleCallInForm}
              // date={selectedDate}
              userEvents={userEvents}
              setToast={setToast}
              setShowToast={setShowToast}
              view={view}
              shiftToChange={shiftToChange}
              refetch={refetch}
              // dayObject={selectedDay}
              // showDayActions={showDayActions}
            />
          </DialogContent>
        </Dialog>
        <Dialog open={editShift} fullWidth maxWidth="md">
          <Grid
            container
            component={DialogContent}
            direction="column"
            wrap="nowrap"
            justify="space-between"
            style={{
              padding: 30,
              overflowX: "hidden",
              overflowY: "auto",
              height: 675,
            }}
          >
            {shiftToEdit ? (
              <MangEditShift
                shiftEvent={shiftToEdit}
                closeDialog={toggleEditShift}
                setToast={setToast}
                setShowToast={setShowToast}
                setErrorToast={setErrorToast}
                setShowErrorToast={setShowErrorToast}
                refetch={refetch}
              />
            ) : null}
          </Grid>
        </Dialog>
        <Dialog open={softRequest} fullWidth maxWidth="xs">
          <DialogContent style={{ padding: 30 }}>
            <SoftTimeOff
              closeDialog={toggleSoftRequest}
              scheduleEndDate={scheduleEndDate}
              invalidDates={invalidDates}
              setToast={setToast}
              setShowToast={setShowToast}
              maxGreyout={maxGreyout}
              schedulePeriodWeeks={schedulePeriodWeeks}
              softRequests={userSoftRequests}
            />
          </DialogContent>
        </Dialog>
        <Dialog open={editSoftRequest} fullWidth maxWidth="xs">
          <DialogContent style={{ padding: 30 }}>
            {softRequestIdToEdit && (
              <EditSoftTimeOff
                closeDialog={toggleEditSoftRequestForm}
                // dayObject={selectedDay}
                scheduleEndDate={scheduleEndDate}
                invalidDates={invalidDates}
                setToast={setToast}
                setShowToast={setShowToast}
                // showDayActions={showDayActions}
                maxGreyout={maxGreyout}
                schedulePeriodWeeks={schedulePeriodWeeks}
                softRequests={userSoftRequests}
                softRequestIdToEdit={softRequestIdToEdit}
                setSoftRequestIdToEdit={setSoftRequestIdToEdit}
              />
            )}
          </DialogContent>
        </Dialog>
        <Dialog open={timeOffRequestForm} fullWidth maxWidth="sm">
          <DialogContent
            style={{ padding: 30, height: 650, position: "relative" }}
          >
            <TimeOffRequestForm
              closeDialog={toggleTimeOffForm}
              // dayObject={selectedDay}
              // scheduleEndDate={scheduleEndDate}
              invalidDates={invalidDates}
              setToast={setToast}
              setShowToast={setShowToast}
              employees={employeeResources}
              // showDayActions={showDayActions}
            />
          </DialogContent>
        </Dialog>
        <Dialog open={editTimeOffRequest} fullWidth maxWidth="sm">
          <DialogContent
            style={{ padding: 30, height: 650, position: "relative" }}
          >
            {timeOffRequestIdToEdit && (
              <EditTimeOffRequestForm
                closeDialog={toggleEditTimeOffForm}
                // dayObject={selectedDay}
                // scheduleEndDate={scheduleEndDate}
                userSoft={userSoftRequests}
                // userTimeOffRequests={userTimeOff}
                setToast={setToast}
                setShowToast={setShowToast}
                userTimeOff={userTimeOff}
                // showDayActions={showDayActions}
                timeOffRequestIdToEdit={timeOffRequestIdToEdit}
                setTimeOffRequestIdToEdit={setTimeOffRequestIdToEdit}
                refetch={refetch}
              />
            )}
          </DialogContent>
        </Dialog>
        {/* <Popover
          id='add-request'
          anchorEl={addRequestPopoverAnchor}
          anchorOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
          keepMounted
          open={Boolean(addRequestPopoverAnchor)}
          onClose={() => setAddRequestPopoverAnchor(null)}
        >
          <Paper style={{ padding: 16 }}>
            <Grid container justify='space-between' style={{marginBottom: 16}}>
              <Grid item>
                <Typography variant='h5'>Add Request</Typography>
              </Grid>
              <Grid item>
                <IconButton 
                  aria-label='close' 
                  color='secondary' 
                  size='small' 
                  onClick={() => setAddRequestPopoverAnchor(null)} 
                >
                  <CloseIcon />
                </IconButton>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item>
                <Button 
                  variant='outlined' 
                  color='primary'
                  onClick={() => {
                    toggleSoftRequest();
                    setAddRequestPopoverAnchor(null);
                  }}
                  style={{width: 150}}
                >
                  Soft Request
                </Button>
              </Grid>
              <Grid item>
                <Button 
                  variant='outlined' 
                  color='primary'
                  onClick={() => {
                    toggleTimeOffForm();
                    setAddRequestPopoverAnchor(null);
                  }}
                  style={{width: 150}}
                >
                  Time Off Request
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </Popover> */}
        <Snackbar
          open={showToast}
          autoHideDuration={3000}
          onClose={() => setShowToast(false)}
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
        >
          <MuiAlert
            elevation={6}
            onClose={() => setShowToast(false)}
            severity="success"
          >
            <Typography>{toastMessage()}</Typography>
          </MuiAlert>
        </Snackbar>
        <Snackbar
          open={showErrorToast}
          autoHideDuration={3000}
          onClose={() => setShowErrorToast(false)}
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
        >
          <MuiAlert
            elevation={6}
            onClose={() => setShowErrorToast(false)}
            severity="error"
          >
            <Typography>{errorToastMessage()}</Typography>
          </MuiAlert>
        </Snackbar>
      </>
    );
  }
}

export default EmpCalendar;
