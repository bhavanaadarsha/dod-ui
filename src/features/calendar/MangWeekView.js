import React, { useState, useEffect } from 'react';
import { add, sub, format, eachDayOfInterval } from 'date-fns';
import { 
    makeStyles, Table, TableBody, 
    TableCell, TableContainer, TableHead, 
    TableRow, Paper, Typography, 
    IconButton, Grid, CircularProgress,
    Button 
} from '@material-ui/core';
import UnfoldMoreIcon from '@material-ui/icons/UnfoldMore';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import { useQuery } from '@apollo/client';
import { GET_ALL_USERS_AND_SHIFTS } from '../../api/gqlQueries';
import { filterListVar, selectedDateVar, userVar } from '../../cache';
import sun from '../../assets/quizImages/sun.png';
import Brightness3Icon from '@material-ui/icons/Brightness3';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import CheckIcon from '@material-ui/icons/Check';

const useStyles = makeStyles((theme) => ({
    paper: {
        height: 1000, 
        backgroundColor: '#F7F7F7', 
        overflow: 'hidden', 
        padding: 10
    },
    filledCell: {
        backgroundColor: 'white',
        borderRight: '1px solid #CCCCCC',
        padding: 5
    },
    filledPersonalBox: {
        backgroundColor: '#F5E1DB',
        borderRadius: 8,
        padding: 10
    },
    filledNonPersonalBox: {
        backgroundColor: '#DFE0E2',
        borderRadius: 8,
        padding: 10
    },
    // filledTimeOffBox: {
    //     backgroundColor: '#DFDCDA',
    //     borderRadius: 8,
    //     padding: 10
    // },
    emptyCell: {
        backgroundColor: 'white',
        borderRight: '1px solid #CCCCCC',
    },
    row: {
        height: 50
    },
    subtitle: {
        fontSize: 12,
        color: theme.palette.secondary.main
    },
    todayButton: {
        padding: 0,
        fontSize: 16,
        marginTop: -2,
        marginLeft: -16
    }
}));

const MangWeekView = ({ date, events, hardRequests, softRequests, draftStart, draftEnd }) => {
    const classes = useStyles();
    const [startDate, setStartDate] = useState();
    const [arrayOfDates, setArrayOfDates] = useState([]);
    const [weekEvents, setWeekEvents] = useState([]);
    const [sortDirection, setSortDirection] = useState('asc');
    const [draftSchedule, setDraftSchedule] = useState(false);
    const currentUser = userVar();
    const officeId = currentUser.office.id;
    const filters = filterListVar();
    const allFilters = filters.allFilters;

    const { loading, error, data } = useQuery(GET_ALL_USERS_AND_SHIFTS, {
        variables: {id: parseInt(officeId)}
    });
    
    useEffect(() => {
        let selectedDate;

        if (date) {
            selectedDate = new Date(date);
        } else {
            selectedDate = new Date();
        }

        selectedDate.setHours(0, 0, 0, 0);

        const dayNumber = selectedDate.getDay();
        const firstDayOfWeek = sub(selectedDate, { days: dayNumber });

        setStartDate(firstDayOfWeek);
        selectedDateVar(firstDayOfWeek);

        if (firstDayOfWeek >= draftStart && firstDayOfWeek <= draftEnd) {
            setDraftSchedule(true);
        } else {
            setDraftSchedule(false);
        }
    }, []);

    useEffect(() => {
        const array = [];
        if (startDate) {
            for (let i = 0; i < 7; i++) {
                const day = startDate;
                let newDay = add(day, { days: i });
                newDay = format(newDay, 'MM/dd/yyyy');
                array.push(newDay);
            }
        }
        setArrayOfDates(array);
        const lastDate = new Date(array[6]);
        lastDate.setHours(23, 59, 59, 59);
        if (startDate && lastDate) {
            const weekEvents = events.filter(event => (
                (new Date(event.start) >= startDate) && (new Date(event.start) <= lastDate)
            ));
            setWeekEvents(weekEvents);
        }
    }, [startDate]);

    if (loading) {
        return (
            <Paper className={classes.paper}>
                <CircularProgress color='primary'/>
            </Paper>
        );
    } else if (error) {
        console.log(error);
        return <Typography>Something went wrong. Please try again.</Typography>;
    } else {
        const users = data.employees[0].employeeSet;

        const comparator =
            sortDirection === 'asc'
                ? (a, b) => {
                    return a.lastName.localeCompare(b.lastName);
                }
                : (a, b) => {
                    return b.lastName.localeCompare(a.lastName);
                };

        const reverseDirection = () => (sortDirection === 'asc' ? 'desc' : 'asc');
        let sortedUsers = users.filter(user => user.firstName !== null);
        sortedUsers = sortedUsers.sort(comparator);

        // const filterEvents = (events) => {
        //     let filteredEvents;
        //     if (allFilters.includes('Personal')) {
        //         filteredEvents = events.filter(event => {
        //             const participantIds = event.participants 
        //                 ? event.participants.map(participant => participant.id)
        //                 : [];
        //             return participantIds.includes(user.id) || event.
        //         });
        //     } else {
    
        //     }
        // };
        
        const columns = arrayOfDates.map(date => {
            return (
                <TableCell key={date} style={{backgroundColor: '#F7F7F7', paddingBottom: 8, paddingTop: 8}}>
                    <Typography style={{textAlign: 'center', fontSize: 16}}>{format(new Date(date), 'EEE')}</Typography>
                    <Typography style={{textAlign: 'center'}}>{format(new Date(date), 'dd MMM yyyy')}</Typography>
                </TableCell>
            );
        });

        const userEvents = (user) => {
            const userName = user.firstName + ' ' + user.lastName;
            let jobTitle = user.employeeskillSet.find(skill => (
                skill.skill.variety === 'JOB_TYPE'
            ));
            jobTitle = jobTitle ? jobTitle.skill.name : '';
            // const office = user.assignmentSet.find(a => a.office.id === officeId);
            // let jobTitle = office ? office.role.name : user.assignmentSet[0].role.name;
            // jobTitle = jobTitle.slice(0, 1).toUpperCase() + jobTitle.slice(1).toLowerCase();

            if (allFilters.includes('All Shifts')){
                const userShiftIds = user.shifts.filter(shift => shift.office.id === officeId)
                    .map(shift => shift.id);
                
                const shiftObject = {};

                let userFilteredTimeOff = [];
                const userTimeOff = hardRequests.filter(request => request.employee.id === user.id);
                if (allFilters.includes('All Requests')){
                    if (user.id === currentUser.id){
                        userFilteredTimeOff = [...userTimeOff, ...softRequests];
                    } else {
                        userFilteredTimeOff = [...userTimeOff];
                    }
                } else {
                    if (allFilters.includes('Pending Time Off')) {
                        const pending = userTimeOff.filter(request => request.approvedby === null);
                        userFilteredTimeOff = [...userFilteredTimeOff, ...pending];
                    }
                    if (allFilters.includes('Approved Time Off')) {
                        const approved = userTimeOff.filter(request => request.approvedby !== null);
                        userFilteredTimeOff = [...userFilteredTimeOff, ...approved];  
                    }
                    if (allFilters.includes('Soft Requests') && user.id === currentUser.id) {
                        userFilteredTimeOff = [...userFilteredTimeOff, ...softRequests];
                    }
                }

                if (userFilteredTimeOff.length > 0) {
                    userFilteredTimeOff.forEach(e => {
                        console.log(e);
                        if (e.type === 'softRequest'){
                            let formattedDate = format(e.start, 'MM/dd/yyyy');
                            shiftObject[formattedDate] = {
                                title: e.eventTitle,
                                extra: e.highPriority ? 'high priority' : 'standard priority'
                            };
                        } else {
                            let fd = new Date(`${e.firstday}T08:00:00`);
                            let ld = new Date(`${e.lastday}T08:00:00`);
                            let arrayofTimeOff = eachDayOfInterval(
                                { start: fd, end: ld }
                            );
                            
                            arrayofTimeOff.forEach(date => {
                                let formattedDate = format(date, 'MM/dd/yyyy');
                                shiftObject[formattedDate] = {
                                    title: e.type.name,
                                    extra: e.approvedby !== null ? 'approved' : 'pending'
                                };
                            });
                        }
                    });
                }
                weekEvents.forEach(e => {
                    const shiftDateStart = format(new Date(e.start), 'MM/dd/yyyy');
                    let working = userShiftIds.includes(e.eventId);
                    if (working && (allFilters.includes(e.eventTitle) || allFilters.includes('All Missions'))) {
                        const amOrPm = e.eventTitle.substr(0, e.eventTitle.indexOf('-'));
                        shiftObject[shiftDateStart] = {
                            title: e.eventTitle,
                            extra: amOrPm.includes('a') ? 'am' : 'pm'
                        };
                    }
                });

                const userRow = createRow(arrayOfDates, shiftObject, userName, user.id, jobTitle);
                return userRow;
            } else if (allFilters.includes('Personal') && user.id === currentUser.id) {
                const userShiftIds = user.shifts.filter(shift => shift.office.id === officeId)
                    .map(shift => shift.id);
                
                const shiftObject = {};
                
                let userFilteredTimeOff = [];
                const userTimeOff = hardRequests.filter(request => request.employee.id === user.id);
                if (allFilters.includes('All Requests')){
                    userFilteredTimeOff = [...userTimeOff, ...softRequests];
                } else {
                    if (allFilters.includes('Pending Time Off')) {
                        const pending = userTimeOff.filter(request => request.approvedby === null);
                        userFilteredTimeOff = [...userFilteredTimeOff, ...pending];
                    }
                    if (allFilters.includes('Approved Time Off')) {
                        const approved = userTimeOff.filter(request => request.approvedby !== null);
                        userFilteredTimeOff = [...userFilteredTimeOff, ...approved];  
                    }
                    if (allFilters.includes('Soft Requests')) {
                        userFilteredTimeOff = [...userFilteredTimeOff, ...softRequests];
                    }
                }
                
                if (userFilteredTimeOff.length > 0) {
                    userFilteredTimeOff.forEach(e => {
                        if (e.type === 'softRequest'){
                            let formattedDate = format(e.start, 'MM/dd/yyyy');
                            shiftObject[formattedDate] = {
                                title: e.eventTitle,
                                extra: e.highPriority ? 'high priority' : 'standard priority'
                            };
                        } else {
                            let fd = new Date(`${e.firstday}T08:00:00`);
                            let ld = new Date(`${e.lastday}T08:00:00`);
                            let arrayofTimeOff = eachDayOfInterval(
                                { start: fd, end: ld }
                            );
                            
                            arrayofTimeOff.forEach(date => {
                                let formattedDate = format(date, 'MM/dd/yyyy');
                                shiftObject[formattedDate] = {
                                    title: e.type.name,
                                    extra: e.approvedby !== null ? 'approved' : 'pending'
                                };
                            });
                        }
                    });
                }
                        
                weekEvents.forEach(e => {
                    const shiftDateStart = format(new Date(e.start), 'MM/dd/yyyy');
                    let working = userShiftIds.includes(e.eventId);
                    if (working) {
                        const amOrPm = e.eventTitle.substr(0, e.eventTitle.indexOf('-'));
                        shiftObject[shiftDateStart] = {
                            title: e.eventTitle,
                            extra: amOrPm.includes('a') ? 'am' : 'pm'
                        };
                    }
                });
                const userRow = createRow(arrayOfDates, shiftObject, userName, user.id, jobTitle);
                return userRow;
            } else {
                const shiftObject = {};
                const userRow = createRow(arrayOfDates, shiftObject, userName, user.id, jobTitle);
                return userRow;
            }
        };
    
        const createRow = (weekDates, shiftObject, userName, userId, jobTitle) => {
            const personal = userId === currentUser.id;
            const dateCells = weekDates.map(date => {
                if (Object.keys(shiftObject).includes(date)) {
                    // const title = shiftObject[date].includes('-')
                    //     ? shiftObject[date].title.substr(0, shiftObject[date].indexOf('-'))
                    //     : null;
                    const dayObject = shiftObject[date];
                    return (
                        <TableCell key={date} className={classes.filledCell}>
                            <div className={personal ? classes.filledPersonalBox : classes.filledNonPersonalBox}>
                                <Grid container alignItems='center'>
                                    <Grid item>
                                        {dayObject.extra === 'am' &&
                                            <img alt='day shift' src={sun} style={{width: 20, marginTop: 3, marginRight: 5}}/>
                                        }
                                        {dayObject.extra === 'pm' &&
                                            <Brightness3Icon style={{width: 20, marginTop: 3, marginRight: 5}}/>
                                        }
                                        {dayObject.extra === 'high priority' &&
                                            <PriorityHighIcon style={{width: 20, marginTop: 3, marginRight: 5}}/>
                                        }
                                        {dayObject.extra === 'approved' &&
                                            <CheckIcon style={{width: 20, marginTop: 3, marginRight: 5}}/>
                                        }
                                    </Grid>
                                    <Grid item>
                                        <Typography variant='subtitle1'>{dayObject.title}</Typography>
                                    </Grid>
                                </Grid>
                            </div>
                        </TableCell>
                    );
                } else {
                    return <TableCell key={date} className={classes.emptyCell}></TableCell>;
                }
            });
    
            const userRow = (
                <TableRow key={userId} className={classes.row}>
                    <TableCell 
                        className={classes.emptyCell} 
                        style={{ 
                            maxWidth: 225, 
                            padding: 5, 
                            paddingLeft: 10, 
                            verticalAlign: 'top',
                            backgroundColor: '#F7F7F7' 
                        }}
                    >
                        <Typography noWrap>{userName}</Typography>
                        <Typography noWrap className={classes.subtitle}>{jobTitle}</Typography>
                    </TableCell>
                    {dateCells}
                </TableRow>
            );
            return userRow;
        };
    
        const rows = [];
        
        sortedUsers.forEach((u) => {
            const userRow = userEvents(u);
            rows.push(userRow);
        });
    
        const changeToNextWeek = () => {
            const lastDay = arrayOfDates[6];
            const newFirstDay = add(new Date(lastDay), { days: 1 });
            setStartDate(newFirstDay);
            selectedDateVar(newFirstDay);

            if (newFirstDay >= draftStart && newFirstDay <= draftEnd) {
                setDraftSchedule(true);
            } else {
                setDraftSchedule(false);
            }
        };
    
        const changeToPreviousWeek = () => {
            const newFirstDay = sub(startDate, { days: 7 });
            setStartDate(newFirstDay);
            selectedDateVar(newFirstDay);
            
            if (newFirstDay >= draftStart && newFirstDay <= draftEnd) {
                setDraftSchedule(true);
            } else {
                setDraftSchedule(false);
            }
        };

        const changeToToday = () => {
            const today = new Date();
    
            today.setHours(0, 0, 0, 0);
    
            const dayNumber = today.getDay();
            const firstDayOfWeek = sub(today, { days: dayNumber });
    
            setStartDate(firstDayOfWeek);
            selectedDateVar(today);
    
            if (firstDayOfWeek >= draftStart && firstDayOfWeek <= draftEnd) {
                setDraftSchedule(true);
            } else {
                setDraftSchedule(false);
            }
        };
    
        return (
            <Paper className={classes.paper} >
                <Grid 
                    container 
                    wrap='nowrap' 
                    align='center' 
                    justify='space-between' 
                    style={{ backgroundColor: '#F7F7F7' }}
                >
                    <Grid item container justify='flex-start' xs={5} style={{ paddingLeft: 3 }} >
                        <Grid item>
                            <Typography variant="h3" style={{ color: '#007BFF', fontWeight: 400 }}>
                                {startDate ? format(startDate, 'MMMM') + ' ' : null}
                                <span style={{ color: '#007BFF', fontWeight: 100 }}>
                                    {startDate ? format(startDate, 'yyyy') : null}
                                </span>
                            </Typography>
                        </Grid>
                        <Grid 
                            item 
                            container 
                            spacing={1} 
                            xs={4} 
                            style={{ marginTop: -15, marginRight: -15, marginLeft: 15 }}
                        >
                            <Grid item>
                                <IconButton 
                                    style={{ color: '#007BFF', fontSize: 26 }} 
                                    onClick={changeToPreviousWeek}
                                >
                                    <KeyboardArrowLeftIcon />
                                </IconButton>
                            </Grid>
                            <Grid item>
                                <Typography
                                    style={{
                                        color: '#007BFF',
                                        // fontWeight: 500,
                                        fontSize: 16,
                                        paddingTop: 12,
                                        paddingBottom: 12,
                                        marginLeft: -15,
                                        marginRight: -15
                                    }}
                                >
                                    Week
                                    {/* {arrayOfDates.length === 7 ? format(new Date(arrayOfDates[0]), 'M/d') + 
                                    ' - ' + format(new Date(arrayOfDates[6]), 'M/d') : null} */}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <IconButton 
                                    style={{ color: '#007BFF' }} 
                                    onClick={changeToNextWeek}
                                >
                                    <KeyboardArrowRightIcon />
                                </IconButton>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <Button 
                                style={{color: '#007BFF'}} 
                                className={classes.todayButton} 
                                onClick={changeToToday}
                            >
                                Today
                            </Button>
                        </Grid>
                    </Grid>
                    {draftSchedule &&
                        <Grid item xs={2}>
                            <Typography variant="h3" wrap='nowrap' style={{ color: '#8CADE1' }}>
                                Draft
                            </Typography>
                        </Grid>
                    }
                    <Grid item xs={5}></Grid>
                </Grid>
                <TableContainer style={{ backgroundColor: '#F7F7F7', height: 930 }}>
                    <Table stickyHeader style={{ height: 930 }}>
                        <TableHead >
                            <TableRow >
                                <TableCell style={{ backgroundColor: '#F7F7F7' }}>
                                    <Grid container justify='space-between'>
                                        <Grid item xs={8}>
                                            <Typography>Employee</Typography>
                                        </Grid>
                                        <Grid item xs={3}>
                                            <IconButton 
                                                onClick={() => setSortDirection(reverseDirection())} 
                                                style={{marginTop: -20, marginBottom: -20}}
                                            >
                                                <UnfoldMoreIcon />
                                            </IconButton>
                                        </Grid>
                                    </Grid>
                                </TableCell>
                                {columns}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        );
    }  
};

export default MangWeekView;