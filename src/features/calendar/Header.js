import React from "react";
import { Grid, Typography } from "@material-ui/core";
import {
  CalendarNav,
  CalendarNext,
  CalendarPrev,
  CalendarToday,
} from "@mobiscroll/react-beta";
import WarningIcon from "@material-ui/icons/Warning";
import AlertOutline from "mdi-material-ui/AlertOutline";
import { sub } from "date-fns";
import { userVar } from "../../cache";
import roles from "../../Roles/roles";
import { format } from "date-fns";

const Header = ({
  selectedDate,
  draftStart,
  draftEnd,
  view,
  empFitness,
  printPreview,
}) => {
  const user = userVar();
  if (
    selectedDate >= sub(draftStart, { days: 1 }) &&
    selectedDate <= draftEnd
  ) {
    return (
      <Grid container justify="space-between" className="md-custom-header">
        <Grid item container justify="flex-start" spacing={1} xs={5}>
          <Grid item>
            <CalendarNav />
          </Grid>
          <Grid item>
            <CalendarPrev />
            {view === 0 ? (
              <span style={{ color: "#007BFF", marginTop: 4 }}>Month</span>
            ) : (
              <span style={{ color: "#007BFF", marginTop: 4 }}>Week</span>
            )}
            <CalendarNext />
          </Grid>
          <Grid item>
            <CalendarToday />
          </Grid>
        </Grid>
        <Grid item container xs={2} justify="center">
          <Grid item>
            <Typography
              variant="h3"
              style={{ color: "#8CADE1", paddingTop: 8 }}
            >
              Draft
            </Typography>
          </Grid>
        </Grid>
        {user.role === roles.MANAGER && view === 0 && !printPreview? (
          <>
            <Grid item container xs={5} justify="flex-end" spacing={1}>
              <Grid item>
                <Typography style={{ paddingTop: 8, paddingRight: 8 }}>
                  <WarningIcon
                    style={{
                      fontSize: 18,
                      marginBottom: -2,
                      marginLeft: 4,
                      opacity: 0.9,
                    }}
                  />{" "}
                  = Understaffed
                </Typography>
              </Grid>
              <Grid item>
                <Typography style={{ paddingTop: 8, paddingRight: 8 }}>
                  <AlertOutline
                    style={{
                      fontSize: 18,
                      marginBottom: -2,
                      marginLeft: 4,
                      opacity: 0.9,
                    }}
                  />{" "}
                  = Overstaffed
                </Typography>
              </Grid>
            </Grid>
            <Grid item container xs={12} justify="flex-end">
              <Typography variant="body1" style={{ paddingRight: 8 }}>
                **Hover to see details
              </Typography>
            </Grid>
          </>
        ) : (
          <Grid item xs={5}></Grid>
        )}
        {!printPreview && (
          <Grid item container justify="center">
            <Typography variant="h6">
              {empFitness.score === "" || empFitness.ratio === ""
                ? ""
                : "Your schedule quality for this schedule period: " +
                  empFitness.ratio.toFixed(2) * 100 +
                  "%"}
            </Typography>
          </Grid>
        )}
        {
          printPreview &&
          <Grid item container justify="center">
            <Typography variant="h6">
              {user.office.name}
            </Typography>
          </Grid>
        }
      </Grid>
    );
  } else {
    return (
      <Grid container justify="space-between" className="md-custom-header">
        <Grid item container justify="flex-start" spacing={2} xs={6}>
          <Grid item>
            <CalendarNav />
          </Grid>
          <Grid item>
            <CalendarPrev />
            {view === 0 ? (
              <span style={{ color: "#007BFF", marginTop: 4 }}>Month</span>
            ) : (
              <span style={{ color: "#007BFF", marginTop: 4 }}>Week</span>
            )}
            <CalendarNext />
          </Grid>
          <Grid item>
            <CalendarToday />
          </Grid>
        </Grid>
        {user.role === roles.MANAGER && view === 0 && !printPreview ? (
          <>
            <Grid item container xs={6} justify="flex-end" spacing={1}>
              <Grid item>
                <Typography style={{ paddingTop: 8, paddingRight: 8 }}>
                  <WarningIcon
                    style={{
                      fontSize: 18,
                      marginBottom: -2,
                      marginLeft: 4,
                    }}
                  />{" "}
                  = Understaffed
                </Typography>
              </Grid>
              <Grid item>
                <Typography style={{ paddingTop: 8, paddingRight: 8 }}>
                  <AlertOutline
                    style={{
                      fontSize: 18,
                      marginBottom: -2,
                      marginLeft: 4,
                    }}
                  />{" "}
                  = Overstaffed
                </Typography>
              </Grid>
            </Grid>
            <Grid item container xs={12} justify="flex-end">
              <Typography variant="body1" style={{ paddingRight: 8 }}>
                **Hover to see details
              </Typography>
            </Grid>
          </>
        ) : (
          <Grid item xs={6}></Grid>
        )}
        {!printPreview && (
          <Grid item container justify="center">
            <Typography variant="h6">
              {empFitness.score === "" || empFitness.ratio === ""
                ? ""
                : "Your schedule quality for this schedule period: " +
                  empFitness.ratio.toFixed(2) * 100 +
                  "%"}
            </Typography>
          </Grid>
        )}
        {
          printPreview &&
          <Grid item container justify="center">
            <Typography variant="h6">
              {user.office.name}
            </Typography>
          </Grid>
        }
      </Grid>
    );
  }
};

export default Header;
