import React from 'react';
import { 
    Typography, makeStyles, Grid,
    Table, TableBody, TableCell, 
    TableContainer, TableHead, TableRow,
    IconButton 
} from '@material-ui/core';
import WarningIcon from '@material-ui/icons/Warning';
import AlertOutline from 'mdi-material-ui/AlertOutline';
import CloseIcon from '@material-ui/icons/Close';
import { format, getMinutes } from 'date-fns';

const useStyles = makeStyles((theme) => ({
    text: {
        color: 'white'
    }, 
    darkText: {
        color: 'black'
    }, 
    underText: {
        color: theme.palette.primary.light,
        fontWeight: 'bold'
    }, 
    // table: {
    //     minWidth: 350
    // },
    icon: {
        padding: 0,
        marginRight: 5,
        marginBottom: -2
    }
}));

const ShiftSlackBySkill = ({shift, slacks, tooltip, closeDialog}) => {
    const classes = useStyles();

    let skills = ['Acuity'];
    slacks.forEach(slack => {
        if (slack.count) {
            !skills.includes(slack.skill.name) && skills.push(slack.skill.name);
        }
    });
    
    const timeRows = [];
    slacks.forEach(slack => {
        const start = new Date(slack.start);
        const end = new Date(slack.end);
        // const startMinutes = getMinutes(start);
        // const endMinutes = getMinutes(end);
        // const formattedStart = startMinutes === 0 ? format(start, 'haaaaa') : format(start, 'h:mmaaaaa');
        // const formattedEnd = endMinutes === 0 ? format(end, 'haaaaa') : format(end, 'h:mmaaaaa');
        const formattedStart = format(start, 'HH:mm');
        const formattedEnd = format(end, 'HH:mm');
        const title = `${formattedStart} - ${formattedEnd}`;
        const existing = timeRows.find(row => row.time === title);

        let index;
        let count;
        let slackNumber;
        let empty;
        let over = false;
        let under = false;
        if (slack.lower || slack.upper) {
            index = skills.indexOf('Acuity Staffing');
            over = slack.assigned > slack.upper;
            under = slack.assigned < slack.lower;
            
            if (over) {
                slackNumber = ` (+${slack.assigned - slack.upper})`;
            } else if (under) {
                slackNumber = ` (${slack.assigned - slack.lower})`;
            } else {
                slackNumber = '';
            }
            count = slack.assigned;
            empty = false;
        } else {
            index = skills.indexOf(slack.skill.name);

            if (slack.slack > 0) {
                slackNumber = ` (+${slack.slack})`;
                over = true;
            } else if (slack.slack < 0) {
                slackNumber = ` (${slack.slack})`;
                under = true;
            } else {
                slackNumber = ''
            }
            count = slack.count;
            empty = slack.count === 0 && slack.slack === 0;
        }

        if (existing) {
            if (empty) {
                return;
            } else {
                over = existing.over ? existing.over : over;
                under = existing.under ? existing.under : under;
                const values = [...existing.values];
                values.splice(index, 1, `${count}${slackNumber}`);
                existing.values = values;
                existing.over = over;
                existing.under = under;
            }
        } else {
            const values = skills.map(skill => null);
            if (empty) {
                timeRows.push({
                    time: title, 
                    start: slack.start, 
                    under: under,
                    over: over,
                    values: values
                });
            } else {
                values.splice(index, 1, `${count} ${slackNumber}`);
                timeRows.push({
                    time: title, 
                    start: slack.start, 
                    under: under,
                    over: over,
                    values: values
                });
            }
        }
    });

    const slacksRows = () => {
        const sortedByTime = [...timeRows];
        sortedByTime.sort((a,b) => new Date(a.start) - new Date(b.start));

        return sortedByTime.map((slack, rowIndex) => {
            return (
                <TableRow key={slack.time} className={classes.row}>
                    <TableCell className={classes.darkText} align='center'>
                        {slack.time}
                    </TableCell>
                    <TableCell className={classes.darkText}>
                        {slack.under &&
                            <WarningIcon style={{fontSize: 18}}/>
                        }
                        {slack.over && !slack.under &&
                            <AlertOutline style={{fontSize: 18}}/>
                        }
                    </TableCell>
                    {slack.values.map((value, index) => {
                        return (
                            <TableCell 
                                key={`${value}-${rowIndex}-${index}`} 
                                className={value && value.includes('-') ? classes.underText : classes.darkText} 
                                align='left'
                            >
                                {value}
                            </TableCell>
                        )
                    })}
                </TableRow>
            );
        })
    };
    
    return (  
        <TableContainer>
            {tooltip
                ? <Typography variant='h4' className={classes.text} style={{marginLeft: 12, marginTop: 8}}>
                    {format(new Date(shift.start), 'dd MMM yyyy')}
                </Typography>
                : <Grid container justify='space-between'>
                    <Grid item>
                        <Typography variant='h4' className={classes.darkText} >
                            {format(new Date(shift.start), 'dd MMM yyyy')}
                        </Typography>
                    </Grid>
                    <Grid item>
                        <IconButton aria-label='close' color='secondary' size='small' onClick={closeDialog} >
                            <CloseIcon />
                        </IconButton>
                    </Grid>
                </Grid>
            }
            
            <Table size='small' >
                <TableHead>
                    <TableRow>
                        <TableCell className={tooltip ? classes.text : classes.darkText} align='center'>
                            Time
                        </TableCell>
                        <TableCell></TableCell>
                        {skills.map(skill => (
                            <TableCell 
                                className={tooltip ? classes.text : classes.darkText}
                                key={skill}
                                align='left'    
                            >
                                {skill}
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {slacksRows()}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
 
export default ShiftSlackBySkill;