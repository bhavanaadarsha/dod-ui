import React, { useState } from 'react';
import { 
    Grid, Typography, Paper, 
    makeStyles, Button, Dialog,
    DialogContent, TextField, IconButton
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { format } from 'date-fns';
import { useMutation } from '@apollo/client';
import { MANAGER_APPROVE_EMPLOYEE_AVAILABILITY, MANAGER_DENY_EMPLOYEE_AVAILABILITY } from '../../api/gqlQueries';
import { filterListVar, selectedDateVar, userVar } from '../../cache';

const useStyles = makeStyles((theme) => ({
    button: {
        width: 75
    },
    addSpacing: {
        marginTop: 10,
        marginBottom: 10,
    },
    paper: {
        padding: 30,
        paddingLeft: 50,
        paddingRight: 50,
        marginTop: 10, 
        marginBottom: 10,
        backgroundColor: theme.palette.background.default
    },
    input: {
        width: 384
    }
}));

const MangTimeOffApproval = (props) => {
    const classes = useStyles();

    const {
        closeView,
        timeOffRequests, 
        setToast, 
        setShowToast,
        setErrorToast,
        setShowErrorToast,
        // setSelectedDate
    } = props;

    const pendingRequests = timeOffRequests.filter(request => request.approvedby === null);
    const earliestRequest = pendingRequests.sort((a, b) => (
        new Date(a.firstday) - new Date(b.firstday)
    ))[0];
    const earliestDate = earliestRequest 
        ? new Date(`${earliestRequest.firstday}T08:00:00`)
        : new Date();

    const user = userVar();
    // const history = useHistory();

    const [showDenialReason, setShowDenialReason] = useState(false);
    const [denialReason, setDenialReason] = useState('');
    const [requestToDeny, setRequestToDeny] = useState();

    const [approveRequest] = useMutation(MANAGER_APPROVE_EMPLOYEE_AVAILABILITY, {
        onCompleted(data) {
            console.log(data);
            setToast('Manager Approve Time Off Request');
            setShowToast(true);
        },
        onError(error){
            console.error(error);
            setErrorToast('Error Approve Time Off');
            setShowErrorToast(true);
        }
    });

    const [denyRequest] = useMutation(MANAGER_DENY_EMPLOYEE_AVAILABILITY, {
        update(cache, { data: { denyTimeOffRequestWithNotifications }}) {
            cache.evict({
                id: `EmployeeAvailabilityNode:${denyTimeOffRequestWithNotifications.timeOffRequest.id}`
            });
        },
        onCompleted(data) {
            console.log(data);
            setShowDenialReason(false);
            setRequestToDeny();
            setDenialReason('');
            setToast('Manager Deny Time Off Request');
            setShowToast(true);
        },
        onError(error){
            console.error(error);
            setErrorToast('Error Deny Time Off');
            setShowErrorToast(true);
        }
    });

    const handleApproveRequest = (id) => {
        //send notification to employee to say request approved
        //change event in backend from pending to approved
        //delete notification
        approveRequest({variables: {
            id: id, 
            approvingManager: parseInt(user.id)
        }});
    };

    const handleDenyRequest = (id) => {
        //send notification to employee to say denied
        //delete event in backend
        //delete notification
        denyRequest({variables: {
            id: id,
            denyingManager: parseInt(user.id),
            deniedText: denialReason
        }});
    };

    const handleViewCalendarClick = () => {
        // history.push('/Landing/time-off-calendar')
        // closeApproval();
        // setSelectedDate(earliestDate);
        selectedDateVar(earliestDate);
        const event = {};
        closeView();
        // changeView(event, 0);
        // setView(0);
        // setCalView({
        //     calendar: { type: 'month', labels: true },
        // });
        // setTimeOffApproval(false);
        filterListVar({
            allFilters: ['All Shifts', 'Pending Time Off'],
            shiftsFilters: ['All Shifts'],
            missionFilters: [],
            requestsFilters: ['Pending Time Off']
        });
    };

    const renderedRequests = pendingRequests.map(request => {
        const firstDay = `${request.firstday}T08:00:00`;
        const lastDay = `${request.lastday}T08:00:00`;
        return (
            <Paper key={request.id} className={classes.paper}>
                <Grid container justify='space-between' className={classes.addSpacing}>
                    <Grid item xs={8} >
                        <Typography variant='h5'>
                            {request.employee.firstName} {request.employee.lastName} - {request.type.name}
                        </Typography>
                    </Grid>
                    <Grid item container justify='flex-end' xs={4} spacing={2}>
                        <Grid item>
                            <Button 
                                variant='outlined' 
                                color='primary' 
                                onClick={() => {setShowDenialReason(true); setRequestToDeny(request);}} 
                                className={classes.button}
                            >
                                Deny
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button 
                                variant='contained' 
                                color='primary' 
                                onClick={() => handleApproveRequest(request.id)} 
                                className={classes.button}
                            >
                                Approve
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Typography variant='subtitle1'>
                    Start: {format(new Date(firstDay), 'dd MMM yyyy')}
                    {request.startTime ? ` at ${request.startTime.slice(0,5)}` : ''}
                </Typography>
                <Typography variant='subtitle1'>
                    End: {format(new Date(lastDay), 'dd MMM yyyy')}
                    {request.endTime ? ` at ${request.endTime.slice(0,5)}` : ''}
                </Typography>
            </Paper>
        );
    });

    if(pendingRequests.length !== 0){
        return (  
            <Grid 
                container 
                component={Paper} 
                direction="column" 
                wrap='nowrap' 
                spacing={1} 
                style={{backgroundColor: '#F7F7F7', padding: 20, height: 600, overflow: 'auto'}}
            >
                <Grid item container justify='space-between'>
                    <Grid item style={{margin: 20, marginBottom: 10}}>
                        <Typography variant='h3'>Time Off Requests</Typography>
                        <Button color='primary' onClick={handleViewCalendarClick}>
                            View in Calendar
                        </Button>
                    </Grid>
                    <Grid item>
                        <IconButton onClick={closeView}><CloseIcon /></IconButton>
                    </Grid>
                </Grid>
                {renderedRequests}
                <Dialog open={showDenialReason} fullWidth maxWidth='xs'>
                    <DialogContent style={{ padding: 30 }}>
                        <Grid container direction='column' spacing={2}>
                            <Grid item >
                                <Typography variant='h2'>Reason for Denial</Typography>
                            </Grid>
                            <Grid item >
                                <TextField
                                    variant='outlined'
                                    value={denialReason}
                                    onChange={e => setDenialReason(e.target.value)}
                                    className={classes.input}
                                    onKeyPress={e => {e.key === 'Enter' && handleDenyRequest(requestToDeny.id);}}
                                />
                            </Grid>
                            <Grid item container justify='flex-end' spacing={1}>
                                <Grid item>
                                    <Button 
                                        variant='contained' 
                                        color='primary' 
                                        className={classes.button} 
                                        onClick={() => handleDenyRequest(requestToDeny.id)}
                                    >
                                        Submit
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Button 
                                        variant='outlined' 
                                        color='primary' 
                                        className={classes.button} 
                                        onClick={() => {setShowDenialReason(false); setDenialReason('');}}
                                    >
                                        Cancel
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </DialogContent>
                </Dialog>
            </Grid>
        );
    } else {
        return (
            <Grid 
                container 
                component={Paper} 
                justify='space-between' 
                style={{backgroundColor: '#F7F7F7', padding: 20}}
            >
                <Grid item>
                    <Typography variant='h3' style={{margin: 20}}>No Time Off Requests</Typography>
                </Grid>
                <Grid item>
                    <IconButton onClick={closeView}><CloseIcon /></IconButton>
                </Grid>
            </Grid>
        );
    }
};
 
export default MangTimeOffApproval;