import React from 'react';
import { 
    Typography, makeStyles,
    Table, TableBody, TableCell, 
    TableContainer, TableHead, TableRow,
} from '@material-ui/core';
import WarningIcon from '@material-ui/icons/Warning';
import AlertOutline from 'mdi-material-ui/AlertOutline';
import { format } from 'date-fns';

const useStyles = makeStyles((theme) => ({
    text: {
        color: 'white'
    }, 
    darkText: {
        color: 'black'
    }, 
    underText: {
        // color: theme.palette.primary.light,
        color: 'white',
        fontWeight: 'bold'
    }, 
    timeText: {
        color: 'white',
        width: 140,
        paddingRight: 0
    }, 
    underTimeText: {
        color: 'white',
        // color: theme.palette.primary.light,
        fontWeight: 'bold',
        width: 140,
        paddingRight: 0
    }, 
    // table: {
    //     minWidth: 350
    // },
    icon: {
        padding: 0,
        marginRight: 5,
        marginBottom: -2,
        color: 'white'
        // color: theme.palette.primary.light,
    }
}));

const ShiftSlack = ({startDate, slacks}) => {
    const classes = useStyles();

    const slacksRows = () => {
        const sortedByTime = [...slacks];
        sortedByTime.sort((a,b) => new Date(a.start) - new Date(b.start));

        let aggregatedSlacks = [];
        sortedByTime.forEach(slack => {
            if (slack.upper || slack.lower) {
                const skill = 'Acuity Staffing';
                const startTime = format(new Date(slack.start), 'HH:mm');
                const existsAndConsecutive = aggregatedSlacks.find(existingSlack => (
                    existingSlack.skill === skill &&
                    existingSlack.upper === slack.upper &&
                    existingSlack.lower === slack.lower &&
                    existingSlack.assigned === slack.assigned &&
                    format(new Date(existingSlack.end), 'HH:mm') === startTime
                ));
    
                if (existsAndConsecutive) {
                    existsAndConsecutive.end = new Date(slack.end);
                } else  {
                    aggregatedSlacks.push({
                        skill: skill,
                        start: new Date(slack.start),
                        end: new Date(slack.end),
                        upper: slack.upper,
                        lower: slack.lower,
                        assigned: slack.assigned,
                        id: slack.id
                    });
                } 
            } else {
                const skill = slack.skill.name;
                const startTime = format(new Date(slack.start), 'HH:mm');
                const existsAndConsecutive = aggregatedSlacks.find(existingSlack => (
                    existingSlack.skill === skill &&
                    existingSlack.count === slack.count &&
                    existingSlack.slack === slack.slack &&
                    format(new Date(existingSlack.end), 'HH:mm') === startTime
                ));
    
                if (existsAndConsecutive) {
                    existsAndConsecutive.end = new Date(slack.end);
                } else  {
                    aggregatedSlacks.push({
                        skill: skill,
                        start: new Date(slack.start),
                        end: new Date(slack.end),
                        count: slack.count,
                        slack: slack.slack,
                        id: slack.id
                    });
                }
            }
        });

        return aggregatedSlacks.map(slack => {
            const startTime = format(new Date(slack.start), 'HH:mm');
            const endTime = format(new Date(slack.end), 'HH:mm');

            if (slack.lower || slack.upper) {
                return (
                    <TableRow key={'acuity' + slack.id} className={classes.row}>
                        {slack.lower > slack.assigned
                            ? <>
                                <TableCell className={classes.underTimeText}>
                                    {startTime} - {endTime}
                                </TableCell>
                                <TableCell size='small'>
                                    <WarningIcon fontSize='small' className={classes.icon}/>
                                </TableCell>
                                <TableCell className={classes.underText}>
                                    Acuity
                                </TableCell>
                                <TableCell className={classes.underText} align='left'>
                                    {slack.lower} - {slack.upper}
                                </TableCell>
                                <TableCell className={classes.underText} align='left'>
                                    {slack.assigned}
                                </TableCell>
                            </>
                            : slack.upper < slack.assigned
                                ? <>
                                    <TableCell className={classes.timeText}>
                                        {startTime} - {endTime}
                                    </TableCell>
                                    <TableCell size='small'>
                                        <AlertOutline fontSize='small' className={classes.icon}/>
                                    </TableCell>
                                    <TableCell className={classes.text}>
                                        Acuity
                                    </TableCell>
                                    <TableCell className={classes.text} align='left'>
                                        {slack.lower} - {slack.upper}
                                    </TableCell>
                                    <TableCell className={classes.text} align='left'>
                                        {slack.assigned}
                                    </TableCell>
                                </>
                                : <>
                                    <TableCell className={classes.timeText}>
                                        {startTime} - {endTime}
                                    </TableCell>
                                    <TableCell size='small'></TableCell>
                                    <TableCell className={classes.text}>
                                        Acuity
                                    </TableCell>
                                    <TableCell className={classes.text} align='left'>
                                        {slack.lower} - {slack.upper}
                                    </TableCell>
                                    <TableCell className={classes.text} align='left'>
                                        {slack.assigned}
                                    </TableCell>
                                </>
                        }
                    </TableRow>
                );
            } else {
                if (slack.count === 0 && slack.slack === 0) {
                    return null;
                } else {
                    return (
                        <TableRow key={slack.id} className={classes.row}>
                            {slack.slack < 0 
                                ? <>
                                    <TableCell className={classes.underTimeText}>
                                        {startTime} - {endTime}
                                    </TableCell>
                                    <TableCell size='small'>
                                        <WarningIcon fontSize='small' className={classes.icon}/>
                                    </TableCell>
                                    <TableCell className={classes.underText}>
                                        {slack.skill}
                                    </TableCell>
                                    <TableCell className={classes.underText} align='left'>
                                        {slack.count - slack.slack}
                                    </TableCell>
                                    <TableCell className={classes.underText} align='left'>
                                        {slack.count}
                                    </TableCell>
                                </>
                                : <>
                                    <TableCell className={classes.timeText}>
                                        {startTime} - {endTime}
                                    </TableCell>
                                    <TableCell size='small'></TableCell>
                                    <TableCell className={classes.text}>
                                        {slack.skill}
                                    </TableCell>
                                    <TableCell className={classes.text} align='left'>
                                        {slack.count - slack.slack}
                                    </TableCell>
                                    <TableCell className={classes.text} align='left'>
                                        {slack.count}
                                    </TableCell>
                                </>
                            }
                        </TableRow>
                    );
                }
            }
        })
    };
    
    return (  
        <TableContainer>
            <Typography variant='h4' className={classes.text} style={{marginLeft: 12, marginTop: 8}}>
                {format(startDate, 'dd MMM yyyy')}
            </Typography>
            <Table size='small' className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell className={classes.text}>Time</TableCell>
                        <TableCell size='small'></TableCell>
                        <TableCell className={classes.text}>Skill</TableCell>
                        <TableCell className={classes.text}>Required</TableCell>
                        <TableCell className={classes.text}>Scheduled</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {slacksRows()}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
 
export default ShiftSlack;