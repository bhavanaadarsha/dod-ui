import React, { useEffect, useState } from 'react';
import ShiftType from './ShiftType';
import PreferenceQuiz from './PreferenceQuiz';
import { 
    Grid, Typography, Button, Snackbar,
    CircularProgress, Dialog, DialogContent,
    makeStyles, Slider, FormLabel,
    Checkbox, FormControlLabel, Collapse
} from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import MoodIcon from '@material-ui/icons/Mood';
import MoodBadIcon from '@material-ui/icons/MoodBad';
import SentimentNeutralIcon from '../../assets/quizImages/SentimentNeutralIcon';
import SentimentDissatisfiedIcon from '@material-ui/icons/SentimentDissatisfied';
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfied';
import InfoIcon from '@material-ui/icons/Info';
import { Formik } from 'formik';
import { mapPrefsFromBackend, mapPrefsToBackend } from '../../helpers/mapPreferences';
import { useQuery, useMutation } from '@apollo/client';
import { 
    BATCH_UPDATE_PREFERENCES, GET_PREFERENCES, 
    UPDATE_PREFS_SETTINGS, BATCH_CREATE_PREFERENCES
} from '../../api/gqlQueries';
import { userVar } from '../../cache';
import DaysOfTheWeek from './DaysOfTheWeek';
import DaysOffInARow from './DaysOffInARow';
import ReschedulingPreferences from './ReschedulingPreferences';

const useStyles = makeStyles(() => ({
    headerSpacing: {
        marginTop: 30
    },
    button: {
        width: 110
    },
    tabs: {
        marginTop: 20,
        marginBottom: 30
    },
    tab: {
        width: 235
    },
    sliderBox: {
        maxWidth: 425,
        marginTop: 16
    },
    coloredText: {
        color: '#8CADE1'
    }
}));

function Preferences(){
    const classes = useStyles();

    const user = userVar();

    const [showToast, setShowToast] = useState(false);
    const [showErrorToast, setShowErrorToast] = useState(false);
    const [status, setStatus] = useState(false);
    const [showQuiz, setShowQuiz] = useState(user.alwaysOpenGuide || false);
    const [showExample, setShowExample] = useState(user.showPrefsExample ? user.showPrefsExample : true);
    const [example, setExample] = useState(0);
  
    const toggleQuiz = () => setShowQuiz(!showQuiz);

    const { loading, error, data, refetch } = useQuery(GET_PREFERENCES, {
        variables: {
            id: parseInt(user.id),
            office: parseInt(user.office.id)
        }
    });

    const [batchCreatePreferences] = useMutation(BATCH_CREATE_PREFERENCES, {
        onCompleted(data) {
            console.log(data);
            refetch();
            setShowToast(true);
            setStatus(false);
            setShowQuiz(false);
        },
        onError(error) {
            console.error(error);
            setShowErrorToast(true);
        }
    });

    const [batchUpdatePreferences] = useMutation(BATCH_UPDATE_PREFERENCES, {
        onCompleted(data) {
            console.log(data);
            setShowToast(true);
            setStatus(false);
            setShowQuiz(false);
        },
        onError(error) {
            console.error(error);
            setShowErrorToast(true);
        }
    });
    
    const [updateUser] = useMutation(UPDATE_PREFS_SETTINGS, {
        onCompleted(data) {
            console.log(data);
        },
        onError(error){
            console.error(error);
        }
    });

    const marks = [
        {
            value: -2, 
            label: <MoodBadIcon style={{paddingLeft: 15}}/>
        },{
            value: -1, 
            label: <SentimentDissatisfiedIcon />
        },{ 
            value: 0, 
            label: <SentimentNeutralIcon />
        },{
            value: 1, 
            label: <SentimentSatisfiedIcon />
        },{
            value: 2, 
            label: <MoodIcon style={{paddingRight: 15}}/>
        },
    ];

    if(loading) {
        return <CircularProgress color='primary' />;
    } else if(error) {
        console.error(error);
        return <Typography>Something went wrong. Please try again.</Typography>;
    } else {
        let shiftPreferences = mapPrefsFromBackend(data.preferences);

        let daysIndicators = [];
        let dayTypes = [];
        let shiftIndicators = [];
        let shiftTypes = [];

        data.preferenceTypes.forEach(type => {
            const shifts = ['DAYS', 'NIGHTS', 'SWINGS'];
            const days = [
                'MONDAY', 'TUESDAY', 'WEDNESDAY', 
                'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'
            ];
            if (shifts.includes(type.type)) {
                shiftTypes.push({
                    id: parseInt(type.id), 
                    name: type.type
                });
            } else if (days.includes(type.type)) {
                dayTypes.push({
                    id: parseInt(type.id),
                    name: type.type
                });
            }
        });
        data.preferenceIndicators.forEach(indicator => {
            const day = dayTypes.find(type => (
                parseInt(type.id) === parseInt(indicator.preferenceType)
            ));
            const shift = shiftTypes.find(type => (
                parseInt(type.id) === parseInt(indicator.preferenceType)
            ));
            if (day) {
                daysIndicators.push({
                    ...indicator,
                    prefName: day.name
                });
            } else if (shift) {
                shiftIndicators.push({
                    ...indicator,
                    prefName: shift.name
                });
            }
        });

        const handleSubmit = (values) => {
            const newValues = values;
            newValues.workAdditionalShifts = newValues.workAdditionalShifts === true ? 1 : 0;

            const originalPrefsTypeIds = data.preferences.map(preference => (
                preference.type.id
            ));
            const prefTypesToBeCreated = data.preferenceTypes.filter(type => (
                !originalPrefsTypeIds.includes(type.id)
            ));

            const backend = mapPrefsToBackend(
                newValues, data.preferences, prefTypesToBeCreated, parseInt(user.id)
            );
            
            if (backend.update.length > 0) {
                batchUpdatePreferences({variables: {
                    input: backend.update
                }});
            }
            
            if (backend.create.length > 0) {
                batchCreatePreferences({variables: {
                    input: backend.create
                }});
            }
        };

        return (
            <>
                <Formik
                    initialValues={{
                        workAdditionalShifts: shiftPreferences.workAdditionalShifts === 1 
                            ? true : false,
                        
                        day: shiftPreferences.day || 0,
                        night: shiftPreferences.night || 0,
                        swing: shiftPreferences.swing || 0,
                        
                        monday: shiftPreferences.monday || 0,
                        tuesday: shiftPreferences.tuesday || 0,
                        wednesday: shiftPreferences.wednesday || 0,
                        thursday: shiftPreferences.thursday || 0,
                        friday: shiftPreferences.friday || 0,
                        saturday: shiftPreferences.saturday || 0,
                        sunday: shiftPreferences.sunday || 0,

                        oneDay: shiftPreferences.oneDay || 0,
                        twoDays: shiftPreferences.twoDays || 0,
                        threeDays: shiftPreferences.threeDays || 0,
                        fourDays: shiftPreferences.fourDays || 0,
                        
                        overtime: shiftPreferences.overtime || 0,
                    }}
                    onSubmit={handleSubmit}
                    enableReinitialize
                >
                    {({ handleSubmit, handleChange, values, errors, setFieldValue, handleReset }) => (
                        <form onSubmit={handleSubmit}>
                            <Grid container direction='row' >
                                <Grid item container className={classes.headerSpacing} xs={12}>
                                    <Grid item xs={6}>
                                        <Typography variant='h3'>Preferences</Typography>
                                    </Grid>
                                    <Grid item container justify='flex-end' spacing={2} xs={6}>
                                        <Grid item>
                                            <Button
                                                variant='outlined'
                                                color='primary'
                                                onClick={toggleQuiz}
                                                className={classes.button}
                                            >
                                                Open Guide
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Button
                                                variant='outlined'
                                                color='primary'
                                                onClick={() => {
                                                    handleReset(); 
                                                    setStatus(false); 
                                                }}
                                                className={classes.button}
                                                disabled={!status}
                                            >
                                                Cancel
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Button
                                                variant='contained'
                                                color='primary'
                                                type='submit'
                                                className={classes.button}
                                                disabled={!status}
                                            >
                                                Save
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid item xs={12} style={{marginBottom: 16}}>
                                    <Typography>
                                        Set your preferences for the next schedule
                                    </Typography>
                                </Grid>
                                <Collapse in={user.showPrefsExample} timeout={{exit: 750}}>
                                    <Grid item sm={12} md={5} style={{marginBottom: 48}}>
                                            <Grid item container direction='column' xs={10}>
                                                <Grid item>
                                                    <Typography>
                                                        <InfoIcon style={{marginRight: 5}} fontSize='small' />
                                                        <b>Example: </b>
                                                        If you really love working day shift, slide the slider all the way to the right to the 
                                                        <MoodIcon style={{height: 18, marginBottom: -4, marginLeft: 2}} />
                                                        . If you somewhat dislike working day shift, slide the slider one notch to the left of neutral to the 
                                                        <SentimentDissatisfiedIcon style={{height: 18, marginBottom: -4, marginLeft: 2}}/>.
                                                    </Typography>
                                                    <Typography variant='body2' className={classes.coloredText}>
                                                        Note: This is for you to play with and will not affect your preferences
                                                    </Typography>
                                                </Grid>
                                                <Grid item container justify='space-between' className={classes.sliderBox}>
                                                    <Grid item style={{marginTop: 4}}>
                                                        <FormLabel>
                                                            Day Shift
                                                        </FormLabel>
                                                    </Grid>
                                                    <Grid item xs={9}>
                                                        <Slider 
                                                            value={example}
                                                            aria-labelledby="discrete-slider"
                                                            valueLabelDisplay="off"
                                                            step={1}
                                                            marks={marks}
                                                            min={-2}
                                                            max={2} 
                                                            onChange={(e, value) => setExample(value)}
                                                        />
                                                    </Grid>
                                                </Grid>
                                                <Grid item>
                                                    <FormControlLabel
                                                        control={<Checkbox checked={!showExample}
                                                            onChange={(e) => {
                                                                setShowExample(!e.target.checked);
                                                                updateUser({variables: {
                                                                    id: parseInt(user.id),
                                                                    input: {showPrefsExample: false}
                                                                }});
                                                                const userVariable = userVar();
                                                                userVar({...userVariable, showPrefsExample: false});
                                                            }}
                                                            name='show-example'
                                                        />}
                                                        label="Do not show example again"
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item md={6}></Grid>
                                </Collapse>
                                <Grid item container sm={12} md={6} direction='column'>
                                    <Grid item style={{marginBottom: 48}}>
                                        <ShiftType 
                                            values={values}
                                            setFieldValue={setFieldValue}
                                            setStatus={setStatus}
                                            indicators={shiftIndicators}
                                            // originalPrefs={data.preferences}
                                        />
                                    </Grid>
                                    <Grid item style={{marginBottom: 24}}>
                                        <DaysOffInARow 
                                            values={values}
                                            setFieldValue={setFieldValue}
                                            setStatus={setStatus}
                                        />
                                    </Grid>
                                    <Grid item style={{marginBottom: 24}}>
                                        <ReschedulingPreferences
                                            values={values}
                                            setFieldValue={setFieldValue}
                                            setStatus={setStatus}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item container sm={12} md={6} direction='column'>
                                    <Grid item style={{marginBottom: 48}}>
                                        <DaysOfTheWeek 
                                            values={values}
                                            setFieldValue={setFieldValue}
                                            setStatus={setStatus}
                                            indicators={daysIndicators}
                                            // originalPrefs={data.preferences}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid item container justify='flex-end' spacing={2} xs={12}>
                                    <Grid item>
                                        <Button
                                            variant='outlined'
                                            color='primary'
                                            onClick={() => {
                                                handleReset(); 
                                                setStatus(false); 
                                            }}
                                            className={classes.button}
                                            disabled={!status}
                                        >
                                            Cancel
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button
                                            variant='contained'
                                            color='primary'
                                            type='submit'
                                            className={classes.button}
                                            disabled={!status}
                                        >
                                            Save
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </form>
                    )}
                </Formik>
                <Snackbar 
                    open={showToast} 
                    autoHideDuration={3000} 
                    onClose={() => setShowToast(false)} 
                    anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                >
                    <MuiAlert elevation={6} onClose={() => setShowToast(false)} severity='success'>
                        <Typography>Preferences saved!</Typography>
                    </MuiAlert>
                </Snackbar>
                <Snackbar 
                    open={showErrorToast} 
                    autoHideDuration={3000} 
                    onClose={() => setShowErrorToast(false)} 
                    anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                >
                    <MuiAlert elevation={6} onClose={() => setShowErrorToast(false)} severity='error'>
                        <Typography>Unable to save preferences. Please make sure you have completed every step.</Typography>
                    </MuiAlert>
                </Snackbar>
                <Dialog open={showQuiz} fullWidth maxWidth='md'>
                    <DialogContent style={{ padding: 30, overflowX: 'hidden', height: 685, position: 'relative' }}>
                        <PreferenceQuiz 
                            closeDialog={toggleQuiz} 
                            shiftPreferences={shiftPreferences}
                            batchUpdatePreferences={batchUpdatePreferences}
                            batchCreatePreferences={batchCreatePreferences}
                            originalPrefs={data.preferences}
                            prefTypes={data.preferenceTypes}
                        />
                    </DialogContent>
                </Dialog>
            </>
        );
    }
}

export default Preferences;