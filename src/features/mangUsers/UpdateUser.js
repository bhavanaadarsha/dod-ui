import React, { useEffect, useState } from "react";
import {
  Button,
  Grid,
  TextField,
  Typography,
  Checkbox,
  Select,
  MenuItem,
  Divider,
  makeStyles,
  FormControlLabel,
  Dialog,
  DialogContent,
  Snackbar,
  InputAdornment,
} from "@material-ui/core";
import { Formik, useFormikContext } from "formik";
import * as Yup from "yup";
import MangUsers from "./MangTeam";
import { useMutation, useQuery, gql, useLazyQuery } from "@apollo/client";
import {
  ADD_EMPLOYEE,
  UPDATE_EMPLOYEE,
  ADD_EMPLOYEE_SKILLS,
  DELETE_EMPLOYEE_SKILLS,
  FIND_SKILL_TYPE,
  UPDATE_EMPLOYEE_ASSIGNMENT,
  EMPLOYEE_TYPES,
  FIND_EMPLOYEE_BY_ID,
  CREATE_EMPLOYEE_ASSIGNMENT,
  ALL_OFFICES,
  ALL_SHIFT_TYPES,
  // CREATE_EMPLOYEE_PREFERENCES,
  PREFERENCE_TYPE,
  JOB_TYPE_ROLE,
  Check_Email_Exists,
  BATCH_CREATE_HIRED_SHIFTS,
  BATCH_UPDATE_HIRED_SHIFTS,
  BATCH_DELETE_HIRED_SHIFTS,
} from "../../api/gqlQueries";
import MultiSelect from "../general/MultiSelectSkills";
import {
  updateSkillSet,
  UpdateAssignment,
  findskillset,
} from "../../helpers/updateUserDetails";
import MuiAlert from "@material-ui/lab/Alert";
import { userVar } from "../../cache";
import { Prompt } from "react-router-dom";
import CustomShiftType from "./CustomShiftType";
import { mapHiredShiftsManagerView } from "../../helpers/hiredShifts";

const useStyles = makeStyles(() => ({
  divider: {
    border: "0.5px solid thin #333333",
    verticalAlign: "bottom",
  },
  inputWidth: {
    width: "100%",
  },
  headerSpacing: {
    marginTop: 30,
    marginBottom: 20,
  },
  button: {
    width: 80,
  },
}));
const validationSchema = Yup.object({
  firstName: Yup.string().required("Required"),
  lastName: Yup.string().required("Required"),
  email: Yup.string().required("Required").email(),
  phone: Yup.string().required("Required"),
  employmentType: Yup.string().required("Required"),
  department: Yup.string().required("Required"),
  jobType: Yup.string().required("Required"),
  shiftType: Yup.string().required("Required"),
  empStartDate: Yup.string().required("Required"),
});

const PromptIfDirty = () => {
  const formik = useFormikContext();
  return (
    <Prompt
      when={formik.dirty && formik.submitCount === 0}
      message="Are you sure you want to leave? You have unsaved changes on this form."
    />
  );
};

const UpdateUser = (props) => {
  const refetch = props.refetch;
  const classes = useStyles();
  const user = props.user;
  const selectedOfc = props.selectedOfc;
  const [open, setOpen] = useState(props.open);
  const [showDialog, setshowDialog] = useState(false);
  const [snackbaropen, setsnackbaropen] = useState(false);
  const [errorsnackbaropen, seterrorsnackbaropen] = useState(false);
  const [snackBarMessage, setsnackBarMessage] = useState(false);
  const [shiftTypes, setShiftTypes] = useState();
  const [selectedShiftTypes, setSelectedShiftTypes] = useState();
  const [customShiftTypes, setCustomShiftTypes] = useState(false);
  const [shiftTypeTouched, setShiftTypeTouched] = useState(false);

  const loggedInUser = userVar();
  //const [newFloatUnit, setNewFloatUnit] = useState();
  const alloffices = useQuery(ALL_OFFICES, {
    onError(error) {
      console.error(error);
    },
  });
  const officeid = selectedOfc;
  // !selectedOfc || selectedOfc === null || selectedOfc === ""
  //   ? loggedInUser.office.id
  //   : selectedOfc;
  const office =
    !alloffices.loading &&
    alloffices.data.offices.find(
      (e) => e.id === (!selectedOfc ? loggedInUser.office : selectedOfc)
    );

  let matchingAssignment;
  if (user) {
    const matching = user.assignmentSet.find(
      (assignment) => assignment.office.id === officeid
    );
    matchingAssignment = matching ? matching : user.assignmentSet[0];
  }

  const [updateEmployee] = useMutation(UPDATE_EMPLOYEE, {
    onError(error) {
      console.error(error);
    },
  });
  const allEmployeeTypes = useQuery(EMPLOYEE_TYPES, {
    onError(error) {
      console.error(error);
    },
  });
  const allskilltypes = useQuery(FIND_SKILL_TYPE, {
    onError(error) {
      console.error(error);
    },
  });
  const findemployeeById = useQuery(FIND_EMPLOYEE_BY_ID, {
    variables: {
      id: user != null && parseInt(user.id),
    },
    onError(error) {
      console.error(error);
    },
  });
  const jobtyperoles = useQuery(JOB_TYPE_ROLE, {
    onError(error) {
      console.error(error);
    },
  });

  const certs =
    !allskilltypes.loading &&
    findskillset(allskilltypes.data.skills, "CERTIFICATION");
  const userCerts = user != null && findskillset(user.skills, "CERTIFICATION");

  const trng =
    !allskilltypes.loading &&
    findskillset(allskilltypes.data.skills, "TRAINING", officeid);
  const userTrng = user != null && findskillset(user.skills, "TRAINING");

  const lics =
    !allskilltypes.loading &&
    findskillset(allskilltypes.data.skills, "LICENSE");
  const userLics = user != null && findskillset(user.skills, "LICENSE");

  const educ =
    !allskilltypes.loading &&
    findskillset(allskilltypes.data.skills, "EDUCATION");
  const useredu = user != null && findskillset(user.skills, "EDUCATION");

  const jobType =
    !allskilltypes.loading &&
    findskillset(allskilltypes.data.skills, "JOB_TYPE", officeid);
  const userjobType = user && user.skills && user.skills.filter(e=>e.variety==="JOB_TYPE");

  const preferenceTypes = useQuery(PREFERENCE_TYPE, {
    onError(error) {
      console.error(error);
    },
  });
  const allShiftTypes = useQuery(ALL_SHIFT_TYPES, {
    variables: { office: parseInt(officeid) },
    onError(error) {
      console.error(error);
    },
  });

  useEffect(() => {
    if (allShiftTypes.data) {
      let shiftTypes = [];
      allShiftTypes.data.shiftDefinitions.forEach((shift) => {
        const existingShift = shiftTypes.find(
          (type) => type.name === shift.description
        );
        if (existingShift) {
          existingShift.ids.push({
            day: shift.weekday.name,
            id: shift.id,
          });
        } else {
          const shiftObject = {
            name: shift.description,
            ids: [
              {
                day: shift.weekday.name,
                id: shift.id,
              },
            ],
          };
          shiftTypes.push(shiftObject);
        }
      });
      setShiftTypes(shiftTypes);
    }
  }, [allShiftTypes]);

  // const [createEmpPreferences] = useMutation(CREATE_EMPLOYEE_PREFERENCES);

  const [updateEmployeeAssignment] = useMutation(UPDATE_EMPLOYEE_ASSIGNMENT, {
    onCompleted() {
      refetch();
    },
    onError(error) {
      console.error(error);
    },
  });

  const [addEmployeeSkills] = useMutation(ADD_EMPLOYEE_SKILLS, {
    update(cache, { data: { createEmployeeSkill } }) {
      cache.modify({
        fields: {
          employeeSkill(existingEmployeeSkill = []) {
            const newEmpSkillRef = cache.writeFragment({
              data: createEmployeeSkill.employeeSkill,
              fragment: gql`
                fragment NewEmployeeSkill on EmployeeSkillNode {
                  skill {
                    id
                    variety
                    name
                  }
                }
              `,
            });
            return [...existingEmployeeSkill, newEmpSkillRef];
          },
        },
      });
    },
    onCompleted() {
      refetch();
    },
    onError(error) {
      console.error(error);
    },
  });

  const [deletemployeeSkills] = useMutation(DELETE_EMPLOYEE_SKILLS, {
    update(cache, { data: { deleteEmployeeSkill } }) {
      cache.evict({
        data: { id: deleteEmployeeSkill.deletedId },
      });
    },
    onError(error) {
      console.error(error);
    },
  });

  const handleUpdateEmployee = (values) => {
    const roleid =
      !jobtyperoles.loading &&
      jobtyperoles.data.jobTypeRoles.find(
        (i) => i.jobType.name === values.jobType
      ).role.id;
    const employeetypeid =
      !allEmployeeTypes.loading &&
      allEmployeeTypes.data.employeeTypes.find(
        (i) => i.name === values.employmentType
      ).id;
    const officeid = alloffices.data.offices.find(
      (i) => i.name === values.department
    ).id;
    const jt = jobType.find((i) => i.name === values.jobType);
    // const shiftTypeid = allShiftTypes.data.shiftTypes.find(
    //   (i) => i.name === values.shiftType
    // ).id;
    const phone = values.phone.replace(/\D/g, "");
    updateEmployee({
      variables: {
        id: values.id,
        input: {
          firstName: values.firstName,
          lastName: values.lastName,
          email: values.email,
          personalEmail: values.personalEmail,
          primaryPhone: phone,
          password: values.password,
        },
      },
    });

    if (shiftTypeTouched) {
      const originalShifts = matchingAssignment.hiredshiftSet;
      const assignmentId = matchingAssignment.id;
      const selectedShiftTypesLength = selectedShiftTypes
        .map((shift) => shift.ids)
        .flat().length;

      if (selectedShiftTypesLength === originalShifts.length) {
        let inputData = [];
        selectedShiftTypes.forEach((shiftType) => {
          const idsWithAssignment = shiftType.ids.map((id, index) => {
            return {
              id: originalShifts[index].id,
              assignment: assignmentId,
              shiftDefinition: id.id,
            };
          });
          inputData = [...inputData, ...idsWithAssignment];
        });
        batchUpdateHiredShifts({ variables: { input: inputData } });
      } else if (selectedShiftTypes.length === 0) {
        const originalIds = originalShifts.map((shift) => shift.id);
        batchDeleteHiredShifts({ variables: { ids: originalIds } });
      } else {
        const originalIds = originalShifts.map((shift) => shift.id);
        batchDeleteHiredShifts({ variables: { ids: originalIds } });
        let inputData = [];
        selectedShiftTypes.forEach((shiftType) => {
          const idsWithAssignment = shiftType.ids.map((id) => {
            return {
              assignment: assignmentId,
              shiftDefinition: id.id,
            };
          });
          inputData = [...inputData, ...idsWithAssignment];
        });
        batchCreateHiredShifts({ variables: { input: inputData } });
      }
    }

    UpdateAssignment(
      updateEmployeeAssignment,
      matchingAssignment.id,
      employeetypeid,
      roleid,
      officeid,
      values.empStartDate,
      values.schedulable,
      // shiftTypeid,
      values.rotationLength
    );
    updateSkillSet(
      userLics,
      values.license,
      addEmployeeSkills,
      values.id,
      findemployeeById,
      deletemployeeSkills,
      "LICENSE"
    );
    updateSkillSet(
      userTrng,
      values.training,
      addEmployeeSkills,
      values.id,
      findemployeeById,
      deletemployeeSkills,
      "TRAINING"
    );
    updateSkillSet(
      userCerts,
      values.certifications,
      addEmployeeSkills,
      values.id,
      findemployeeById,
      deletemployeeSkills,
      "CERTIFICATION"
    );
    updateSkillSet(
      useredu,
      values.education,
      addEmployeeSkills,
      values.id,
      findemployeeById,
      deletemployeeSkills,
      "EDUCATION"
    );
    updateSkillSet(
      userjobType,
      jt,
      addEmployeeSkills,
      values.id,
      findemployeeById,
      deletemployeeSkills,
      "JOB_TYPE"
    );
    setsnackBarMessage("Employee Successfully updated");
    setsnackbaropen(true);
  };

  const [batchCreateHiredShifts] = useMutation(BATCH_CREATE_HIRED_SHIFTS, {
    onError(error) {
      console.error(error);
    },
  });
  const [batchUpdateHiredShifts] = useMutation(BATCH_UPDATE_HIRED_SHIFTS, {
    onError(error) {
      console.error(error);
    },
  });
  const [batchDeleteHiredShifts] = useMutation(BATCH_DELETE_HIRED_SHIFTS, {
    onError(error) {
      console.error(error);
    },
  });

  const [createEmployeeAssignment] = useMutation(CREATE_EMPLOYEE_ASSIGNMENT, {
    onCompleted(data) {
      if (selectedShiftTypes.length !== 0) {
        const assignmentId = data.createAssignment.assignment.id;
        let inputData = [];
        selectedShiftTypes.forEach((shiftType) => {
          const idsWithAssignment = shiftType.ids.map((id) => {
            return {
              assignment: assignmentId,
              shiftDefinition: id.id,
            };
          });
          inputData = [...inputData, ...idsWithAssignment];
        });
        batchCreateHiredShifts({ variables: { input: inputData } });
      }
    },
    onError(error) {
      console.error(error);
    },
  });

  const [vals, setvals] = useState({});

  const [addEmployee] = useMutation(ADD_EMPLOYEE, {
    update(cache, { data: { createEmployee } }) {
      cache.modify({
        fields: {
          employee(existingEmployees = []) {
            const newEmpRef = cache.writeFragment({
              data: createEmployee.employee,
              fragment: gql`
                fragment NewEmployee on EmployeeNode {
                  id
                }
              `,
            });
            return [...existingEmployees, newEmpRef];
          },
        },
      });
    },
    onCompleted(data) {
      const a = data.createEmployee.employee.id;
      const roleid =
        !jobtyperoles.loading &&
        jobtyperoles.data.jobTypeRoles.find(
          (i) => i.jobType.name === vals.jobType
        ).role.id;
      const employeetypeid = allEmployeeTypes.data.employeeTypes.find(
        (i) => i.name === vals.employmentType
      ).id;
      const officeid = alloffices.data.offices.find(
        (i) => i.name === vals.department
      ).id;
      const jt = jobType.find((i) => i.name === vals.jobType);

      // const shiftTypeid = allShiftTypes.data.shiftTypes.find(
      //   (i) => i.name === vals.shiftType
      // ).id;
      createEmployeeAssignment({
        variables: {
          input: {
            employee: parseInt(a),
            office: parseInt(officeid),
            primary: true,
            role: parseInt(roleid),
            employeeType: parseInt(employeetypeid),
            startDate: vals.empStartDate,
            schedulable: vals.schedulable,
            rotationLength: vals.rotationLength,
          },
        },
      });
      //create employee skillset

      updateSkillSet(
        userLics,
        vals.license,
        addEmployeeSkills,
        parseInt(data.createEmployee.employee.id),
        findemployeeById,
        deletemployeeSkills,
        "LICENSE"
      );
      updateSkillSet(
        userTrng,
        vals.training,
        addEmployeeSkills,
        parseInt(data.createEmployee.employee.id),
        deletemployeeSkills,
        "TRAINING"
      );
      updateSkillSet(
        userCerts,
        vals.certifications,
        addEmployeeSkills,
        parseInt(data.createEmployee.employee.id),
        findemployeeById,
        deletemployeeSkills,
        "CERTIFICATION"
      );
      updateSkillSet(
        useredu,
        vals.education,
        addEmployeeSkills,
        parseInt(data.createEmployee.employee.id),
        findemployeeById,
        deletemployeeSkills,
        "EDUCATION"
      );
      updateSkillSet(
        userjobType,
        jt,
        addEmployeeSkills,
        parseInt(data.createEmployee.employee.id),
        findemployeeById,
        deletemployeeSkills,
        "JOB_TYPE"
      );

      //create preferences for this employee
      // !preferenceTypes.loading &&
      //   preferenceTypes.data.preferenceTypes.map((element) =>
      //     createEmpPreferences({
      //       variables: {
      //         input: {
      //           employee: parseInt(a),
      //           type: parseInt(element.id),
      //           value: 0,
      //         },
      //       },
      //     })
      //   );
      refetch();
      setvals({});
      setsnackBarMessage("Employee Successfully Added");
      setsnackbaropen(true);
    },
    onError(error) {
      console.error(error);
      setsnackBarMessage(error);
      seterrorsnackbaropen(true);
    },
  });
  const [emailExist] = useLazyQuery(Check_Email_Exists, {
    onCompleted(data) {
      if (data.emailExists != null) {
        setsnackBarMessage(
          "Email with the same user exists. Please try with another email."
        );
        seterrorsnackbaropen(true);
      } else {
        handleAddEmployee(vals);
        seterrorsnackbaropen(false);
        setsnackBarMessage("");
      }
    },
    onError(error) {
      console.error(error);
    },
  });
  const handleAddEmployee = (values) => {
    const phone = values.phone.replace(/\D/g, "");
    addEmployee({
      variables: {
        input: {
          firstName: values.firstName,
          lastName: values.lastName,
          email: values.email,
          personalEmail: values.personalEmail,
          password: "Test@123",
          primaryPhone: phone,
          hasChangedDefaultPassword: false,
        },
      },
    });
    setvals(values);
  };
  const handlePhoneFormat = (e) => {
    let input = e.replace(/\D/g, "");

    if (input.length >= 10) {
      input =
        "(" +
        input.slice(0, 3) +
        ")-" +
        input.slice(3, 6) +
        "-" +
        input.slice(6, 10);
    } else {
      input = e;
    }

    return input;
  };

  const getUserRole = (value) => {
    let role =
      !jobtyperoles.loading &&
      jobtyperoles.data.jobTypeRoles.find((e) => e.jobType.name === value);
    console.log(role);
    if (role && role.role.name === "ADMIN") {
      return true;
    } else {
      return false;
    }
  };

  const handleAdminRoleChange = () => {
    setsnackBarMessage("You dont have permission to change this user's role to Admin");
    seterrorsnackbaropen(true);
  };

  return (
    <main variant="body">
      {open ? (
        <>
          <Snackbar
            open={snackbaropen}
            autoHideDuration={3000}
            onClose={() => {
              setsnackbaropen(false);
              setOpen(false);
            }}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <MuiAlert
              onClose={() => {
                setsnackbaropen(false);
                setsnackBarMessage("");
              }}
              severity="success"
            >
              {snackBarMessage}
            </MuiAlert>
          </Snackbar>
          <Snackbar
            open={errorsnackbaropen}
            autoHideDuration={6000}
            onClose={() => {
              seterrorsnackbaropen(false);
            }}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <MuiAlert
              onClose={() => seterrorsnackbaropen(false)}
              severity="error"
            >
              {snackBarMessage}
            </MuiAlert>
          </Snackbar>
          <Formik
            initialValues={
              user == null
                ? {
                    firstName: "",
                    lastName: "",
                    email: "",
                    personalEmail: "",
                    phone: "",
                    password: "",
                    certifications: [],
                    license: [],
                    training: [],
                    education: [],
                    employmentType: "",
                    shiftType: "",
                    department: loggedInUser.isAdmin
                      ? office
                        ? office.name
                        : loggedInUser.office.name
                      : loggedInUser.office.name,
                    schedulable: "",
                    jobType: "",
                    empStartDate: "",
                    preferenceUpdatedDate: "",
                    rotationLength: 0,
                  }
                : {
                    id: parseInt(user.id),
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                    personalEmail: user.personalEmail,
                    phone: user.primaryPhone,
                    id: user.id,
                    certifications: userCerts ? userCerts : [],
                    license: userLics ? userLics : [],
                    training: userTrng ? userTrng : [],
                    education: useredu ? useredu : [],
                    employmentType: matchingAssignment.employeeType.name,
                    shiftType: mapHiredShiftsManagerView(
                      matchingAssignment.hiredshiftSet
                    ),
                    department: matchingAssignment.office.name,
                    schedulable: matchingAssignment.schedulable,
                    jobType: userjobType && userjobType.length > 0 ? userjobType[0].name : "",
                    empStartDate: matchingAssignment.startDate,
                    preferenceUpdatedDate: user.preferenceSet[0]
                      ? user.preferenceSet[0].updatedAt
                      : "",
                    rotationLength: matchingAssignment.rotationLength,
                    //floatList: user.float_list
                  }
            }
            validationSchema={validationSchema}
            onSubmit={(values) => {
              if (user === null) {
                emailExist({
                  variables: {
                    email: values.email,
                  },
                });
                setvals(values);
              } else {
                handleUpdateEmployee(values);
              }
            }}
          >
            {({
              handleSubmit,
              handleChange,
              values,
              errors,
              setFieldValue,
              setFieldError,
              isValid,
              dirty,
            }) => (
              <form onSubmit={handleSubmit}>
                <PromptIfDirty />
                <Grid container direction="row" spacing={3}>
                  <Grid item xs={6} className={classes.headerSpacing}>
                    <Typography variant="h3"> Update Profile</Typography>
                  </Grid>
                  <Grid item xs={2} className={classes.headerSpacing}></Grid>
                  <Grid
                    item
                    xs={2}
                    align="right"
                    className={classes.headerSpacing}
                  >
                    <Button
                      variant="outlined"
                      onClick={() => setOpen(false)}
                      color="primary"
                      className={classes.button}
                    >
                      Cancel
                    </Button>
                  </Grid>
                  <Grid
                    item
                    xs={2}
                    align="left"
                    className={classes.headerSpacing}
                  >
                    {user == null ? (
                      <Button
                        type="submit"
                        disabled={!dirty}
                        variant="contained"
                        color="primary"
                        className={classes.button}
                      >
                        Add
                      </Button>
                    ) : (
                      <Button
                        type="submit"
                        disabled={!dirty}
                        variant="contained"
                        color="primary"
                        className={classes.button}
                      >
                        Update
                      </Button>
                    )}
                  </Grid>
                  <Grid item xs={9} align="right">
                    <Divider className={classes.divider} />
                  </Grid>
                  <Grid item xs={3}></Grid>
                  <Grid item xs={9} align="center">
                    <Typography variant="h4">Contact Details</Typography>
                  </Grid>
                  <Grid item xs={3}></Grid>

                  <Grid item xs={4}>
                    <Typography variant="h5">First Name</Typography>
                    <TextField
                      id="first-name"
                      variant="outlined"
                      name="firstName"
                      className={classes.inputWidth}
                      value={values.firstName}
                      onChange={handleChange}
                    />
                    <Typography color="primary" variant="subtitle2">
                      {errors.firstName}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant="h5">Last Name</Typography>
                    <TextField
                      className={classes.inputWidth}
                      id="last-name"
                      variant="outlined"
                      name="lastName"
                      value={values.lastName}
                      onChange={handleChange}
                    />
                    <Typography color="primary" variant="subtitle2">
                      {errors.lastName}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}></Grid>

                  <Grid item xs={4}>
                    <Typography variant="h5">Work Email</Typography>
                    <TextField
                      className={classes.inputWidth}
                      id="email"
                      variant="outlined"
                      name="email"
                      value={values.email}
                      onChange={handleChange}
                    />
                    <Typography color="primary" variant="subtitle2">
                      {errors.email}
                    </Typography>
                  </Grid>
                  {/* <Grid item xs={4}></Grid> */}
                  <Grid item xs={4}>
                    <Typography variant="h5">Personal Email</Typography>
                    <TextField
                      className={classes.inputWidth}
                      id="personalEmail"
                      variant="outlined"
                      name="personalEmail"
                      value={values.personalEmail}
                      onChange={handleChange}
                    />
                    <Typography color="primary" variant="subtitle2">
                      {errors.personalEmail}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}></Grid>
                  <Grid item xs={4}>
                    <Typography variant="h5">Phone</Typography>
                    <TextField
                      id="phone"
                      variant="outlined"
                      className={classes.inputWidth}
                      name="phone"
                      InputProps={{
                        startAdornment: (
                          <InputAdornment position="start">+1</InputAdornment>
                        ),
                      }}
                      placeholder="xxx-xxx-xxxx"
                      value={values.phone}
                      onChange={(e) =>
                        setFieldValue(
                          "phone",
                          handlePhoneFormat(e.target.value)
                        )
                      }
                    />

                    <Typography color="primary" variant="subtitle2">
                      {errors.phone}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <Typography variant="h5">
                      Allow User to be on Schedule
                    </Typography>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={values.schedulable ? true : false}
                          onChange={handleChange}
                          name="schedulable"
                        />
                      }
                      label="Yes"
                    />
                  </Grid>
                  <Grid item xs={4}></Grid>

                  <Grid item xs={9}>
                    <Divider className={classes.divider} />
                  </Grid>
                  <Grid item xs={3}></Grid>
                  <Grid item xs={9} align="center">
                    <Typography variant="h4">Skillset</Typography>
                  </Grid>
                  <Grid item xs={3}></Grid>

                  <Grid item xs={3}>
                    <Typography variant="h5">Certifications</Typography>
                    <MultiSelect
                      options={
                        certs.length > 0 ? certs : [{ id: 0, name: "none" }]
                      }
                      name="certifications"
                      id="certifications"
                      placeholder="certifications"
                      onChange={setFieldValue}
                      val={values.certifications}
                      getOptionSelected={(option, value) =>
                        option.name === value.name
                      }
                      getOptionLabel={(option) => option.name}
                    />
                    <Typography color="primary" variant="subtitle2">
                      {errors.certifications}
                    </Typography>
                  </Grid>
                  <Grid item xs={3}>
                    <Typography variant="h5">Training</Typography>
                    <MultiSelect
                      options={
                        trng.length > 0 ? trng : [{ id: 0, name: "none" }]
                      }
                      name="training"
                      id="training"
                      placeholder="training"
                      onChange={setFieldValue}
                      val={values.training}
                      getOptionSelected={(option, value) =>
                        option.name === value.name
                      }
                      getOptionLabel={(option) => option.name}
                    />
                    <Typography color="primary" variant="subtitle2">
                      {errors.training}
                    </Typography>
                  </Grid>
                  <Grid item xs={3}>
                    <Typography variant="h5">Licensure</Typography>
                    <MultiSelect
                      options={
                        lics.length > 0 ? lics : [{ id: 0, name: "none" }]
                      }
                      name="license"
                      id="license"
                      placeholder="license"
                      onChange={setFieldValue}
                      val={values.license}
                      getOptionSelected={(option, value) =>
                        option.name === value.name
                      }
                      getOptionLabel={(option) => option.name}
                    />
                  </Grid>
                  <Grid item xs={3}></Grid>
                  <Grid item xs={4}></Grid>
                  <Grid item xs={9}>
                    <Divider className={classes.divider} />
                  </Grid>
                  <Grid item xs={3}></Grid>
                  <Grid item xs={9} align="center">
                    <Typography variant="h4">Employment Details</Typography>
                  </Grid>
                  <Grid item xs={3}></Grid>
                  <Grid item xs={4}>
                    <Typography variant="h5">Employment Type</Typography>
                    <Select
                      id="employmentType"
                      name="employmentType"
                      value={values.employmentType}
                      onChange={handleChange}
                      className={classes.inputWidth}
                      variant="outlined"
                    >
                      {allEmployeeTypes?.data?.employeeTypes?.map((e) => (
                        <MenuItem key={e.id} value={e.name}>
                          {e.name}
                        </MenuItem>
                      ))}
                    </Select>
                    <Typography color="primary" variant="subtitle2">
                      {errors.employmentType}
                    </Typography>
                  </Grid>

                  <Grid item xs={4}>
                    <Typography variant="h5">Department</Typography>
                    <Select
                      id="department"
                      name="department"
                      value={values.department}
                      onChange={
                        user
                          ? (e) => {
                              setshowDialog(true);
                              setFieldValue("department", e.target.value);
                            }
                          : handleChange
                      }
                      disabled={loggedInUser.isAdmin ? false : true}
                      className={classes.inputWidth}
                      variant="outlined"
                    >
                      {alloffices.data.offices.map((i) => (
                        <MenuItem key={i.id} value={i.name}>
                          {i.name}
                        </MenuItem>
                      ))}
                    </Select>
                    <Typography color="primary" variant="subtitle2">
                      {errors.department}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}></Grid>

                  <Grid item xs={4}>
                    <Typography variant="h5">Job Type</Typography>
                    <Select
                      id="jobTitle"
                      variant="outlined"
                      name="jobTitle"
                      value={values.jobType ? values.jobType : ""}
                      onChange={(e) =>
                        getUserRole(e.target.value) && !loggedInUser.isAdmin
                          ? handleAdminRoleChange()
                          : setFieldValue("jobType", e.target.value)
                      }
                      className={classes.inputWidth}
                      disabled={
                        getUserRole(values.jobType) && !loggedInUser.isAdmin
                      }
                    >
                      {!allskilltypes.loading &&
                        jobType.length > 0 &&
                        jobType.map((i, index) => (
                          <MenuItem key={index} value={i.name}>
                            {i.name}
                          </MenuItem>
                        ))}
                    </Select>
                    <Typography color="primary" variant="subtitle2">
                      {errors.jobType}
                    </Typography>
                  </Grid>

                  <Grid item xs={4}>
                    <Typography variant="h5">Assignment Start Date</Typography>
                    <TextField
                      id="empStartDate"
                      name="empStartDate"
                      variant="outlined"
                      type="date"
                      value={values.empStartDate}
                      onChange={handleChange}
                      className={classes.inputWidth}
                    />
                    <Typography color="primary" variant="subtitle2">
                      {errors.empStartDate}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}></Grid>

                  <Grid item xs={4}>
                    <Typography variant="h5">Shift Type</Typography>
                    <Select
                      id="shiftType"
                      name="shiftType"
                      value={values.shiftType}
                      onBlur={() => setShiftTypeTouched(true)}
                      onChange={(event) => {
                        handleChange(event);
                        if (event.target.value === "Any Shift") {
                          setSelectedShiftTypes([]);
                        } else if (event.target.value !== "Custom") {
                          const matchingShiftType = shiftTypes.find(
                            (shiftType) => shiftType.name === event.target.value
                          );
                          setSelectedShiftTypes([matchingShiftType]);
                        }
                      }}
                      className={classes.inputWidth}
                      variant="outlined"
                    >
                      <MenuItem value={"Any Shift"}>Any Shift</MenuItem>
                      {shiftTypes &&
                        shiftTypes.map((shiftType) => (
                          <MenuItem key={shiftType.name} value={shiftType.name}>
                            {shiftType.name}
                          </MenuItem>
                        ))}
                      <MenuItem
                        value="Custom"
                        onClick={() => setCustomShiftTypes(true)}
                      >
                        <Typography color="primary">
                          Custom Shift Schedule
                        </Typography>
                      </MenuItem>
                    </Select>
                    <Typography color="primary" variant="subtitle2">
                      {errors.shiftType}
                    </Typography>
                  </Grid>

                  <Grid item xs={4}>
                    <Typography variant="h5">Rotate Shifts</Typography>
                    <TextField
                      id="rotationLength"
                      name="rotationLength"
                      variant="outlined"
                      type="number"
                      value={values.rotationLength}
                      onChange={handleChange}
                      className={classes.inputWidth}
                    />
                    <Typography variant="body2">
                      Enter value in weeks. Set to 0 if employee does not rotate
                      shifts.
                    </Typography>
                    <Typography color="primary" variant="subtitle2">
                      {errors.rotationLength}
                    </Typography>
                  </Grid>
                  <Grid item xs={4}></Grid>

                  <Grid item xs={4}>
                    <Typography variant="h5">Education</Typography>
                    <MultiSelect
                      options={
                        educ.length > 0 ? educ : [{ id: 0, name: "none" }]
                      }
                      name="education"
                      id="education"
                      placeholder="education"
                      onChange={setFieldValue}
                      val={values.education}
                      getOptionSelected={(option, value) =>
                        option.name === value.name
                      }
                      getOptionLabel={(option) => option.name}
                    />
                    <Typography color="primary" variant="subtitle2">
                      {errors.education}
                    </Typography>
                  </Grid>

                  {values.preferenceUpdatedDate != "" && (
                    <Grid item xs={4}>
                      <Typography variant="h5">
                        Preference Last Updated
                      </Typography>
                      <TextField
                        id="preferenceUpdatedDate"
                        name="preferenceUpdatedDate"
                        variant="outlined"
                        type="date"
                        value={values.preferenceUpdatedDate.slice(0, 10)}
                        disabled={true}
                        onChange={handleChange}
                        className={classes.inputWidth}
                      />
                    </Grid>
                  )}
                  <Grid item xs={4}></Grid>
                  {/* <Grid item xs={4}> */}
                  {/* <Typography variant="h5">Maximum Gray Out Dates</Typography>
                    <TextField
                      id="maxgrayOutDates"
                      className={classes.inputWidth}
                      disabled
                      variant="outlined"
                      name="maxgrayOutDates"
                      value={values.maxgrayOutDates}
                      onChange={handleChange}
                    />
                    <Typography color="primary" variant="subtitle2">
                      {errors.maxgrayOutDates}
                    </Typography> */}
                  {/* <FormGroup id="education"
                      name="education">
                      <FormControlLabel id="bsn" name="bsn"
                          control={<Checkbox value="BSN" onChange={handleChange} />}
                          label="BSN"
                      />
                      <FormControlLabel id="adn" name="adn"
                          control={<Checkbox value="ADN" onChange={handleChange} />}
                          label="ADN"
                      />
                      <FormControlLabel id="lpn" name="lpn"
                          control={<Checkbox value="LPN" onChange={handleChange} />}
                          label="LPN"
                      />
                      <FormControlLabel id="rn" name="rn"
                          control={<Checkbox valu="RN" onChange={handleChange} />}
                          label="RN"
                      />

                  </FormGroup> */}
                  {/* <Typography color="primary" variant="subtitle2">
                      {errors.education}
                    </Typography> */}
                  {/* </Grid> */}

                  <Grid item xs={9}>
                    <Divider className={classes.divider} />
                  </Grid>
                  {/* <Grid item xs={3}></Grid> 
                                <Grid item xs={9} align="center"><Typography variant="h4">Floating</Typography></Grid>
                                <Grid item xs={3}></Grid>
                                <FieldArray name='floatList' render={(arrayHelpers) => (
                                    <>
                                        <Grid item xs={9} container direction='column'>
                                            {values.floatList.length > 0 
                                                ? values.floatList.map((float, index) => (
                                                    <Grid key={float} item container spacing={1} alignItems='center'>
                                                        <Grid item >
                                                            <Typography>{float}</Typography>
                                                        </Grid>
                                                        <Grid item >
                                                            <IconButton onClick={() => arrayHelpers.remove(index)}><CloseIcon fontSize='small'/></IconButton>
                                                        </Grid>
                                                    </Grid>
                                                ))
                                                : null
                                            }
                                        </Grid>
                                        <Grid item container xs={12} justify='flex-start' spacing={3}>
                                            <Grid item xs={4}>
                                                <Select 
                                                    id='add-float'
                                                    className={classes.inputWidth}
                                                    variant="outlined"
                                                    value={newFloatUnit}
                                                    onChange={e => setNewFloatUnit(e.target.value)}
                                                >
                                                    {officeNames && officeNames.filter(name => !values.floatList.includes(name)).map(name => (
                                                        <MenuItem key={name} value={name}>{name}</MenuItem>
                                                    ))}
                                                </Select>
                                            </Grid>
                                            <Grid item xs={2}>
                                                <Button variant='outlined' color='primary' disabled={Boolean(newFloatUnit === '')} onClick={() => {
                                                    arrayHelpers.push(newFloatUnit);
                                                    setNewFloatUnit('');
                                                }}
                                                >  
                                                    Add
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </>
                                )}/> */}

                  <Grid item xs={8}></Grid>
                  <Grid
                    item
                    xs={2}
                    align="right"
                    className={classes.headerSpacing}
                  >
                    <Button
                      variant="outlined"
                      onClick={() => setOpen(false)}
                      color="primary"
                      className={classes.button}
                    >
                      Cancel
                    </Button>
                  </Grid>
                  <Grid
                    item
                    xs={2}
                    align="left"
                    className={classes.headerSpacing}
                  >
                    {/* {user == null ? (
                      <Button
                        type="submit"
                        disabled={!dirty}
                        variant="contained"
                        color="primary"
                        className={classes.button}
                      >
                        Add
                      </Button>
                    ) : ( */}
                    <Button
                      type="submit"
                      disabled={(!dirty && !shiftTypeTouched) || !isValid}
                      variant="contained"
                      color="primary"
                      className={classes.button}
                    >
                      Save
                    </Button>
                    {/* )} */}
                  </Grid>
                </Grid>
                <Dialog open={customShiftTypes} fullWidth maxWidth="sm">
                  <DialogContent
                    style={{
                      padding: 20,
                      overflowX: "hidden",
                      textAlign: "center",
                    }}
                  >
                    <CustomShiftType
                      allShiftTypes={shiftTypes}
                      userShiftTypes={
                        user ? matchingAssignment.hiredshiftSet : []
                      }
                      closeDialog={() => setCustomShiftTypes(false)}
                      selectedShiftTypes={selectedShiftTypes}
                      setSelectedShiftTypes={setSelectedShiftTypes}
                      setFieldError={setFieldError}
                      rotationValue={values.rotationLength}
                    />
                  </DialogContent>
                </Dialog>
              </form>
            )}
          </Formik>
          <Dialog open={showDialog} fullWidth maxWidth="xs">
            <DialogContent
              style={{ padding: 20, overflowX: "hidden", textAlign: "center" }}
            >
              <Typography>
                You are changing the user's department. The shifts he is
                currently assigned to for this department will not be affected
                by this change.
              </Typography>
              <br />
              <br />
              <Button
                onClick={() => setshowDialog(false)}
                color="primary"
                variant="contained"
              >
                OK
              </Button>
            </DialogContent>
          </Dialog>
        </>
      ) : (
        <MangUsers selOfc={office && office.name} />
      )}
    </main>
  );
};

export default UpdateUser;
