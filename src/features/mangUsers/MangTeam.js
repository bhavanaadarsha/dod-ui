import React, { useEffect, useState } from "react";
import {
  Paper,
  Box,
  Button,
  IconButton,
  Typography,
  makeStyles,
  Container,
  Grid,
  CircularProgress,
  Dialog,
  DialogContent,
  Snackbar,
  Select,
  MenuItem,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import UpdateUser from "./UpdateUser";
import { useQuery, useMutation } from "@apollo/client";
import {
  Get_Employees,
  Delete_Employee,
  ALL_OFFICES,
} from "../../api/gqlQueries";
import MuiAlert from "@material-ui/lab/Alert";
import { userVar } from "../../cache";
import { formatPhoneNumber } from "../../helpers/updateUserDetails";
import { Link } from "react-router-dom";
import {
  DataGrid,
  GridToolbarContainer,
  GridColumnsToolbarButton,
  GridFilterToolbarButton,
  GridToolbarExport,
} from "@material-ui/data-grid";
import TuneIcon from "@material-ui/icons/Tune";
import { mapHiredShiftsManagerView } from "../../helpers/hiredShifts";

const useStyles = makeStyles(() => ({
  dt: {
    "& .MuiDataGrid-columnsContainer": {
      backgroundColor: "#EAEAEA",
      color: "#333333",
    },
    "& .MuiButton-iconSizeSmall": {
      color: "rgba(134, 134, 137, 1)",
    },
    "& .MuiButton-label": {
      color: "rgba(134, 134, 137, 1)",
      fontSize: "15px",
    },
    border: "none",
    fontSize: "15px",
  },
  headerSpacing: {
    marginTop: 30,
    marginBottom: 20,
  },
}));

function MangUsers(props) {
  const classes = useStyles();
  const [openForm, setOpenForm] = useState(false);
  const [currentUser, setCurrentUser] = useState(null);
  const user = userVar();
  const allOffices = useQuery(ALL_OFFICES, {
    onError(error) {
      console.error(error);
    },
  });
  const [selectedOffice, SetselectedOffice] = useState(
    user != null && user.office.name
  );
  const [officeid, setofficeid] = useState(user.office.id);
  const { loading, data, error, refetch } = useQuery(Get_Employees, {
    variables: {
      id: user.isAdmin
        ? officeid
          ? parseInt(officeid)
          : parseInt(user.office.id)
        : parseInt(user.office.id),
    },
    onError(error) {
      console.error(error);
    },
  });

  const CustomToolbar = () => {
    return (
      <GridToolbarContainer style={{ justifyContent: "flex-end" }}>
        <GridColumnsToolbarButton />
        <GridFilterToolbarButton />
        <GridToolbarExport />
      </GridToolbarContainer>
    );
  };

  useEffect(() => {
    setofficeid(user.office.id);
    SetselectedOffice(
      props.selOfc ? props.selOfc : user != null && user.office.name
    );
  }, []);

  useEffect(() => {
    let o =
      !allOffices.loading &&
      allOffices.data.offices.find((i) => i.name === selectedOffice);
    setofficeid(o.id);
  }, [selectedOffice]);

  const handleOfficeChange = (e) => {
    let o =
      !allOffices.loading && allOffices.data.offices.find((i) => i.name === e);
    setofficeid(o.id);
    SetselectedOffice(e);
  };

  const [deleteEmployee] = useMutation(Delete_Employee, {
    update(cache, { data: { deleteEmployee } }) {
      cache.evict({
        id: `EmployeeNode:${deleteEmployee.deletedId}`,
      });
    },
    onCompleted() {
      refetch();
      setsnackBarMessage("Employee successfully deleted");
      setsnackbaropen(true);
    },
    onError(error) {
      console.error(error);
      setsnackBarMessage("Something went wrong. Please try again");
      seterrorsnackbaropen(true);
    },
  });

  const [showDialog, setshowDialog] = useState(false);
  const [snackbaropen, setsnackbaropen] = useState(false);
  const [errorsnackbaropen, seterrorsnackbaropen] = useState(false);
  const [snackBarMessage, setsnackBarMessage] = useState(false);
  const [useridtodelete, setuseridtodelete] = useState("");

  const handleDeleteUser = () => {
    deleteEmployee({ variables: { id: parseInt(useridtodelete) } });
    setuseridtodelete("");
    setshowDialog(false);
  };

  const getShiftType = (assignmentSet) => {
    const officeIdToMatch = officeid ? officeid : user.office.id;
    let matchingAssignment = assignmentSet.find(
      (assignment) => assignment.office.id === officeIdToMatch
    );
    matchingAssignment = matchingAssignment
      ? matchingAssignment
      : assignmentSet[0];
    return mapHiredShiftsManagerView(matchingAssignment.hiredshiftSet);
  };

  const columns = [
    {
      field: "Name",
      headerName: "Name",
      width: 200,
      renderCell: (params) => (
          <Button
            color="secondary"
            onClick={() => {
              setOpenForm(!openForm);
              setCurrentUser(params.row);
            }}
            style={{ textDecoration: "underline" }}
          >
            {params.getValue("lastName")+" " || ""}
            {params.getValue("firstName") || ""}
          </Button>
      ),
      valueGetter: (params) =>
      `${(params.row.lastName) || ""}`+" "+`${params.row.firstName || ""}`,
    },
    {
      field: "email",
      width: 200,
      headerName: "Email",
      type: "email",
    },
    {
      field: "primaryPhone",
      width: 150,
      headerName: "Phone",
      valueGetter: (params) =>
        `${formatPhoneNumber(params.row.primaryPhone) || ""}`,
    },
    {
      field: "jobType",
      width: 150,
      headerName: "JobType",
      valueGetter: (params) =>
        `${
          (params.row.skills.length > 0 &&
            params.row.skills.filter((i) => i.variety === "JOB_TYPE")[0]
              ?.name) ||
          ""
        }`,
    },
    {
      field: "shiftType",
      width: 200,
      headerName: "ShiftType",
      valueGetter: (params) =>
        `${getShiftType(params.row.assignmentSet) || ""}`,
    },
    {
      field: "employmentType",
      width: 150,
      headerName: "EmploymentType",
      valueGetter: (params) =>
        `${params.row.assignmentSet[0].employeeType.name || ""}`,
    },
    {
      field: "department",
      width: 150,
      headerName: "Department",
      valueGetter: (params) =>
        `${params.row.assignmentSet[0].office.name || ""}`,
    },
    {
      field: "Assignment Start Date",
      width: 150,
      headerName: "AssignmentStartDate",
      valueGetter: (params) => `${params.row.assignmentSet[0].startDate || ""}`,
    },
    {
      field: "Certification",
      width: 150,
      headerName: "Certification",
      hide: true,
      valueGetter: (params) =>
        `${
          params.row.skills
            .filter((i) => i.variety === "CERTIFICATION")
            ?.map((e) => e.name) || ""
        }`,
    },
    {
      field: "License",
      width: 150,
      headerName: "Licensure",
      hide: true,
      renderCell: (params) =>
        `${
          params.row.skills
            .filter((i) => i.variety === "LICENSE")
            ?.map((e) => e.name) || ""
        }`,
    },
    {
      field: "Training",
      width: 150,
      headerName: "Training",
      hide: true,
      renderCell: (params) =>
        `${
          params.row.skills
            .filter((i) => i.variety === "TRAINING")
            ?.map((e) => e.name) || ""
        }`,
    },
    {
      field: "Education",
      width: 150,
      headerName: "Education",
      hide: true,
      renderCell: (params) =>
        `${
          params.row.skills
            .filter((i) => i.variety === "EDUCATION")
            ?.map((e) => e.name) || ""
        }`,
    },
    {
      field: "Edit User",
      width: 150,
      headerName: "Edit User",
      filterable: false,
      sortable: false,
      filterable: false,
      sortable: false,
      renderCell: (params) => (
        <IconButton
          onClick={() => {
            setOpenForm(!openForm);
            setCurrentUser(params.row);
          }}
          color="secondary"
        >
          <EditIcon />
        </IconButton>
      ),
    },
    {
      field: "Delete User",
      width: 150,
      headerName: "Remove User",
      filterable: false,
      sortable: false,
      exportable: false,
      renderCell: (params) => (
        <IconButton
          onClick={() => {
            setuseridtodelete(params.row.id);
            setshowDialog(true);
          }}
          color="secondary"
        >
          <DeleteIcon />
        </IconButton>
      ),
    },
  ];

  const rows = !loading && data.offices[0].employeeSet;

  if (loading) {
    return <CircularProgress color="primary" />;
  } else if (error) {
    return <div>Something went wrong please try again</div>;
  } else {
    return (
      <main variant="body">
        {!openForm ? (
          <Container className={classes.root}>
            <Snackbar
              open={snackbaropen}
              autoHideDuration={3000}
              onClose={() => setsnackbaropen(false)}
              anchorOrigin={{ vertical: "top", horizontal: "center" }}
            >
              <MuiAlert
                onClose={() => {
                  setsnackbaropen(false);
                  setsnackBarMessage("");
                }}
                severity="success"
              >
                {snackBarMessage}
              </MuiAlert>
            </Snackbar>
            <Snackbar
              open={errorsnackbaropen}
              autoHideDuration={6000}
              onClose={() => seterrorsnackbaropen(false)}
              anchorOrigin={{ vertical: "top", horizontal: "center" }}
            >
              <MuiAlert
                onClose={() => seterrorsnackbaropen(false)}
                severity="error"
              >
                {snackBarMessage}
              </MuiAlert>
            </Snackbar>
            <Dialog open={showDialog} fullWidth maxWidth="xs">
              <DialogContent
                style={{
                  padding: 20,
                  overflowX: "hidden",
                  textAlign: "center",
                }}
              >
                <Typography>
                  Are you sure you want to delete this Employee?
                </Typography>
                <br />
                <br />
                <Button
                  onClick={handleDeleteUser}
                  color="primary"
                  variant="contained"
                >
                  Yes
                </Button>
                <Button
                  style={{ marginLeft: "20px" }}
                  onClick={() => setshowDialog(false)}
                  color="primary"
                  variant="outlined"
                >
                  No
                </Button>
              </DialogContent>
            </Dialog>

            <Grid container className={classes.headerSpacing}>
              <Grid item xs={6}>
                <Typography variant="h3">Manage Team</Typography>
              </Grid>
              {/* <Grid item container justify="flex-end" xs={2}>
                <Grid item>
                  <Button
                    color="primary"
                    variant="contained"
                    component={Link}
                    to="/ImportAvailability"
                  >
                    Import Availability
                  </Button>
                </Grid>
              </Grid>
              <Grid item container justify="flex-end" xs={2}>
                {user.isAdmin && (
                  <Grid item>
                    <Button
                      color="primary"
                      variant="contained"
                      component={Link}
                      to="/ImportEmployees"
                    >
                      Import Employees
                    </Button>
                  </Grid>
                )}
              </Grid> */}
              <Grid item container justify="flex-end" xs={2}>
                <Grid item>
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={() => {
                      setOpenForm(!openForm);
                      setCurrentUser(null);
                    }}
                  >
                    Add User
                  </Button>
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <Typography>
                  {!user.isAdmin
                    ? selectedOffice
                      ? selectedOffice
                      : user.office.name
                    : null}
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Box mt={2} mb={1}>
                  {user.isAdmin && (
                    <>
                      <Select
                        id="selectoffice"
                        name="selectoffice"
                        value={selectedOffice}
                        variant="outlined"
                        onChange={(e) => handleOfficeChange(e.target.value)}
                      >
                        {allOffices?.data?.offices?.map((i) => (
                          <MenuItem key={i.id} value={i.name}>
                            {i.name}
                          </MenuItem>
                        ))}
                      </Select>
                      <br />
                    </>
                  )}
                </Box>
              </Grid>
            </Grid>
            {error ? (
              <Paper>
                Something went wrong.{error.message}. Please try again later.
              </Paper>
            ) : loading ? (
              <CircularProgress color="primary" />
            ) : (
              <>
                <div style={{ height: 700 }}>
                  <DataGrid
                    className={classes.dt}
                    rows={rows}
                    columns={columns}
                    pageSize={10}
                    components={{
                      Toolbar: CustomToolbar,
                    }}
                  />
                </div>
              </>
            )}
          </Container>
        ) : (
          <UpdateUser
            refetch={refetch}
            user={currentUser}
            open={true}
            selectedOfc={officeid ? officeid : user.office.id}
          />
        )}
      </main>
    );
  }
}
export default MangUsers;
