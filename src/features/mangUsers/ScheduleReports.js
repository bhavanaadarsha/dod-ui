import React, { useEffect, useState } from "react";
import {
  SCHEDULE_FITNESS,
  ALL_OFFICES,
  GET_SCHEDULE_PERIODS,
} from "../../api/gqlQueries";
import { userVar } from "../../cache";
import { useLazyQuery, useQuery } from "@apollo/client";
import {
  Grid,
  Typography,
  Box,
  CircularProgress,
  Select,
  MenuItem,
} from "@material-ui/core";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
import { format } from "date-fns";

function calculateMode(arr) {
  var mode = 0;
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < i; j++) {
      if (arr[j] === arr[i]) {
        mode = arr[j];
        count++;
      }
    }
  }
  return mode;
}

function calculateMedian(arr) {
  const arrSort = arr.sort();
  var half = Math.floor(arrSort.length / 2);
  if (arrSort.length % 2) {
    return parseFloat(arrSort[half]);
  } else {
    return (
      (parseFloat(arrSort[half - 1]) + parseFloat(arrSort[half])) /
      2
    ).toFixed(2);
  }
}

function calculateAverage(arr) {
  let av = 0;
  for (let i = 1; i < arr.length; i++) {
    av = av + parseFloat(arr[i]);
  }
  av = av / arr.length;
  return av.toFixed(2);
}

const ScheduleReports = () => {
  const user = userVar();
  const [officeId, SetOfficeId] = useState(user.office.id);
  const [allSchedulePeriods, SetallSchedulePeriods] = useState([]);
  const [allScores, SetallScores] = useState([]);
  const [showLoader, SetshowLoader] = useState(false);

  const scheduleFitness = useQuery(SCHEDULE_FITNESS, {
    variables: {
      office: officeId === "" ? parseInt(user.office.id) : parseInt(officeId),
    },
    onError(error){
      console.error(error);
    }
  });

  const [allschedulePeriods, { loading, called }] = useLazyQuery(
    GET_SCHEDULE_PERIODS,
    {
      onCompleted(data) {
        SetallSchedulePeriods(data.schedulePeriods);
      },
      onError(error){
        console.error(error);
      }
    }
  );

  const alloffices = useQuery(ALL_OFFICES, {
    onError(error){
      console.error(error);
    }
  });
  const averageFitnessData =
    allScores.length > 0 &&
    allScores
      .filter((x) => x.id != "")
      .map((x) => {
        return {
          name: "Schedule Period",
          averageFitness: parseFloat(x.averageScore),
          schedulePeriods: x.schedulePeriod,
          maxAverageScore: x.maxAverage,
        };
      });

  const medianFitnessData =
    allScores.length > 0 &&
    allScores
      .filter((x) => x.id != "")
      .map((x) => {
        return {
          name: "Schedule Period",
          medianFitness: parseFloat(x.medianScore),
          schedulePeriods: x.schedulePeriod,
        };
      });

  const modeFitnessData =
    allScores.length > 0 &&
    allScores
      .filter((x) => x.id != "")
      .map((x) => {
        return {
          name: "Schedule Period",
          modeFitness: parseFloat(x.modeScore),
          schedulePeriods: x.schedulePeriod,
        };
      });

  useEffect(() => {
    let a = [];
    let b = [];
    let scores = [
      {
        id: "",
        schedulePeriod: "",
        averageScore: "",
        medianScore: "",
        modeScore: "",
        maxAverage: "",
      },
    ];
    allSchedulePeriods.length > 0 &&
      allSchedulePeriods.forEach((x) => {
        !scheduleFitness.loading &&
          scheduleFitness.data.employeeFitnesses
            .filter((m) => m.schedulePeriod && m.schedulePeriod.id === x.id)
            .map((e) => {
              a.push(parseFloat(e.score).toFixed(2));
              b.push(parseFloat(e.maxScore).toFixed(2));
            });
        scores.push({
          id: x.id,
          schedulePeriod:
            format(new Date(x.start), "MMM-dd") +
            " to " +
            format(new Date(x.end), "MMM-dd"),
          averageScore: calculateAverage(a),
          medianScore: calculateMedian(a),
          modeScore: calculateMode(a),
          maxAverage: calculateAverage(b),
        });
      });
    SetallScores(scores);
  }, [officeId]);

  useEffect(() => {
    let a = [];
    let b = [];
    let scores = [
      {
        id: "",
        schedulePeriod: "",
        averageScore: "",
        medianScore: "",
        modeScore: "",
        maxAverage: "",
      },
    ];
    allSchedulePeriods.length > 0 &&
      allSchedulePeriods.forEach((x) => {
        !scheduleFitness.loading &&
          scheduleFitness.data.employeeFitnesses
            .filter((m) => m.schedulePeriod.id === x.id)
            .map((e) => {
              a.push(parseFloat(e.score).toFixed(2));
              b.push(parseFloat(e.maxScore).toFixed(2));
            });
        scores.push({
          id: x.id,
          schedulePeriod:
            format(new Date(x.start), "MMM-dd") +
            " to " +
            format(new Date(x.end), "MMM-dd"),
          averageScore: calculateAverage(a),
          medianScore: calculateMedian(a),
          modeScore: calculateMode(a),
          maxAverage: calculateAverage(b),
        });
      });
    SetallScores(scores);
  }, [!scheduleFitness.loading]);

  const handleOfficeChange = (value) => {
    setTimeout(()=>{
      SetshowLoader(false);
    }, 2000);
    SetOfficeId(value);
    allschedulePeriods({
      variables: {
        officeId: parseInt(value),
      },
    });
  };

  if (scheduleFitness.loading || loading || showLoader || allScores.length<=0) {
    return <CircularProgress color="primary" />;
  } else {
    return (
      <Grid container direction="row" spacing={4}>
        <Grid item xs={12}>
          <Box m={2}>
            <Typography variant="h3">Schedule Fitness Reports</Typography>
          </Box>
        </Grid>
        <Grid item xs={3}>
          {user.isAdmin ? (
            <Box m={2}>
              <Select
                name="office"
                variant="outlined"
                value={officeId}
                onChange={(e) => {
                  SetOfficeId(e.target.value);
                  handleOfficeChange(e.target.value);
                  SetshowLoader(true);
                }}
              >
                {!alloffices.loading &&
                  alloffices.data.offices.map((e, index) => (
                    <MenuItem key={index} value={e.id}>
                      {e.name}
                    </MenuItem>
                  ))}
              </Select>
            </Box>
          ) : (
            <Box m={2}>
              <Typography variant="h5">{user.office.name}</Typography>
            </Box>
          )}
        </Grid>
        <Grid item xs={9}></Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={5}>
          <Typography variant="h5">Average Schedule Quality</Typography>
          <LineChart
            width={500}
            height={300}
            data={averageFitnessData}
            margin={{
              top: 10,
              right: 30,
              left: 20,
              bottom: 10,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="schedulePeriods" />
            <YAxis dataKey="averageFitness" />
            <Tooltip />
            <Legend />
            <Line
              type="monotone"
              dataKey="averageFitness"
              stroke="#f15a25"
              activeDot={{ r: 8 }}
            />
            <Line
              type="monotone"
              dataKey="maxAverageScore"
              stroke="#595a5c"
              activeDot={{ r: 8 }}
            />
          </LineChart>
        </Grid>
        <Grid item xs={5}>
          <Typography variant="h5">Median Schedule Quality</Typography>
          <LineChart
            width={500}
            height={300}
            data={medianFitnessData}
            margin={{
              top: 10,
              right: 30,
              left: 20,
              bottom: 10,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="schedulePeriods" />
            <YAxis dataKey="medianFitness" />
            <Tooltip />
            <Legend
              margin={{
                top: 20,
              }}
            />
            <Line
              type="monotone"
              dataKey="medianFitness"
              stroke="#f15a25"
              activeDot={{ r: 8 }}
            />
          </LineChart>
        </Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={5}>
          <Typography variant="h5">Schedule Quality - Mode</Typography>
          <LineChart
            width={500}
            height={300}
            data={modeFitnessData}
            margin={{
              top: 10,
              right: 30,
              left: 20,
              bottom: 10,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="schedulePeriods" />
            <YAxis dataKey="modeFitness" />
            <Tooltip />
            <Legend />
            <Line
              type="monotone"
              dataKey="modeFitness"
              stroke="#f15a25"
              activeDot={{ r: 8 }}
            />
          </LineChart>
        </Grid>
        <Grid item xs={6}></Grid>
      </Grid>
    );
  }
};

export default ScheduleReports;
