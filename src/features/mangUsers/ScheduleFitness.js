import React, { useEffect, useState } from "react";
import {
  Paper,
  CircularProgress,
  makeStyles,
  Dialog,
  DialogContent,
  IconButton,
  Container,
  Grid,
  Typography,
  Select,
  MenuItem,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  withStyles,
  Box,
} from "@material-ui/core";
import {
  DataGrid,
  GridToolbarContainer,
  GridColumnsToolbarButton,
  GridFilterToolbarButton,
  GridToolbarExport,
  gridColumnsTotalWidthSelector,
} from "@material-ui/data-grid";
import {
  SCHEDULE_FITNESS,
  GET_SCHEDULE_PERIODS,
  ALL_OFFICES,
  GET_PREFERENCES,
} from "../../api/gqlQueries";
import { userVar } from "../../cache";
import { useQuery, useLazyQuery } from "@apollo/client";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { format } from "date-fns";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles(() => ({
  dt: {
    "& .MuiDataGrid-columnsContainer": {
      backgroundColor: "#EAEAEA",
      color: "#333333",
    },
    "& .MuiButton-iconSizeSmall": {
      color: "rgba(134, 134, 137, 1)",
    },
    "& .MuiButton-label": {
      color: "rgba(134, 134, 137, 1)",
      fontSize: "15px",
    },
    border: "none",
    fontSize: "15px",
  },
  headerSpacing: {
    marginTop: 30,
    marginBottom: 20,
  },
}));

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "#EAEAEA",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRight: "5",
    paddingLeft: "5",
  },
  body: {
    fontSize: 14,
    backgroundColor: "#ffffff",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRight: "5",
    paddingLeft: "5",
    borderBottom: "none",
    borderTop: "1px solid lightgrey",
  },
}))(TableCell);

function ScheduleFitness() {
  const classes = useStyles();
  const user = userVar();
  const [officeId, SetOfficeId] = useState(user.office.id);
  const [allSchedulePeriod, setallSchedulePeriod] = useState([]);
  const [schedulePeriodData, setSchedulePeriodData] = useState("");
  const [selectedoffice, Setselectedoffice] = useState(user.office.name);
  const [selectedUser, setSelectedUser] = useState("");
  const [selectedUserPreferences, SetselectedUserPreferences] = useState([]);
  const [selectedUserName, SetselectedUserName] = useState("");
  const [selectSchedulePeriod, setselectSchedulePeriod] = useState(
    "Select Schedule Period"
  );
  const [schedulePeriods, SetschedulePeriods] = useState([]);

  const [showDialog, setshowDialog] = useState(false);

  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const [rows, setrows] = useState([]);

  const d = new Date();

  const [schedulePeriod] = useLazyQuery(GET_SCHEDULE_PERIODS, {
    onCompleted(data) {
      let schedulePeriods = ["Select Schedule Period"];
      data.schedulePeriods &&
        data.schedulePeriods.length > 0 &&
        data.schedulePeriods.forEach((e) => {
          schedulePeriods.push(
            format(new Date(e.start), "MMM-dd-yyyy") +
              " to " +
              format(new Date(e.end), "MMM-dd-yyyy")
          );
          if (startDate === "" || endDate === "") {
            if (d >= new Date(e.start) && d <= new Date(e.end)) {
              setselectSchedulePeriod(
                format(new Date(e.start), "MMM-dd-yyyy") +
                  " to " +
                  format(new Date(e.end), "MMM-dd-yyyy")
              );
              setSchedulePeriodData(e.id);
            }
            else{
              setselectSchedulePeriod("");
              setrows([]);
            }
          }
        });
      setallSchedulePeriod(data.schedulePeriods);
      SetschedulePeriods(schedulePeriods);
    },
    onError(error){
      console.error(error);
    }
  });

  const [scheduleFitness, { loading, error }] = useLazyQuery(SCHEDULE_FITNESS, {
    onCompleted(data) {
      setrows(data.employeeFitnesses);
    }
  });

  useEffect(() => {
    schedulePeriod({
      variables: {
        officeId: parseInt(user.office.id),
      },
    });
    Setselectedoffice(user.office.name);
  }, []);

  useEffect(() => {
    schedulePeriod({
      variables: {
        officeId: parseInt(officeId),
      },
    });
    setStartDate("");
    setEndDate("");
  }, [officeId]);

  useEffect(() => {
    if (schedulePeriodData!="") {
      scheduleFitness({
        variables: {
          schedulePeriod: parseInt(schedulePeriodData),
        },
      });
    } else {
      setrows([]);
    }
  }, [schedulePeriodData]);

  const CustomToolbar = () => {
    return (
      <GridToolbarContainer style={{ justifyContent: "flex-end" }}>
        <GridColumnsToolbarButton />
        <GridFilterToolbarButton />
        <GridToolbarExport />
      </GridToolbarContainer>
    );
  };

  const columns = [
    {
      field: "Name",
      headerName: "Name",
      width: 300,
      valueGetter: (params) =>
        `${
          params.row.employee.firstName + " " + params.row.employee.lastName ||
          ""
        }`,
    },
    {
      field: "score",
      width: 300,
      headerName: "Schedule Quality Score",
      valueGetter: (params) => `${params.row.score.toFixed(2)}`,
    },
    {
      field: "maxScore",
      width: 400,
      hide: true,
      headerName: "Max Score you can get based on your preferences",
      valueGetter: (params) => `${params.row.maxScore.toFixed(2)}`,
    },
    {
      field: "ratio",
      width: 300,
      headerName: "Overall Schedule Quality",
      valueGetter: (params) => `${params.row.ratio.toFixed(2)*100+'%'}`,
    },
    {
      field: "User Preferences",
      width: 300,
      headerName: "View User Preferences",
      filterable: false,
      sortable: false,
      renderCell: (params) => (
        <IconButton
          onClick={() => {
            setshowDialog(true);
            setSelectedUser(params.row.employee.id);
            SetselectedUserName(
              params.row.employee.firstName + " " + params.row.employee.lastName
            );
          }}
          color="secondary"
        >
          <VisibilityIcon />
        </IconButton>
      ),
    },
  ];

  const alloffices = useQuery(ALL_OFFICES, {
    onError(error){
      console.error(error);
    }
  });

  const [getPreferences] = useLazyQuery(GET_PREFERENCES, {
    onCompleted(data) {
      SetselectedUserPreferences(data.preferences);
    },
    onError(error){
      console.error(error);
    }
  });

  useEffect(() => {
    selectedUser != "" &&
      getPreferences({
        variables: {
          id: parseInt(selectedUser),
        },
      });
  }, [selectedUser]);

  const handleOfficeChange = (value) => {
    let ofc =
      !alloffices.loading &&
      alloffices.data.offices.find((e) => e.name === value);
    SetOfficeId(ofc.id);
  };

  const handleSchedulePeriodChange = (value) => {
    let start = value.substr(0, value.indexOf("to"));
    let end = value.split("to")[1];
    setStartDate(start);
    setEndDate(end);
    allSchedulePeriod.length > 0 &&
      allSchedulePeriod.find((e) => {
        if (
          new Date(e.start).toDateString() === new Date(start).toDateString() &&
          new Date(e.end).toDateString() === new Date(end).toDateString()
        ) {
          setSchedulePeriodData(e.id);
        }
      });
  };

  if (error) {
    console.log(error);
    return (
      <Paper>
        Something went wrong.{error.message}. Please try again later.
      </Paper>
    );
  } else if (loading) {
    return <CircularProgress color="primary" />;
  } else {
    return (
      <Container>
        <Dialog open={showDialog} fullWidth maxWidth="md">
          <DialogContent
            style={{ padding: 20, overflowX: "hidden", textAlign: "center" }}
          >
            <Box style={{ textAlign: "right" }}>
              <IconButton onClick={() => setshowDialog(false)} color="primary">
                <CloseIcon />
              </IconButton>
            </Box>

            <Typography>
              <b>Preferences - {selectedUserName}</b>
            </Typography>
            <Grid container direction="row" spacing={4}>
              <Grid item xs={2}></Grid>
              <Grid item xs={8}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <StyledTableCell colSpan={2}></StyledTableCell>
                    </TableRow>
                    <TableRow>
                      <StyledTableCell>Preference Type</StyledTableCell>
                      <StyledTableCell>Value</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {selectedUserPreferences.length &&
                      selectedUserPreferences.map((e, index) => (
                        <TableRow key={index}>
                          <StyledTableCell>{e.type.type}</StyledTableCell>
                          <StyledTableCell>{e.value}</StyledTableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </Grid>
              <Grid item xs={2}></Grid>
            </Grid>
          </DialogContent>
        </Dialog>
        <Grid container direction="row" spacing={2}>
          <Grid item xs={12} style={{ marginTop: 30 }}>
            <Typography variant="h3">Schedule Quality Report</Typography>
          </Grid>
          <Grid item xs={3}>
            {user.isAdmin ? (
              <Select
                name="office"
                variant="outlined"
                value={selectedoffice}
                onChange={(e) => {
                  Setselectedoffice(e.target.value);
                  handleOfficeChange(e.target.value);
                }}
              >
                {!alloffices.loading &&
                  alloffices.data.offices.map((e, index) => (
                    <MenuItem key={index} value={e.name}>
                      {e.name}
                    </MenuItem>
                  ))}
              </Select>
            ) : (
              <Typography variant="h5">{user.office.name}</Typography>
            )}
          </Grid>
          <Grid item xs={4} style={{ textAlign: "left" }}>
            <Select
              name="schedulePeriod"
              variant="outlined"
              value={selectSchedulePeriod}
              onChange={(e) => {
                setselectSchedulePeriod(e.target.value);
                handleSchedulePeriodChange(e.target.value);
              }}
              disabled={schedulePeriods.length <= 1 ? true : false}
            >
              {schedulePeriods &&
                schedulePeriods.length > 0 &&
                schedulePeriods.map((e, index) => (
                  <MenuItem key={index} value={e}>
                    {e}
                  </MenuItem>
                ))}
            </Select>
          </Grid>
          <Grid item xs={12}>
            <div style={{ height: 700 }}>
              <DataGrid
                className={classes.dt}
                rows={rows}
                columns={columns}
                pageSize={10}
                components={{
                  Toolbar: CustomToolbar,
                }}
              />
            </div>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

export default ScheduleFitness;
