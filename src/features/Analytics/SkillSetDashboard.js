import React, { useEffect, useState } from "react";
import {
  Grid,
  Typography,
  Box,
  CircularProgress,
  Select,
  MenuItem,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  withStyles,
  Divider,
  makeStyles
} from "@material-ui/core";
import { userVar } from "../../cache";
import {
  ANALYTICS_DATA,
  EMPLOYEE_NAMES,
  GET_SCHEDULE_PERIODS,
  FIND_SKILL_TYPE,
  SCHEDULE_FITNESS,
} from "../../api/gqlQueries";
import { useQuery, useLazyQuery } from "@apollo/client";
import AnalyticsWidget from "./AnalyticsGraphWidget";
import Plot from "react-plotly.js";
import { format } from "date-fns";
import { calculateAverage } from "../../helpers/analyticsHelpers";

const useStyles = makeStyles(() => ({
  select: {
    minWidth: 225
  }
}));

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "#EAEAEA",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRight: "5",
    paddingLeft: "5",
  },
  body: {
    fontSize: 14,
    backgroundColor: "#ffffff",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRight: "5",
    paddingLeft: "5",
    borderBottom: "none",
    borderTop: "1px solid lightgrey",
  },
}))(TableCell);

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

const SkillSetDashboard = () => {
  const classes = useStyles();
  
  const user = userVar();

  const [allSchedulePeriods, SetAllSchedulePeriods] = useState([]);
  const [selectSchedulePeriod, SetSelectSchedulePeriod] = useState("0");
  const [officeAnalyticsData, SetOfficeAnalyticsData] = useState([]);
  const [skillsetGraph, SetskillsetGraph] = useState(true);
  const [skillsetsq, Setskillsetsq] = useState(true);
  const [allemps, SetAllemps] = useState([]);
  const [showLoader, SetShowLoader] = useState(false);
  const [empFitnesses, SetEmpFitnesses] = useState([]);
  const { loading, error, data } = useQuery(ANALYTICS_DATA, {
    variables: {
      officeId: parseInt(user.office.id),
      source: "ALGORITHM",
    },
  });

  const scheduleFitness = useQuery(
    SCHEDULE_FITNESS,
    {
      variables: {
        office: parseInt(user.office.id),
      },
    },
    {
      onError(error) {
        console.error(error);
      },
    }
  );

  useEffect(() => {
    !scheduleFitness.loading &&
      SetEmpFitnesses(scheduleFitness.data.employeeFitnesses);
  }, [!scheduleFitness.loading]);

  const SkillType = useQuery(
    FIND_SKILL_TYPE,
    {
      variables: {
        office: parseInt(user.office.id),
      },
    },
    {
      onError(error) {
        console.error(error);
      },
    }
  );

  const getSkillName = (value) => {
    const findSkill =
      !SkillType.loading && SkillType.data.skills.find((x) => x.id === value);
    if (findSkill) {
      return findSkill.name;
    } else {
      return "";
    }
  };

  const getEmployeeName = (value) => {
    let a = value.toString();
    const findEmp = allemps.find((emp) => emp.id === a);
    if (findEmp) {
      return findEmp.firstName + " " + findEmp.lastName;
    } else {
      return "";
    }
  };

  const getSchedulePeriod = (value) => {
    const sch = allSchedulePeriods.find(
      (x) => parseInt(x.id) === parseInt(value)
    );
    if (sch) {
      return (
        format(new Date(`${sch.start}T12:00:00`), "dd MMM yyyy") +
        " to " +
        format(new Date(`${sch.end}T12:00:00`), "dd MMM yyyy")
      );
    } else {
      return "";
    }
  };

  useEffect(() => {
    SetOfficeAnalyticsData([]);
    let officeData = [...officeAnalyticsData];
    if (!loading) {
      data &&
        data.analyticsDatums &&
        data.analyticsDatums.length > 0 &&
        data.analyticsDatums.map((m) => {
          const parsed = JSON.parse(m.data);
          officeData.push(parsed);
        });
      const allSchedules = data ? [...data.offices[0].scheduleperiodSet] : [];
      allSchedules.sort((a, b) => new Date(b.start) - new Date(a.start));
      SetAllSchedulePeriods(allSchedules);
      SetAllemps(data && data.offices[0].employeeSet);

      if (selectSchedulePeriod === "0") {
        SetOfficeAnalyticsData(officeData);
      } else {
        SetOfficeAnalyticsData(
          officeData.filter(
            (e) => parseInt(e.schedulePeriod) === parseInt(selectSchedulePeriod)
          )
        );
      }
    }

    return () => {
      SetOfficeAnalyticsData([]);
    };
  }, [!loading]);

  useEffect(() => {
    let officeData = [...officeAnalyticsData];
    if (!loading) {
      data &&
        data.analyticsDatums &&
        data.analyticsDatums.length > 0 &&
        data.analyticsDatums.map((m) => {
          const parsed = JSON.parse(m.data);
          officeData.push(parsed);
        });
      if (selectSchedulePeriod === "0") {
        SetOfficeAnalyticsData(officeData);
      } else {
        SetOfficeAnalyticsData(
          officeData.filter(
            (e) => parseInt(e.schedulePeriod) === parseInt(selectSchedulePeriod)
          )
        );
      }
    }
    return () => {
      SetOfficeAnalyticsData([]);
    };
  }, [selectSchedulePeriod]);

  const allschedules =
    officeAnalyticsData.length > 0 &&
    officeAnalyticsData.map((e) => getSchedulePeriod(e.schedulePeriod));

  const boxdata =
    !SkillType.loading &&
    SkillType.data.skills.map((s) => {
      const data = [...officeAnalyticsData];
      data.sort((a,b) => parseInt(a.schedulePeriod) - parseInt(b.schedulePeriod));
      return {
        type: selectSchedulePeriod === "0" ? "line" : "box",
        boxpoints: "all",
        x:
          data.length > 0 &&
          data.map((e) => getSchedulePeriod(e.schedulePeriod)),
        y:
          data.length > 0 &&
          data.map((e) => {
            let empSkills = e.employeeSkills
              .filter((x) => parseInt(x.skill) === parseInt(s.id))
              .map((x) => x.employee);
            var allFitnessForSkill = [];
            empSkills.map((x) => {
              let emp = e.employees.find((m) => m.id === x);
              // if(s.name==="Manager"){
              //   console.log(emp)
              // }
              let f = (emp.satisfaction / emp.maxFitness) * 100;
              allFitnessForSkill.push(f);
            });
            let av = allFitnessForSkill.reduce((a, b) => a + b, 0);
            av = av / allFitnessForSkill.length;
            return av;
          }),
        name: s.name,
      };
    });

  const constrainedSkillSetD1 =
    !SkillType.loading &&
    SkillType.data.skills.map((s) => {
      const data = [...officeAnalyticsData];
      data.sort((a,b) => parseInt(a.schedulePeriod) - parseInt(b.schedulePeriod));
      return {
        type: selectSchedulePeriod === "0" ? "line" : "box",
        boxpoints: "all",
        x:
          data.length > 0 &&
          data.map((e) => getSchedulePeriod(e.schedulePeriod)),
        y:
          data.length > 0 &&
          data.map((e) => {
            let val = e.officeSkillSummary
              .filter((m) => m.skill === s.id)
              .map((x) => {
                let required = x.required.includes("days")
                  ? parseInt(x.required.substr(0, x.required.indexOf("days")))
                  : parseInt(x.required.substr(0, x.required.indexOf(":")));
                let available = x.available.includes("days")
                  ? parseInt(x.available.substr(0, x.available.indexOf("days")))
                  : parseInt(x.available.substr(0, x.available.indexOf(":")));
                let finalVal;
                if (required != 0 && !x.required.includes("days")) {
                  if (required < 24) {
                    required = Math.floor(required / 24).toFixed(2);
                  } else {
                    let extra = required - 24;
                    required = 1 + Math.floor(extra / 24).toFixed(2);
                  }
                }
                if (available != 0 && x.available.includes("days")) {
                  finalVal = ((required / available) * 100).toFixed(2);
                } else if (available != 0 && !x.available.includes("days")) {
                  if (available < 24) {
                    finalVal = 100;
                  } else {
                    let avl = 1 + (24 / available - 24).toFixed(2);
                    finalVal = required / avl;
                  }
                } else {
                  finalVal = 100;
                }
                return finalVal;
              });
            let av =
              val.length > 0
                ? val.reduce((a, b) => a + b, 0) / val.length
                : val;
            return av;
          }),
        name: s.name,
      };
    });

  // const constrainedSkillSetD2 =
  //   !SkillType.loading &&
  //   SkillType.data.skills.map((s) => {
  //     return {
  //       type: selectSchedulePeriod === "0" ? "line" : "box",
  //       boxpoints: "all",
  //       x: selectSchedulePeriod === "0"
  //       ? officeAnalyticsData.length > 0 &&
  //         officeAnalyticsData.map((e) =>
  //           getSchedulePeriod(e.schedulePeriod)
  //         )
  //       : !SkillType.loading && SkillType.data.skills.map((s) => s.name),
  //       y:
  //         officeAnalyticsData.length > 0 &&
  //         officeAnalyticsData.map((e) => {
  //           let val = e.officeSkillSummary.filter(m=>m.skill===s.id)
  //             .map((x) => {
  //               let upper = x.upper.includes("days")
  //                 ? parseInt(x.upper.substr(0, x.upper.indexOf("days")))
  //                 : parseInt(x.upper.substr(0, x.upper.indexOf(":")));
  //               let available = x.available.includes("days")
  //                 ? parseInt(x.available.substr(0, x.available.indexOf("days")))
  //                 : parseInt(x.available.substr(0, x.available.indexOf(":")));
  //               let finalVal;
  //               if (upper != 0 && !x.upper.includes("days")) {
  //                 if (upper < 24) {
  //                   upper = Math.floor(upper / 24).toFixed(2);
  //                 } else {
  //                   let extra = upper - 24;
  //                   upper = 1 + Math.floor(extra / 24).toFixed(2);
  //                 }
  //               }
  //               if (available != 0 && x.available.includes("days")) {
  //                 finalVal = ((upper / available) * 100).toFixed(2);
  //               } else if (available != 0 && !x.available.includes("days")) {
  //                 if (available < 24) {
  //                   finalVal = 100;
  //                 } else {
  //                   let avl = 1 + (24 / available - 24).toFixed(2);
  //                   finalVal = upper / avl;
  //                 }
  //               } else {
  //                 finalVal = 100;
  //               }
  //               return finalVal;
  //             });
  //             console.log(val.length)
  //             let av = val.length>0 ? val.reduce((a,b)=>a+b,0)/val.length:val;
  //           return av;
  //         }),
  //       name: s.name,
  //     };

  //   });

  // const constrainedSkillSetData1 =
  //   officeAnalyticsData.length > 0 &&
  //   officeAnalyticsData.map((e) => {
  //     let arr1 = {
  //       mode:
  //         parseInt(selectSchedulePeriod) === 0 ? "lines+markers" : "markers",
  //       type: "scatter",
  //       x:
  //         parseInt(selectSchedulePeriod) === 0 && allschedules
  //           ? allschedules.map((e) => e)
  //           : e.officeSkillSummary.map((x) => getSkillName(x.skill)),
  //       y: e.officeSkillSummary.map((x) => {
  //         let required = x.required.includes("days")
  //           ? parseInt(x.required.substr(0, x.required.indexOf("days")))
  //           : parseInt(x.required.substr(0, x.required.indexOf(":")));
  //         let available = x.available.includes("days")
  //           ? parseInt(x.available.substr(0, x.available.indexOf("days")))
  //           : parseInt(x.required.substr(0, x.required.indexOf(":")));
  //         if (required != 0 && !x.required.includes("days")) {
  //           if (required < 24) {
  //             required = Math.floor(required / 24).toFixed(2);
  //           } else {
  //             let extra = required - 24;
  //             required = 1 + Math.floor(extra / 24).toFixed(2);
  //           }
  //         }
  //         if (available != 0 && x.available.includes("days")) {
  //           return ((required / available) * 100).toFixed(2);
  //         } else if (available != 0 && !x.available.includes("days")) {
  //           if (available < 24) {
  //             return 100;
  //           } else {
  //             let avl = 1 + (24 / available - 24).toFixed(2);
  //             return required / avl;
  //           }
  //         } else {
  //           return 100;
  //         }
  //       }),
  //       name:
  //         parseInt(selectSchedulePeriod) === 0
  //           ? getSchedulePeriod(e.schedulePeriod)
  //           : "Required/Available",
  //       text: e.officeSkillSummary.map((x) => getSkillName(x.skill)),
  //     };
  //     return arr1;
  //   });

  // const constrainedSkillSetData2 =
  //   officeAnalyticsData.length > 0 &&
  //   officeAnalyticsData.map((e) => {
  //     let arr1 = {
  //       mode: e.schedulePeriod === "0" ? "lines+markers" : "markers",
  //       type: "scatter",
  //       x: e.officeSkillSummary.map((x) => getSkillName(x.skill)),
  //       y: e.officeSkillSummary.map((x) => {
  //         let required = x.required.includes("days")
  //           ? parseInt(x.required.substr(0, x.required.indexOf("days")))
  //           : parseInt(x.required.substr(0, x.required.indexOf(":")));
  //         let upper = x.upper.includes("days")
  //           ? parseInt(x.upper.substr(0, x.upper.indexOf("days")))
  //           : parseInt(x.upper.substr(0, x.upper.indexOf(":")));
  //         if (required != 0 && !x.required.includes("days")) {
  //           if (required < 24) {
  //             required = Math.floor(required / 24).toFixed(2);
  //           } else {
  //             let extra = required - 24;
  //             required = 1 + Math.floor(extra / 24).toFixed(2);
  //           }
  //         }
  //         if (upper != 0 && x.upper.includes("days")) {
  //           return ((required / upper) * 100).toFixed(2);
  //         } else if (upper != 0 && !x.upper.includes("days")) {
  //           if (upper < 24) {
  //             return 100;
  //           } else {
  //             let upp = 1 + (24 / upper - 24).toFixed(2);
  //             return required / upp;
  //           }
  //         } else {
  //           return 100;
  //         }
  //       }),
  //       name: "Required/Upper",
  //       text: e.officeSkillSummary.map((x) => getSkillName(x.skill)),
  //     };
  //     return arr1;
  //   });

  //test

  const handleScheduleSelect = (e) => {
    SetShowLoader(true);
    SetSelectSchedulePeriod("");
    setTimeout(() => {
      SetSelectSchedulePeriod(e.target.value);
      SetShowLoader(false);
    }, 500);
  };
  if (loading || showLoader || !officeAnalyticsData) {
    return <CircularProgress color="primary" />;
  } else if (error) {
    console.error(error);
    return <Typography>Something went wrong. Please try again.</Typography>;
  } else {
    let schedulePeriods = [];
    if (data && data.offices.length > 0 && data.offices[0].scheduleperiodSet) {
      schedulePeriods = [...data.offices[0].scheduleperiodSet];
      schedulePeriods.sort((a,b) => parseInt(b.id) - parseInt(a.id))
    }

    return (
      <div>
        <Grid container direction="row" spacing={4} alignItems="center">
          <Grid item xs={12}>
            <Box mt={2}>
              <Typography variant="h3">Skill Set</Typography>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box m={2}>
              <Select
                variant="outlined"
                value={selectSchedulePeriod}
                onChange={(e) => {
                  handleScheduleSelect(e);
                }}
                className={classes.select}
              >
                <MenuItem key="0" value="0">
                  All Schedule Periods
                </MenuItem>
                {schedulePeriods.map((e, index) => (
                    <MenuItem key={index} value={e.id}>
                      {format(new Date(`${e.start}T08:00:00`), "dd MMM yyyy") +
                        " to " +
                        format(new Date(`${e.end}T08:00:00`), "dd MMM yyyy")}
                    </MenuItem>
                  ))}
              </Select>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box mt={2}>
              
              {selectSchedulePeriod === "0" && (
                <>
                  <Typography variant="h5">Overview</Typography>
                  <br />
                  <br />
                  <Table>
                    <TableHead>
                      <TableRow>
                        <StyledTableCell>Skill</StyledTableCell>
                        <StyledTableCell>
                          No. of Hours Required for this Skill Type
                        </StyledTableCell>
                        <StyledTableCell>
                          No. of Hours Available for this Skill Type
                        </StyledTableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {officeAnalyticsData &&
                        officeAnalyticsData.length > 0 &&
                        officeAnalyticsData
                          .sort(
                            (a, b) => 
                              parseInt(b.schedulePeriod) -
                              parseInt(a.schedulePeriod)
                          )
                          .map((e) =>
                            e.officeSkillSummary.map((x, index, arr) => (
                              <React.Fragment key={index + 'overview'}>
                                <Box>
                                  {index != 0 &&
                                  arr[index - 1] &&
                                  arr[index - 1].schedulePeriod !=
                                    e.schedulePeriod ? (
                                    ""
                                  ) : (
                                    <b>{getSchedulePeriod(e.schedulePeriod)}</b>
                                  )}
                                </Box>
                                <TableRow key={index} style={{ marginTop: 5 }}>
                                  <StyledTableCell>
                                    {getSkillName(x.skill)}
                                  </StyledTableCell>
                                  <StyledTableCell>
                                    {x.required.includes("days")
                                      ? parseInt(
                                          x.required.substr(
                                            0,
                                            x.required.indexOf("days")
                                          )
                                        ) * 24
                                      : parseInt(
                                          x.required.substr(
                                            0,
                                            x.required.indexOf(":")
                                          )
                                        )}
                                  </StyledTableCell>
                                  <StyledTableCell>
                                    {x.available.includes("days")
                                      ? parseInt(
                                          x.available.substr(
                                            0,
                                            x.required.indexOf("days")
                                          )
                                        ) * 24
                                      : parseInt(
                                          x.available.substr(
                                            0,
                                            x.available.indexOf(":")
                                          )
                                        )}
                                  </StyledTableCell>
                                </TableRow>
                              </React.Fragment>
                            ))
                          )
                          .slice(0,3)}
                          {/* .slice(Math.max(officeAnalyticsData.length - 3, 1))} */}
                    </TableBody>
                  </Table>
                </>
              )}
            </Box>
          </Grid>

          <AnalyticsWidget
            gridSize={12}
            open={skillsetGraph}
            SetOpen={SetskillsetGraph}
            data={constrainedSkillSetD1}
            chartType={4}
            SetChartType={null}
            header="Utilization Rate by Skill Set"
            layout={{
              yaxis: {
                title: {
                  text: "Utilization Rate in %",
                },
                rangemode: 'tozero'
              },
            }}
          />

          <AnalyticsWidget
            gridSize={12}
            open={skillsetsq}
            SetOpen={Setskillsetsq}
            data={boxdata}
            chartType={4}
            SetChartType={null}
            header="Schedule Quality by Skill Set"
            layout={{
              yaxis: {
                title: {
                  text: "Schedule Quality in %",
                },
                rangemode: 'tozero'
              },
            }}
          />

        </Grid>
      </div>
    );
  }
};

export default SkillSetDashboard;
