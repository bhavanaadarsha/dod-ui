import React, { useEffect, useState } from 'react';
import {
    Grid, Typography, Box,
    CircularProgress, Select, MenuItem,
    makeStyles, TextField, Button
} from '@material-ui/core';
import { userVar } from '../../cache';
import { ANALYTICS_DATA } from '../../api/gqlQueries';
import { useQuery } from '@apollo/client';
import AnalyticsWidget from './AnalyticsGraphWidget';
import { format, isSameDay, sub } from 'date-fns';

const useStyles = makeStyles(() => ({
  select: {
    minWidth: 225
  }
}));

const PreferenceDashboard = () => {
  const classes = useStyles();
  
  const user = userVar();

  const [preferencesToView, setPreferencesToView] = useState('all');
  const [schedulePeriodToView, setSchedulePeriodToView] = useState('');
  const [timeFrameMonths, setTimeFrameMonths] = useState(6);

  const {loading, error, data} = useQuery(ANALYTICS_DATA, {
    variables: {
      officeId: parseInt(user.office.id),
      source: 'ALGORITHM'
    }
  });
  
  useEffect(() => {
    if (data) {
      const parsed = [];
      data.analyticsDatums.forEach(datum => {
        if (datum.data){
          const parsedData = JSON.parse(datum.data);
          parsed.push(parsedData);
        }
      });
      // const officeSchedulePeriodIds = data.offices[0].scheduleperiodSet.map(
      //   sched => sched.id
      // );
      
      // const parsedForOffice = parsed.filter(dataSet => (
      //   dataSet.schedulePeriod && 
      //   officeSchedulePeriodIds.includes(dataSet.schedulePeriod)
      // ));
      parsed.sort((a,b) => parseInt(b.schedulePeriod) - parseInt(a.schedulePeriod));

      if (parsed.length > 0) {
        setSchedulePeriodToView(parsed[0].schedulePeriod);
      }
    }
  }, [data]);
    
  if (loading) {
    return <CircularProgress color='primary' />;
  } else if (error) {
    console.error(error);
    return <Typography>Something went wrong. Please try again.</Typography>;
  } else {
    //parse the data entries from JSON
    const parsed = [];
    data.analyticsDatums.forEach(datum => {
      if (datum.data){
        const parsedData = JSON.parse(datum.data);
        parsed.push(parsedData);
      }
    });

    // const officeSchedulePeriodIds = data.offices[0].scheduleperiodSet.map(
    //   sched => sched.id
    // );

    //find only the entries for the selected office that come from the algorithm
    // const parsedForOffice = parsed.filter(dataSet => (
    //   dataSet.schedulePeriod && 
    //   officeSchedulePeriodIds.includes(dataSet.schedulePeriod)
    // ));

    const mappedToLabel = {
      'DAYS': 'Day Shift', 
      'SWINGS': 'Swing Shift', 
      'NIGHTS': 'Night Shift',
      'MONDAY': 'Monday', 
      'TUESDAY': 'Tuesday', 
      'WEDNESDAY': 'Wednesday', 
      'THURSDAY': 'Thursday',
      'FRIDAY': 'Friday', 
      'SATURDAY': 'Saturday', 
      'SUNDAY': 'Sunday',
      'ONEDAY': 'One Day', 
      'TWODAYS': 'Two Days', 
      'THREEDAYS': 'Three Days', 
      'FOURDAYS': 'Four Days'
    };

    if (parsed.length > 0) {

      const orderPrefs = (prefs) => {
        //define the order we want the categories to be displayed
        const orderCategories = [
          'DAYS', 'SWINGS', 'NIGHTS',
          'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY',
          'FRIDAY', 'SATURDAY', 'SUNDAY',
          'ONEDAY', 'TWODAYS', 'THREEDAYS', 'FOURDAYS'
        ];
        
        const ordered = [];
        let sumValues = [];
        orderCategories.forEach(category => {
          //find each preference type object in the preference array
          const match = prefs.find(pref => pref.type === category);

          let x;
          let y;
          if (match) {
            //sort the employees by last name so all values stay in same order
            match.yValues.sort((a,b) => a.lastName.localeCompare(b.lastName));
            //map the yValues and return the first initial and last name for 
            //x axis of the bar graph
            x = match.yValues.map(value => (
              `${value.firstName[0]?.toUpperCase()}. ${value.lastName}`
            ));
            //map the y values of each stacked bar to be the contribution in 
            //percentage form
            y = match.yValues.map(value => value.contribution * 100);
            if (sumValues.length > 0) {
              sumValues = sumValues.map((value, index) => {
                return value + y[index]
              });
            } else {
              sumValues = y;
            }
          } else {
            x = [];
            y = [];
          }

          const dataForGraph = {
            x: x,
            y: y,
            name: match ? mappedToLabel[match.type] : '',
            type: 'bar'
          }
          ordered.push(dataForGraph);
        });
        
        if (ordered.length > 0) {
          const softRequestYValues = sumValues.map(value => {
            if (value < 99.9) {
              return 100 - value;
            } else {
              return 0;
            }
          });
          const softRequestBar = {
            x: ordered[0].x,
            y: softRequestYValues,
            name: 'Soft Requests',
            type: 'bar'
          };
          ordered.push(softRequestBar);
        }
        return ordered;
      };
  
      const orderPrefsByCategory = (prefs) => {
        //define the preference category groups
        const shiftCategories = ['DAYS', 'SWINGS', 'NIGHTS'];
        const dayCategories = [
          'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY',
          'FRIDAY', 'SATURDAY', 'SUNDAY'
        ];
        const daysOffCategories = ['ONEDAY', 'TWODAYS', 'THREEDAYS', 'FOURDAYS'];
  
        //define the three stacked bars we want for each employee
        const orderedByCategory = [
          {
            x: [],
            y: [],
            name: 'Shift Type',
            type: 'bar'
          },
          {
            x: [],
            y: [],
            name: 'Days of the Week',
            type: 'bar'
          },
          {
            x: [],
            y: [],
            name: 'Days Off',
            type: 'bar'
          },
        ];

        prefs.forEach(pref => {
          //sort yValues by last name so all values will be in the same order
          pref.yValues.sort((a,b) => a.lastName.localeCompare(b.lastName));
  
          //find the object to push the accumulated values on based on 
          //preference category
          let dataObject;
          if (shiftCategories.includes(pref.type)) {
            dataObject = orderedByCategory.find(object => (
              object.name === 'Shift Type'
            ));
          } else if (dayCategories.includes(pref.type)) {
            dataObject = orderedByCategory.find(object => (
              object.name === 'Days of the Week'
            ));
          } else if (daysOffCategories.includes(pref.type)) {
            dataObject = orderedByCategory.find(object => (
              object.name === 'Days Off'
            ));
          }
  
          //if x values and y values arrays are not empty, we need to 
          //accumulate the values for the category
          if (dataObject.x.length > 0 && dataObject.y.length > 0) {
            const newValues = pref.yValues.map((value, index) => (
              (value.contribution * 100) + dataObject.y[index]
            ));
            dataObject.y = newValues;
          //if x values and y values are empty, we need to push new arrays
          //onto x and y for starting values
          } else {
            const x = pref.yValues.map(value => (
              `${value.firstName[0]?.toUpperCase()}. ${value.lastName}`
            ));
            const y = pref.yValues.map(value => value.contribution * 100);
            dataObject.x = x;
            dataObject.y = y;
          }

        });
        
        let sumValues = []
        orderedByCategory.forEach(object => {
          if (sumValues.length > 0) {
            sumValues = sumValues.map((value, index) => {
              return value + object.y[index]
            });
          } else {
            sumValues = object.y;
          }
        });

        if (orderedByCategory.length > 0) {
          const softRequestYValues = sumValues.map(value => {
            if (value < 99.9) {
              return 100 - value;
            } else {
              return 0;
            }
          });
          const softRequestBar = {
            x: orderedByCategory[0].x,
            y: softRequestYValues,
            name: 'Soft Requests',
            type: 'bar'
          };
          orderedByCategory.push(softRequestBar);
        }

        return orderedByCategory;
      };
      
      const dataForAllSchedules = [];
      parsed.forEach(parsedDatum => {
        //filter out preference types with null contribution
        const filterOutNullContribution = 
          parsedDatum.employeePreferences.filter(pref => (
            (pref.contribution || pref.contribution === 0) &&
            pref.contribution !== null
          ));
    
        const preferenceData = [];
        // const employeeData = [];
        filterOutNullContribution.forEach(pref => {
          //look for preference type existing in preference data array
          //to make sure only one entry exists for each preference type
          const prefType = preferenceData.find(data => (
            data.type === pref.preference
          ));
  
          //find the employee the preference belongs to in order to 
          //display their name in the bar graph
          const findEmployee = data.offices[0].employeeSet.find(
            (employee) => parseInt(employee.id) === 
            parseInt(pref.employee)
          );

          
          if (findEmployee) {
            //create an array of employee information for the employees that have
            //preference data for specific schedule period
            // const employeeExistsInDataArray = employeeData.find(employee => (
            //   parseInt(employee.id) === parseInt(pref.employee)
            // ));
            // if (!employeeExistsInDataArray) {
            //   employeeData.push({
            //     id: pref.employee,
            //     firstName: findEmployee.firstName,
            //     lastName: findEmployee.lastName
            //   });
            // }

            //if prefType already exists in data set, push a new yValue object
            if (prefType) {
              prefType.yValues.push({
                contribution: pref.contribution,
                value: pref.value,
                employeeId: pref.employee,
                firstName: findEmployee .firstName,
                lastName: findEmployee.lastName
              });
            //if prefType does not exist, push a new object for the preference type
            } else {
              preferenceData.push({
                type: pref.preference,  
                yValues: [
                  {
                    contribution: pref.contribution,
                    value: pref.value,
                    employeeId: pref.employee,
                    firstName: findEmployee.firstName,
                    lastName: findEmployee.lastName
                  }
                ]
              });
            }
          }
        });
  
        //organize the newly formatted data by schedule period
        const dataBySchedule = {
          schedulePeriod: parsedDatum.schedulePeriod,
          data: preferenceData,
          // employeeData: employeeData
        };
        dataForAllSchedules.push(dataBySchedule);
      });
  
      //find the schedule periods that have analytics data available
      //to put in dropdown selection
      const schedulePeriodsWithData = dataForAllSchedules.map(
        (scheduleData) => {
          const match = data.offices[0].scheduleperiodSet.find(sched => (
            parseInt(sched.id) === parseInt(scheduleData.schedulePeriod)
          ))
          if (match) {
            return {
              id: scheduleData.schedulePeriod,
              start: new Date(`${match.start}T08:00:00`),
              end: new Date(`${match.end}T23:00:00`) 
            }
          } else {
            return null;
          }
      });
  
      const handleChangeView = (value) => {
        setPreferencesToView(value);
      };
  
      const handleChangeSchedulePeriod = (value) => {
        setSchedulePeriodToView(value);
      };
      
      const getDataIndividualPrefs = () => {
        //find data set for selected schedule period
        let dataToShow = dataForAllSchedules.find(schedData => (
          parseInt(schedData.schedulePeriod) === parseInt(schedulePeriodToView)
        ));

        if (dataToShow) {
          dataToShow = dataToShow.data;
        } else {
          setSchedulePeriodToView(dataForAllSchedules[0].schedulePeriod);
          dataToShow = dataForAllSchedules[0].data;
        }
  
        //determine if user is viewing all employees or a specific employee
        if (preferencesToView === 'all'){
          return orderPrefs(dataToShow);
        } else {
          //define the format that needs to be passed to analytics graph widget
          const newData = {
            x: [],
            y: [],
            name: '',
            type: 'bar'
          };
  
          //define the order of the preference categories we want to show
          const orderCategories = [
            'DAYS', 'SWINGS', 'NIGHTS',
            'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY',
            'FRIDAY', 'SATURDAY', 'SUNDAY',
            'ONEDAY', 'TWODAYS', 'THREEDAYS', 'FOURDAYS'
          ];
  
          //go through each category in order and add employee's preference
          //types to x value array and contribution values to y value array
          orderCategories.forEach(category => {
            const match = dataToShow.find(pref => pref.type === category);
            if (match) {
              const title = mappedToLabel[match.type];
              const employeeMatch = match.yValues.find(object => (
                parseInt(object.employeeId) === parseInt(preferencesToView)
              ));
              
              if (employeeMatch) {
                newData.x.push(title);
                newData.y.push(employeeMatch.contribution * 100);
                newData.name = `${employeeMatch.firstName} ${employeeMatch.lastName}`;
              } 
            }
          });

          const sumOfValues = newData.y.reduce((sum, value) => (
            value + sum
          ), 0);
          if (sumOfValues === 0) {
            newData.y.push(0);
          } else if (sumOfValues < 99.9) {
            newData.y.push(100 - sumOfValues);
          } else {
            newData.y.push(0);
          }
          newData.x.push('Soft Requests');
  
          //return data in an array for graph widget
          return [newData];
        }
      };
  
      const getDataPrefCategories = () => {
        //find data set for selected schedule period
        let dataToShow = dataForAllSchedules.find(schedData => (
          parseInt(schedData.schedulePeriod) === parseInt(schedulePeriodToView)
        ));
        
        if (dataToShow) {
          dataToShow = dataToShow.data;
        } else {
          setSchedulePeriodToView(dataForAllSchedules[0].schedulePeriod);
          dataToShow = dataForAllSchedules[0].data;
        }
  
        //determine if user is viewing all employees or a specific employee
        if (preferencesToView === 'all'){
          return orderPrefsByCategory(dataToShow);
        } else {
          //define the preference category groups
          const shiftCategories = ['DAYS', 'SWINGS', 'NIGHTS'];
          const dayCategories = [
            'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY',
            'FRIDAY', 'SATURDAY', 'SUNDAY'
          ];
          const daysOffCategories = ['ONEDAY', 'TWODAYS', 'THREEDAYS', 'FOURDAYS'];
  
          //define the three bars we want to be shown
          const newData = {
            x: ['Shift Type', 'Days of the Week', 'Days Off', 'Soft Requests'],
            y: [0, 0, 0],
            name: '',
            type: 'bar'
          };
  
          //accumulate date by category into each bar and put it in the 
          //data object to send to graph widget
          dataToShow.forEach(pref => {
            let index;
            if (shiftCategories.includes(pref.type)) {
              index = 0;
            } else if (dayCategories.includes(pref.type)) {
              index = 1;
            } else if (daysOffCategories.includes(pref.type)) {
              index = 2;
            }
  
            const matching = pref.yValues.find(object => (
              parseInt(object.employeeId) === parseInt(preferencesToView)
            ));
  
            if (matching) {
              newData.y[index] = newData.y[index] + (matching.contribution * 100);
              newData.name = `${matching.firstName} ${matching.lastName}`;
            }
          });

          const sumOfValues = newData.y.reduce((sum, value) => (
            value + sum
          ), 0);
          if (sumOfValues === 0) {
            newData.y.push(0);
          } else if (sumOfValues < 99.9) {
            newData.y.push(100 - sumOfValues);
          } else {
            newData.y.push(0);
          }
  
          //return data in an array for graph widget
          return [newData];
        }
      };
  
      const organizePrefDataByEmployee = () => {
        let parsedInTimeFrame = [...parsed];
        if (timeFrameMonths) {
          const startDateToView = sub(new Date(), {months: timeFrameMonths});
          const schedulesInTimeFrame = schedulePeriodsWithData.filter(sched => (
            isSameDay(sched.start, startDateToView) ||
            sched.start > startDateToView ||
            sched.end > startDateToView
          ));
          const scheduleIds = schedulesInTimeFrame.map(sched => parseInt(sched.id));
          parsedInTimeFrame = parsed.filter(datum => (
            scheduleIds.includes(parseInt(datum.schedulePeriod))
          ));
        }

        const prefsByEmployee = [];
        parsedInTimeFrame.forEach(dataSet => {
          dataSet.employeePreferences.forEach(preference => {
            const employeeExists = prefsByEmployee.find(pref => (
              parseInt(pref.employeeId) === parseInt(preference.employee)
            ));
            if (employeeExists) {
              const scheduleExists = employeeExists.prefSets.find(prefSet => (
                parseInt(prefSet.schedulePeriod) === 
                parseInt(dataSet.schedulePeriod)
              ));
              
              if (scheduleExists) {
                scheduleExists.preferences.push({
                  type: preference.preference,
                  value: preference.value
                });
              } else {
                employeeExists.prefSets.push({
                  schedulePeriod: dataSet.schedulePeriod,
                  preferences: [
                    {
                      type: preference.preference, 
                      value: preference.value
                    }
                  ]
                });
              }
            } else {
              prefsByEmployee.push({
                employeeId: preference.employee,
                prefSets: [
                  {
                    schedulePeriod: dataSet.schedulePeriod,
                    preferences: [
                      {
                        type: preference.preference, 
                        value: preference.value
                      }
                    ]
                  }
                ]
              });
            }
          });
        });
        return prefsByEmployee;
      }
  
      const getDataForPrefChanges = (preferencesByEmployee) => {
        const graphData = {
          x: [],
          y: [],
          name: 'Shift Type',
          type: 'bar'
        };

        preferencesByEmployee.forEach(preferenceObject => {
          let changeCount = 0;
          preferenceObject.prefSets.sort((a, b) => (
            parseInt(a.schedulePeriod) - parseInt(b.schedulePeriod)
          ));
          preferenceObject.prefSets.forEach((prefSet, index) => {
            if (index !== preferenceObject.prefSets.length - 1) {
              const currentPreferences = prefSet.preferences;
              const nextPreferences = preferenceObject.prefSets[index+1].preferences;
  
              let same = true;
              let i = 0;
              while (same && i < currentPreferences.length) {
                const match = nextPreferences.find(pref => (
                  pref.type === currentPreferences[i].type
                ));
                if (match) {
                  same = match.value === currentPreferences[i].value
                }
                i++;
              }
              if (!same) {
                changeCount += 1;
              }
            }
          });
  
          const findEmployee = data.offices[0].employeeSet.find(
            (employee) => parseInt(employee.id) === 
            parseInt(preferenceObject.employeeId)
          );
          const employeeLabel = findEmployee
            ? `${findEmployee.firstName[0]?.toUpperCase()}. ${findEmployee.lastName}`
            : ''
          graphData.x.push(employeeLabel);
          graphData.y.push(changeCount);
        });
        return [graphData];
      }

      const mostInfluential = (graphData) => {
        if (preferencesToView === 'all') {
          const preferenceWithTotal = graphData.map(datum => {
            const total = datum.y.reduce((sum, value) => {
              return sum + value;
            }, 0);
            return {
              name: datum.name,
              total
            };
          });
          return preferenceWithTotal.sort((a,b) => (
            b.total - a.total
          ))[0].name;
        } else {
          const values = [...graphData[0].y];
          values.sort((a,b) => b - a);
          const indexOfHighest = graphData[0].y.indexOf(values[0]);
          const nameOfHighest = graphData[0].x[indexOfHighest];
          return nameOfHighest;
        }

      };

      schedulePeriodsWithData.sort((a,b) => (
        parseInt(b.id) - parseInt(a.id)
      ));
  
      return (
        <div>
          <Grid container direction='row' spacing={4} alignItems='center'>
            <Grid item xs={12}>
              <Box mt={2}>
                <Typography variant='h3'>Preferences</Typography>
              </Box>
            </Grid>
            <Grid item container alignItems='center' spacing={2}>
              <Grid item>
                <Typography>View Data For: </Typography>
              </Grid>
              <Grid item>
                <Select 
                  variant='outlined'
                  value={preferencesToView}
                  onChange={e => handleChangeView(e.target.value)}
                  className={classes.select}
                >
                  <MenuItem value='all'>All Employees</MenuItem>
                  {data.offices[0].employeeSet.map(employee => (
                    <MenuItem key={employee.id} value={employee.id}>
                      {employee.firstName}{' '}{employee.lastName}
                    </MenuItem>
                  ))}
                </Select>
              </Grid>
              <Grid item>
                <Select 
                  variant='outlined'
                  value={schedulePeriodToView}
                  onChange={e => handleChangeSchedulePeriod(e.target.value)}
                  className={classes.select}
                >
                  {schedulePeriodsWithData.map(sched => (
                    <MenuItem key={sched.id} value={sched.id}>
                      {format(sched.start, 'dd MMM yyyy')} -
                      {format(sched.end, ' dd MMM yyyy')}
                    </MenuItem>
                  ))}
                </Select>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <Box mt={2}>
                <Typography variant="h5">Overview</Typography>
                <br />
                <Typography>
                  Preference with Highest Effect on Schedule Quality: <b>
                    {mostInfluential(getDataIndividualPrefs())}
                  </b>
                </Typography>
                <Typography>
                  Category with Highest Effect on Schedule Quality: <b>
                    {mostInfluential(getDataPrefCategories())}
                  </b>
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} style={{marginBottom: 16}}>
              <AnalyticsWidget
                data={getDataPrefCategories()}
                layout={preferencesToView === 'all' ? {barmode: 'stack'} : null}
                gridSize={12}
                chartType='4'
                SetChartType={() => null}
                open={true}
                SetOpen={() => null}
                header='Preference Contribution to Satisfaction by Category'
              />
            </Grid>
            <Grid item xs={12} style={{marginBottom: 16}}>
              <AnalyticsWidget
                data={getDataIndividualPrefs()}
                layout={preferencesToView === 'all' 
                  ? {barmode: 'stack', height: 500} 
                  : null
                }
                gridSize={12}
                chartType='4'
                SetChartType={() => null}
                open={true}
                SetOpen={() => null}
                header='Preference Contribution to Satisfaction'
              />
            </Grid>
            <Grid item container xs={12} spacing={2} alignItems='center'>
              <Grid item>
                <Typography>
                  Time Frame for Preference Changes in Months:
                </Typography>
              </Grid>
              <Grid item>
                <TextField 
                  type='number'
                  variant='outlined'
                  name='timeFrameMonths'
                  onChange={(e) => {
                    setTimeFrameMonths(e.currentTarget.value);
                  }}
                  value={timeFrameMonths}
                  style={{width: 50}}
                />
              </Grid>
            </Grid>
            <Grid item xs={12} style={{marginBottom: 16}}>
              <AnalyticsWidget
                data={getDataForPrefChanges(organizePrefDataByEmployee())}
                gridSize={12}
                chartType='4'
                SetChartType={() => null}
                open={true}
                SetOpen={() => null}
                header='Preference Changes by Employee'
              />
            </Grid>
          </Grid>
        </div>
      );
    } else {
      return (
        <div>
          <Grid container direction='row' spacing={4} alignItems='center'>
            <Grid item xs={12}>
              <Box mt={2}>
                <Typography variant='h3'>Preferences</Typography>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Typography>
                No analytics data to show
              </Typography>
            </Grid>
          </Grid>
        </div>
      );
    }
  }
}
 
export default PreferenceDashboard;