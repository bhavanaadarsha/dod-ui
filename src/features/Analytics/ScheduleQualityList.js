import React, { useEffect, useState } from "react";
import {
  Paper,
  CircularProgress,
  makeStyles,
  Dialog,
  DialogContent,
  IconButton,
  Container,
  Grid,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  withStyles,
  Box
} from "@material-ui/core";
import {
  DataGrid,
  GridToolbarContainer,
  GridColumnsToolbarButton,
  GridFilterToolbarButton,
  GridToolbarExport,
} from "@material-ui/data-grid";
import { SCHEDULE_FITNESS, GET_PREFERENCES } from "../../api/gqlQueries";
import {useLazyQuery } from "@apollo/client";
import VisibilityIcon from "@material-ui/icons/Visibility";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles(() => ({
  dt: {
    "& .MuiDataGrid-columnsContainer": {
      backgroundColor: "#EAEAEA",
      color: "#333333",
    },
    "& .MuiButton-iconSizeSmall": {
      color: "rgba(134, 134, 137, 1)",
    },
    "& .MuiButton-label": {
      color: "rgba(134, 134, 137, 1)",
      fontSize: "15px",
    },
    border: "none",
    fontSize: "15px",
  },
  headerSpacing: {
    marginTop: 35,
    marginBottom: 20,
  },
}));

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "#EAEAEA",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRight: "5",
    paddingLeft: "5",
  },
  body: {
    fontSize: 14,
    backgroundColor: "#ffffff",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRight: "5",
    paddingLeft: "5",
    borderBottom: "none",
    borderTop: "1px solid lightgrey",
  },
}))(TableCell);

function ScheduleQualityList(props) {
  const classes = useStyles();
  const [selectedUser, setSelectedUser] = useState("");
  const [selectedUserPreferences, SetselectedUserPreferences] = useState([]);
  const [selectedUserName, SetselectedUserName] = useState("");
  const [showDialog, setshowDialog] = useState(false);
  const [rows, setrows] = useState([]);

  const [scheduleFitness, { loading, error }] = useLazyQuery(SCHEDULE_FITNESS, {
    onCompleted(data) {
      setrows(data.employeeFitnesses);
    },
    onError(error) {
      console.error(error)
    }
  });

  useEffect(() => {
    if (props.schedulePeriod != 0) {
      scheduleFitness({
        variables: {
          schedulePeriod: parseInt(props.schedulePeriod),
        },
      });
    } else {
      scheduleFitness({
        variables:{
          office:parseInt(props.officeId)
        }
      });
    }
  }, [props.schedulePeriod]);

  const CustomToolbar = () => {
    return (
      <GridToolbarContainer style={{ justifyContent: "flex-end" }}>
        <GridColumnsToolbarButton />
        <GridFilterToolbarButton />
        <GridToolbarExport />
      </GridToolbarContainer>
    );
  };

  const columns = [
    {
      field: "Name",
      headerName: "Name",
      width: 300,
      valueGetter: (params) =>
        `${
          params.row.employee.firstName + " " + params.row.employee.lastName ||
          ""
        }`,
    },
    {
      field: "score",
      width: 300,
      headerName: "Schedule Quality Score",
      valueGetter: (params) => `${params.row.score.toFixed(2)}`,
    },
    {
      field: "maxScore",
      width: 400,
      hide: true,
      headerName: "Max Score you can get based on your preferences",
      valueGetter: (params) => `${params.row.maxScore.toFixed(2)}`,
    },
    {
      field: "ratio",
      width: 300,
      headerName: "Overall Schedule Quality",
      valueGetter: (params) => `${params.row.ratio.toFixed(2) * 100 + "%"}`,
    },
    {
      field: "User Preferences",
      width: 300,
      headerName: "View User Preferences",
      filterable: false,
      sortable: false,
      renderCell: (params) => (
        <IconButton
          onClick={() => {
            setshowDialog(true);
            setSelectedUser(params.row.employee.id);
            SetselectedUserName(
              params.row.employee.firstName + " " + params.row.employee.lastName
            );
          }}
          color="secondary"
        >
          <VisibilityIcon />
        </IconButton>
      ),
    },
  ];

  const [getPreferences] = useLazyQuery(GET_PREFERENCES, {
    onCompleted(data) {
      SetselectedUserPreferences(data.preferences);
    },
    onError(error) {
      console.error(error)
    }
  });

  useEffect(() => {
    selectedUser != "" &&
      getPreferences({
        variables: {
          id: parseInt(selectedUser),
          office: parseInt(props.officeId)
        },
      });
  }, [selectedUser]);

  if (error) {
    return (
      <Paper>
        Something went wrong.{error.message}. Please try again later.
      </Paper>
    );
  } else if (loading) {
    return <CircularProgress color="primary" />;
  } else {
    return (
      <Container>
        <Dialog open={showDialog} fullWidth maxWidth="md">
          <DialogContent
            style={{ padding: 20, overflowX: "hidden", textAlign: "center" }}
          >
            <Box style={{ textAlign: "right" }}>
              <IconButton onClick={() => setshowDialog(false)} color="primary">
                <CloseIcon />
              </IconButton>
            </Box>

            <Typography>
              <b>Preferences - {selectedUserName}</b>
            </Typography>
            <Grid container direction="row" spacing={4}>
              <Grid item xs={2}></Grid>
              <Grid item xs={8}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <StyledTableCell colSpan={2}></StyledTableCell>
                    </TableRow>
                    <TableRow>
                      <StyledTableCell>Preference Type</StyledTableCell>
                      <StyledTableCell>Value</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {selectedUserPreferences.length &&
                      selectedUserPreferences.map((e, index) => (
                        <TableRow key={index}>
                          <StyledTableCell>{e.type.type}</StyledTableCell>
                          <StyledTableCell>{e.value}</StyledTableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </Grid>
              <Grid item xs={2}></Grid>
            </Grid>
          </DialogContent>
        </Dialog>
        <Grid container direction="row" spacing={2} component={Paper}>
          <Grid item xs={12} style={{ marginTop: 30 }}>
            <Typography variant="h5">Schedule Quality Report</Typography>
          </Grid>
          <Grid item xs={12}>
            <div style={{ height: 700 }}>
              <DataGrid
                className={classes.dt}
                rows={rows}
                columns={columns}
                pageSize={10}
                components={{
                  Toolbar: CustomToolbar,
                }}
              />
            </div>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

export default ScheduleQualityList;
