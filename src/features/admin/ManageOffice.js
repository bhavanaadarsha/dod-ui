import React, { useState } from "react";
import {
  Box,
  Button,
  TableCell,
  makeStyles,
  Grid,
  Typography,
  IconButton,
  withStyles,
  TextField,
  CircularProgress,
  Dialog,
  DialogContent,
  Snackbar,
  Select,
  MenuItem,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { useQuery, useMutation, gql } from "@apollo/client";
import {
  GET_OFFICE,
  DELETE_OFFICE,
  LOCATION,
  ADD_OFFICE,
  MODEL_TYPE,
  CREATE_SHIFT_DEFINITIONS,
  SHIFT_DEFINITION_TYPE,
  WEEKDAY,
  ALL_OFFICES,
} from "../../api/gqlQueries";
import MuiAlert from "@material-ui/lab/Alert";
import OfficeSetupGuide from "./OfficeSetupGuide";
import { userVar } from "../../cache";
import { handleShiftDefinitionCreate } from "../../helpers/officeHelpers";
import {
  DataGrid,
  GridToolbarContainer,
  GridColumnsToolbarButton,
  GridFilterToolbarButton,
  GridToolbarExport,
} from "@material-ui/data-grid";

const useStyles = makeStyles((theme) => ({
  dt: {
    "& .MuiDataGrid-columnsContainer": {
      backgroundColor: "#EAEAEA",
      color: "#333333",
    },
    "& .MuiButton-iconSizeSmall": {
      color: "rgba(134, 134, 137, 1)",
    },
    "& .MuiButton-label": {
      color: "rgba(134, 134, 137, 1)",
      fontSize: "15px",
    },
    border: "none",
    fontSize: "15px",
  },
  root: {
    width: "100%",
  },
  shape: {
    border: theme.shape.border,
    boxSizing: theme.shape.boxSizing,
    boxShadow: theme.shape.boxShadow,
    borderRadius: theme.shape.borderRadius,
  },
  table: {
    border: theme.shape.border,
  },
  headerSpacing: {
    marginTop: 30,
    marginBottom: 20,
  },
  button: {
    width: 80,
  },
}));

function ManageOffice(props) {
  const classes = useStyles();
  const { loading, error, data, refetch } = useQuery(ALL_OFFICES, {
    onError(error){
      console.error(error);
    }
  });
  const modelTypes = useQuery(MODEL_TYPE, {
    onError(error){
      console.error(error);
    }
  });
  const [modelType, SetModelType] = useState("Select Shift Type");
  const offices = !loading && !error && data?.offices;

  const user = userVar();

  const userOffice =
    !loading &&
    offices.length > 0 &&
    offices.filter((e) => e.id === user.office.id);

  const columns = [
    {
      field: "name",
      headerName: "Name",
      width: 350,
    },
    {
      field: "Total Employees",
      width: 350,
      headerName: "Total Employees",
      valueGetter: (params) => `${params.row.assignmentSet.length || ""}`,
    },
    {
      field: "Edit Office",
      width: 200,
      headerName: "Edit Office",
      filterable: false,
      sortable: false,
      renderCell: (params) => (
        <IconButton
          onClick={() => {
            setOpenForm(true);
            setCurrentOffice(params.row);
            setofficeid(params.row.id);
          }}
          color="secondary"
        >
          <EditIcon />
        </IconButton>
      ),
    },
    {
      field: "Delete Office",
      width: 200,
      headerName: "Remove Office",
      filterable: false,
      sortable: false,
      renderCell: (params) => (
        <IconButton
          onClick={() => {
            setofficeid(params.row);
            setshowDialog(true);
          }}
          color="secondary"
        >
          <DeleteIcon />
        </IconButton>
      ),
    },
  ];

  const rows = user.isAdmin ? !loading && !error && data?.offices : userOffice;

  const [deleteOffice] = useMutation(DELETE_OFFICE, {
    update(cache, { data: { deleteOffice } }) {
      cache.evict({
        data: { id: deleteOffice.deletedId },
      });
    },
    onCompleted() {
      refetch();
      setsnackBarMessage("Department successfully deleted");
      setsnackbaropen(true);
    },
    onError(error) {
      console.error(error)
      setsnackBarMessage("Something went wrong. Please try again");
      seterrorsnackbaropen(true);
    },
  });
  const [openForm, setOpenForm] = useState(
    props.open != null ? props.open : false
  );
  const [currentOffice, setCurrentOffice] = useState(null);
  const [showDialog, setshowDialog] = useState(false);
  const [officeid, setofficeid] = useState("");
  const [snackbaropen, setsnackbaropen] = useState(false);
  const [errorsnackbaropen, seterrorsnackbaropen] = useState(false);
  const [snackBarMessage, setsnackBarMessage] = useState(false);
  const [addDepartment, setAddDepartment] = useState(false);
  const [officeInput, setOfficeInput] = useState("");
  const [dialogMessage, setshowDialogMessage] = useState(false);
  const locationDetails = useQuery(LOCATION, {
    onError(error){
      console.error(error);
    }
  });

  const handleDeleteOffice = () => {
    deleteOffice({ variables: { id: parseInt(officeid.id) } });
    setofficeid("");
    setshowDialog(false);
  };

  const [createShiftDefinition] = useMutation(CREATE_SHIFT_DEFINITIONS, {
    onError(error){
      console.error(error);
    }
  });
  const shiftDefinitionTypes = useQuery(SHIFT_DEFINITION_TYPE, {
    onError(error){
      console.error(error);
    }
  });
  const weekdays = useQuery(WEEKDAY, {
    onError(error){
      console.error(error);
    }
  });

  const [addOffice] = useMutation(ADD_OFFICE, {
    update(cache, { data: { createOffice } }) {
      cache.modify({
        fields: {
          office(existingOffices = []) {
            const newOfficeRef = cache.writeFragment({
              data: createOffice.office,
              fragment: gql`
                fragment NewOffice on OfficeNode {
                  office {
                    id
                    name
                  }
                }
              `,
            });
            return [...existingOffices, newOfficeRef];
          },
        },
      });
    },
    onCompleted(data) {
      handleShiftDefinitionCreate(
        createShiftDefinition,
        parseInt(data.createOffice.office.id),
        shiftDefinitionTypes,
        weekdays,
        modelType
      );
      refetch();
      setAddDepartment(!addDepartment);
      setOfficeInput("");
      setsnackBarMessage("Department Successfully Added");
      setsnackbaropen(true);
    },
    onError(error) {
      console.error(error);
      setsnackBarMessage("something went wrong. please try again");
      setsnackbaropen(true);
    },
  });

  const handleAddOffice = () => {
    var officeInputLen = officeInput.trim().length;
    const modelTypeID =
      !modelTypes.loading &&
      modelTypes.data.preferenceModelTypes.length > 0 &&
      modelTypes.data.preferenceModelTypes.find(
        (e) => e.modelname === modelType
      );
    if (officeInputLen > 0) {
      addOffice({
        variables: {
          input: {
            name: officeInput,
            address1: "",
            address2: "",
            address3: "",
            locationId: parseInt(locationDetails.data.locations[0].id),
            modeltype: parseInt(modelTypeID.id),
          },
        },
      });
    }
  };

  const CustomToolbar = () => {
    return (
      <GridToolbarContainer style={{ justifyContent: "flex-end" }}>
        <GridColumnsToolbarButton />
        <GridFilterToolbarButton />
        <GridToolbarExport />
      </GridToolbarContainer>
    );
  };

  return (
    <main container="true" className={classes.root}>
      {!openForm ? (
        <>
          <Snackbar
            open={snackbaropen}
            autoHideDuration={3000}
            onClose={() => setsnackbaropen(false)}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <MuiAlert
              onClose={() => {
                setsnackbaropen(false);
                setsnackBarMessage("");
              }}
              severity="success"
            >
              {snackBarMessage}
            </MuiAlert>
          </Snackbar>
          <Snackbar
            open={errorsnackbaropen}
            autoHideDuration={6000}
            onClose={() => seterrorsnackbaropen(false)}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <MuiAlert
              onClose={() => seterrorsnackbaropen(false)}
              severity="error"
            >
              {snackBarMessage}
            </MuiAlert>
          </Snackbar>
          <Dialog open={showDialog} fullWidth maxWidth="xs">
            <DialogContent
              style={{ padding: 20, overflowX: "hidden", textAlign: "center" }}
            >
              <Typography>
                Are you sure you want to delete this department?
              </Typography>
              <br />
              <br />
              <Button
                onClick={handleDeleteOffice}
                color="primary"
                variant="contained"
              >
                Yes
              </Button>
              <Button
                style={{ marginLeft: "20px" }}
                onClick={() => setshowDialog(false)}
                color="primary"
                variant="outlined"
              >
                No
              </Button>
            </DialogContent>
          </Dialog>
          <Box m={2}>
            <Grid container className={classes.headerSpacing}>
              <Grid item xs={6}>
                <Typography variant="h3">Departments</Typography>
              </Grid>
              <Grid item container justify="flex-end" xs={6}>
                {user.isAdmin && (
                  <Grid item>
                    <Button
                      color="primary"
                      variant="contained"
                      onClick={() => setAddDepartment(!addDepartment)}
                      className={classes.button}
                    >
                      Add
                    </Button>
                  </Grid>
                )}
              </Grid>
            </Grid>
            {loading ? (
              <CircularProgress color="primary" />
            ) : (
              <>
                <div style={{ height: user.isAdmin ? 600 : 300 }}>
                  <DataGrid
                    className={classes.dt}
                    rows={rows}
                    columns={columns}
                    pageSize={10}
                    components={{
                      Toolbar: CustomToolbar,
                    }}
                  />
                </div>
              </>
            )}
          </Box>
        </>
      ) : (
        <OfficeSetupGuide office={currentOffice}/>
      )}
      <Dialog open={addDepartment} fullWidth maxWidth="sm">
        <DialogContent
          style={{ padding: 20, overflowX: "hidden", textAlign: "center" }}
        >
          <Box style={{ textAlign: "right" }}>
            <Button onClick={() => setAddDepartment(!addDepartment)}>x</Button>
          </Box>
          <Grid container direction="row" spacing={3}>
            <Grid item xs={12} style={{ textAlign: "center" }}>
              <Typography variant="h3">
                {!locationDetails.loading &&
                  locationDetails.data.locations[0].name}
              </Typography>
            </Grid>

            <Grid item xs={12} style={{ textAlign: "center" }}>
              <Typography variant="h4">Department</Typography>
              <Typography variant="body1">
                Add Departments
              </Typography>
            </Grid>
            <Grid item xs={1}></Grid>
            <Grid item xs={3}>
              <Select
                id="departmentshiftType"
                name="departmentshiftType"
                variant="outlined"
                value={modelType}
                className={classes.inputWidth}
                onChange={(e) => {
                  SetModelType(e.target.value);
                }}
              >
                {
                  <MenuItem value="Select Shift Type" disabled selected>
                    Select Shift Type
                  </MenuItem>
                }
                {!modelTypes.loading &&
                  modelTypes.data.preferenceModelTypes.length > 0 &&
                  modelTypes.data.preferenceModelTypes.map((i, index) => (
                    <MenuItem key={index} id={i.id} value={i.modelname}>
                      {i.modelname}
                    </MenuItem>
                  ))}
              </Select>
            </Grid>
            <Grid item xs={3}>
              <TextField
                type="text"
                variant="outlined"
                value={officeInput ? officeInput : ""}
                onChange={(e) => setOfficeInput(e.target.value)}
                onKeyPress={(event) => {
                  if (event.key === "Enter") {
                    handleAddOffice();
                  }
                }}
                className={classes.inputWidth}
                placeholder="New Department"
              />
            </Grid>
            <Grid item xs={2}>
              <Button
                variant="contained"
                disabled={
                  officeInput.length === 0 ||
                  modelType === "Select Shift Type" ||
                  modelType === ""
                    ? true
                    : false
                }
                onClick={handleAddOffice}
                color="primary"
              >
                Add
              </Button>
            </Grid>
            <Grid item xs={2}>
              <Button
                variant="outlined"
                onClick={() => setAddDepartment(!addDepartment)}
                color="primary"
              >
                Cancel
              </Button>
            </Grid>
            <Grid item xs={1}></Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    </main>
  );
}

export default ManageOffice;
