import React, { useEffect } from "react";
import {
  makeStyles,
  Stepper,
  StepButton,
  Button,
  Typography,
  Step,
  Box,
  Grid,
} from "@material-ui/core";
import DepartmentTrainings from "./DepartmentTrainings";
import DepartmentJobTitles from "./DepartmentJobTypes";
import OfficeShifts from "./OfficeShifts";
import ErrorIcon from "@material-ui/icons/Error";
import { GET_OFFICE } from "../../api/gqlQueries";
import { useQuery } from "@apollo/client";
import UpdateOfficeForm from "./updateOfficeForm";
import { DepartmentJobTypeVar, DepartmentTrainingVar } from "../../cache";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  button: {
    marginRight: theme.spacing(1),
  },
  completed: {
    display: "inline-block",
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return [
    "Add Job Types",
    "Add Unit Specific Skillset Requirements",
    "Add Office,Shifts & Schedule Parameters",
    "Add Shift Requirements",
  ];
}

function getStepContent(step, office) {
  switch (step) {
    case 0:
      return (
        <DepartmentJobTitles
          office={office.id}
          officename={office.name}
        />
      );
    case 1:
      return (
        <DepartmentTrainings
          office={office.id}
          officename={office.name}
        />
      );
    case 2:
      return <OfficeShifts office={office.id} />;
    case 3:
      return (
        <UpdateOfficeForm
          open={true}
          office={office.id}
        />
      );
    default:
      return "Unknown step";
  }
}

function OfficeSetupGuide(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [completed, setCompleted] = React.useState({});
  const [error, setError] = React.useState(false);
  const [errorMsg, setErrorMsg] = React.useState({});
  const steps = getSteps();
  const offices = useQuery(GET_OFFICE, {
    onError(error){
      console.error(error);
    }
  });
  const office = props.office;
  const departmentJobTypeVar = DepartmentJobTypeVar();
  const departmentTrainingVar = DepartmentTrainingVar();

  //let jt = office.skillSet.map((e) => e.variety === "JOB_TYPE");
  //let trn = office.skillSet.map((e) => e.variety === "TRAINING");

  const totalSteps = () => {
    return steps.length;
  };

  const completedSteps = () => {
    return Object.keys(completed).length;
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const allStepsCompleted = () => {
    //return completedSteps() === totalSteps();
  };

  // useEffect(() => {
  //   if (jt.length > 0) {
  //     departmentJobTypeVar.isEmpty = false;
  //   }
  //   setError(true);
  //   setErrorMsg("Add at least one department Job Type");
  // }, [departmentJobTypeVar.isEmpty]);

  // useEffect(() => {
  //   if (trn.length > 0) {
  //     departmentTrainingVar.isEmpty = false;
  //   }
  //   setError(true);
  //   setErrorMsg("Add at least one department Training");
  // }, [departmentTrainingVar.isEmpty]);

  useEffect(() => {
    setError(false);
    setErrorMsg("");
  }, [!departmentJobTypeVar.isEmpty]);

  useEffect(() => {
    setError(false);
    setErrorMsg("");
  }, [!departmentTrainingVar.isEmpty]);

  const handleNext = () => {
    const newActiveStep = isLastStep()
      ? steps.findIndex((step, i) => !(i in completed))
      : activeStep + 1;
    setErrorMsg("");
    setError(false);
    if (activeStep === 0) {
      if (departmentJobTypeVar.isEmpty) {
        setError(true);
        setErrorMsg("Add at least one department job type");
      } else {
        setError(false);
        setErrorMsg("");
        setActiveStep(newActiveStep);
      }
    }

    if (activeStep === 1) {
      if (departmentTrainingVar.isEmpty) {
        setError(true);
        setErrorMsg("Add at least one department training");
      } else {
        setError(false);
        setErrorMsg("");
        setActiveStep(newActiveStep);
      }
    }

    if (activeStep === 2) {
      setError(false);
      setErrorMsg("");
      setActiveStep(newActiveStep);
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const handleComplete = () => {
    const newCompleted = completed;
    newCompleted[activeStep] = true;
    setCompleted(newCompleted);
  };

  const handleReset = () => {
    setActiveStep(0);
    setCompleted({});
  };

  return (
    <div className={classes.root}>
      <Box mt={4}>
        <Typography variant="h3">Setup Guide</Typography>
      </Box>
      <Stepper nonLinear activeStep={activeStep}>
        {steps.map((label, index) => (
          <Step key={label}>
            <StepButton
              onClick={handleStep(index)}
              completed={completed[index]}
            >
              {label}
            </StepButton>
          </Step>
        ))}
      </Stepper>
      <div>
        {allStepsCompleted() ? (
          <div>
            <Typography className={classes.instructions}>
              All steps completed - you&apos;re finished
            </Typography>
            <Button onClick={handleReset}>Reset</Button>
          </div>
        ) : (
          <div>
            <Grid container direction="row" spacing={2}>
              <Grid
                item
                xs={8}
                style={{ textAlign: "right", verticalAlign: "top" }}
              >
                <Box>
                  {error && (
                    <Typography variant="subtitle2" color="error">
                      <ErrorIcon
                        color="error"
                        style={{ verticalAlign: "middle" }}
                      />{" "}
                      {errorMsg}
                    </Typography>
                  )}
                </Box>
              </Grid>
              <Grid item xs={4}>
                <Box mb={4} style={{ textAlign: "right" }}>
                  {activeStep != 3 && 
                    <>
                      <Button
                        disabled={activeStep === 0}
                        onClick={handleBack}
                        className={classes.button}
                      >
                        Back
                      </Button>
                      <Button
                        variant="outlined"
                        color="primary"
                        onClick={handleNext}
                        className={classes.button}
                      >
                        Next
                      </Button>
                    </>
                  }
                </Box>
              </Grid>

              {getStepContent(activeStep, office)}
            </Grid>
          </div>
        )}
      </div>
    </div>
  );
}

export default OfficeSetupGuide;
