import React, { useEffect, useState } from 'react';
import {
    makeStyles,
    Grid,
    Typography,
    TextField,
    Button,
    Divider,
    Snackbar,
    Box
} from '@material-ui/core';
import { useQuery, useMutation, useReactiveVar } from '@apollo/client';
import {
    HOSPITAL_WORKINGMAX, CREATE_HOSPITAL_WORKINGMAX,
    UPDATE_HOSPITAL_WORKINGMAX, GET_CONSTRAINT_TYPE
} from '../../api/gqlQueries';
import { Formik, Field, getIn } from 'formik';
import { CreateHospitalConstraint, UpdateHospitalConstraint, findConstraint, findofficeConstraint } from '../../helpers/officeHelpers';
import MuiAlert from '@material-ui/lab/Alert';
import * as Yup from 'yup';
import { WorkingMaximumsVar, addOfficeErrVar, appsettingsVar } from '../../cache';

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        display: 'flex'
    },
    inputWidth: {
        width: '100%'
    },
    smalltextbox: {
        width: '25%'
    },
    divider: {
        border: '0.5px solid thin #333333',
        verticalAlign: 'bottom'
    }
}));

const ErrorMessage = ({ name }) => (
    <Field
        render={({ form }) => {
            const error = getIn(form.errors, name);
            return <Typography variant="subtitle2" color="primary">{error ? error : null}</Typography>;
        }}
    />
);

const validationSchema = Yup.object({
    maxdaysOffwithoutPTO: Yup.number().required('Required'),
    maxhoursperday: Yup.number().required('Required'),
    maxconsecutivedays: Yup.number().required('Required')
});



function WorkingMaximums() {
    const classes = useStyles();
    const constraintSet = useQuery(HOSPITAL_WORKINGMAX, {
        onError(error){
            console.error(error);
          }
    });
    const constraintsset = !constraintSet.loading && constraintSet.data.constraints;
    const allConstraintTypes = useQuery(GET_CONSTRAINT_TYPE, {
        onError(error){
            console.error(error);
          }
    });
    const allConstraints = !allConstraintTypes.loading && allConstraintTypes.data.constraintDefinitions;
    let office = null;
    const mdowptoid = findConstraint(allConstraints, 'MAXDAYSOFF');
    const mdowpto = findofficeConstraint(constraintsset, 'MAXDAYSOFF', office);
    const mhpdid = findConstraint(allConstraints, 'MAXDAILYHR');
    const mhpd = findofficeConstraint(constraintsset, 'MAXDAILYHR', office);
    const mcdid = findConstraint(allConstraints, 'MAXDAYS');
    const mcd = findofficeConstraint(constraintsset, 'MAXDAYS', office);
    const [snackbaropen, setsnackbaropen] = useState(false);
    const [errorsnackbaropen, seterrorsnackbaropen] = useState(false);
    const [snackBarMessage, setsnackBarMessage] = useState(false);

    const [createConstraints] = useMutation(CREATE_HOSPITAL_WORKINGMAX, {
        onCompleted() {
            constraintSet.refetch();
        },
        onError(error){
            console.error(error);
        }
    });
    const [updateConstraints] = useMutation(UPDATE_HOSPITAL_WORKINGMAX, {
        onCompleted() {
            constraintSet.refetch();
        },
        onError(error){
            console.error(error);
        }
    });

    const workingmaximumsvar = useReactiveVar(WorkingMaximumsVar);
    const addofficeerrvar = useReactiveVar(addOfficeErrVar);

    useEffect(() => {
        if (!mdowpto || !mhpd || !mcd) {
            workingmaximumsvar.isEmpty = true;
            addofficeerrvar.error = true;
        }
        else{
            workingmaximumsvar.isEmpty = false;
            addofficeerrvar.error = false;
        }
    }, [mdowpto, mhpd, mcd]);


    const handleHospitalConstraints = (values) => {
        if (values.maxdaysOffwithoutPTO != '') {
            mdowpto ? UpdateHospitalConstraint(updateConstraints, values.maxdaysOffwithoutPTO, mdowpto.id, mdowptoid.id) :
                CreateHospitalConstraint(createConstraints, values.maxdaysOffwithoutPTO, mdowptoid.id);
        }
        if (values.maxhoursperday != '') {
            mhpd ? UpdateHospitalConstraint(updateConstraints, values.maxhoursperday, mhpd.id, mhpdid.id) :
                CreateHospitalConstraint(createConstraints, values.maxhoursperday, mhpdid.id);
        }
        if (values.maxconsecutivedays != '') {
            mcd ? UpdateHospitalConstraint(updateConstraints, values.maxconsecutivedays, mcd.id, mcdid.id) :
                CreateHospitalConstraint(createConstraints, values.maxconsecutivedays, mcdid.id);
        }
        setsnackBarMessage('Working Maximums Successfully Updated');
        setsnackbaropen(true);
    };

    return (
        <Grid container direction="row" spacing={4} display="flex" style={{ border: '0.5px solid lightgray' }}>

            <Snackbar open={snackbaropen} autoHideDuration={4000} onClose={() => { setsnackbaropen(false); }} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <MuiAlert onClose={() => { setsnackbaropen(false); setsnackBarMessage(''); }} severity="success">
                    {snackBarMessage}
                </MuiAlert>
            </Snackbar>
            <Snackbar open={errorsnackbaropen} autoHideDuration={6000} onClose={() => { seterrorsnackbaropen(false) }} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <MuiAlert onClose={() => seterrorsnackbaropen(false)} severity="error">
                    {snackBarMessage}
                </MuiAlert>
            </Snackbar>
            <Grid item xs={12} align="right"><Divider className={classes.divider} /></Grid>

            <Formik
                enableReinitialize
                initialValues={{
                    maxdaysOffwithoutPTO: mdowpto ? mdowpto.value : '',
                    maxhoursperday: mhpd ? mhpd.value : '',
                    maxconsecutivedays: mcd ? mcd.value : ''
                }}
                onSubmit={values => {
                    handleHospitalConstraints(values);
                }
                }
                validationSchema={validationSchema}
            >
                {({ handleSubmit, handleChange, values, errors, dirty }) => (
                    <form onSubmit={handleSubmit}>
                        <Grid container direction="row" spacing={4}>
                            <Grid item xs={1}></Grid>
                            <Grid item xs={4}><Typography variant="h4">Working Maximums</Typography>
                            </Grid>
                            <Grid item xs={4}><Box style={{ textAlign: 'right' }}><Button variant="contained" color="primary" type="submit" disabled={!dirty}>Save</Button></Box>
                            </Grid>
                            <Grid item xs={3}></Grid>

                            <Grid item xs={1}></Grid>
                            <Grid item xs={4}> <Typography>Max # Days off per week without PTO</Typography>
                                <TextField
                                    variant="outlined"
                                    id="maxdaysOffwithoutPTO"
                                    name="maxdaysOffwithoutPTO"
                                    value={values.maxdaysOffwithoutPTO}
                                    onChange={handleChange}
                                    className={classes.smalltextbox}

                                />
                                <ErrorMessage name="maxdaysOffwithoutPTO" />
                            </Grid>
                            <Grid item xs={4}> <Typography>Max # of Hours worked in a 24-hour Period</Typography>
                                <TextField
                                    variant="outlined"
                                    id="maxhoursperday"
                                    name="maxhoursperday"
                                    value={values.maxhoursperday}
                                    onChange={handleChange}
                                    className={classes.smalltextbox}
                                />
                                <ErrorMessage name="maxhoursperday" />
                            </Grid>
                            <Grid item xs={3}></Grid>

                            <Grid item xs={1}></Grid>
                            {/* <Grid item xs={4}>
                                    <Typography>Max Consecutive # Days per Week Worked</Typography>
                                    <TextField
                                        variant="outlined"
                                        id="maxConsecutivedaysperweek"
                                        name="maxConsecutivedaysperweek"
                                        value={values.maxConsecutivedaysperweek}
                                        onChange={handleChange}
                                        className={classes.smalltextbox}
                                    />
                                </Grid> */}
                            <Grid item xs={4}>
                                <Typography>Max # of consecutive Days that can be worked</Typography>
                                <TextField
                                    variant="outlined"
                                    id="maxconsecutivedays"
                                    name="maxconsecutivedays"
                                    value={values.maxconsecutivedays}
                                    onChange={handleChange}
                                    className={classes.smalltextbox}
                                />
                                <ErrorMessage name="maxconsecutivedays" />
                            </Grid>
                            <Grid item xs={4}></Grid>

                            <Grid item xs={3}></Grid>
                        </Grid>
                        <br />
                    </form>
                )}

            </Formik>



        </Grid>
    );
}

export default WorkingMaximums;