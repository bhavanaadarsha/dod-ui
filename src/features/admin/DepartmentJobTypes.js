import React, { useEffect, useState } from "react";
import {
  Grid,
  Typography,
  TextField,
  Button,
  IconButton,
  Table,
  TableRow,
  TableCell,
  TableHead,
  TableBody,
  withStyles,
  Dialog,
  DialogContent,
  Snackbar,
  Divider,
  makeStyles,
  Box,
  Select,
  MenuItem,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { useMutation, gql, useLazyQuery, useQuery } from "@apollo/client";
import {
  ADD_LOCATION_SKILLS,
  DELETE_SKILL,
  FIND_SKILL_TYPE,
  ROLES,
  JOB_TYPE_ROLE,
  CREATE_JOB_TYPE_ROLE,
  CREATE_SKILLSET_CONSTRAINT,
  GET_CONSTRAINT_TYPE,
  SHIFT_DEFINITIONS,
  GET_OFFICE,
} from "../../api/gqlQueries";
import MuiAlert from "@material-ui/lab/Alert";
import { DepartmentJobTypeVar } from "../../cache";
import { findConstraint, getShifts } from "../../helpers/officeHelpers";
import { format } from "date-fns";
import { parse } from "graphql";

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    display: "flex",
  },
  divider: {
    border: "0.5px solid thin #333333",
    verticalAlign: "bottom",
  },
}));

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "#EAEAEA",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRottom: "0",
    paddingLeft: "20px",
  },
  body: {
    fontSize: 14,
    backgroundColor: "#ffffff",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRottom: "0",
    paddingLeft: "20px",
  },
}))(TableCell);

function DepartmentJobTypes(props) {
  const classes = useStyles();
  const [trngInput, setTrngInput] = useState("");
  const [showDialog, setshowDialog] = useState(false);
  const [snackbaropen, setsnackbaropen] = useState(false);
  const [errorsnackbaropen, seterrorsnackbaropen] = useState(false);
  const [snackBarMessage, setsnackBarMessage] = useState(false);
  const [trainingidtodelete, setTrainingidtodelete] = useState("");
  const [applicationRole, SetapplicationRole] = useState("EMPLOYEE");
  const [shiftTypes, SetShiftTypes] = useState([]);
  const departmentJobTypeVar = DepartmentJobTypeVar();
  const roles = useQuery(ROLES, {
    onError(error){
      console.error(error);
    }
  });
  const jobTypeRole = useQuery(JOB_TYPE_ROLE, {
    onError(error){
      console.error(error);
    }
  });

  const [shiftDefinitions] = useLazyQuery(SHIFT_DEFINITIONS, {
    onCompleted(data) {
      let shifts = getShifts(data.shiftDefinitions);
      SetShiftTypes(shifts);
    },
    onError(error){
      console.error(error);
    }
  });


  const [getOffice,{refetch,loading}] = useLazyQuery(GET_OFFICE, {
    onError(error){
      console.error(error);
    }
  });

  useEffect(() => {
    getOffice({
      variables:{
        id:parseInt(props.office)
      }
    });
  }, [!loading]);

  

  useEffect(() => {
    shiftDefinitions({
      variables: {
        officeId: parseInt(props.office),
      },
    });
  }, [!shiftDefinitions.loading]);

  const allSkills = useQuery(FIND_SKILL_TYPE, {
    variables: { office: props.office },
    onError(error){
      console.error(error);
    }
  });

  const allConstraintTypes = useQuery(GET_CONSTRAINT_TYPE, {
    onError(error){
      console.error(error);
    }
  });
  const allconstrinttypes =
    !allConstraintTypes.loading &&
    !allConstraintTypes.error &&
    allConstraintTypes.data;

  const [createSkillConstraints] = useMutation(CREATE_SKILLSET_CONSTRAINT, {
    onCompleted() {
      refetch();
    },
    onError(error){
      console.error(error);
    }
  });

  const [deleteSkill] = useMutation(DELETE_SKILL, {
    update(cache, { data: { deleteSkill } }) {
      cache.evict({
        data: { id: deleteSkill.deletedId },
      });
    },
    onCompleted() {
      refetch();
      allSkills.refetch();
      jobTypeRole.refetch();
      let a = allSkills.data.skills.map((i) => i.variety === "JOB_TYPE");
      if (a.length <= 0) {
        departmentJobTypeVar.isEmpty = true;
      }
    },
    onError(error){
      console.error(error);
    }
  });

  const [createjobTypeRole] = useMutation(CREATE_JOB_TYPE_ROLE, {
    onCompleted() {
      refetch();
      allSkills.refetch();
      jobTypeRole.refetch();
    },
    onError(error){
      console.error(error);
    }
  });

  const [addSkills] = useMutation(ADD_LOCATION_SKILLS, {
    update(cache, { data: { createSkill } }) {
      cache.modify({
        fields: {
          employeeSkill(existingSkill = []) {
            const newSkillRef = cache.writeFragment({
              data: createSkill.skill,
              fragment: gql`
                fragment NewSkill on createSkill {
                  skill {
                    skill {
                      id
                      name
                    }
                  }
                }
              `,
            });
            return [...existingSkill, newSkillRef];
          },
        },
      });
    },
    onCompleted(d) {
      let roleId =
        !roles.loading &&
        roles.data.roles.find((e) => e.name === applicationRole);
      createjobTypeRole({
        variables: {
          input: {
            jobType: parseInt(d.createSkill.skill.id),
            role: parseInt(roleId.id),
          },
        },
      });
      const typeid = findConstraint(
        allconstrinttypes.constraintDefinitions,
        "SKILLCOVER"
      );

      shiftTypes.length > 0 &&
        shiftTypes.forEach((element) => {
          createSkillConstraints({
            variables: {
              input: {
                value: 0,
                office: parseInt(props.office),
                monday: true,
                tuesday: true,
                wednesday: true,
                thursday: true,
                friday: true,
                saturday: true,
                sunday: true,
                effectstart: format(new Date(), "yyyy-MM-dd"),
                skill: parseInt(d.createSkill.skill.id),
                type: parseInt(typeid.id),
                starttime: element.startTime,
                endtime: element.endTime,
              },
            },
          });
        });
      refetch();
      jobTypeRole.refetch();
      departmentJobTypeVar.isEmpty = false;
      setTrngInput("");
    },
    onError(error){
      console.error(error);
    }
  });

  const handleAddTraining = () => {
    if (trngInput != "") {
      addSkills({
        variables: {
          input: {
            name: trngInput,
            variety: "JOB_TYPE",
            office: parseInt(props.office),
          },
        },
      });
      setTrngInput("");
      refetch();
      jobTypeRole.refetch();
    } else {
      setsnackBarMessage("Please enter all required values");
      seterrorsnackbaropen(true);
    }
  };

  const handleDeleteTraining = () => {
    deleteSkill({ variables: { id: parseInt(trainingidtodelete) } });
    setTrainingidtodelete("");
    setsnackBarMessage("Department job title successfully deleted");
    setsnackbaropen(true);
    setshowDialog(false);
  };

  return (
    <div style={{ marginTop: "20px" }}>
      <Grid
        container
        display="flex"
        style={{ border: "0.5px solid lightgray" }}
      >
        <Box m={2}>
          {
            <Grid container direction="row" spacing={4} display="flex">
              <Snackbar
                open={snackbaropen}
                autoHideDuration={3000}
                onClose={() => setsnackbaropen(false)}
                anchorOrigin={{ vertical: "top", horizontal: "center" }}
              >
                <MuiAlert
                  onClose={() => {
                    setsnackbaropen(false);
                    setsnackBarMessage("");
                  }}
                  severity="success"
                >
                  {snackBarMessage}
                </MuiAlert>
              </Snackbar>
              <Snackbar
                open={errorsnackbaropen}
                autoHideDuration={6000}
                onClose={() => seterrorsnackbaropen(false)}
                anchorOrigin={{ vertical: "top", horizontal: "center" }}
              >
                <MuiAlert
                  onClose={() => seterrorsnackbaropen(false)}
                  severity="error"
                >
                  {snackBarMessage}
                </MuiAlert>
              </Snackbar>
              <Dialog open={showDialog} fullWidth maxWidth="xs">
                <DialogContent
                  style={{
                    padding: 20,
                    overflowX: "hidden",
                    textAlign: "center",
                  }}
                >
                  <Typography>
                    Are you sure you want to delete this Job Type?
                  </Typography>
                  <br />
                  <br />
                  <Button
                    onClick={handleDeleteTraining}
                    onKeyPress={(event) => {
                      if (event.key === "Enter") {
                        handleDeleteTraining();
                      }
                    }}
                    color="primary"
                    variant="contained"
                  >
                    Yes
                  </Button>
                  <Button
                    style={{ marginLeft: "20px" }}
                    onClick={() => setshowDialog(false)}
                    color="primary"
                    variant="outlined"
                  >
                    No
                  </Button>
                </DialogContent>
              </Dialog>
              <Grid item xs={12}>
                <Typography variant="h3">{props.office.name}</Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant="h4">Job Type</Typography>{" "}
                <Typography>
                  Add Department Specific Job Type for <b>{props.officename}</b>
                </Typography>
              </Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={2}>
                <TextField
                  type="text"
                  variant="outlined"
                  name="newcert"
                  onChange={(e) => {
                    setTrngInput(e.currentTarget.value);
                  }}
                  onKeyPress={(event) => {
                    if (event.key === "Enter") {
                      handleAddTraining();
                    }
                  }}
                  value={trngInput}
                  placeholder="New Job Type"
                />
              </Grid>
              <Grid item xs={2}>
                <Select
                  variant={"outlined"}
                  style={{ width: "70%" }}
                  name="applicationRole"
                  value={applicationRole}
                  onChange={(e) => {
                    SetapplicationRole(e.target.value);
                  }}
                >
                  {!roles.loading &&
                    roles.data.roles.length > 0 &&
                    roles.data.roles.map((e, index) => (
                      <MenuItem key={index} value={e.name}>
                        {e.name}
                      </MenuItem>
                    ))}
                </Select>
              </Grid>
              <Grid item xs={1} align="left">
                <Button
                  variant="contained"
                  onClick={handleAddTraining}
                  color="primary"
                >
                  Add
                </Button>
              </Grid>
              <Grid item xs={6}></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={6} align="right">
                <Table>
                  <TableHead>
                    <TableRow>
                      <StyledTableCell colSpan={2}></StyledTableCell>
                    </TableRow>

                    <TableRow>
                      <StyledTableCell>Job Type</StyledTableCell>
                      <StyledTableCell>Application Access Role</StyledTableCell>
                      <StyledTableCell>Delete</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {!jobTypeRole.loading &&
                      jobTypeRole.data.jobTypeRoles.length > 0 &&
                      jobTypeRole.data.jobTypeRoles
                        .filter((e) => e.jobType.office.id === props.office)
                        .map((element, ind) => (
                          <TableRow key={ind}>
                            <StyledTableCell>
                              {element.jobType.name}
                            </StyledTableCell>
                            <StyledTableCell>
                              {element.role.name}
                            </StyledTableCell>
                            <StyledTableCell>
                              <IconButton
                                onClick={() => {
                                  setTrainingidtodelete(element.jobType.id);
                                  setshowDialog(true);
                                }}
                              >
                                <DeleteIcon />
                              </IconButton>
                            </StyledTableCell>
                          </TableRow>
                        ))}
                  </TableBody>
                </Table>
              </Grid>
              <Grid item xs={5}></Grid>
              <Grid item xs={12}>
                <Divider className={classes.divider} />
              </Grid>
            </Grid>
          }
        </Box>
      </Grid>
    </div>
  );
}

export default DepartmentJobTypes;
