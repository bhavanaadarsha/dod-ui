import React, { useEffect, useState } from "react";
import {
  Grid,
  Typography,
  Button,
  Select,
  MenuItem,
  Box,
  TextField,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  withStyles,
  makeStyles,
  Container,
  Snackbar,
  Dialog,
  DialogContent,
  CircularProgress,
  IconButton,
  FormControlLabel,
  Switch,
  Divider,
} from "@material-ui/core";
import { useQuery, useMutation, useLazyQuery } from "@apollo/client";
import {
  UPDATE_OFFICE,
  SHIFT_DEFINITIONS,
  BATCH_CREATE_SHIFT_DEFINITIONS,
  BATCH_DELETE_SHIFT_DEFINITIONS,
  SHIFT_DEFINITION_TYPE,
  WEEKDAY,
  GET_CONSTRAINT_TYPE,
  CREATE_SKILLSET_CONSTRAINT,
  GET_OFFICE,
  BATCH_DELETE_CONSTRAINTS,
} from "../../api/gqlQueries";
import { timezones } from "../../helpers/officeHelpers";
import MuiAlert from "@material-ui/lab/Alert";
import { Formik, Form, FieldArray } from "formik";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  getShifts,
  findConstraint,
  findofficeConstraint,
} from "../../helpers/officeHelpers";
import { format } from "date-fns";

const useStyles = makeStyles(() => ({
  inputWidth: {
    width: "100%",
  },
  mediumtextbox: {
    width: "50%",
  },
  smalltextbox: {
    width: "25%",
  },
  loading: {
    display: "block",
    margin: "auto",
  },
  textColor: {
    color: "#F15A29",
  },
  headerSpacing: {
    marginTop: 30,
    marginBottom: 20,
  },
  button: {
    width: 80,
  },
  autocompleteInput: {
    width: "100%",
    padding: 0,
  },
}));

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "#EAEAEA",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRottom: "0",
    border: "none",
    textAlign: "center",
  },
  body: {
    fontSize: 14,
    backgroundColor: "#ffffff",
    paddingTop: "5px",
    paddingBottom: "0",
    paddingRottom: "0",
    border: "none",
    textAlign: "center",
  },
}))(TableCell);

function OfficeShifts(props) {
  const classes = useStyles();
  const [office, SetOffice] = useState({});

  const [getOffice, { refetch, loading }] = useLazyQuery(GET_OFFICE, {
    onCompleted(data) {
      SetOffice(data.offices[0]);
    },
    onError(error){
      console.error(error);
    }
  });

  useEffect(() => {
    getOffice({
      variables: {
        id: parseInt(props.office),
      },
    });
  }, [!loading]);

  const allConstraintTypes = useQuery(GET_CONSTRAINT_TYPE, {
    onError(error){
      console.error(error);
    }
  });
  const allconstrinttypes =
    !allConstraintTypes.loading &&
    !allConstraintTypes.error &&
    allConstraintTypes.data;

  const shiftDefinitions = useQuery(SHIFT_DEFINITIONS, {
    variables: {
      officeId: parseInt(props.office),
    },
    onError(error){
      console.error(error);
    }
  });

  const [createSkillConstraints] = useMutation(CREATE_SKILLSET_CONSTRAINT, {
    onCompleted(d) {
      refetch();
      shiftDefinitions.refetch();
      allConstraintTypes.refetch();
    },
    onError(error){
      console.error(error);
    }
  });

  const [batchcreateShiftDefinition] = useMutation(
    BATCH_CREATE_SHIFT_DEFINITIONS,
    {
      onCompleted(data) {
        console.log(data);
        refetch();
        shiftDefinitions.refetch();
        setsnackBarMessage("Changes Succesfully Updated");
        setsnackbaropen(true);
      },
    }
  );


  const [batchDeleteShiftDefitions] = useMutation(
    BATCH_DELETE_SHIFT_DEFINITIONS,
    {
      onCompleted(data) {
        console.log(data);
        refetch();
        shiftDefinitions.refetch();
      },
    }
  );

  const [batchdeleteConstraint] = useMutation(BATCH_DELETE_CONSTRAINTS, {
    onCompleted(data) {
      console.log(data);
    },
    onError(error){
      console.error(error);
    }
  });

  const shiftDefinitionTypes = useQuery(SHIFT_DEFINITION_TYPE, {
    onError(error){
      console.error(error);
    }
  });
  const weekday = useQuery(WEEKDAY, {
    onError(error){
      console.error(error);
    }
  });

  const [updateOffice] = useMutation(UPDATE_OFFICE, {
    onCompleted() {
      shiftDefinitions.refetch();
      refetch();
    },
    onError(error){
      console.error(error);
    }
  });

  const [snackbaropen, setsnackbaropen] = useState(false);
  const [errorsnackbaropen, seterrorsnackbaropen] = useState(false);
  const [snackBarMessage, setsnackBarMessage] = useState(false);
  const [showDialog, setshowDialog] = useState(false);

  const handleShiftsSave = (values) => {
    const shiftData = [];
    shiftDefinitions.data.shiftDefinitions.map((element) => {
      shiftData.push(parseInt(element.id));
    });
    batchDeleteShiftDefitions({
      variables: {
        ids: shiftData,
      },
    });
    values.forEach((element) => {
      let typeId =
        !shiftDefinitionTypes.loading &&
        shiftDefinitionTypes.data.shiftType2s.find(
          (e) => e.type === element.shiftType
        );

      let inputData =
        !weekday.loading &&
        weekday.data.weekdays.map((e) => {
          return {
            office: parseInt(office.id),
            start: element.startTime,
            end: element.endTime,
            type: parseInt(typeId.id),
            weekday: parseInt(e.id),
            description: element.description,
          };
        });
      batchcreateShiftDefinition({
        variables: {
          input: inputData,
        },
      });
    });
  };

  const [shiftstodelete, setshiftstodelete] = useState({});

  const handleDeleteShifts = () => {
    setshowDialog(false);
    let selectedShifts = shiftDefinitions.data.shiftDefinitions.filter(
      (i) =>
        i.description === shiftstodelete.description &&
        i.type.type === shiftstodelete.shiftType &&
        i.office.id === office.id
    );
    let shiftsData = [];
    let officeConstraintData = [];
    selectedShifts.forEach((e) => {
      shiftsData.push(parseInt(e.id));
      office.constraintSet
        .filter((x) => x.starttime === e.start && x.endtime === e.end)
        .map((i) => {
          //deleteConstraint({ variables: { id: parseInt(i.id) } });
          officeConstraintData.push(i.id);
        });
    });
    batchDeleteShiftDefitions({
      variables: {
        ids: shiftsData,
      },
    });
    batchdeleteConstraint({
      variables: {
        ids: officeConstraintData,
      },
    });
  };

  if (loading || shiftDefinitions.loading) {
    return <CircularProgress color="primary" />;
  } else {
    return (
      <main variant="body">
        <Container style={{ margin: 0, padding: 0 }}>
          <Dialog open={showDialog} fullWidth maxWidth="xs">
            <DialogContent
              style={{
                padding: 20,
                overflowX: "hidden",
                textAlign: "center",
              }}
            >
              <Typography>
                Are you sure you want to delete this Shift?
              </Typography>
              <br />
              <br />
              <Button
                onClick={handleDeleteShifts}
                onKeyPress={(event) => {
                  if (event.key === "Enter") {
                    handleDeleteShifts();
                  }
                }}
                color="primary"
                variant="contained"
              >
                Yes
              </Button>
              <Button
                style={{ marginLeft: "20px" }}
                onClick={() => setshowDialog(false)}
                color="primary"
                variant="outlined"
              >
                No
              </Button>
            </DialogContent>
          </Dialog>
          <Snackbar
            open={snackbaropen}
            autoHideDuration={2000}
            onClose={() => {
              setsnackbaropen(false);
            }}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <MuiAlert
              onClose={() => {
                setsnackbaropen(false);
                setsnackBarMessage("");
              }}
              severity="success"
            >
              {snackBarMessage}
            </MuiAlert>
          </Snackbar>
          <Snackbar
            open={errorsnackbaropen}
            autoHideDuration={6000}
            onClose={() => {
              seterrorsnackbaropen(false);
            }}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <MuiAlert
              onClose={() => seterrorsnackbaropen(false)}
              severity="error"
            >
              {snackBarMessage}
            </MuiAlert>
          </Snackbar>

          <Grid container direction="row" spacing={4}>
            <Grid item className={classes.headerSpacing}>
              <Typography variant="h3">Department Shifts</Typography>
            </Grid>
            <Grid item xs={10}>
              <Formik
                enableReinitialize
                initialValues={{
                  name: office.name,
                  shifts: getShifts(
                    !shiftDefinitions.loading &&
                      shiftDefinitions.data.shiftDefinitions
                  ).shifts,
                  officeTimezone: office.timezone,
                  samlLoginOnly: office.samlLoginOnly,
                  name: office.name,
                  officeModelType: office.modeltype
                    ? office.modeltype.modelname
                    : "",
                  shiftSwitchBufferDays: office.shiftSwitchBufferDays
                    ? office.shiftSwitchBufferDays
                    : "",
                  maxTimeOff: office.maxGreyoutRequests
                    ? office.maxGreyoutRequests
                    : "",
                  allowCallIns: office.allowCallIns,
                  schedulePublishedBufferWeeks:
                    office.schedulePublishedBufferWeeks
                      ? office.schedulePublishedBufferWeeks
                      : "",

                  numberDaysBeforePreferencesDue:
                    office.numberDaysBeforePreferencesDue
                      ? office.numberDaysBeforePreferencesDue
                      : "",
                  reviewWindowDays: office.reviewWindowDays
                    ? office.reviewWindowDays
                    : "",
                  scheduleDuration: office.scheduleDuration,
                }}
                onSubmit={(values) => {
                  updateOffice({
                    variables: {
                      id: parseInt(office.id),
                      input: {
                        timezone: values.officeTimezone,
                        maxGreyoutRequests: parseInt(values.maxTimeOff),
                        allowCallIns: values.allowCallIns,
                        shiftSwitchBufferDays: parseInt(
                          values.shiftSwitchBufferDays
                        ),
                        samlLoginOnly: values.samlLoginOnly,
                        numberDaysBeforePreferencesDue: parseInt(
                          values.numberDaysBeforePreferencesDue
                        ),
                        schedulePublishedBufferWeeks: parseInt(
                          values.shiftSwitchBufferDays
                        ),
                        scheduleDuration: parseInt(values.scheduleDuration),
                      },
                    },
                  });
                  let elementLength = 0;
                  values.shifts.forEach((element) => {
                    if (values.officeModelType === "Traditional 24 x 7") {
                      let startVal = element.startTime.split(":")[0];
                      let endVal = element.endTime.split(":")[0];
                      let diff = parseInt(endVal) - parseInt(startVal);
                      if (diff > 12) {
                        setsnackBarMessage(
                          "Shift Duration should be 12 hours or less. Please change Shift Start and End Times and try again"
                        );
                        seterrorsnackbaropen(true);
                      } else {
                        elementLength++;
                      }
                      if (elementLength === values.shifts.length) {
                        handleShiftsSave(values.shifts);
                      }
                    } else {
                      let startVal = element.startTime.split(":")[0];
                      let endVal = element.endTime.split(":")[0];
                      let diff = parseInt(endVal) - parseInt(startVal);

                      if (diff > 8) {
                        setsnackBarMessage(
                          "Shift Duration should be 8 hours or less. Please change Shift Start and End Times and try again"
                        );
                        seterrorsnackbaropen(true);
                      } else {
                        elementLength++;
                      }
                      if (elementLength === values.shifts.length) {
                        handleShiftsSave(values.shifts);
                      }
                    }
                  });
                }}
              >
                {({ values, handleChange, dirty, setFieldValue }) => (
                  <Form>
                    <Grid container direction="row" item xs={12} spacing={4}>
                      <Grid item xs={4}>
                        <Box mb={1}>
                          <Typography variant="h5">Department Name</Typography>
                        </Box>
                        <TextField
                          id="departmentName"
                          name="departmentName"
                          variant="outlined"
                          disabled
                          value={values.name ? values.name : ""}
                          className={classes.inputWidth}
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Box mb={1}>
                          <Typography variant="h5">Shift Type</Typography>
                        </Box>
                        <TextField
                          id="departmentshiftType"
                          name="departmentshiftType"
                          variant="outlined"
                          value={
                            values.officeModelType ? values.officeModelType : ""
                          }
                          className={classes.inputWidth}
                          disabled
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Box mb={1}>
                          <Typography variant="h5">Time Zone</Typography>
                        </Box>
                        <Select
                          id="officeTimezone"
                          name="officeTimezone"
                          variant="outlined"
                          value={
                            values.officeTimezone
                              ? values.officeTimezone
                              : "UTC"
                          }
                          onChange={handleChange}
                          className={classes.inputWidth}
                        >
                          {timezones.map((timezone, ind) => (
                            <MenuItem key={ind} id={timezone} value={timezone}>
                              {timezone}
                            </MenuItem>
                          ))}
                        </Select>
                      </Grid>
                      <Grid item xs={12}>
                        <Typography>
                          <b>Office Login Type</b>
                        </Typography>
                      </Grid>
                      <Grid item xs={12}>
                        <Box mb={1}>
                          <Typography>Allow SAML Login Only</Typography>
                        </Box>
                        <FormControlLabel
                          control={
                            <Switch
                              checked={values.samlLoginOnly ? true : false}
                              onChange={(e) =>
                                setFieldValue("samlLoginOnly", e.target.checked)
                              }
                              name="samlLoginOnly"
                              color={
                                values.samlLoginOnly ? "primary" : "secondary"
                              }
                            />
                          }
                          label="Only SAML Login"
                        ></FormControlLabel>
                      </Grid>

                      <Grid item xs={12}>
                        <Typography>
                          <b>Request and Switch Shifts</b>
                        </Typography>
                      </Grid>
                      <Grid item xs={4}>
                        <Box mb={1}>
                          <Typography>Time-Off Requests</Typography>
                        </Box>
                        <Typography variant="body2"># of Requests</Typography>
                        <TextField
                          variant="outlined"
                          className={classes.smalltextbox}
                          id="maxTimeOff"
                          name="maxTimeOff"
                          value={values.maxTimeOff ? values.maxTimeOff : ""}
                          onChange={handleChange}
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Box mb={1}>
                          <Typography>Switch shift at least</Typography>
                        </Box>
                        <Typography variant="body2">days in advance</Typography>
                        <TextField
                          variant="outlined"
                          id="shiftSwitchBufferDays"
                          name="shiftSwitchBufferDays"
                          value={
                            values.shiftSwitchBufferDays
                              ? values.shiftSwitchBufferDays
                              : ""
                          }
                          className={classes.smalltextbox}
                          onChange={handleChange}
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Box mb={1}>
                          <Typography>Allow Call In's for Employees</Typography>
                        </Box>
                        <FormControlLabel
                          control={
                            <Switch
                              checked={values.allowCallIns ? true : false}
                              onChange={(e) =>
                                setFieldValue("allowCallIns", e.target.checked)
                              }
                              name="allowCallIns"
                              color={
                                values.allowCallIns ? "primary" : "secondary"
                              }
                            />
                          }
                          label="Allow Call In's"
                        ></FormControlLabel>
                      </Grid>
                      <Grid item xs={12}>
                        <Divider className={classes.divider} />
                      </Grid>
                      <Grid item xs={12}>
                        <Typography>
                          <b>Scheduling</b>
                        </Typography>
                      </Grid>
                      <Grid item xs={4}>
                        <Box mb={1}>
                          <Typography>
                            Preferences due before schedule is published
                          </Typography>
                        </Box>
                        <Typography variant="body2">in days</Typography>

                        <TextField
                          id="numberDaysBeforePreferencesDue"
                          name="numberDaysBeforePreferencesDue"
                          variant="outlined"
                          value={
                            values.numberDaysBeforePreferencesDue
                              ? values.numberDaysBeforePreferencesDue
                              : ""
                          }
                          onChange={handleChange}
                          className={classes.smalltextbox}
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Box mb={1}>
                          <Typography>Publish Schedule</Typography>
                        </Box>
                        <Typography variant="body2">
                          weeks in advance before schedule start date
                        </Typography>

                        <TextField
                          variant="outlined"
                          id="schedulePublishedBufferWeeks"
                          name="schedulePublishedBufferWeeks"
                          value={
                            values.schedulePublishedBufferWeeks
                              ? values.schedulePublishedBufferWeeks
                              : ""
                          }
                          className={classes.smalltextbox}
                          onChange={handleChange}
                        />
                      </Grid>
                      <Grid item xs={4}>
                        <Box mb={1}>
                          <Typography>Schedule Duration</Typography>
                        </Box>
                        <Typography variant="body2">in weeks</Typography>

                        <TextField
                          variant="outlined"
                          id="scheduleDuration"
                          name="scheduleDuration"
                          value={
                            values.scheduleDuration
                              ? values.scheduleDuration
                              : ""
                          }
                          className={classes.smalltextbox}
                          onChange={handleChange}
                        />
                      </Grid>

                      <Grid item xs={12}>
                        <Divider className={classes.divider} />
                      </Grid>
                      <Grid item xs={10}>
                        <FieldArray name="shifts">
                          {({ push, remove }) => (
                            <Table>
                              <TableHead>
                                <TableRow>
                                  <StyledTableCell colSpan={5}>
                                    Shift Settings for department
                                  </StyledTableCell>
                                </TableRow>
                                <TableRow>
                                  <StyledTableCell>Shift Name</StyledTableCell>
                                  <StyledTableCell>Shift Type</StyledTableCell>
                                  <StyledTableCell>Start Time</StyledTableCell>
                                  <StyledTableCell>End Time</StyledTableCell>
                                  <StyledTableCell></StyledTableCell>
                                </TableRow>
                              </TableHead>
                              <TableBody>
                                {values.shifts.length > 0 &&
                                  values.shifts.map((e, index) => (
                                    <TableRow key={index}>
                                      <StyledTableCell>
                                        <TextField
                                          id={`shifts.${index}.description`}
                                          name={`shifts.${index}.description`}
                                          variant="outlined"
                                          value={e.description}
                                          onChange={handleChange}
                                        />
                                      </StyledTableCell>
                                      <StyledTableCell>
                                        <Select
                                          id={`shifts.${index}.shiftType`}
                                          name={`shifts.${index}.shiftType`}
                                          variant="outlined"
                                          value={e.shiftType}
                                          onChange={handleChange}
                                        >
                                          {!shiftDefinitionTypes.loading &&
                                            shiftDefinitionTypes.data
                                              .shiftType2s.length > 0 &&
                                            shiftDefinitionTypes.data.shiftType2s.map(
                                              (x, i) => (
                                                <MenuItem
                                                  key={i}
                                                  value={x.type}
                                                >
                                                  {x.type}
                                                </MenuItem>
                                              )
                                            )}
                                        </Select>
                                      </StyledTableCell>
                                      <StyledTableCell>
                                        <TextField
                                          id={`shifts.${index}.startTime`}
                                          name={`shifts.${index}.startTime`}
                                          variant="outlined"
                                          value={e.startTime}
                                          onChange={handleChange}
                                          type="time"
                                          disabled={
                                            e.startTime != "" ? true : false
                                          }
                                        />
                                      </StyledTableCell>
                                      <StyledTableCell>
                                        <TextField
                                          id={`shifts.${index}.endTime`}
                                          name={`shifts.${index}.endTime`}
                                          variant="outlined"
                                          value={e.endTime}
                                          onChange={handleChange}
                                          type="time"
                                          disabled={
                                            e.endTime != "" ? true : false
                                          }
                                        />
                                      </StyledTableCell>
                                      <StyledTableCell>
                                        <IconButton
                                          color="secondary"
                                          onClick={() => {
                                            remove(index);
                                            setshowDialog(true);
                                            setshiftstodelete(e);
                                          }}
                                        >
                                          <DeleteIcon />
                                        </IconButton>
                                      </StyledTableCell>
                                    </TableRow>
                                  ))}
                                <TableRow>
                                  <StyledTableCell>
                                    <Button
                                      color="primary"
                                      startIcon={<AddIcon />}
                                      onClick={() =>
                                        push({
                                          description: "",
                                          shiftType: "",
                                          startTime: "",
                                          endTime: "",
                                        })
                                      }
                                    >
                                      Add Another Shift
                                    </Button>
                                  </StyledTableCell>
                                </TableRow>
                              </TableBody>
                            </Table>
                          )}
                        </FieldArray>
                      </Grid>

                      <Grid item xs={1}></Grid>
                    </Grid>
                    <Box m={4} style={{ textAlign: "right" }}>
                      <Button
                        type="submit"
                        disabled={!dirty}
                        color="primary"
                        variant="contained"
                      >
                        Submit
                      </Button>
                    </Box>
                  </Form>
                )}
              </Formik>
            </Grid>
          </Grid>
        </Container>
      </main>
    );
  }
}

export default OfficeShifts;
