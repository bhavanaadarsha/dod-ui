import React, { useEffect, useState } from "react";
import {
  Grid,
  Typography,
  TextField,
  Button,
  IconButton,
  Table,
  TableRow,
  TableCell,
  TableHead,
  TableBody,
  withStyles,
  Dialog,
  DialogContent,
  Snackbar,
  Divider,
  makeStyles,
  Box,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import { useQuery, useMutation, gql, useLazyQuery } from "@apollo/client";
import {
  FIND_SKILL_TYPE,
  GET_OFFICE,
  ADD_LOCATION_SKILLS,
  DELETE_SKILL,
  CREATE_SKILLSET_CONSTRAINT,
  GET_CONSTRAINT_TYPE,
  SHIFT_DEFINITIONS,
} from "../../api/gqlQueries";
import MuiAlert from "@material-ui/lab/Alert";
import { DepartmentTrainingVar } from "../../cache";
import { findConstraint, getShifts } from "../../helpers/officeHelpers";
import { format } from "date-fns";

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
    display: "flex",
  },
  divider: {
    border: "0.5px solid thin #333333",
    verticalAlign: "bottom",
  },
}));

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "#EAEAEA",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRottom: "0",
    paddingLeft: "20px",
  },
  body: {
    fontSize: 14,
    backgroundColor: "#ffffff",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRottom: "0",
    paddingLeft: "20px",
  },
}))(TableCell);

function DepartmentTrainings(props) {
  const classes = useStyles();
  const [trngInput, setTrngInput] = useState("");
  const [showDialog, setshowDialog] = useState(false);
  const [snackbaropen, setsnackbaropen] = useState(false);
  const [errorsnackbaropen, seterrorsnackbaropen] = useState(false);
  const [snackBarMessage, setsnackBarMessage] = useState(false);
  const [trainingidtodelete, setTrainingidtodelete] = useState("");
  const [shiftTypes, SetShiftTypes] = useState([]);
  const departmentTrainingVar = DepartmentTrainingVar();
  const [shiftDefinitions] = useLazyQuery(SHIFT_DEFINITIONS, {
    onCompleted(data) {
      let shifts = getShifts(data.shiftDefinitions);
      SetShiftTypes(shifts);
    },
    onError(error){
      console.error(error);
    }
  });

  const [getOffice,{refetch,loading}] = useLazyQuery(GET_OFFICE, {
    onError(error){
      console.error(error);
    }
  });

  useEffect(() => {
    getOffice({
      variables:{
        id:parseInt(props.office)
      }
    });
  }, [!loading]);

  useEffect(() => {
    shiftDefinitions({
      variables: {
        officeId: parseInt(props.office),
      },
    });
  }, [!shiftDefinitions.loading]);

  const allSkills = useQuery(FIND_SKILL_TYPE, {
    variables: { office: props.office },
    onError(error){
      console.error(error);
    }
  });

  const [deleteSkill] = useMutation(DELETE_SKILL, {
    update(cache, { data: { deleteSkill } }) {
      cache.evict({
        data: { id: deleteSkill.deletedId },
      });
    },
    onCompleted() {
      refetch();
      let a = allSkills.data.skills.map((i) => i.variety === "JOB_TYPE");
      if (a.length <= 0) {
        departmentTrainingVar.isEmpty = true;
      }
    },
    onError(error){
      console.error(error);
    }
  });

  const allConstraintTypes = useQuery(GET_CONSTRAINT_TYPE, {
    onError(error){
      console.error(error);
    }
  });
  const allconstrinttypes =
    !allConstraintTypes.loading &&
    !allConstraintTypes.error &&
    allConstraintTypes.data;

  const [createSkillConstraints] = useMutation(CREATE_SKILLSET_CONSTRAINT, {
    onCompleted() {
      allSkills.refetch();
      refetch();
    },
    onError(error){
      console.error(error);
    }
  });

  const [addSkills] = useMutation(ADD_LOCATION_SKILLS, {
    update(cache, { data: { createEmployeeSkill } }) {
      cache.modify({
        fields: {
          employeeSkill(existingEmployeeSkill = []) {
            const newEmpSkillRef = cache.writeFragment({
              data: createEmployeeSkill.employeeSkill,
              fragment: gql`
                fragment NewEmployeeSkill on EmployeeSkillNode {
                  skill {
                    id
                    variety
                    name
                    office {
                      id
                    }
                  }
                }
              `,
            });
            return [...existingEmployeeSkill, newEmpSkillRef];
          },
        },
      });
    },
    onCompleted(d) {
      const typeid = findConstraint(
        allconstrinttypes.constraintDefinitions,
        "SKILLCOVER"
      );

      shiftTypes.length > 0 &&
        shiftTypes.forEach((element) => {
          createSkillConstraints({
            variables: {
              input: {
                value: 0,
                office: parseInt(props.office),
                monday: true,
                tuesday: true,
                wednesday: true,
                thursday: true,
                friday: true,
                saturday: true,
                sunday: true,
                effectstart: format(new Date(), "yyyy-MM-dd"),
                skill: parseInt(d.createSkill.skill.id),
                type: parseInt(typeid.id),
                starttime: element.startTime,
                endtime: element.endTime,
              },
            },
          });
        });
      departmentTrainingVar.isEmpty = false;
      refetch();
      setTrngInput("");
    },
    onError(error){
      console.error(error);
    }
  });
  const handleAddTraining = () => {
    if (trngInput.length > 0) {
      addSkills({
        variables: {
          input: {
            name: trngInput,
            variety: "TRAINING",
            office: parseInt(props.office),
          },
        },
      });
      setTrngInput("");
      refetch();
      allSkills.refetch();
    }
  };
  const handleDeleteTraining = () => {
    deleteSkill({ variables: { id: parseInt(trainingidtodelete) } });
    setTrainingidtodelete("");
    setsnackBarMessage("Unit Specific Skillset successfully deleted");
    setsnackbaropen(true);
    setshowDialog(false);
  };

  return (
    <div style={{ marginTop: "20px" }}>
      <Grid
        container
        display="flex"
        style={{ border: "0.5px solid lightgray" }}
      >
        <Box m={2}>
          {
            <Grid container direction="row" spacing={4} display="flex">
              <Snackbar
                open={snackbaropen}
                autoHideDuration={3000}
                onClose={() => setsnackbaropen(false)}
                anchorOrigin={{ vertical: "top", horizontal: "center" }}
              >
                <MuiAlert
                  onClose={() => {
                    setsnackbaropen(false);
                    setsnackBarMessage("");
                  }}
                  severity="success"
                >
                  {snackBarMessage}
                </MuiAlert>
              </Snackbar>
              <Snackbar
                open={errorsnackbaropen}
                autoHideDuration={6000}
                onClose={() => seterrorsnackbaropen(false)}
                anchorOrigin={{ vertical: "top", horizontal: "center" }}
              >
                <MuiAlert
                  onClose={() => seterrorsnackbaropen(false)}
                  severity="error"
                >
                  {snackBarMessage}
                </MuiAlert>
              </Snackbar>
              <Dialog open={showDialog} fullWidth maxWidth="xs">
                <DialogContent
                  style={{
                    padding: 20,
                    overflowX: "hidden",
                    textAlign: "center",
                  }}
                >
                  <Typography>
                    Are you sure you want to delete this Unit Specific
                    Requirement?
                  </Typography>
                  <br />
                  <br />
                  <Button
                    onClick={handleDeleteTraining}
                    onKeyPress={(event) => {
                      if (event.key === "Enter") {
                        handleDeleteTraining();
                      }
                    }}
                    color="primary"
                    variant="contained"
                  >
                    Yes
                  </Button>
                  <Button
                    style={{ marginLeft: "20px" }}
                    onClick={() => setshowDialog(false)}
                    color="primary"
                    variant="outlined"
                  >
                    No
                  </Button>
                </DialogContent>
              </Dialog>
              <Grid item xs={12}>
                <Typography variant="h3">{props.office.name}</Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant="h4">
                  Unit Specific Skillset Requirements
                </Typography>{" "}
                <Typography>
                  Add Unit Specific Skillset Requirements for{" "}
                  <b>{props.officename}</b>
                </Typography>
              </Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={2}>
                <TextField
                  type="text"
                  variant="outlined"
                  name="newcert"
                  onChange={(e) => {
                    setTrngInput(e.currentTarget.value);
                  }}
                  onKeyPress={(event) => {
                    if (event.key === "Enter") {
                      handleAddTraining();
                    }
                  }}
                  value={trngInput}
                  placeholder="New Skillset"
                />
              </Grid>
              <Grid item xs={2} align="left">
                <Button
                  variant="contained"
                  onClick={handleAddTraining}
                  color="primary"
                >
                  Add
                </Button>
              </Grid>
              <Grid item xs={7}></Grid>
              <Grid item xs={1}></Grid>
              <Grid item xs={6} align="right">
                <Table>
                  <TableHead>
                    <TableRow>
                      <StyledTableCell colSpan={2}></StyledTableCell>
                    </TableRow>

                    <TableRow>
                      <StyledTableCell>Unit Specific Skill Set</StyledTableCell>
                      <StyledTableCell>Delete</StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {!allSkills.loading &&
                      allSkills.data.skills.length > 0 &&
                      allSkills.data.skills
                        .filter((i) => i.variety === "TRAINING")
                        .map((element, ind) => (
                          <TableRow key={ind}>
                            <StyledTableCell>{element.name}</StyledTableCell>
                            <StyledTableCell>
                              <IconButton
                                onClick={() => {
                                  setTrainingidtodelete(element.id);
                                  setshowDialog(true);
                                }}
                              >
                                <DeleteIcon />
                              </IconButton>
                            </StyledTableCell>
                          </TableRow>
                        ))}
                  </TableBody>
                </Table>
              </Grid>
              <Grid item xs={5}></Grid>
              <Grid item xs={12}>
                <Divider className={classes.divider} />
              </Grid>
            </Grid>
          }
        </Box>
      </Grid>
    </div>
  );
}

export default DepartmentTrainings;
