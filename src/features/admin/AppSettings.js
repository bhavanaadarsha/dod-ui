import React from 'react';
import {
    makeStyles, Tabs, Tab, Grid, Typography
} from '@material-ui/core';
// import HospitalSettings from './HospitalSettings';
import ApplicationSettings from './ApplicationSettings';
import Integrations from './Integrations';
// import DepartmentSettings from './DepartmentSettings';


const useStyles = makeStyles(() => ({
    headerSpacing: {
        marginTop: 30,
        marginBottom: 20
    }
}));



function AppSettings(props) {
    const classes = useStyles();
    const tab = props.location.props!=null && props.location.props.tab;
    const [value, setValue] = React.useState(tab? tab:0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div>
            <Typography variant="h3" className={classes.headerSpacing}>Settings</Typography>
            <Grid container direction='column' spacing={2}>
                <Grid item xs={12}>
                    <Tabs
                        value={value}
                        textColor="primary"
                        onChange={handleChange}
                    >
                        {/* <Tab label="Hospital" />
                        <Tab label="Department" /> */}
                        <Tab label="Application" />
                        <Tab label="Integrations" />
                    </Tabs>
                </Grid>
                <Grid item xs={12} style={{marginLeft: 15}}>
                    {/* {value === 0 && (
                        <HospitalSettings />
                    )}
                    {value === 1 && (
                        <DepartmentSettings />
                    )}  */}
                    {value === 0 && (
                        <ApplicationSettings />
                    )}
                    {value === 1 && (
                        <Integrations />
                    )}
                </Grid>
            </Grid>
        </div >
    );
}

export default AppSettings;