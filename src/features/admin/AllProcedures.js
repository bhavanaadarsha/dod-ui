import React from "react";
import {
  DataGrid,
  GridToolbarContainer,
  GridColumnsToolbarButton,
  GridFilterToolbarButton,
  GridToolbarExport,
} from "@material-ui/data-grid";
import {
  Typography,
  makeStyles,
  Grid,
  CircularProgress,
} from "@material-ui/core";
import { useQuery, useMutation } from "@apollo/client";
import {
  PROCEDURE_REQUIREMENTS,
  PROCEDURE_EMPLOYEE_CONSTRAINTS,
} from "../../api/gqlQueries";
import DeleteIcon from "@material-ui/icons/Delete";
import { userVar } from "../../cache";

const useStyles = makeStyles(() => ({
  dt: {
    "& .MuiDataGrid-columnsContainer": {
      backgroundColor: "#EAEAEA",
      color: "#333333",
    },
    "& .MuiButton-iconSizeSmall": {
      color: "rgba(134, 134, 137, 1)",
    },
    "& .MuiButton-label": {
      color: "rgba(134, 134, 137, 1)",
      fontSize: "15px",
    },
    border: "none",
    fontSize: "15px",
  },
  headerSpacing: {
    marginTop: 30,
    marginBottom: 20,
  },
}));

const AllProcedures = () => {
  const classes = useStyles();
  const user = userVar();
  const CustomToolbar = () => {
    return (
      <GridToolbarContainer style={{ justifyContent: "flex-end" }}>
        <GridColumnsToolbarButton />
        <GridFilterToolbarButton />
        <GridToolbarExport />
      </GridToolbarContainer>
    );
  };

  const procedureRequirements = useQuery(
    PROCEDURE_REQUIREMENTS,
    {
      variables: {
        office: parseInt(user.office.id),
      },
    },
    {
      onError(error) {
        console.log(error);
      },
    }
  );

  const columns = [
    {
      field: "status",
      headerName: "Status",
      width: 100,
      renderCell: (params) => <div>Added</div>,
    },
    {
      field: "name",
      headerName: "MSN",
      width: 250,
    },
    {
      field: "earliestDate",
      headerName: "T/O Time Z",
      width: 300,
    },
    {
      field: "latestDate",
      headerName: "Land Z",
      width: 200,
    },
    {
      field: "duration",
      headerName: "Duration",
      width: 200,
    },
    {
      field: "office",
      headerName: "Department",
      width: 200,
      renderCell: (params) => <div>{params.row.office.name}</div>,
    },
  ];

  const rows =
    !procedureRequirements.loading &&
    procedureRequirements.data.procedureRequirements.length > 0 &&
    procedureRequirements.data.procedureRequirements;

  if (procedureRequirements.loading) {
    return <CircularProgress color="primary" />;
  } else if (procedureRequirements.error) {
    return <div>Something went wrong try again later</div>;
  } else if (procedureRequirements.data) {
    return (
      <Grid container className={classes.headerSpacing}>
        <Grid item xs={6}>
          <Typography variant="h3">Mission Details</Typography>
        </Grid>
        <Grid item xs={12}>
          <div>
            <div style={{ height: 700 }}>
              <DataGrid
                className={classes.dt}
                rows={rows}
                columns={columns}
                components={{
                  Toolbar: CustomToolbar,
                }}
              />
            </div>
          </div>
        </Grid>
      </Grid>
    );
  }
};

export default AllProcedures;
