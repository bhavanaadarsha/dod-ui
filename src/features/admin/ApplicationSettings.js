import React, { useState, useEffect } from "react";
import {
  Grid,
  Typography,
  Button,
  Box,
  Paper,
  TextField,
  Snackbar,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { SketchPicker } from "react-color";
import { useMutation, useQuery, useReactiveVar } from "@apollo/client";
import { UPDATE_LOCATION, PUBLICLOCATION, FILE_UPLOAD } from "../../api/gqlQueries";
import { appsettingsVar } from "../../cache";

function ApplicationSettings() {
  const locationSettings = useQuery(PUBLICLOCATION, {
    onError(error){
      console.error(error);
    }
  });
  const currentColor = useReactiveVar(appsettingsVar);
  const [clr, setClr] = useState(currentColor.color);
  const [snackbaropen, setsnackbaropen] = useState(false);
  const [snackBarMessage, setsnackBarMessage] = useState(false);
  const [errorsnackbaropen, seterrorsnackbaropen] = useState(false);

  const [updateLocation] = useMutation(UPDATE_LOCATION, {
    onError(error){
      console.error(error);
    }
  });

  const handleColorChange = () => {
    appsettingsVar({ color: clr });
    updateLocation({
      variables: {
        id: parseInt(locationSettings.data.locationPublicSettings[0].id),
        input: { hexcolor: currentColor.color },
      },
    });
  };

  const SUPPORTED_FORMATS = [
    "image/jpg",
    "image/jpeg",
    "image/png",
    "image/gif",
  ];

  const FILE_SIZE = 160 * 1024;

  useEffect(() => {
    currentColor.color = clr;
  }, [clr]);

  const [fileUpload] = useMutation(FILE_UPLOAD, {
    onCompleted() {
      locationSettings.refetch();
      appsettingsVar({ logo: locationSettings.data.locationPublicSettings[0].logo }); 
      setsnackBarMessage("Logo Successfully Changed");
      setsnackbaropen(true);
    },
    onError(error) {
      console.error(error);
      setsnackBarMessage(
        "Something went wrong. Please try uploading the logo again."
      );
      seterrorsnackbaropen(true);
    },
  });

  const handleFileUpload = ({
    target: {
        validity,
        files: [file],
    },
}) => {
    if (validity.valid) {
      fileUpload({
        variables: {id: parseInt(locationSettings.data.locationPublicSettings[0].id),file},
      });
    }
  };

  return (
    <Grid container direction="row" spacing={4} display="flex">
      <Snackbar
        open={snackbaropen}
        autoHideDuration={3000}
        onClose={() => setsnackbaropen(false)}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <MuiAlert
          onClose={() => {
            setsnackbaropen(false);
            setsnackBarMessage("");
          }}
          severity="success"
        >
          {snackBarMessage}
        </MuiAlert>
      </Snackbar>
      <Snackbar
        open={errorsnackbaropen}
        autoHideDuration={3000}
        onClose={() => seterrorsnackbaropen(false)}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <MuiAlert onClose={() => seterrorsnackbaropen(false)} severity="error">
          {snackBarMessage}
        </MuiAlert>
      </Snackbar>
      <Grid item xs={12}>
        <Typography>
          <b>Logo</b>
        </Typography>
      </Grid>
      <Grid item xs={2}>
        <Box ml={4}>
          <Typography>Upload Logo</Typography>
        </Box>
      </Grid>
      <Grid item xs={6}>
        <input
          id="file"
          name="file"
          type="file"
          variant="outlined"
          onChange={handleFileUpload}
        />
        {/* <Formik
          initialValues={{ file: null }}
          onSubmit={(values) => {
            currentColor.logo = values.file.name;
            console.log(values.file);
            const baseURL = window.location.origin;
            const environment = process.env.NODE_ENV;
            // if (environment === "development" || baseURL.includes("amplify")) {
            //   SetUrl(
            //     "https://backendtest.balancescheduler.com/assets/logos/" +
            //       currentColor.logo
            //   );
            // } else {
            //   SetUrl(
            //     "https://backenddemo.balancescheduler.com/assets/logos/" +
            //       currentColor.logo
            //   );
            // }
            mutate({ variables: values.file })
            
          }}
          validationSchema={Yup.object().shape({
            file: Yup.mixed()
              .required("A file is required")
              .test(
                "fileSize",
                "File too large",
                (value) => value && value.size <= FILE_SIZE
              )
              .test(
                "fileFormat",
                "Unsupported Format",
                (value) => value && SUPPORTED_FORMATS.includes(value.type)
              ),
          })}
        >
          {({ handleSubmit, setFieldValue, errors, values }) => (
            <form onSubmit={handleSubmit}>
              <Grid direction="row" container spacing={2}>
                <Grid item xs={6}>
                  <TextField
                    id="file"
                    name="file"
                    type="file"
                    variant="outlined"
                    onChange={(event) => {
                      setFieldValue("file", event.target.files[0]);
                    }}
                  />
                  <Typography color="primary" variant="subtitle2">
                    {errors.file}
                  </Typography>
                </Grid>
                <Grid item xs={6} align="left">
                  <Button type="submit" variant="outlined">
                    Update Logo
                  </Button>
                </Grid>
                <Grid item xs={6}>
                  {url != "" && (
                    <img src={url} width="200" alt="Duality Logo" />
                  )}
                </Grid>
              </Grid>
            </form>
          )}
        </Formik> */}
      </Grid>

      <Grid item xs={4}></Grid>
      <Grid item xs={12}>
        <Typography>
          <b>Colors</b>
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Typography>
          Choose your accent color for the application bar:
        </Typography>
      </Grid>
      <Grid item container direction="row" xs={6}>
        <Grid item xs={8}>
          <Box component={Paper} p={5} align="left">
            <Box mb={2}>
              <Typography>
                <b>Select Color</b>
              </Typography>
            </Box>
            <SketchPicker
              color={clr}
              onChangeComplete={(e) => {
                setClr(e.hex);
                handleColorChange(e.hex);
              }}
            />
          </Box>
        </Grid>
        <Grid item xs={4}></Grid>
        <Grid item xs={4}></Grid>
        <Grid item xs={2} align="right">
          <Box mt={3}>
            <Button
              color="primary"
              variant="outlined"
              onClick={() => {
                updateLocation({
                  variables: {
                    id: parseInt(
                      locationSettings.data.locationPublicSettings[0].id
                    ),
                    input: { hexcolor: "#F15A25" },
                  },
                });
              }}
            >
              Reset
            </Button>
          </Box>
        </Grid>
        <Grid item xs={2} align="right">
          <Box mt={3}>
            <Button
              color="primary"
              variant="contained"
              onClick={() => handleColorChange(currentColor.color)}
            >
              Apply
            </Button>
          </Box>
        </Grid>

        <Grid item xs={4}></Grid>
      </Grid>
      <Grid item xs={6}></Grid>
    </Grid>
  );
}
export default ApplicationSettings;
