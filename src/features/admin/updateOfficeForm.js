import React, { useState, useEffect } from "react";
import {
  Grid,
  Typography,
  Button,
  FormControlLabel,
  Switch,
  Box,
  TextField,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  withStyles,
  makeStyles,
  Container,
  Snackbar,
  Divider,
  CircularProgress,
  IconButton,
  Dialog,
  DialogContent,
} from "@material-ui/core";
import { Formik, FieldArray, getIn, Field, useFormikContext } from "formik";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";
import { useQuery, useMutation, useLazyQuery, gql } from "@apollo/client";
import {
  FIND_SKILL_TYPE,
  UPDATE_CONSTRAINT,
  GET_CONSTRAINT_TYPE,
  CREATE_CONSTRAINT,
  DELETE_CONSTRAINT,
  UPDATE_OFFICE,
  EMPLOYEE_TYPES,
  MODEL_TYPE,
  CREATE_SKILLSET_CONSTRAINT,
  SHIFT_DEFINITIONS,
  GET_OFFICE,
  DELETE_EMPLOYEE_TYPES,
  CREATE_EMPLOYEE_TYPE,
} from "../../api/gqlQueries";
import {
  UpdateHospitalConstraint,
  CreateHospitalConstraint,
  findConstraint,
  findofficeConstraint,
  findemployeeTypeOfficeConstraint,
  CreateHourlyRequirementsConstraints,
  getShifts,
  getskillsetforDaysofweek,
  saveskillsetforDaysofweek,
  getEmployeeTypeValues,
  handleDeleteSkillSetConstraints,
} from "../../helpers/officeHelpers";
import MuiAlert from "@material-ui/lab/Alert";
import SkillCoverReq from "./SkillCoverReq";
import { Prompt } from "react-router-dom";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";

const useStyles = makeStyles(() => ({
  inputWidth: {
    width: "100%",
  },
  mediumtextbox: {
    width: "50%",
  },
  smalltextbox: {
    width: "25%",
  },
  loading: {
    display: "block",
    margin: "auto",
  },
  textColor: {
    color: "#F15A29",
  },
  headerSpacing: {
    marginTop: 30,
    marginBottom: 20,
  },
  button: {
    width: 80,
  },
  autocompleteInput: {
    width: "100%",
    padding: 0,
  },
}));

const StyledTableCell = withStyles(() => ({
  head: {
    backgroundColor: "#EAEAEA",
    paddingTop: "0",
    paddingBottom: "0",
    paddingRottom: "0",
    border: "none",
    textAlign: "center",
  },
  body: {
    fontSize: 14,
    backgroundColor: "#ffffff",
    paddingTop: "5px",
    paddingBottom: "0",
    paddingRottom: "0",
    border: "none",
    textAlign: "center",
  },
}))(TableCell);

const ErrorMessage = ({ name }) => (
  <Field>
    {({ form }) => {
      const error = getIn(form.errors, name);
      return (
        <Typography variant="subtitle2" color="primary">
          {error ? error : null}
        </Typography>
      );
    }}
  </Field>
);

const validationSchema = Yup.object({
  // fulltimemin: Yup.number().required("Required"),
  // fulltimemax: Yup.number().required("Required"),
  // parttimemin: Yup.number().required("Required"),
  // parttimemax: Yup.number().required("Required"),
  // tempmin: Yup.number().required("Required"),
  // tempmax: Yup.number().required("Required"),
  // floatmin: Yup.number().required("Required"),
  // floatmax: Yup.number().required("Required"),
  // prnmin: Yup.number().required("Required"),
  // prnmax: Yup.number().required("Required"),
  // maxdaysOffwithoutPTO: Yup.number().required("Required"),
  // maxhoursperday: Yup.number().required("Required"),
  // maxconsecutivedays: Yup.number().required("Required"),
});

const PromptIfDirty = () => {
  const formik = useFormikContext();
  return (
    <Prompt
      when={formik.submitCount === 0}
      message="Are you sure you want to leave? You have unsaved changes on this form."
    />
  );
};

function UpdateOfficeForm(props) {
  const classes = useStyles();
  const modelTypes = useQuery(MODEL_TYPE, {
    onError(error){
      console.error(error);
    }
  });
  const allskilltypes = useQuery(FIND_SKILL_TYPE, {
    onError(error){
      console.error(error);
    }
  });
  const history = useHistory();
  const [office, SetOffice] = useState({});
  const [showDialog, setshowDialog] = useState(false);
  const [dialogMessage, setDialogMessage] = useState("");
  const [deleteempType, setDeleteEmpType] = useState(false);
  const [idtoDelete, setIdtoDelete] = useState("");
  const [duplicateRow, SetDuplicateRow] = useState(false);
  const [valuestodelete, setValuestoDelete] = useState({});
  const [otherform, setotherform] = useState(false);
  const [starttime, setstarttime] = useState("");
  const [endtime, setendtime] = useState("");

  const [getOffice, { refetch, loading }] = useLazyQuery(GET_OFFICE, {
    onCompleted(data) {
      refetch();
      SetOffice(data.offices[0]);
    },
    onError(error){
      console.error(error);
    }
  });

  useEffect(() => {
    getOffice({
      variables: {
        id: parseInt(props.office),
      },
    });
  }, [!loading]);

  const shiftDefinitions = useQuery(SHIFT_DEFINITIONS, {
    variables: {
      officeId: parseInt(office.id),
    },
    onError(error){
      console.error(error);
    }
  });

  const allConstraintTypes = useQuery(GET_CONSTRAINT_TYPE, {
    onError(error){
      console.error(error);
    }
  });
  const allconstrinttypes =
    !allConstraintTypes.loading &&
    !allConstraintTypes.error &&
    allConstraintTypes.data;
  const staffTable = office.constraintSet?.filter(
    (i) => i.skill != null && i.skill.variety === "JOB_TYPE"
  );
  const trainingTable = office.constraintSet?.filter(
    (i) => i.skill != null && i.skill.variety === "TRAINING"
  );
  const trng =
    !allskilltypes.loading &&
    allskilltypes.data.skills?.filter(
      (i) => i.variety === "TRAINING" && i.office?.id === office.id
    );
  const alljobtypes =
    !allskilltypes.loading &&
    allskilltypes.data.skills?.filter(
      (i) => i.variety === "JOB_TYPE" && i.office?.id === office.id
    );

  const [createEmployeeType] = useMutation(CREATE_EMPLOYEE_TYPE, {
    update(cache, { data: { createEmployeeType } }) {
      cache.modify({
        fields: {
          employeeType(existingEmployeeType = []) {
            const newEmployeeTypeRef = cache.writeFragment({
              data: createEmployeeType.employeeType,
              fragment: gql`
                fragment NewemployeeType on createEmployeeType {
                  employeeType {
                    id
                    name
                  }
                }
              `,
            });
            return [...existingEmployeeType, newEmployeeTypeRef];
          },
        },
      });
    },
    onCompleted(d) {
      const id = { id: d.createEmployeeType.employeeType.id };
      const values = { ...vals, ...id };
      handleEmployeeTypeConstraints(values);
      refetch();
      allEmployeeTypes.refetch();
    },
    onError(error){
      console.error(error);
    }
  });

  const [updateContraints] = useMutation(UPDATE_CONSTRAINT, {
    onCompleted(data) {
      refetch();
    },
    onError(error){
      console.error(error);
    }
  });
  const [createContraints] = useMutation(CREATE_CONSTRAINT,{
    onCompleted(){
      refetch();
    },
    onError(error){
      console.error(error);
    }
  });
  const [createSkillConstraints] = useMutation(CREATE_SKILLSET_CONSTRAINT, {
    onCompleted(data) {
      console.log(data)
      refetch();
    },
    onError(error){
      console.error(error);
    }
  });

  const [deleteConstraint] = useMutation(DELETE_CONSTRAINT,{
    onCompleted(data){
      console.log(data)
      refetch();
    },
    onError(error){
      console.error(error);
    }
  });

  const getshifts = getShifts(
    !shiftDefinitions.loading && shiftDefinitions.data.shiftDefinitions
  );

  const [deleteEmployeeType] = useMutation(DELETE_EMPLOYEE_TYPES, {
    onCompleted() {
      setshowDialog(false);
      setDialogMessage("");
      setDeleteEmpType(false);
      setIdtoDelete("");
      refetch();
      allEmployeeTypes.refetch();
    },
    onError(error){
      console.error(error);
    }
  });

  const allEmployeeTypes = useQuery(EMPLOYEE_TYPES, {
    onError(error){
      console.error(error);
    }
  });
  const [snackbaropen, setsnackbaropen] = useState(false);
  const [errorsnackbaropen, seterrorsnackbaropen] = useState(false);
  const [snackBarMessage, setsnackBarMessage] = useState(false);
  const [vals, Setvals] = useState({});

  const handleEmployeeTypeConstraints = (element) => {
    let fteminhrwk =
      !allConstraintTypes.loading &&
      findemployeeTypeOfficeConstraint(
        office.constraintSet,
        "FTEMINHRWK",
        element.employeeType
      );
    let minhr =
      !allConstraintTypes.loading &&
      findConstraint(allconstrinttypes.constraintDefinitions, "FTEMINHRWK");

    if (!fteminhrwk) {
      console.log(fteminhrwk);
      CreateHourlyRequirementsConstraints(
        createContraints,
        element.requiredWeeklyHrs,
        minhr.id,
        office.id,
        element.id
      );
    } else {
      UpdateHospitalConstraint(
        updateContraints,
        element.requiredWeeklyHrs,
        fteminhrwk.id,
        minhr.id
      );
    }
    let ftemaxhrwk =
      !allConstraintTypes.loading &&
      findemployeeTypeOfficeConstraint(
        office.constraintSet,
        "FTEMAXHRWK",
        element.employeeType
      );
    let maxhr =
      !allConstraintTypes.loading &&
      findConstraint(allconstrinttypes.constraintDefinitions, "FTEMAXHRWK");

    if (!ftemaxhrwk) {
      CreateHourlyRequirementsConstraints(
        createContraints,
        element.maxWeeklyHrs,
        maxhr.id,
        office.id,
        element.id
      );
    } else {
      UpdateHospitalConstraint(
        updateContraints,
        element.maxWeeklyHrs,
        ftemaxhrwk.id,
        maxhr.id
      );
    }

    let maxdaysoff =
      !allConstraintTypes.loading &&
      findemployeeTypeOfficeConstraint(
        office.constraintSet,
        "MAXDAYSOFF",
        element.employeeType
      );
    let maxdaysoffid =
      !allConstraintTypes.loading &&
      findConstraint(allconstrinttypes.constraintDefinitions, "MAXDAYSOFF");

    if (!maxdaysoff) {
      CreateHourlyRequirementsConstraints(
        createContraints,
        element.maxDaysOff,
        maxdaysoffid.id,
        office.id,
        element.id
      );
    } else {
      UpdateHospitalConstraint(
        updateContraints,
        element.maxDaysOff,
        maxdaysoff.id,
        maxdaysoffid.id
      );
    }

    let maxdays =
      !allConstraintTypes.loading &&
      findemployeeTypeOfficeConstraint(
        office.constraintSet,
        "MAXDAYS",
        element.employeeType
      );
    let maxdaysid =
      !allConstraintTypes.loading &&
      findConstraint(allconstrinttypes.constraintDefinitions, "MAXDAYS");

    if (!maxdays) {
      CreateHourlyRequirementsConstraints(
        createContraints,
        element.maxConsecutiveDays,
        maxdaysid.id,
        office.id,
        element.id
      );
    } else {
      UpdateHospitalConstraint(
        updateContraints,
        element.maxConsecutiveDays,
        maxdays.id,
        maxdaysid.id
      );
    }

    let maxHoursPerDay =
      !allConstraintTypes.loading &&
      findemployeeTypeOfficeConstraint(
        office.constraintSet,
        "MAXDAILYHR",
        element.employeeType
      );
    let maxhoursid =
      !allConstraintTypes.loading &&
      findConstraint(allconstrinttypes.constraintDefinitions, "MAXDAILYHR");

    if (!maxHoursPerDay) {
      CreateHourlyRequirementsConstraints(
        createContraints,
        element.maxHoursPerDay,
        maxhoursid.id,
        office.id,
        element.id
      );
    } else {
      UpdateHospitalConstraint(
        updateContraints,
        element.maxHoursPerDay,
        maxHoursPerDay.id,
        maxhoursid.id
      );
    }
  };

  const handleUpdateOffice = (values) => {
    saveskillsetforDaysofweek(
      values.staffRequirements,
      staffTable,
      alljobtypes,
      office,
      updateContraints,
      createSkillConstraints,
      allconstrinttypes,
      getshifts.shifts,
      deleteConstraint
    );
    saveskillsetforDaysofweek(
      values.trainingRequirements,
      trainingTable,
      trng,
      office,
      updateContraints,
      createSkillConstraints,
      allconstrinttypes,
      getshifts.shifts,
      deleteConstraint
    );
    values.employeeWorkingMaximums.forEach((element) => {
      if (element.id === "") {
        Setvals(element);
        createEmployeeType({
          variables: {
            input: {
              name: element.employeeType,
            },
          },
        });
      } else {
        handleEmployeeTypeConstraints(element);
      }
    });

    refetch();
    setsnackBarMessage("Department successfully updated");
    setsnackbaropen(true);
  };

  if (loading) {
    return <CircularProgress />;
  } else {
    const modelType = office.modeltype;
    return (
      <main variant="body">
        <Container style={{ margin: 0, padding: 0 }}>
          <Snackbar
            open={snackbaropen}
            autoHideDuration={2000}
            onClose={() => {
              setsnackbaropen(false);
              history.push("/Admin");
            }}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <MuiAlert
              onClose={() => {
                setsnackbaropen(false);
                setsnackBarMessage("");
              }}
              severity="success"
            >
              {snackBarMessage}
            </MuiAlert>
          </Snackbar>
          <Snackbar
            open={errorsnackbaropen}
            autoHideDuration={6000}
            onClose={() => {
              seterrorsnackbaropen(false);
            }}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
          >
            <MuiAlert
              onClose={() => seterrorsnackbaropen(false)}
              severity="error"
            >
              {snackBarMessage}
            </MuiAlert>
          </Snackbar>
          <Grid container direction="row" spacing={4}>
            <Grid item xs={12}>
              <Formik
                enableReinitialize
                initialValues={{
                  id: office.id,
                  departmentName: office.name,
                  staffRequirements:
                    !shiftDefinitions.loading &&
                    getskillsetforDaysofweek(
                      staffTable,
                      getshifts.shifts,
                      alljobtypes
                    ),
                  trainingRequirements:
                    !shiftDefinitions.loading &&
                    getskillsetforDaysofweek(
                      trainingTable,
                      getshifts.shifts,
                      trng
                    ),
                  employeeWorkingMaximums:
                    !allEmployeeTypes.loading &&
                    getEmployeeTypeValues(
                      allEmployeeTypes.data.employeeTypes,
                      office.constraintSet
                    ),
                }}
                validationSchema={validationSchema}
                onSubmit={(values) => {
                  handleUpdateOffice(values);
                }}
              >
                {({
                  handleSubmit,
                  handleChange,
                  values,
                  errors,
                  setFieldValue,
                }) => (
                  <form onSubmit={handleSubmit}>
                    <PromptIfDirty />
                    <Grid container direction="row" spacing={4}>
                      <Grid
                        item
                        container
                        xs={12}
                        className={classes.headerSpacing}
                      >
                        {office ? (
                          <Grid item xs={6} style={{ padding: 0 }}>
                            <Typography variant="h3">
                              Edit Department
                            </Typography>
                          </Grid>
                        ) : (
                          <Grid item xs={6} style={{ padding: 0 }}>
                            <Typography variant="h3">Add Department</Typography>
                          </Grid>
                        )}
                        <Grid
                          item
                          container
                          justify="flex-end"
                          xs={6}
                          spacing={2}
                        >
                          <Grid item>
                            <Button
                              color="primary"
                              variant="outlined"
                              onClick={() => history.push("/Admin")}
                              className={classes.button}
                            >
                              Cancel
                            </Button>
                          </Grid>
                          <Grid item>
                            <Button
                              color="primary"
                              variant="contained"
                              type="submit"
                              className={classes.button}
                            >
                              Save
                            </Button>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid item xs={4}>
                        <Box mb={1}>
                          <Typography variant="h5">Department Name</Typography>
                        </Box>
                        <TextField
                          id="departmentName"
                          name="departmentName"
                          variant="outlined"
                          value={
                            values.departmentName ? values.departmentName : ""
                          }
                          className={classes.inputWidth}
                          disabled
                        />
                      </Grid>
                    <Grid item xs={12}>
                        <Divider className={classes.divider} />
                      </Grid>
                      <Grid item xs={10}>
                        <FieldArray name="employeeWorkingMaximums">
                          {({ push, remove }) => (
                            <Table>
                              <TableHead>
                                <TableRow>
                                  <StyledTableCell
                                    colSpan={6}
                                    style={{ textAlign: "left" }}
                                  >
                                    Employee Type Requirements
                                  </StyledTableCell>
                                </TableRow>
                                <TableRow>
                                  <StyledTableCell>
                                    Employee Type
                                  </StyledTableCell>
                                  <StyledTableCell>
                                    Required Weekly Hours
                                  </StyledTableCell>
                                  <StyledTableCell>
                                    Max Weekly Hours
                                  </StyledTableCell>
                                  <StyledTableCell>
                                    Max Days Off
                                    <br />
                                    (without PTO)
                                  </StyledTableCell>
                                  <StyledTableCell>
                                    Max Consecutive Days
                                    <br />
                                    (that can be worked)
                                  </StyledTableCell>
                                  <StyledTableCell>
                                    Max Hours Per Day
                                  </StyledTableCell>
                                </TableRow>
                              </TableHead>
                              <TableBody>
                                {values.employeeWorkingMaximums &&
                                  values.employeeWorkingMaximums.map(
                                    (e, index) => (
                                      <TableRow key={index}>
                                        <StyledTableCell>
                                          {e.id ? (
                                            e.employeeType
                                          ) : (
                                            <TextField
                                              id={`employeeWorkingMaximums.${index}.employeeType`}
                                              name={`employeeWorkingMaximums.${index}.employeeType`}
                                              variant="outlined"
                                              className={classes.inputWidth}
                                              onChange={(e) =>
                                                setFieldValue(
                                                  `employeeWorkingMaximums.${index}.employeeType`,
                                                  e.target.value
                                                )
                                              }
                                            />
                                          )}
                                        </StyledTableCell>
                                        <StyledTableCell>
                                          <TextField
                                            id={`employeeWorkingMaximums.${index}.requiredWeeklyHrs`}
                                            name={`employeeWorkingMaximums.${index}.requiredWeeklyHrs`}
                                            variant="outlined"
                                            value={e.requiredWeeklyHrs}
                                            onChange={handleChange}
                                            className={classes.mediumtextbox}
                                          />
                                        </StyledTableCell>
                                        <StyledTableCell>
                                          <TextField
                                            id={`employeeWorkingMaximums.${index}.maxWeeklyHrs`}
                                            name={`employeeWorkingMaximums.${index}.maxWeeklyHrs`}
                                            variant="outlined"
                                            value={e.maxWeeklyHrs}
                                            onChange={handleChange}
                                            className={classes.mediumtextbox}
                                          />
                                        </StyledTableCell>
                                        <StyledTableCell>
                                          <TextField
                                            id={`employeeWorkingMaximums.${index}.maxDaysOff`}
                                            name={`employeeWorkingMaximums.${index}.maxDaysOff`}
                                            variant="outlined"
                                            value={e.maxDaysOff}
                                            onChange={handleChange}
                                            className={classes.mediumtextbox}
                                          />
                                        </StyledTableCell>
                                        <StyledTableCell>
                                          <TextField
                                            id={`employeeWorkingMaximums.${index}.maxConsecutiveDays`}
                                            name={`employeeWorkingMaximums.${index}.maxConsecutiveDays`}
                                            variant="outlined"
                                            value={e.maxConsecutiveDays}
                                            onChange={handleChange}
                                            className={classes.mediumtextbox}
                                          />
                                        </StyledTableCell>
                                        <StyledTableCell>
                                          <TextField
                                            id={`employeeWorkingMaximums.${index}.maxHoursPerDay`}
                                            name={`employeeWorkingMaximums.${index}.maxHoursPerDay`}
                                            variant="outlined"
                                            value={e.maxHoursPerDay}
                                            onChange={handleChange}
                                            className={classes.mediumtextbox}
                                          />
                                        </StyledTableCell>
                                        {/* <StyledTableCell>
                                          <IconButton
                                            color="secondary"
                                            onClick={() => {
                                              if (e.id) {
                                                setDialogMessage(
                                                  "Are you sure you want to delete this employee type and related values?"
                                                );
                                                setshowDialog(true);
                                                setDeleteEmpType(true);
                                                setIdtoDelete(e.id);
                                                remove(index);
                                              } else {
                                                remove(index);
                                              }
                                            }}
                                            style={{ padding: 0 }}
                                          >
                                            <DeleteIcon />
                                          </IconButton>
                                        </StyledTableCell> */}
                                      </TableRow>
                                    )
                                  )}
                                <TableRow>
                                  <StyledTableCell
                                    style={{
                                      textAlign: "left",
                                      borderBottom: "none",
                                    }}
                                  >
                                    <Box ml={3}>
                                      <Button
                                        color="primary"
                                        startIcon={<AddIcon />}
                                        onClick={() =>
                                          push({
                                            id: "",
                                            employeeType: "",
                                            requiredWeeklyHrs: "40",
                                            maxWeeklyHrs: "40",
                                            maxDaysOff: "2",
                                            maxConsecutiveDays: "5",
                                            maxHoursPerDay: "12",
                                          })
                                        }
                                      >
                                        Add Employee Type Requirements
                                      </Button>
                                    </Box>
                                  </StyledTableCell>
                                </TableRow>
                              </TableBody>
                            </Table>
                          )}
                        </FieldArray>
                      </Grid>
                      <Grid item xs={12}>
                        <SkillCoverReq
                          name="staffRequirements"
                          getDailyShifts={getshifts}
                          modelType={modelType}
                          values={values.staffRequirements}
                          setFieldValue={setFieldValue}
                          ErrorMessage={ErrorMessage}
                          handleChange={handleChange}
                          classes={classes}
                          alljobtypes={alljobtypes}
                          deleteConstraint={deleteConstraint}
                          constraintSet={office.constraintSet}
                          setshowDialog={setshowDialog}
                          setDialogMessage={setDialogMessage}
                          SetDuplicateRow={SetDuplicateRow}
                          setValuestoDelete={setValuestoDelete}
                          setotherform={setotherform}
                        />
                      </Grid>
                      <Grid item xs={12}>
                        <SkillCoverReq
                          name="trainingRequirements"
                          getDailyShifts={getshifts}
                          modelType={modelType}
                          values={values.trainingRequirements}
                          setFieldValue={setFieldValue}
                          ErrorMessage={ErrorMessage}
                          handleChange={handleChange}
                          classes={classes}
                          alljobtypes={trng}
                          deleteConstraint={deleteConstraint}
                          constraintSet={office.constraintSet}
                          setshowDialog={setshowDialog}
                          setDialogMessage={setDialogMessage}
                          SetDuplicateRow={SetDuplicateRow}
                          setValuestoDelete={setValuestoDelete}
                          setotherform={setotherform}
                        />
                      </Grid>
                    </Grid>
                    <Grid item container justify="flex-end" xs={12} spacing={2}>
                      <Grid item>
                        <Button
                          color="primary"
                          variant="outlined"
                          onClick={() => history.push("/Admin")}
                          className={classes.button}
                        >
                          Cancel
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          color="primary"
                          variant="contained"
                          type="submit"
                          className={classes.button}
                        >
                          Save
                        </Button>
                      </Grid>
                    </Grid>
                  </form>
                )}
              </Formik>
            </Grid>
          </Grid>
          <Dialog open={showDialog} fullWidth maxWidth="xs">
            <DialogContent
              style={{
                padding: 20,
                overflowX: "hidden",
                textAlign: "center",
              }}
            >
            <Typography>{dialogMessage}</Typography>
              <br />
              <br />
              {duplicateRow ? (
                <>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      setshowDialog(false);
                      SetDuplicateRow(false);
                    }}
                  >
                    OK
                  </Button>
                </>
              ) : (
                <>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() =>
                      deleteempType
                        ? deleteEmployeeType({
                            variables: { id: parseInt(idtoDelete) },
                          })
                        : handleDeleteSkillSetConstraints(
                            valuestodelete,
                            office.constraintSet,
                            deleteConstraint,
                            getshifts,
                            setshowDialog
                          )
                    }
                  >
                    Yes
                  </Button>
                  <Button
                    style={{ marginLeft: "20px" }}
                    onClick={() => setshowDialog(false)}
                    color="primary"
                    variant="outlined"
                  >
                    No
                  </Button>
                </>
              )}
            </DialogContent>
          </Dialog>
        </Container>
      </main>
    );
  }
}

export default UpdateOfficeForm;
