import React, { useEffect, useState } from "react";
import AuthenticatedApp from "./AuthenticatedApp";
import UnauthenticatedApp from "./UnauthenticatedApp";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { 
  isLoggedInVar, appsettingsVar 
} from "../../cache";
import { useQuery, useReactiveVar,useMutation,useApolloClient } from "@apollo/client";
import { PUBLICLOCATION, VERIFY_TOKEN } from "../../api/gqlQueries";
import defaultTheme from "../../themes/default-theme.json";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import LogRocket from 'logrocket';

function App() {
  const environment = process.env.NODE_ENV;
  const baseURL = window.location.origin;
  
  let appId;
  if (environment === "development") {
    appId = null;
  } else if (baseURL.includes("amplify")) {
    appId = 'ix1pxb/test-dev-dod-ui';
  } else {
    appId = 'ix1pxb/demo-dod-ui';
  }
  console.log(appId);
  
  appId && LogRocket.init(appId);

  const client = useApolloClient();
  const isLoggedIn = useReactiveVar(isLoggedInVar);
  const publicLocationSettings = useQuery(PUBLICLOCATION, {
    onError(error){
      console.error(error);
    }
  });
  const appvar = useReactiveVar(appsettingsVar);
  appvar.color =
    !publicLocationSettings.loading &&
    publicLocationSettings.data != null &&
    publicLocationSettings.data.locationPublicSettings[0].hexcolor;
  const [clr, setclr] = useState(appvar.color);

  const myTheme = {
    ...defaultTheme,
    palette: {
      ...defaultTheme.palette,
      primary: { main: clr ? clr : defaultTheme.palette.primary.main },
    },
  };
  const theme = createMuiTheme(myTheme);

  const [verifyToken] = useMutation(VERIFY_TOKEN, {
    onCompleted(data) {
      console.log(data);
    },
    onError(err){
      console.error(err);
      isLoggedInVar(false);
      sessionStorage.removeItem("jwt");
      sessionStorage.clear("jwt")
      client.cache.reset();
    }
  });

  useEffect(()=>{
    if (sessionStorage.getItem("jwt")) {
      verifyToken({
        variables: {
          token: sessionStorage.getItem("jwt"),
        },
      });
    }
  },[])

  useEffect(() => {
    setclr(appvar.color);
  }, [appvar.color]);

  return (
    <BrowserRouter>
      <Route>
        <MuiThemeProvider theme={theme}>
          <Switch>
            {isLoggedIn ? <AuthenticatedApp /> : <UnauthenticatedApp />}
          </Switch>
        </MuiThemeProvider>
      </Route>
    </BrowserRouter>
  );
}

export default App;
