import React, { useEffect, useState } from "react";
import clsx from "clsx";
import MenuIcon from "@material-ui/icons/Menu";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Avatar from "@material-ui/core/Avatar";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import DashBoardSideNav from "../navigation/DashBoardSideNav";
import Preferences from "../preferences/Preferences";
import Landing from "../calendar/Landing";
import ManageUsers from "../mangUsers/MangTeam";
import ManageOffice from "../admin/ManageOffice";
import Profile from "../profile/Profile";
import Notifications from "../notifications/Notifications";
import FirstTimeUser from "../userAuthentication/firstTimeUser";
import OfficeSetupGuide from "../admin/OfficeSetupGuide";
import ImportData from "../importData/ImportData";
import LocationSettings from "../admin/LocationSettings";
import RolesandPermissions from "../admin/RolesandPermissions";
import AnalyticsOverview from "../Analytics/AnalyticsOveview";
import PreferenceDashboard from "../Analytics/PreferenceDashboard";
import SkillSetDashboard from "../Analytics/SkillSetDashboard";
import TimeOffDashboard from "../Analytics/TimeOffAnalytics";
// import UpcomingShift from '../upcomingShift/UpcomingShift';
import CreateSchedule from "../createSchedule/CreateSchedule";
import AllProcedures from "../admin/AllProcedures";
import Requests from "../requests/Requests";
import { Switch, Route, Link, useHistory } from "react-router-dom";
import {
  Container,
  Box,
  List,
  CssBaseline,
  makeStyles,
  Drawer,
  AppBar,
  Toolbar,
  Typography,
  Divider,
  Button,
  IconButton,
  Badge,
  Popover,
  ListItem,
} from "@material-ui/core";
import Roles from "../../Roles/roles";
import NotificationsDropdown from "../notifications/NotificationsDropdown";
import AppSettings from "../admin/AppSettings";
import Copyright from "./Copyright";
import {
  filterListVar,
  isLoggedInVar,
  userVar,
  initialScheduleStartDateVar,
  newScheduleStartDateVar,
  selectedDateVar,
  appsettingsVar,
} from "../../cache";
import { useQuery, useApolloClient, useMutation } from "@apollo/client";
import {
  GET_NOTIFICATION_COUNT,
  GET_USER_NAME,
  ABILITIES,
  PUBLICLOCATION,
  REFRESH_TOKEN
} from "../../api/gqlQueries";
import Ability from "../../Roles/defineAbility";
import axios from "axios";
import { useIdleTimer } from 'react-idle-timer';
import { mapHiredShifts } from "../../helpers/hiredShifts";
import { fromUnixTime, differenceInSeconds } from 'date-fns';
import LogRocket from 'logrocket';

const drawerWidth = 270;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  mainContainer: {
    marginLeft: "15px",
    marginBottom: "50px",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  dividerColor: {
    border: "0.5px solid#ffffff",
    height: "50px",
    marginRight: "20px",
    marginLeft: "20px",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    position: "fixed",
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  menuButton: {
    marginRight: 10,
  },
  title: {
    flexGrow: 1,
  },
  content: {
    flexGrow: 1,
    // height: '250vh',
    overflow: "none",
  },
  drawerPaper: {
    position: "sticky",
    paddingTop: 50,
    flexGrow: 1,
    height: "200vh",
    whiteSpace: "nowrap",
    overflow: "none",
    width: drawerWidth,
    background: "#EDE9E0",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    position: "sticky",
    paddingTop: 50,
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
}));

function AuthenticatedApp() {
  const client = useApolloClient();
  const classes = useStyles();
  const history = useHistory();

  const locationSettings = useQuery(PUBLICLOCATION, {
    onError(error){
      console.error(error);
    }
  });
  const [logourl, setlogourl] = useState("");
  const [logoErr, setLogoErr] = useState(false);

  const currentLogo =
    !locationSettings.loading &&
    locationSettings.data &&
    locationSettings.data.locationPublicSettings[0].logo;
  const appSettings = appsettingsVar();
  appSettings.logo = currentLogo;
  const baseURL = window.location.origin;
  const environment = process.env.NODE_ENV;

  const [refreshToken] = useMutation(REFRESH_TOKEN, {
    onCompleted(data) {
      console.log(data);
      const expires = fromUnixTime(data.refreshToken.payload.exp);
      const now = new Date();
      const distanceInSeconds = differenceInSeconds(expires, now) - 10;

      sessionStorage.setItem("jwt", data.refreshToken.token);
      setTimeout(refresh, 1000 * distanceInSeconds);
    },
    onError(err){
      console.error(err);
      logout();
    }
  });
  
  function refresh() {
    if (sessionStorage.getItem("jwt")) {
      refreshToken({variables: {
        token: sessionStorage.getItem("jwt")
      }});
    }
  };

  const handleLogout = () => {
    isLoggedInVar(false);
    userVar({});
    filterListVar({});
    selectedDateVar(new Date());
    initialScheduleStartDateVar(null);
    newScheduleStartDateVar(false);
    sessionStorage.removeItem("jwt");
    sessionStorage.clear("jwt");
    client.cache.reset();
  };

  const logout = () => {
    handleLogout();
    history.push('/');
  };

  useIdleTimer({
    timeout: 1000 * 60 * 30,
    onIdle: logout,
    stopOnIdle: true
  });

  useEffect(() => {
    refresh();
  }, []);

  useEffect(() => {
    const currentLogo =
      !locationSettings.loading &&
      locationSettings.data &&
      locationSettings.data.locationPublicSettings[0].logo;
    const baseURL = window.location.origin;
    const environment = process.env.NODE_ENV;
    appSettings.logo = currentLogo;
    if (environment === "development") {
      setlogourl("https://backendtest.balancescheduler.com" + appSettings.logo);
    } else if (baseURL.includes("amplify")) {
      setlogourl(baseURL+appSettings.logo);
    } else {
      setlogourl(baseURL+appSettings.logo);
    }
  }, [!locationSettings.loading]);

  useEffect(() => {
    const currentLogo =
      !locationSettings.loading &&
      locationSettings.data &&
      locationSettings.data.locationPublicSettings[0].logo;
    const baseURL = window.location.origin;
    const environment = process.env.NODE_ENV;
    appSettings.logo = currentLogo;
    if (environment === "development") {
      setlogourl("https://backendtest.balancescheduler.com" + appSettings.logo);
    } else if (baseURL.includes("amplify")) {
      setlogourl(appSettings.logo);
    } else {
      setlogourl(appSettings.logo);
    }
  }, [appSettings.logo]);
  console.log(logourl)

  const { loading, error, data } = useQuery(GET_USER_NAME, {
    onCompleted() {
      const user = data.me;
      LogRocket.identify(user.id, {
        email: user.email
      });

      const primaryAssignment = user.assignmentSet.find(
        (assignment) => assignment.primary === true
      );
      const userRole = primaryAssignment
        ? primaryAssignment.role.name.toUpperCase()
        : user.assignmentSet[0].role.name.toUpperCase();

      if (userRole === Roles.MANAGER) {
        const filterList = {
          allFilters: ["All Shifts", "All Missions"],
          shiftsFilters: ["All Shifts"],
          missionFilters: ["All Missions"],
          requestsFilters: [],
        };
        filterListVar(filterList);
      } else {
        const filterList = {
          allFilters: [
            "Personal",
            "All Missions",
            "All Requests",
            "Soft Requests",
            "Pending Time Off",
            "Approved Time Off",
          ],
          shiftsFilters: ["Personal"],
          missionFilters: ["All Missions"],
          requestsFilters: [
            "Soft Requests",
            "Pending Time Off",
            "Approved Time Off",
            "All Requests",
          ],
        };
        filterListVar(filterList);
      }
      const userVariable = userVar();
      userVar({
        ...userVariable,
        alwaysOpenGuide: user.alwaysOpenGuide,
        showPrefsExample: user.showPrefsExample,
      });
    }
  });

  const userpermissions = useQuery(ABILITIES, {
    onCompleted(data) {
      SetuserAbilities(data.abilities);
    },
    onError(error){
      console.error(error);
    }
  });

  const [userAbilities, SetuserAbilities] = useState({});

  const { data: notificationsData, loading: notificationsLoading } = useQuery(
    GET_NOTIFICATION_COUNT,
    {
      pollInterval: 15000,
      onError(error){
        console.error(error);
      }
    },
  );

  const [open, setOpen] = useState(true);
  const handleDrawer = () => {
    setOpen(!open);
  };

  const [profileAnchorEl, setProfileAnchorEl] = useState(null);
  const [notificationsAnchorEl, setNotificationsAnchorEl] = useState(null);

  if (
    loading ||
    locationSettings.loading ||
    notificationsLoading ||
    userpermissions.loading
  ) {
    return <Typography>Loading...</Typography>;
  } else if (error) {
    console.log(error);
    return <Typography>Something went wrong. Please try again.</Typography>;
  } else {
    const notificationCount = notificationsData
      ? notificationsData.me.notificationSet.filter(
          (notification) => notification.read !== true
        ).length
      : 0;

    const user = data.me;
    const primaryAssignment = user.assignmentSet.find(
      (assignment) => assignment.primary === true
    );

    const schedulable = primaryAssignment
      ? primaryAssignment.schedulable
      : user.assignmentSet[0].schedulable;
    const primaryUnit = primaryAssignment
      ? primaryAssignment.office
      : user.assignmentSet[0].office;
    const userRole = primaryAssignment
      ? primaryAssignment.role.name.toUpperCase()
      : user.assignmentSet[0].role.name.toUpperCase();
    const shiftType = primaryAssignment
      ? mapHiredShifts(
          primaryAssignment.hiredshiftSet,
          primaryAssignment.rotationLength
        )
      : mapHiredShifts(
          user.assignmentSet[0].hiredshiftSet,
          user.assignmentSet[0].rotationLength
        );

    const userName = user.firstName + " " + user.lastName;

    const userObject = {
      office: primaryUnit,
      role: userRole,
      name: userName,
      id: user.id,
      shiftType: shiftType,
      isEmployee: userRole === Roles.EMPLOYEE,
      isAdmin: userRole === Roles.ADMIN,
      isManager: userRole === Roles.MANAGER,
      isScheduler: userRole === Roles.SCHEDULER,
      isSchedulable: schedulable,
      userabilities: userAbilities,
      // alwaysOpenGuide: user.alwaysOpenGuide,
      // showPrefsExample: user.showPrefsExample
    };

    const userVariable = userVar();
    userVar({ ...userVariable, ...userObject });

    const ability = Ability(userObject);

    axios({
      method: "get",
      url: logourl,
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(function (response) {
        setLogoErr(false);
      })
      .catch(function (error) {
        setLogoErr(true);
        console.error(error);
      });

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="absolute" className={clsx(classes.appBar)}>
          <Toolbar className={classes.toolbar}>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawer}
              className={clsx(classes.menuButton)}
            >
              <MenuIcon />
            </IconButton>
            <Box component={Link} to="/">
              <img
                // src={
                //   appSettings.logo && !logoErr
                //     ? logourl
                //     : "Duality-Systems_white.png"
                // }
                src="Duality-Systems_white.png"
                height={40}
                width={200}
                alt="Duality Logo"
              />
            </Box>
            <Divider className={classes.dividerColor} />
            <Typography
              component="h1"
              variant="h4"
              color="inherit"
              noWrap
              className={classes.title}
            >
              <i
                style={{
                  color: "#d3d3d3",
                  fontFamily: "Tahoma",
                  fontWeight: "650",
                }}
              >
                BALANCE{" "}
              </i>
              <i
                style={{
                  color: "#ffffff",
                  fontFamily: "Tahoma",
                  fontWeight: "650",
                }}
              >
                SCHEDULER
              </i>
            </Typography>

            <IconButton
              color="inherit"
              onClick={(e) => setNotificationsAnchorEl(e.currentTarget)}
            >
              <Badge badgeContent={notificationCount} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <Popover
              anchorEl={notificationsAnchorEl}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "right",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              open={Boolean(notificationsAnchorEl)}
              onClose={() => setNotificationsAnchorEl(null)}
            >
              <Container style={{ padding: 10, overflow: "hidden" }}>
                <NotificationsDropdown
                  closePopover={() => setNotificationsAnchorEl(null)}
                  userId={data.me.id}
                />
              </Container>
            </Popover>

            <Button onClick={(e) => setProfileAnchorEl(e.currentTarget)}>
              {data.me.firstName && data.me.lastName ? (
                <Avatar>{data.me.firstName[0] + data.me.lastName[0]}</Avatar>
              ) : (
                <Avatar>
                  <AccountCircleIcon style={{ fontSize: 40 }} />
                </Avatar>
              )}
            </Button>
            <Popover
              anchorEl={profileAnchorEl}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "right",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              open={Boolean(profileAnchorEl)}
              onClose={() => setProfileAnchorEl(null)}
            >
              <List>
                <ListItem
                  component={Link}
                  to="/Profile"
                  onClick={() => setProfileAnchorEl(null)}
                >
                  My Profile
                </ListItem>
                <ListItem component={Link} to="/UpdatePassword">
                  Update Password
                </ListItem>
                <ListItem component={Link} to="/" onClick={handleLogout}>
                  Logout
                </ListItem>
              </List>
            </Popover>
          </Toolbar>
        </AppBar>

        <Drawer
          variant="permanent"
          classes={{
            paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
          }}
          open={open}
        >
          <Divider />
          <List>
            <DashBoardSideNav />
          </List>
          <Divider />
          <Box
            style={{
              position: "fixed",
              left: 10,
              bottom: 20,
              display: !open && "none",
            }}
          >
            <img
              src="Duality-Systems_gray.png"
              height="40"
              width="200"
              alt="Duality Logo"
            />
            <Copyright />
          </Box>
        </Drawer>
        <div className={classes.content}>
          <div className={classes.appBarSpacer} />
          <Container className={classes.mainContainer}>
            <Box>
              <Switch>
                <>
                  {!data.me.hasChangedDefaultPassword ? (
                    <Route
                      path="/"
                      exact
                      component={() => <FirstTimeUser user={data.me} />}
                    />
                  ) : (
                    <>
                      {ability.can("manage", "Profile") && (
                        <Route path="/Profile" component={Profile} />
                      )}
                      {
                        <Route
                          path="/UpdatePassword"
                          exact
                          component={() => <FirstTimeUser user={data.me} />}
                        />
                      }
                      {ability.can("manage", "Notifications") && (
                        <Route
                          path="/Notifications"
                          component={Notifications}
                        />
                      )}
                      {ability.can("manage", "Calendar") && (
                        <>
                          <Route path="/" exact component={Landing} />
                          <Route path="/Landing" exact component={Landing} />
                          <Route
                            path="/Landing/:date/draft"
                            exact
                            component={Landing}
                          />
                        </>
                      )}
                      {ability.can("manage", "Preferences") && schedulable && (
                        <Route path="/Preferences" component={Preferences} />
                      )}
                      {ability.can("manage", "Schedule") && (
                        <Route
                          path="/CreateSchedule"
                          component={CreateSchedule}
                        />
                      )}
                      {ability.can("manage", "Requests") && (
                        <Route
                          path="/Landing/:request"
                          exact
                          component={Landing}
                        />
                        /* <Route
                          path="/UpcomingShift"
                          component={UpcomingShift}
                        />{" "}
                        */
                      )}

                      {ability.can("manage", "Users") && (
                        <>
                          <Route
                            path="/ManageTeam"
                            component={() => (
                              <ManageUsers open={true} user={user} />
                            )}
                          />
                        </>
                      )}
                      {ability.can("manage", "Users") && (
                        <>
                          <Route
                            path="/ScheduleQualityAnalytics"
                            component={AnalyticsOverview}
                          />
                          <Route
                            path="/PreferenceAnalytics"
                            component={PreferenceDashboard}
                          />
                          <Route
                            path="/SkillSetAnalytics"
                            component={SkillSetDashboard}
                          />
                          <Route 
                            path="/ImportData"
                            component={ImportData}
                          />
                          <Route
                            path="/TimeOffAnalytics"
                            component={TimeOffDashboard}
                          />
                        </>
                      )}
                      {ability.can("manage", "AllOffices") && (
                        <>
                          <Route
                            path="/Admin"
                            component={() => (
                              <ManageOffice open={false} office={null} />
                            )}
                          />
                          <Route
                            path="/SetupGuide"
                            component={OfficeSetupGuide}
                          />
                           <Route
                            path="/MissionDetails"
                            component={AllProcedures}
                          />
                          {/* <Route
                            path="/ImportEmployees"
                            component={ImportEmployees}
                          />

                          <Route
                            path="/ImportProcedures"
                            component={ImportProcedures}
                          />
                          <Route
                            path="/ImportAvailability"
                            component={ImportAvailability}
                          />
                          <Route
                            path="/ImportTrainings"
                            component={ImportTraining}
                          /> */}
                        </>
                      )}
                      {ability.can("manage", "LocationSettings") && (
                        <Route
                          path="/LocationSettings"
                          component={LocationSettings}
                        />
                      )}
                      {ability.can("manage", "AppSettings") && (
                        <Route path="/AppSettings" component={AppSettings} />
                      )}
                      {ability.can("manage", "My Requests") && (
                        <Route path="/MyRequests" component={Requests} />
                      )}
                      {ability.can("manage", "RolesandPermissions") && (
                        <Route
                          path="/RolesandPermissions"
                          component={RolesandPermissions}
                        />
                      )}
                    </>
                  )}
                </>
              </Switch>
            </Box>
          </Container>
        </div>
      </div>
    );
  }
}

export default AuthenticatedApp;
