import React, { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  IconButton,
  Box,
} from "@material-ui/core";
import CalendarIcon from "@material-ui/icons/Event";
import ScheduleIcon from "@material-ui/icons/Schedule";
import EditIcon from "@material-ui/icons/Edit";
import BusinessIcon from "@material-ui/icons/Business";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import SettingsIcon from "@material-ui/icons/Settings";
import PersonIcon from "@material-ui/icons/Person";
import GroupIcon from "@material-ui/icons/Group";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import AssignmentIcon from "@material-ui/icons/Assignment";
import DomainIcon from "@material-ui/icons/Domain";
import QueueIcon from "@material-ui/icons/Queue";
// import ListAltIcon from '@material-ui/icons/ListAlt';
import { userVar } from "../../cache";
import Ability from "../../Roles/defineAbility";
import WorkIcon from "@material-ui/icons/Work";
import LocalLibraryIcon from "@material-ui/icons/LocalLibrary";
import EventAvailableIcon from "@material-ui/icons/EventAvailable";
import AssessmentIcon from "@material-ui/icons/Assessment";
import DashboardIcon from "@material-ui/icons/Dashboard";
import SystemUpdateAltIcon from "@material-ui/icons/SystemUpdateAlt";

function DashBoardSideNav() {
  // const user = useSelector(state => state.user.userData);
  // const userRole = user ? user.role : '';
  const location = useLocation();

  const mapPathnames = {
    "/": 0,
    "/Landing": 0,
    "/Preferences": 1,
    "/CreateSchedule": 2,
    "/ManageTeam": 3,
    "/Admin": 4,
    "/LocationSettings": 6,
    "/Settings": 7,
    "/MyRequests": 8,
  };

  const defaultIndex = mapPathnames[location.pathname]
    ? mapPathnames[location.pathname]
    : 0;

  const [selectedIndex, setSelectedIndex] = React.useState(defaultIndex);
  const [managerShowmore, setManageTeamShowMore] = React.useState(false);
  const [departmentShowmore, setdepartmentShowmore] = React.useState(false);
  const [analyticsShowmore, setanalyticstShowmore] = React.useState(false);

  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
  };

  const user = userVar();
  const ability = Ability(user);

  return (
    <List>
      {ability.can("manage", "Calendar") &&
        ability.can("manage", "Preferences") && (
          <>
            <ListItem
              button
              component={Link}
              to="/"
              selected={selectedIndex === 0}
              onClick={(event) => handleListItemClick(event, 0)}
            >
              <ListItemIcon>
                <CalendarIcon />
              </ListItemIcon>
              <ListItemText>Schedule</ListItemText>
            </ListItem>
            {user.isSchedulable && (
              <ListItem
                button
                component={Link}
                to="/Preferences"
                selected={selectedIndex === 1}
                onClick={(event) => handleListItemClick(event, 1)}
              >
                <ListItemIcon>
                  <EditIcon />
                </ListItemIcon>
                <ListItemText>Preferences</ListItemText>
              </ListItem>
            )}
          </>
        )}
      {ability.can("manage", "My Requests") && (
        <>
          <ListItem
            button
            component={Link}
            to="/MyRequests"
            selected={selectedIndex === 9}
            onClick={(event) => handleListItemClick(event, 9)}
          >
            <ListItemIcon>
              <QueueIcon />
            </ListItemIcon>
            <ListItemText>My Requests</ListItemText>
          </ListItem>
        </>
      )}
      {(ability.can("manage", "Users") ||
        ability.can("manage", "Schedule")) && (
        <ListItem button>
          <ListItemIcon>
            <PersonIcon />
          </ListItemIcon>
          <ListItemText>Team</ListItemText>
          <ListItemIcon>
            <IconButton onClick={() => setManageTeamShowMore(!managerShowmore)}>
              {!managerShowmore ? <ExpandMoreIcon /> : <ExpandLessIcon />}
            </IconButton>
          </ListItemIcon>
        </ListItem>
      )}
      {managerShowmore && (
        <Box ml={3}>
          {ability.can("manage", "Schedule") && (
            <>
              <ListItem
                button
                component={Link}
                to="/CreateSchedule"
                selected={selectedIndex === 2}
                onClick={(event) => handleListItemClick(event, 2)}
              >
                <ListItemIcon>
                  <ScheduleIcon />
                </ListItemIcon>
                <ListItemText>Create Schedule</ListItemText>
              </ListItem>
            </>
          )}
          {ability.can("manage", "Users") && (
            <>
              <ListItem
                button
                component={Link}
                to="/ManageTeam"
                selected={selectedIndex === 3}
                onClick={(event) => handleListItemClick(event, 3)}
              >
                <ListItemIcon>
                  <PersonIcon />
                </ListItemIcon>
                <ListItemText>Manage Team</ListItemText>
              </ListItem>
              <ListItem
                button
                component={Link}
                to="/ImportData"
                selected={selectedIndex === 4}
                onClick={(event) => handleListItemClick(event, 4)}
              >
                <ListItemIcon>
                  <SystemUpdateAltIcon />
                </ListItemIcon>
                <ListItemText>Import Data</ListItemText>
              </ListItem>
            </>
          )}
        </Box>
      )}
      {(ability.can("manage", "AllOffices") ||
        ability.can("manage", "LocationSettings") ||
        ability.can("manage", "AppSettings") ||
        ability.can("manage", "RolesandPermissions")) && (
        <ListItem button>
          <ListItemIcon>
            <BusinessIcon />
          </ListItemIcon>
          <ListItemText>Department</ListItemText>
          <ListItemIcon>
            <IconButton
              onClick={() => setdepartmentShowmore(!departmentShowmore)}
            >
              {!departmentShowmore ? <ExpandMoreIcon /> : <ExpandLessIcon />}
            </IconButton>
          </ListItemIcon>
        </ListItem>
      )}

      {departmentShowmore && (
        <Box ml={3}>
          {ability.can("manage", "AllOffices") && (
            <>
              <ListItem
                button
                component={Link}
                to="/Admin"
                selected={selectedIndex === 5}
                onClick={(event) => handleListItemClick(event, 5)}
              >
                <ListItemIcon>
                  <GroupIcon />
                </ListItemIcon>
                <ListItemText>Departments</ListItemText>
              </ListItem>
              <ListItem
                button
                component={Link}
                to="/MissionDetails"
                selected={selectedIndex === 14}
                onClick={(event) => handleListItemClick(event, 14)}
              >
                <ListItemIcon>
                  <AssignmentIcon />
                </ListItemIcon>
                <ListItemText>Mission Details</ListItemText>
              </ListItem>
            </>
          )}
          {ability.can("manage", "LocationSettings") && (
            <>
              <ListItem
                button
                component={Link}
                to="/LocationSettings"
                selected={selectedIndex === 6}
                onClick={(event) => handleListItemClick(event, 6)}
              >
                <ListItemIcon>
                  <LocalLibraryIcon />
                </ListItemIcon>
                <ListItemText>Setup SkillSet </ListItemText>
              </ListItem>
            </>
          )}

          {ability.can("manage", "AppSettings") && (
            <>
              <ListItem
                button
                component={Link}
                to="/AppSettings"
                selected={selectedIndex === 7}
                onClick={(event) => handleListItemClick(event, 7)}
              >
                <ListItemIcon>
                  <SettingsIcon />
                </ListItemIcon>
                <ListItemText>Settings</ListItemText>
              </ListItem>
            </>
          )}

          {ability.can("manage", "RolesandPermissions") && (
            <>
              <ListItem
                button
                component={Link}
                to="/RolesandPermissions"
                selected={selectedIndex === 8}
                onClick={(event) => handleListItemClick(event, 8)}
              >
                <ListItemIcon>
                  <AccountCircleIcon />
                </ListItemIcon>
                <ListItemText>Roles and Permissions</ListItemText>
              </ListItem>
            </>
          )}
        </Box>
      )}

      {(ability.can("manage", "Users") ||
        ability.can("manage", "Schedule")) && (
        <ListItem button>
          <ListItemIcon>
            <AssessmentIcon />
          </ListItemIcon>
          <ListItemText>Analytics</ListItemText>
          <ListItemIcon>
            <IconButton
              onClick={() => setanalyticstShowmore(!analyticsShowmore)}
            >
              {!analyticsShowmore ? <ExpandMoreIcon /> : <ExpandLessIcon />}
            </IconButton>
          </ListItemIcon>
        </ListItem>
      )}

      {analyticsShowmore && (
        <Box ml={3}>
          {ability.can("manage", "AllOffices") && (
            <>
              <ListItem
                button
                component={Link}
                to="/ScheduleQualityAnalytics"
                selected={selectedIndex === 11}
                onClick={(event) => handleListItemClick(event, 11)}
              >
                <ListItemIcon>
                  <DashboardIcon />
                </ListItemIcon>
                <ListItemText>Schedule Quality</ListItemText>
              </ListItem>
              <ListItem
                button
                component={Link}
                to="/PreferenceAnalytics"
                selected={selectedIndex === 12}
                onClick={(event) => handleListItemClick(event, 12)}
              >
                <ListItemIcon>
                  <DashboardIcon />
                </ListItemIcon>
                <ListItemText>Preferences</ListItemText>
              </ListItem>
              <ListItem
                button
                component={Link}
                to="/SkillSetAnalytics"
                selected={selectedIndex === 13}
                onClick={(event) => handleListItemClick(event, 13)}
              >
                <ListItemIcon>
                  <DashboardIcon />
                </ListItemIcon>
                <ListItemText>Skill Set</ListItemText>
              </ListItem>
              <ListItem
                button
                component={Link}
                to="/TimeOffAnalytics"
                selected={selectedIndex === 14}
                onClick={(event) => handleListItemClick(event, 14)}
              >
                <ListItemIcon>
                  <DashboardIcon />
                </ListItemIcon>
                <ListItemText>Time Off</ListItemText>
              </ListItem>
            </>
          )}
        </Box>
      )}
    </List>
  );
}

export default DashBoardSideNav;
