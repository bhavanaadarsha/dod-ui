import React from "react";
import ReactDOM from "react-dom";
import App from "./features/general/App";
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  createHttpLink,
  gql,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import { createUploadLink } from "apollo-upload-client";

export const localDefs = gql`
  extend type Query {
    isLoggedIn: Boolean!
  }
`;

const environment = process.env.NODE_ENV;
const baseURL = window.location.origin;
let backend = "";
if (environment === "development") {
  // backend = "https://backenddemo.balancescheduler.com/graphql";
   backend = "https://backendtest.balancescheduler.com/graphql";
}
else if (baseURL.includes("amplify")) {
  // backend = "https://backenddemo.balancescheduler.com/graphql";
  backend = "/api/graphql/";
} else {
  backend = "/api/graphql/";
}
console.log("backend url", backend);

const httpLink = createHttpLink({
  uri: backend,
  // uri: 'https://mission-demo-backend.balancescheduler.com/graphql'
});

const uploadLink = createUploadLink({
  // uri: 'https://mission-demo-backend.balancescheduler.com/graphql'
  uri: backend,
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = sessionStorage.getItem("jwt");
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `JWT ${token}` : "",
    },
  };
});

const client = new ApolloClient({
  link: authLink.concat(uploadLink),
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById("root")
);
