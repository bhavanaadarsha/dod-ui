import {
    // gql,
    InMemoryCache,
    // Reference,
    makeVar,
} from '@apollo/client';

export const isLoggedInVar = makeVar(!!sessionStorage.getItem('jwt'));

export const filterListVar = makeVar({});

export const creatingScheduleVar = makeVar(false);

export const userVar = makeVar({});

export const selectedDateVar = makeVar(new Date());

export const initialScheduleStartDateVar = makeVar(null);

export const newScheduleStartDateVar = makeVar(false);

export const appsettingsVar = makeVar({ logo: null, color: '' });

export const EditDepartmentVar = makeVar({ isEmpty: false });

export const DepartmentJobTypeVar = makeVar({ isEmpty: true });

export const DepartmentTrainingVar = makeVar({ isEmpty: true });

export const SelectedOfficeVar = makeVar({ officeId: "" });

export const cache = new InMemoryCache({
    typePolicies: {
        Query: {
            fields: {
                isLoggedIn: {
                    read() {
                        return isLoggedInVar();
                    }
                }
            }
        }
    }
});