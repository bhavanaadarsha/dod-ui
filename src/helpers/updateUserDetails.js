export const updateSkillSet = (
  userCurrentSkill,
  values,
  addEmployeeSkills,
  id,
  findemployeeById,
  deletemployeeSkills,
  variety
) => {
  let names = [];
  if (userCurrentSkill != null && userCurrentSkill.length > 0) {
    userCurrentSkill.forEach((e) => names.push(e.name));
  }

  if (variety === "JOB_TYPE") {
    if (userCurrentSkill != null && userCurrentSkill.length > 0) {
      let x = findemployeeById.data.employees[0].employeeskillSet.find(
        (i) =>
          i.skill.name === userCurrentSkill[0].name &&
          i.skill.variety === userCurrentSkill[0].variety
      );
      if (x) {
        deletemployeeSkills({
          variables: {
            id: parseInt(x.id),
          },
        });
      }
    }
    addEmployeeSkills({
      variables: {
        input: {
          employee: id,
          skill: parseInt(values.id),
        },
      },
    });
  } else {
    const a = [];
    values.forEach((e) => a.push(e.name));
    if (
      userCurrentSkill != null &&
      userCurrentSkill.length > 0 &&
      values.length < userCurrentSkill.length
    ) {
      userCurrentSkill.forEach((e) => {
        if (!a.includes(e.name)) {
          let x = findemployeeById.data.employees[0].employeeskillSet.find(
            (i) => i.skill.variety === variety && i.skill.name === e.name
          );
          if (x) {
            deletemployeeSkills({
              variables: {
                id: parseInt(x.id),
              },
            });
          }
        }
      });
    } else if (
      userCurrentSkill != null &&
      userCurrentSkill.length > 0 &&
      values.length > userCurrentSkill.length
    ) {
      values.forEach((e) => {
        if (!names.includes(e.name)) {
          addEmployeeSkills({
            variables: {
              input: {
                employee: id,
                skill: parseInt(e.id),
              },
            },
          });
        }
      });
    } else {
      userCurrentSkill != null &&
        userCurrentSkill.length > 0 &&
        userCurrentSkill.forEach((e) => {
          if (!a.includes(e.name)) {
            let x = findemployeeById.data.employees[0].employeeskillSet.find(
              (i) => i.skill.variety === variety && i.skill.name === e.name
            );
            if (x) {
              deletemployeeSkills({
                variables: {
                  id: parseInt(x.id),
                },
              });
            }
          }
        });
      values.forEach((e) => {
        if (!names.includes(e.name)) {
          addEmployeeSkills({
            variables: {
              input: {
                employee: id,
                skill: parseInt(e.id),
              },
            },
          });
        }
      });
    }
  }
};

export const UpdateAssignment = (
  updateEmployeeAssignment,
  userid,
  employeetypeid,
  jobtitleid,
  officeid,
  startdate,
  schedulable,
  // shifttypeid,
  rotationLength
) => {
  updateEmployeeAssignment({
    variables: {
      id: parseInt(userid),
      input: {
        employeeType: parseInt(employeetypeid),
        role: parseInt(jobtitleid),
        office: parseInt(officeid),
        primary: true,
        startDate: startdate,
        schedulable: schedulable,
        rotationLength: rotationLength,
      },
    },
  });
};

export const formatPhoneNumber = (str) => {
  let cleaned = ("" + str).replace(/\D/g, "");
  let match;
  if (cleaned.length === 10) {
    match = cleaned.match(/^(\d{3})(\d{3})(\d{4})/);
  } else {
    return str;
  }

  if (match) {
    return "+1 " + " (" + match[1] + ") " + match[2] + " " + match[3];
  }
};

export const findskillset = (skills, value, officeid) => {
  let found;
  if (officeid != null) {
    found =
      skills.length > 0 &&
      skills.filter(
        (i) =>
          i.variety === value && i.office != null && i.office.id === officeid
      );
  } else {
    found = skills.length > 0 && skills.filter((i) => i.variety === value);
  }
  return found;
};

export const handleSort = (employees, sortDirection) => {
  const comparator =
    sortDirection === "asc"
      ? (a, b) => a.lastName?.localeCompare(b.lastName)
      : (a, b) => b.lastName?.localeCompare(a.lastName);
  let emp = employees.sort(comparator);
  return emp;
};

function isValid(str) {
  return !/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str);
}

export const addEmpDetails = (values, batchCreateEmployee) => {
  const inputData = values.map((element) => {
    const phone = !isValid(element.Phone)
      ? element.Phone.replace(/\D/g, "")
      : element.Phone;
    let name = element.Name.includes(",")
      ? element.Name.split(",")
      : element.Name.split(" ");
    let firstName = name[1];
    firstName = firstName.trim();
    let lastName = name[0];
    lastName = lastName.trim();
    return {
      firstName: firstName,
      lastName: lastName,
      email: element.Email,
      password: "Test@123",
      primaryPhone: phone,
      hasChangedDefaultPassword: false,
    };
  });
  batchCreateEmployee({
    variables: { input: inputData },
  });
};

export const handlefileValdation = (data, alloffices, jobtypes) => {
  let errMsg = "";
  let offices = [];
  let alljobTypes = [];
  !alloffices.loading &&
    alloffices.data.offices.map((e) => offices.push(e.name));
  jobtypes.map((e) => alljobTypes.push(e.name));
  for (let i = 0; i < data.length; i++) {
    let err = false;
    if (!err) {
      if (!offices.includes(data[i].Department)) {
        err = true;
        return (errMsg =
          "The department name does not match any departments of this location. Make sure you enter the right department name.");
      }
      if (!alljobTypes.includes(data[i].JobType)) {
        err = true;
        return (errMsg =
          "The Job Type name does not match any Job Types of this department. Make sure you enter the right Job Type name.");
      }
    }
  }
  return errMsg;
};
