export const selectEmployees = (events, shift, date, setError) => {
    const eventsByDate = events.filter(event => (
        new Date(event.start).toDateString() === new Date(date).toDateString()
    ));
    
    let employees;

    if(eventsByDate.length === 0){
        setError('Invalid date or shift selection');
    } else if (shift && shift === 'All Shifts'){
        const repetitiveEmployees = eventsByDate.map(event => event.participants).flat();
        const uniqueEmployees = new Set(repetitiveEmployees);
        employees = Array.from(uniqueEmployees);
        setError('');
    } else if (shift) {
        const matchingShift = eventsByDate.find(event => event.eventTitle === shift);
        if (matchingShift) {
            employees = matchingShift.participants;
            setError('');
        } else {
            setError('Invalid date or shift selection');
        }
    } else {
        const repetitiveEmployees = eventsByDate.map(event => event.participants).flat();
        const uniqueEmployees = new Set(repetitiveEmployees);
        employees = Array.from(uniqueEmployees);
        setError('');
    }
    return employees;
};

export const findEmployeesToSwitch = (currentDateParticipants, desiredDateParticipants) => {
    const currentDateIds = currentDateParticipants.filter(user => user).map(user => user.id);
    const desiredDateIds = desiredDateParticipants.filter(user => user).map(user => user.id);
    const eligibleIds = desiredDateIds.filter(id => !currentDateIds.includes(id));

    return desiredDateParticipants.filter(user => user && eligibleIds.includes(user.id));
};