import { format, add, isSameDay, isEqual } from 'date-fns';
import { myColors } from "./myColors";

//If an event has a tasks attribute, you must go through these tasks
//to find the appropriate shift ID. If it does not have tasks, the 
//eventId will be the shift ID

export const orderShifts = (shifts) => {
    const shiftNames = [];
    if (shifts.length > 0) {
        let formatted = [];
        shifts.forEach(shift => {
            const start = new Date(shift.start);
            const end = new Date(shift.end);
            const formattedStart = format(start, 'HH:mm');
            const formattedEnd = format(end, 'HH:mm');
            const title = `${formattedStart} - ${formattedEnd}`;
            
            if (!shiftNames.includes(title)) {
                shiftNames.push(title);
            }

            const shiftAssignments = shift.shiftassignmentSet.filter(shiftAssignment => (
                shiftAssignment.taskSet && shiftAssignment.taskSet.length === 0 &&
                shiftAssignment.isArchived === false
            ));

            const participants = shiftAssignments.map(shiftAssignment => {
                return shiftAssignment.employee;
            }); 
                
            const participantIds = participants.map(participant => participant.id);
            if (participants.length > 0) {
                const newEvent = {
                    eventId: shift.id,
                    start: start,
                    end: end,
                    participants: [...participants],
                    eventTitle: title,
                    employeeIds: [...participantIds],
                    missionId: 'GHOC',
                    calendars: ['employee', 'day']
                }
                formatted.push(newEvent);

                participants.forEach(participant => {
                    formatted.push({
                        eventId: shift.id,
                        start: start,
                        end: end,
                        participants: [participant],
                        employeeIds: [participant.id],
                        eventTitle: `${participant.firstName} ${participant.lastName}`,
                        missionId: 'GHOC',
                        calendars: ['mission'],
                        skills: participant.skills.map(e=>e.name)
                    });
                });
            }
            
            //create a ghoc event for each day with a list of all participants
            if (participants.length > 0) {
                const ghocExists = formatted.find(event => (
                    event.eventTitle === 'GHOC' &&
                    isSameDay(event.start, start)
                ));
                
                if (ghocExists) {
                    participants.forEach(participant => {
                        if (ghocExists.employeeIds.includes(participant.id)){
                            return;
                        } else {
                            const newIds = [...ghocExists.employeeIds, participant.id];
                            const newParticipants = [...ghocExists.participants, participant];
                            ghocExists.employeeIds = newIds;
                            ghocExists.participants = newParticipants;
                        }
                    });
                    ghocExists.tasks = [...ghocExists.tasks, shift]
                } else {
                    formatted.push({
                        eventId: `${format(start, 'yyyy-MM-dd')}-GHOC`,
                        start: new Date(shift.start),
                        end: add(new Date(shift.start), {hours: 1}),
                        isAllDay: true,
                        participants: [...participants],
                        eventTitle: 'GHOC',
                        employeeIds: [...participantIds],
                        calendars: ['month'],
                        tasks: [shift]
                    });
                }
            }
        });
        formatted = formatted.filter(event => event !== null);
        formatted.sort((a, b) => a.start - b.start);
        shiftNames.sort();
        return {formatted, shiftNames};
    } else {
        return {
            formatted: [],  
            shiftNames: [], 
        };
    }
};

export const formatMissions = (procedureRequirements, resources, events) => {
    const currentMissionResources = [...resources];
    const currentMissionEvents = [...events];
    let i = 0;
    const missionTypes = [];

    procedureRequirements.forEach((procedureReq) => {
        const { procedure } = procedureReq;
        const missionExists = currentMissionResources.find(
          (resource) => resource.name === procedureReq.name
        );
  
  
        if (!missionExists) {
          currentMissionResources.push({
            name: procedureReq.name,
            id: procedureReq.name,
            color: myColors[i],
          });
          if (!missionTypes.includes(procedureReq.names)) {
            i++;
            missionTypes.push(procedureReq.name);
          }
        }
  
        if (procedure && procedure.taskSet && procedure.taskSet.length > 0) {
          const nonArchived = procedure.taskSet.filter(
            (task) =>
              task.shiftAssignment && task.shiftAssignment.isArchived === false
          );
  
          if (nonArchived.length > 0) {
            currentMissionEvents.push({
                eventId: procedure.id,
                eventTitle: procedureReq.name,
                start: new Date(procedure.start),
                end: new Date(procedure.end),
                missionId: procedureReq.name,
                employeeIds: nonArchived.map(
                    (task) => task.shiftAssignment.employee.id
                ),
                participants: nonArchived.map(
                    (task) => task.shiftAssignment.employee
                ),
                calendars: ["mission", "month"],
                tasks: nonArchived,
            });
  
            nonArchived.forEach((task) => {
                currentMissionEvents.push({
                    eventId: task.shiftAssignment.shift.id,
                    eventTitle: `${task.shiftAssignment.employee.firstName} ${task.shiftAssignment.employee.lastName}`,
                    start: new Date(task.start),
                    end: new Date(task.end),
                    missionId: procedureReq.name,
                    employeeIds: [task.shiftAssignment.employee.id],
                    participants: [task.shiftAssignment.employee],
                    calendars: ["mission", "employee"],
                });

                const shiftExists = currentMissionEvents.find(event => (
                    isEqual(event.start, new Date(task.start)) &&
                    isEqual(event.end, new Date(task.end)) &&
                    event.missionId === procedureReq.name &&
                    event.tasks &&
                    event.calendars.includes("day")
                ));
                if (shiftExists) {
                    shiftExists.tasks = [...shiftExists.tasks, task];
                    shiftExists.employeeIds = [
                        ...shiftExists.employeeIds, 
                        task.shiftAssignment.employee.id
                    ]
                    shiftExists.participants = [
                        ...shiftExists.participants, 
                        task.shiftAssignment.employee
                    ]
                } else {
                    currentMissionEvents.push({
                        eventId: procedure.id,
                        eventTitle: format(new Date(task.start), 'HH:mm') + ' - ' +
                            format(new Date(task.end), 'HH:mm'),
                        start: new Date(task.start),
                        end: new Date(task.end),
                        missionId: procedureReq.name,
                        employeeIds: [task.shiftAssignment.employee.id],
                        participants: [task.shiftAssignment.employee],
                        tasks: [task],
                        calendars: ["day"],
                    });
                }
            });
          }
        }
    });
    return {currentMissionEvents, currentMissionResources};
};

export const formatSoftRequests = (requests, userId) => {
    return requests.map((request) => {
        const highPriority = request.value > 5;
        return {
        //   id: request.id,
          eventId: request.id,
          eventTitle: "Soft Time Off",
          start: new Date(`${request.date}T08:00:00`),
          end: new Date(`${request.date}T09:00:00`),
          type: "softRequest",
          highPriority,
          employeeIds: [userId],
          isAllDay: true
        };
      });
};

export const formatTimeOff = (requests, userId) => {
    return requests.map(request => {
        const name = request.employee.firstName + " " + request.employee.lastName;
        const personal = parseInt(request.employee.id) === parseInt(userId);
        
        const newRequest = {
            eventId: request.id,
            eventTitle: `${name}-${request.type.name}`,
            type: "timeOff",
            status: request.approvedby ? "approved" : "pending",
            category: personal ? "personal" : "employee",
            employeeIds: [request.employee.id],
            approvedby: request.approvedby,
            workHours: request.workHours, 
            typeId: request.type.id,
            comment: request.comment,
            deniedText: request.deniedText,
            createdAt: request.createdAt
        }

        if (request.startTime && request.endTime) {
            newRequest.start = new Date(`${request.firstday} ${request.startTime}`);
            newRequest.end = new Date(`${request.lastday} ${request.endTime}`);
            newRequest.isAllDay = false;
        } else {
            newRequest.start = new Date(`${request.firstday}T08:00:00`);
            newRequest.end = new Date(`${request.lastday}T08:00:00`);
            newRequest.isAllDay = true;
        }
        return newRequest;
    });
};

// export const formatEventsForCSV = (events) => {
//     return events.map(event => {
//         if (event.participants) {
//             const participantNames = event.participants.map(participant => (
//                 participant.firstName + ' ' + participant.lastName
//             )).join(', ');
//             return {
//                 start: format(new Date(event.start),'dd MMM yyyy'), 
//                 title: event.eventTitle,
//                 employees: participantNames
//             };
//         } else {
//             return {
//                 start: format(new Date(event.start),'dd MMM yyyy'), 
//                 title: event.eventTitle
//             };
//         }
//     });
// };